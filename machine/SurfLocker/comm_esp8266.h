
/***************************************************************************************************
 * \file       comm_esp8266.h
 **************************************************************************************************/

#ifndef __COMM_ESP8266_H__
#define __COMM_ESP8266_H__

/***************************************************************************************************
 * INCLUSOES
 **************************************************************************************************/
#include <WiFiEsp.h>
#include "SoftwareSerial.h"

/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/

typedef struct
{
    byte mode;				// previsto 3 modos de operacao
                            //      0 - normal
                            //      1 - AP
                            //      2 - bridge de comandos para serial port
    bool founded_hw;
    bool wifi_on;  
} esp8266_config_t;

static esp8266_config_t vEsp8266;

typedef enum
{
    ESP_OK,
    ESP_ERROR_NOT_CONNECTED,
    ESP_ERROR_ON_OPEN_TCP_CONN,
    ESP_ERROR_ON_OPEN_REQUEST,
    ESP_ERROR_USER_CODE,
    ESP_ERROR_UNEXPECTED
} esp8266_errors_e;

/***************************************************************************************************
 * PROTOTIPO DE FUNCOES
 **************************************************************************************************/
void comm_esp8266_setup(void);
void comm_esp8266_init(void);
esp8266_errors_e esp8266_verify_code(String _code, char * _product_code);
esp8266_errors_e esp8266_resume_code(String _code);
esp8266_errors_e esp8266_resume_money(String _code);
esp8266_errors_e esp8266_configure_machine(); 

#endif /* __COMM_ESP8266_H__ */
