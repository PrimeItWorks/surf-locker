

/*****************************************************************************************
 *                             Metodos do uteis diversos
 ****************************************************************************************/

/*****************************************************************************************
 * Decodifica inteiro de 5 casas decimais em hexa imprimivel
 ****************************************************************************************/
void int_to_hex_array(int value, char * addr)
{
    // a representacao do 
    char value_hex[6];

    // formata valor para apresentar no display
    if (value > 10000)
    {
        for (byte i = 0; i < 6; i++)
        {
            addr[i] = '-';
        }
    }
    else
    {
        addr[0] = 0x30 + (value / 10000); value = value % 10000;
        addr[1] = 0x30 + (value / 1000);  value = value % 1000;
        addr[2] = 0x30 + (value / 100);   value = value % 100;
        addr[4] = 0x30 + (value / 10);    value = value % 10;
        addr[5] = 0x30 + (value);
        addr[3] = ',';

        if ((addr[1] == 0x30) && (addr[0] == 0x30))
        {
            addr[0] = ' ';
            addr[1] = ' ';
        }
        else if (addr[0] == 0x30)
        {
            addr[0] = ' ';
        }
    }
}

/*****************************************************************************************
 * DOCUMENTAR
 ****************************************************************************************/
int utils_find_buffer_char(char * buffer, char charactere, int start_pos, int buffer_size)
{
    int i;
    bool founded = false;

    for (i = start_pos; i < buffer_size; i++)
    {
        if (charactere == buffer[i])
        {
            founded = true;
            break;
        }
    }

    if (founded == false)
    {
        i = -1;
    }

    return (i);
}

/*****************************************************************************************
 * DOCUMENTAR
 ****************************************************************************************/
bool utils_is_chars_equals(char * buffer1, char * buffer2, int buffer_size)
{
    bool ans = true;

#if (FW_SERIAL_DEBUG == 2)
        Serial.print(F("utils   : String 1 = "));
        Serial.println(buffer1);
        Serial.print(F("utils   : String 2 = "));
        Serial.println(buffer2);
#endif

    for (int i = 0; i < buffer_size; i++)
    {
        if (buffer1[i] != buffer2[i])
        {
            ans = false;
            break;
        }
    }

    return (ans);
}

