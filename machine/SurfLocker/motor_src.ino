
/*****************************************************************************************
 *                             Metodos dos motores
 ****************************************************************************************/

/*****************************************************************************************
 * DEFINICOES
 ****************************************************************************************/
typedef struct
{
    byte pin;
    byte sensor_pin;
    bool state;
} motor_config_t;

static motor_config_t vMotor[MAX_NUM_DISPENSER];

/*****************************************************************************************
 * Setup do hardware
 ****************************************************************************************/
void motor_setup(void)
{
    pinMode(motor1_run, OUTPUT);
    pinMode(motor2_run, OUTPUT);
    pinMode(motor3_run, OUTPUT);
    pinMode(motor1_stop, INPUT_PULLUP);
    pinMode(motor2_stop, INPUT_PULLUP);
    pinMode(motor3_stop, INPUT_PULLUP);
}

/*****************************************************************************************
 * Inicializacao das variaveis
 ****************************************************************************************/
void motor_init(void)
{
    vMotor[0].pin        = motor1_run;
    vMotor[0].sensor_pin = motor1_stop;
    vMotor[0].state      = false;
    vMotor[1].pin        = motor2_run;
    vMotor[1].sensor_pin = motor2_stop;
    vMotor[1].state      = false;
    vMotor[2].pin        = motor3_run;
    vMotor[2].sensor_pin = motor3_stop;
    vMotor[2].state      = false;

    digitalWrite(vMotor[0].pin, HIGH);
    digitalWrite(vMotor[1].pin, HIGH);
    digitalWrite(vMotor[2].pin, HIGH);
}

/*****************************************************************************************
 * TODO: COMENTAR
 ****************************************************************************************/
bool motor_turnon(byte product_index)
{
    // este metodo bloqueia o processamento ate que a acao termine

    unsigned long time_start = millis();   // pega valor atual do temporizador
    bool motor_error = false;

#if (FW_SERIAL_DEBUG == 1)
    Serial.print(F("motor   : acionamento do motor "));
    Serial.println(product_index);
#endif

#if (FW_SERIAL_DEBUG == 2)
    if ((product_index >= 0) &&
        (product_index <= product_num_of_dispenser()))
    {
        Serial.println(F("motor   : valor valido"));
    }
    else
    {
        Serial.println(F("motor   : valor invalido"));
    }
#endif

    // aciona motor
    digitalWrite(vMotor[product_index].pin, LOW);
    vMotor[product_index].state = true;

#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("motor   : Aguardando chave == 1. Motor iniciando rotacao."));
#endif
    do
    {
        if (millis() > (time_start + 3000))
        {
            // sinaliza erro!!!
            motor_error = true;
            break;
        }
    } while (digitalRead(vMotor[product_index].sensor_pin) == LOW);

#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("motor   : Aguardando chave == 0. Motor finalizando rotacao."));
#endif
    if (motor_error == false)
    {
        do
        {
            // aguarda termino da acao 
            // aguardar delay para monitorar a chave fim-de-curso
            if (millis() > (time_start + 3000))
            {
                // sinaliza erro!!!
                motor_error = true;
                break;
            }
            
        } while (digitalRead(vMotor[product_index].sensor_pin) == HIGH);
    }
    else
    {
#if (FW_SERIAL_DEBUG == 1)
        Serial.print(F("motor   : ERRO!! Motor "));
        Serial.print(product_index + 1);
        Serial.println(F(" travado!!"));
#endif
    }

    // desliga motor
    digitalWrite(vMotor[product_index].pin, HIGH);
    vMotor[product_index].state = false;
#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("motor   : motor desligado"));
#endif

    return (motor_error);

}

