
/***************************************************************************************************
 * \file       product.h
 **************************************************************************************************/

#ifndef __PRODUCT_H__
#define __PRODUCT_H__

/***************************************************************************************************
 * INCLUSOES
 **************************************************************************************************/

/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/

typedef struct
{
    int  value;			// este valor devera permanecer em EEPROM
    byte quantity;
    char code[4 + 1];  // este valor devera permanecer em EEPROM
} product_t;

#define MAX_NUM_DISPENSER 3 
static product_t products[MAX_NUM_DISPENSER];   // define a quantidade de motores associados

// define a quantidade de motores associados
/***************************************************************************************************
 * PROTOTIPO DE FUNCOES
 **************************************************************************************************/
void product_init(void);
byte product_num_of_dispenser(void);
int product_get_value(byte _product);
int product_get_quantity(byte _product);
bool product_update_quantity(byte _product, byte _qty);
void product_delivery_count(byte _product);

#endif /* __PRODUCT_H__ */

