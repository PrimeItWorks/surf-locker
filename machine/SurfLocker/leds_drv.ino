
/*****************************************************************************************
 *                             Metodos dos LEDs
 ****************************************************************************************/

/*****************************************************************************************
 * DEFINICOES
 ****************************************************************************************/
const byte led_status_pin = 13;  // saida do LED do Arduino. Representa funcionamento
const long LED_STATUS_REFRESH_TIME = 500;  // tempo em milisegundos

/*****************************************************************************************
 * Setup do hardware
 ****************************************************************************************/
void leds_setup(void)
{
    pinMode(led_status_pin, OUTPUT);
}

/*****************************************************************************************
 * Inicializacao das variaveis
 ****************************************************************************************/
void leds_init(void)
{
    digitalWrite(led_status_pin, LOW);
}

/*****************************************************************************************
 * LED STATUS refresh
 ****************************************************************************************/
void led_status_handle(void)
{
    static long led_status_last_change_time = 0;
    static bool led_status_state = false;

    if (millis() >= (led_status_last_change_time + LED_STATUS_REFRESH_TIME))
    {
        if (led_status_state)
        {
            digitalWrite(led_status_pin, LOW);
        }
        else
        {
            digitalWrite(led_status_pin, HIGH);
        }

        led_status_state = !led_status_state;  // muda o estado
        led_status_last_change_time = millis();
    }
}

