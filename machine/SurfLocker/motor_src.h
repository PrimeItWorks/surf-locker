//NOVO
/*****************************************************************************//**
 * \file       motor_src.h
 ********************************************************************************/

#ifndef __MOTOR_SRC_H__
#define __MOTOR_SRC_H__

/*********************************************************************************
 * INCLUSOES
 ********************************************************************************/

/*********************************************************************************
 * DEFINICOES
 ********************************************************************************/
const byte motor1_run     = 7;   // TODO: trocar para PWM!!!!!!
const byte motor2_run     = 5;   // TODO: trocar para PWM!!!!!!
const byte motor3_run     = 3;   // TODO: trocar para PWM!!!!!!

const byte motor1_stop    = 13;  // sensor fim-de-curso do motor1
const byte motor2_stop    = 11;   // sensor fim-de-curso do motor2
const byte motor3_stop    = 9;   // sensor fim-de-curso do motor3

const bool SWITCH_POLARITY = 1;

// verificar se devera ser usado
typedef enum
{
    eMOTOR1,
    eMOTOR2,
    eMOTOR3
} motor_e;

/*********************************************************************************
 * PROTOTIPO DE FUNCOES
 ********************************************************************************/
void motor_setup(void);
void motor_init(void);
bool motor_turnon(byte product_index);

#endif /* __MOTOR_SRC_H__ */


// // ANTIGO
// /*****************************************************************************//**
//  * \file       motor_src.h
//  ********************************************************************************/

// #ifndef __MOTOR_SRC_H__
// #define __MOTOR_SRC_H__

// /*********************************************************************************
//  * INCLUSOES
//  ********************************************************************************/

// /*********************************************************************************
//  * DEFINICOES
//  ********************************************************************************/
// const byte motor1_run     = 7;   // TODO: trocar para PWM!!!!!!
// const byte motor2_run     = 6;   // TODO: trocar para PWM!!!!!!
// const byte motor3_run     = 5;   // TODO: trocar para PWM!!!!!!

// const byte motor1_stop    = 10;  // sensor fim-de-curso do motor1
// const byte motor2_stop    = 9;   // sensor fim-de-curso do motor2
// const byte motor3_stop    = 8;   // sensor fim-de-curso do motor3

// const bool SWITCH_POLARITY = 1;

// // verificar se devera ser usado
// typedef enum
// {
//     eMOTOR1,
//     eMOTOR2,
//     eMOTOR3
// } motor_e;

// /*********************************************************************************
//  * PROTOTIPO DE FUNCOES
//  ********************************************************************************/
// void motor_setup(void);
// void motor_init(void);
// bool motor_turnon(byte product_index);

// #endif /* __MOTOR_SRC_H__ */
