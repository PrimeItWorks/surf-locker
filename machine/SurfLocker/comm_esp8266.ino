
/***************************************************************************************************
 *                       Metodos de comunicacao com ESP8266
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/
#define esp8266 Serial3
//#define bridge  Serial2

#define NUM_OF_RETRIES 5

// Server, file, and port
const String hostname = "surflocker-app.azurewebsites.net";
//const String hostname = "79473394.ngrok.io";

const int port = 80;

const long ESP_POLLING_TIME = 10;  // tempo em milisegundos

static bool founded_conection = false;
static char product_code[4 + 1];
static char bufferChar[512];   /// tentaiva de correcao

static unsigned long time_start;

static int i;
static int first_signal, second_signal;
static String bufferString;
static String req;
#define COMM_ESP8266_BAUD_RATE   115200


WiFiEspClient client;
int status = WL_IDLE_STATUS;


/***************************************************************************************************
 * Prototipos de funcoes locais
 **************************************************************************************************/
void _configure_esp_hw(void);
void _mode_client(void);
void _mode_access_point(void);
void _mode_bridge_serial(void);
void _esp8266_rx_buffer_flush(void);

/***************************************************************************************************
 * Setup do hardware
 **************************************************************************************************/
void comm_esp8266_setup(void)
{
    esp8266.begin(COMM_ESP8266_BAUD_RATE);
    WiFi.init(&esp8266);
}

/***************************************************************************************************
 * Inicializacao das variaveis
 **************************************************************************************************/
void comm_esp8266_init(void)
{
    for (byte i = 0; i < 5; i++)
    {
        if (WiFi.status() == WL_NO_SHIELD) {
            Serial.println("Dispositivo WiFi não encontrado");
        }
    }

    for (byte i = 0; i < 5; i++)
    {
      Serial.print("Tentando conectar com: ");
      Serial.println("iPhone de Leandro");
      
      status = WiFi.begin("iPhone de Leandro", "tricolor");
      if (status == WL_CONNECTED) {
        Serial.println("Conectado!");
        printWifiStatus();
        return;
      }
    }    
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
  
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
bool esp8266_wifi_on(void)
{
    return (status == WL_CONNECTED);
}


/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
esp8266_errors_e esp8266_verify_code(String _code, char _product_code [5])
{
    esp8266_errors_e ans = ESP_OK;
    
    if (!client.connect("surflocker-app.azurewebsites.net", 80)) {
      Serial.println("connection failed");
      Serial.println("wait 5 sec...");
      delay(5000);
      return;
    }
  
    // requisição http:
    String req = "GET /api/Vouchers/check/" + _code  + " HTTP/1.1\r\nHost: surflocker-app.azurewebsites.net\r\nConnection: Close";
    
    client.println(req);
    client.println();
  
    // recebendo dados
    Serial.println("dados recebidos do servidor");
    String line = client.readStringUntil('}');
    Serial.println(line);

    if (line.indexOf("false") > 0)
    {
        ans = ESP_ERROR_USER_CODE;
    }
    else if (line.indexOf("true") > 0)
    {
        int size = line.length();
        char bufferChar[size];
        line.toCharArray(bufferChar, size);
      
        int word_index = line.indexOf("productCode");
        word_index = line.indexOf(':', word_index) +2;
        // copia codigo para o buffer apropriado
        for (int j = 0; j < 4; j++)
        {
            _product_code[j] = bufferChar[word_index +j];
        }

        _product_code[4] = '\0';
        
        Serial.println("codigo:");
        Serial.println(_product_code);
    }
    else
    {
        ans = ESP_ERROR_UNEXPECTED;
    }

    Serial.println("closing connection");
    client.stop();
    
    return (ans);

}

esp8266_errors_e esp8266_resume_code(String _code) {
    esp8266_errors_e ans = ESP_OK;
    
    if (!client.connect("surflocker-app.azurewebsites.net", 80)) {
      Serial.println("connection failed");
      Serial.println("wait 5 sec...");
      delay(5000);
      return;
    }
  
    // requisição http:
    String req = "GET /api/Vouchers/resume/" +  _code + "/" + machine_code + " HTTP/1.1\r\nHost: surflocker-app.azurewebsites.net\r\nConnection: Close";
    
    client.println(req);
    client.println();

    Serial.println("closing connection");
    client.stop();
    
    delay(250);
    return (ans);
}

esp8266_errors_e esp8266_resume_money(String _code) {
    esp8266_errors_e ans = ESP_OK;
    
    if (!client.connect("surflocker-app.azurewebsites.net", 80)) {
      Serial.println("connection failed");
      Serial.println("wait 5 sec...");
      delay(5000);
      return;
    }
  
    // requisição http:
    String req = "GET /api/machines/resumemoney/" + machine_code + "/" + _code + " HTTP/1.1\r\nHost: surflocker-app.azurewebsites.net\r\nConnection: Close";
    
    client.println(req);
    client.println();

    Serial.println("closing connection");
    client.stop();
    
    delay(250);
    return (ans);
}

esp8266_errors_e esp8266_configure_machine()
{
    esp8266_errors_e ans = ESP_OK;
    
    if (!client.connect("surflocker-app.azurewebsites.net", 80)) {
      Serial.println("connection failed");
      Serial.println("wait 5 sec...");
      delay(5000);
      return;
    }
  
    // requisição http:
    String req = "GET /api/machines/configure/" + machine_code + " HTTP/1.1\r\nHost: surflocker-app.azurewebsites.net\r\nConnection: Close";
    
    client.println(req);
    client.println();
  
    // recebendo dados
    Serial.println("dados recebidos do servidor");
    String line = client.readStringUntil('}');
    Serial.println(line);

    int size = line.length() +1;
    char bufferChar[size];
    line.toCharArray(bufferChar, size);

    int cod_index = line.indexOf("c1");
    cod_index = cod_index + 5;
    int j = 0;
    for (j = 0; j < 4; j++)
    {
      products[0].code[j] = bufferChar[cod_index + j];
    }
    products[0].code[4] = '\0';

    cod_index = line.indexOf("c2");
    cod_index = cod_index + 5;
    j = 0;
    for (j = 0; j < 4; j++)
    {
        products[1].code[j] = bufferChar[cod_index + j];
    }
    products[1].code[4] = '\0';


    cod_index = line.indexOf("c3");
    cod_index = cod_index + 5;
    j = 0;
    for (j = 0; j < 4; j++)
    {
        products[2].code[j] = bufferChar[cod_index + j];
    }
    products[2].code[4] = '\0';

    cod_index = line.indexOf("q1");
    cod_index = cod_index + 4;
    char q [3];
    j = 0;
    for (j = 0; j < 2; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }

        q[j] = bufferChar[cod_index + j];
    }
    q[3] = '\0';
    products[0].quantity = atoi(q);

    cod_index = line.indexOf("q2");
    cod_index = cod_index + 4;
    q[0] = '\0';
    q[1] = '\0';
    q[2] = '\0';
    q[3] = '\0';
    j = 0;

    for (j = 0; j < 2; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }

        q[j] = bufferChar[cod_index + j];
    }
    q[3] = '\0';
    products[1].quantity = atoi(q); 

    cod_index = line.indexOf("q3");
    cod_index = cod_index + 4;
    q[0] = '\0';
    q[1] = '\0';
    q[2] = '\0';
    q[3] = '\0';
    j = 0;
    for (j = 0; j < 2; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }
        Serial.println(bufferChar[cod_index + j]);


        q[j] = bufferChar[cod_index + j];
    }
    q[3] = '\0';
    Serial.println(q);
    products[2].quantity = atoi(q);
    cod_index = line.indexOf("v1");
    cod_index = cod_index + 4;
    q[0] = '\0';
    q[1] = '\0';
    q[2] = '\0';
    q[3] = '\0';
    j = 0;
    for (j = 0; j < 3; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }

        q[j] = bufferChar[cod_index + j];
    }
    q[3] = '\0';
    products[0].value = atoi(q) * 100;

    cod_index = line.indexOf("v2");
    cod_index = cod_index + 4;
    char v2 [3];
    j = 0;
    for (j = 0; j < 3; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }

        v2[j] = bufferChar[cod_index + j];
    }
    v2[3] = '\0';

    products[1].value = atoi(v2) * 100;

    cod_index = line.indexOf("v3");
    cod_index = cod_index + 4;
    char v3 [3];
    j = 0;
    for (j = 0; j < 3; j++)
    {
        if(bufferChar[cod_index + j] == ',') {
            continue;
        }

        v3[j] = bufferChar[cod_index + j];
    }
    v3[3] = '\0';

    products[2].value = atoi(v3) * 100;
  
    Serial.println("closing connection");
    client.stop();

    return (ans);
}

void _esp8266_rx_buffer_flush(void)
{
    int num_char = 0;
    while (esp8266.available() > 0)
    {
        (void)esp8266.read();       // coloca bytes no lixo
        num_char++;
    }
}
