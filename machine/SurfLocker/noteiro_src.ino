
/*****************************************************************************************
 *                             Metodos do Noteiro
 ****************************************************************************************/

/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/
const byte noteiro_int         = 0;    // define a entrada de interrupcao do noteiro
const long NOTEIRO_ISR_TIMEOUT = 500;  // tempo em milisegundos

typedef struct
{
    unsigned int credit_value;           // armazena o valor do credito atualizado apos interrupcao
    unsigned int credit_value_temp;      // armazena o valor do credito atualizado da interrupcao
    bool updating;
    long isr_last_change_time;
} noteiro_config_t;

static noteiro_config_t vNoteiro;

/*****************************************************************************************
 * Setup do hardware. 
 ****************************************************************************************/
void noteiro_setup(void)
{
    attachInterrupt(noteiro_int, noteiro_isr, RISING);
}

/*****************************************************************************************
 * ISR - noteiro
 ****************************************************************************************/
void noteiro_init(void)
{
    vNoteiro.credit_value         = 0;
    vNoteiro.credit_value_temp    = 0;
    vNoteiro.updating             = false;
    vNoteiro.isr_last_change_time = 0;
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void noteiro_attachInterrupt(void)
{
#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("noteiro : Interrupcao ativada!"));
#endif
    attachInterrupt(noteiro_int, noteiro_isr, RISING);
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void noteiro_detachInterrupt(void)
{
#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("noteiro : Interrupcao desativada!"));
#endif
    detachInterrupt(noteiro_int);
}

/*****************************************************************************************
 * ISR - noteiro
 ****************************************************************************************/
void noteiro_isr(void)
{
#if (FW_SOFT_DEBUG == 1)
    vNoteiro.credit_value_temp   += 1000;
#else
    vNoteiro.credit_value_temp   += 100;
#endif
    vNoteiro.updating             = true;
    vNoteiro.isr_last_change_time = millis();
}

/*****************************************************************************************
 * Run service: verifica o final do recebimento dos pulsos
 ****************************************************************************************/
bool noteiro_run(void)
{
	bool ans = false;

    // Avalia se existe novo credito presente e, se sim, aguarda timeout para adquirir o credito
    if ((vNoteiro.updating == true) &&
        (millis() >= (vNoteiro.isr_last_change_time + NOTEIRO_ISR_TIMEOUT)))
    {
    	vNoteiro.credit_value     += vNoteiro.credit_value_temp;
    	vNoteiro.credit_value_temp = 0;
        vNoteiro.updating          = false;  // "finaliza" a atendimento contagem de pulsos
        ans = true;
#if (FW_SERIAL_DEBUG == 1)
        Serial.println(F("noteiro : Acabou de receber os pulsos."));
#endif
    }

    return (ans);
}

/*****************************************************************************************
 * ISR - noteiro
 ****************************************************************************************/
int noteiro_update_value(void)
{
	int credit = vNoteiro.credit_value;
	vNoteiro.credit_value = 0;
	return (credit);
}

