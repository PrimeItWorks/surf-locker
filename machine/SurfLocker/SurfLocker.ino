
#include "SurfLocker.h"

#include "comm_debug.h"
#include "comm_esp8266.h"
#include "keypad_src.h"
#include "lcd_src.h"
#include "leds_drv.h"
#include "motor_src.h"
#include "noteiro_src.h"

#include "app.h"
#include "credit.h"
#include "product.h"

#include "utils.h"

/*****************************************************************************************
 * Definicoes
 ****************************************************************************************/

static char key_last_pressed = 0xFF;

/*****************************************************************************************
 *                                    APLICACAO
 ****************************************************************************************/
void setup()
{
    // Configuração de hardware
    app_setup();
    
    // Configuração de aplicação
    app_init();
}

//----------------------------------------------------------------------------------------
void loop()
{
    app_run();
}

/* TODO: pode-se colocar um botao fim-de-curso para monitorar a abertura da tampa principal
 * TODO: colocar IR para sinalizar dispenser vazio
 * TODO: criar conexao AP para cadastro da rede e senha
 * TODO: criar modo cliente
 */

