
/*****************************************************************************//**
 * \file       lcd_src.h
 ********************************************************************************/

#ifndef __LCD_SRC_H__
#define __LCD_SRC_H__

/*********************************************************************************
 * INCLUSOES
 ********************************************************************************/
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

/*********************************************************************************
 * DEFINICOES
 ********************************************************************************/
#if (FW_SOFT_DEBUG == 0)
  const byte LCD_ADDR     = 0x27;  // endereco do modulo I2C real do hardware
#else
  const byte LCD_ADDR     = 0x3F;  // endereco do modulo I2C para simulacao
#endif

#define ROW1    0
#define ROW2    1
#define ROW3    2
#define ROW4    3

const byte LCD_NUM_COLS = 20;    // numero de colunas do display
const byte LCD_NUM_ROWS = 4;     // numero de linhas do display

// cria objeto lcd de acordo com LCD_ADDR, LCD_NUM_COLS e LCD_NUM_ROWS
LiquidCrystal_I2C lcd(LCD_ADDR, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // @Uso da 2a biblioteca

typedef enum
{
    MENU_0,
    MENU_1,
    MENU_2,
    MENU_NUMOF
} menus_e;

typedef enum
{
    LCD_MENU_LOCKERPRO,
    LCD_MENU_MONEYBUYITEM,
    LCD_MENU_SERVERBUYITEM,
    LCD_MENU_SUPLIER
} lcd_menus_e;

typedef struct
{
    byte menu_actual;
    byte menu_last;
    byte submenu;
    bool need_update;
    bool update_credit_value;

	char key_pressed;    
} lcd_config_t;

static lcd_config_t vLcd;

const char row_blank[LCD_NUM_COLS + 1] =
    { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0};

// Menu de apresentacao
const char Menu0[][LCD_NUM_COLS + 1] =
{
    { ' ', ' ', ' ', ' ', ' ', 'S', 'u', 'r', 'f', ' ', 'L', 'o', 'c', 'k', 'e', 'r', ' ', ' ', ' ', ' ', 0},
    { ' ', 'P', 'a', 'r', 'a', 'f', 'i', 'n', 'a', ' ', '2', '4', ' ', 'h', 'o', 'r', 'a', 's', ' ', ' ', 0},
    { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'v', 'x', 'x', 'r', 'y', 'y', 0},
    { ' ', 'W', 'i', 'F', 'i', ' ', 'c', 'o', 'n', 'e', 'c', 't', 'a', 'd', 'o', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', 'W', 'i', 'F', 'i', ' ', 'd', 'e', 's', 'c', 'o', 'n', 'e', 'c', 't', 'a', 'd', 'o', ' ', ' ', 0}
};

typedef enum
{
    LCD_MENU0_CUSTOM_MSG1,
    LCD_MENU0_CUSTOM_MSG2,
    LCD_MENU0_FW_VERSION,
    LCD_MENU0_WIFI_ON,
    LCD_MENU0_WIFI_OFF
} lcd_menu0_msgs_e;

// Menu de compra
const char Menu1[][LCD_NUM_COLS + 1] =
{
    { 'C', 'r', 'e', 'd', 'i', 't', 'o', ' ', 'R', '$', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    { 'S', 'e', 'l', 'e', 'c', 'i', 'o', 'n', 'e', '[', '1', ' ', 'a', ' ', 'x', ']', ':', ' ', ' ', ' ', 0},
    { ' ', ' ', ' ', 'A', 'g', 'u', 'a', 'r', 'd', 'e', ' ', 'p', 'r', 'o', 'd', 'u', 't', 'o', ' ', ' ', 0},
    { ' ', '#', ' ', '-', ' ', 'C', 'o', 'n', 'f', 'i', 'r', 'm', 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', '*', ' ', '-', ' ', 'C', 'a', 'n', 'c', 'e', 'l', 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', ' ', ' ', 'I', 'n', 's', 'i', 'r', 'a', ' ', 'd', 'i', 'n', 'h', 'e', 'i', 'r', 'o', ' ', ' ', 0},
    { ' ', ' ', ' ', ' ', ' ', ' ', 'I', 'n', 'v', 'a', 'l', 'i', 'd', 'o', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', 'E', 's', 'c', 'o', 'l', 'h', 'a', ' ', 'o', ' ', 'p', 'r', 'o', 'd', 'u', 't', 'o', ' ', ' ', 0},
    { ' ', ' ', ' ', ' ', 'I', 'n', 'd', 'i', 's', 'p', 'o', 'n', 'i', 'v', 'e', 'l', ' ', ' ', ' ', ' ', 0},
    { ' ', ' ', ' ', ' ', ' ', 'S', 'e', 'm', ' ', 's', 'a', 'l', 'd', 'o', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', ' ', ' ', 'M', 'o', 't', 'o', 'r', ' ', 't', 'r', 'a', 'v', 'a', 'd', 'o', ' ', ' ', ' ', ' ', 0},
    { ' ', '*', ' ', '-', ' ', 'C', 'o', 'm', 'p', 'r', 'a', ' ', 'n', 'o', ' ', 'A', 'P', 'P', ' ', ' ', 0},
    { ' ', ' ', ' ', 'C', 'o', 'n', 'e', 'c', 't', 'a', 'n', 'd', 'o', ' ', '#', ' ', ' ', ' ', ' ', ' ', 0},
    { ' ', 'T', 'e', 'n', 't', 'e', ' ', 'n', 'o', 'v', 'a', 'm', 'e', 'n', 't', 'e', '!', '!', ' ', ' ', 0},
    { ' ', 'T', 'e', 'n', 't', 'e', ' ', 'n', 'o', 'v', 'a', 'm', 'e', 'n', 't', 'e', '!', '!', ' ', ' ', 0},
    { ' ', 'T', 'e', 'n', 't', 'e', ' ', 'n', 'o', 'v', 'a', 'm', 'e', 'n', 't', 'e', '!', '!', ' ', ' ', 0},
    { ' ', 'T', 'e', 'n', 't', 'e', ' ', 'n', 'o', 'v', 'a', 'm', 'e', 'n', 't', 'e', '!', '!', ' ', ' ', 0},
    { ' ', ' ', ' ', 'C', 'o', 'd', 'i', 'g', 'o', ' ', 'i', 'n', 'v', 'a', 'l', 'i', 'd', 'o',' ', ' ', 0}, // erro no codigo - invalido
    { ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'E', 'r', 'r', 'o', ' ', '0', '4', ' ', ' ', ' ', ' ', ' ', ' ', 0},  // erro inesperado
    { ' ', ' ', ' ', ' ', 'I', 'n', 'd', 'i', 's', 'p', 'o', 'n', 'i', 'v', 'e', 'l', ' ', ' ', ' ', ' ', 0},
    { ' ', ' ', ' ', ' ', ' ','O', 'b', 'r', 'i', 'g', 'a', 'd', 'o', '!', '!',  ' ', ' ', ' ', ' ', ' ', 0}
};

typedef enum
{
    LCD_MENU1_SHOW_CREDIT,
    LCD_MENU1_SELECTPRODUCT,
    LCD_MENU1_RELEASE_PRODUCT,
    LCD_MENU1_CONFIRM_PRODUCT,
    LCD_MENU1_CANCEL_PRODUCT,
    LCD_MENU1_ASKFORMONEY,
    LCD_MENU1_ERROR_OPTION,
    LCD_MENU1_ERROR_NULLENTRY,
    LCD_MENU1_ERROR_NOITEM,
    LCD_MENU1_ERROR_NOMONEY,
    LCD_MENU1_ERROR_MOTOR,
    LCD_MENU1_WEB_BUY,
    LCD_MENU1_WEB_CONNETING_SERVER,
    LCD_MENU1_WEB_TRY_AGAIN,
    LCD_MENU1_ERROR_NOT_CONNECTED,
    LCD_MENU1_ERROR_ON_OPEN_TCP_CONN,
    LCD_MENU1_ERROR_ON_OPEN_REQUEST,
    LCD_MENU1_ERROR_USER_CODE,
    LCD_MENU1_ERROR_UNEXPECTED,
    LCD_MENU1_ERROR_MACHINE_WITHOUT_PRODUCT,
    LCD_MENU1_THANKS
} lcd_menu1_msgs_e;

const char Menu1Web[][LCD_NUM_COLS + 1] =
{
    { ' ', ' ', 'D', 'i', 'g', 'i', 't', 'e', ' ', 'o', ' ', 'c', 'o', 'd', 'i', 'g', 'o', ' ', ' ', ' ', 0},
    { 'C', 'o', 'd', 'i', 'g', 'o', ':', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0}
};

typedef enum
{
    LCD_MENU1WEB_SHOW_MESSAGE,
    LCD_MENU1WEB_ASKFORCODE
} lcd_menu1web_msgs_e;

// Menu de recarga
const char Menu2[][LCD_NUM_COLS + 1] =
{
    {'R', 'e', 'c', 'a', 'r', 'g', 'a', ' ', 'd', 'e', ' ', 'p', 'r', 'o', 'd', 'u', 't', 'o', 's', ' ', 0},
    {'D', 'i', 's', 'p', '.', ' ', 'x', ' ', '-', ' ', 'q', 't', 'd', ':', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    {' ', '#', ' ', '-', ' ', 'C', 'o', 'n', 'f', 'i', 'r', 'm', 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    {' ', '*', ' ', '-', ' ', 'C', 'a', 'n', 'c', 'e', 'l', 'a', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 0},
    {' ', 'Q', 'u', 'a', 'n', 't', 'i', 'd', 'a', 'd', 'e', ' ', 's', 'a', 'l', 'v', 'a', ' ', ' ', ' ', 0}
};

typedef enum
{
    LCD_MENU2_PRESENTATION,
    LCD_MENU2_SHOW_QTY,
    LCD_MENU2_CONFIRM,
    LCD_MENU2_CANCEL,
    LCD_MENU2_SAVE_OK
} lcd_menu2_msgs_e;

/*********************************************************************************
 * PROTOTIPO DE FUNCOES
 ********************************************************************************/
void lcd_setup(void);
void lcd_init(void);
void lcd_show_menu_initial(void);
void lcd_show_wifi_status(bool _wifi_on);
void lcd_show_menu_default(void);
void lcd_show_menu_web(void);
void lcd_show_menu_recharge(void);
byte lcd_get_actual_menu(void);
byte lcd_get_menu_max_num(void);
void lcd_update_menu_to(void);

void lcd_menu01_row1_update(void);
void lcd_menu01_row2_update(byte option_select);
void lcd_menu1web_row2_update_code(char _value, byte _position);
void lcd_menu1web_row3_update_retries(char _value);
void lcd_menu02_row2_update(byte product_index);
void lcd_menu02_row2_update_qty(char _value, byte _position);

#endif /* __LCD_SRC_H__ */

