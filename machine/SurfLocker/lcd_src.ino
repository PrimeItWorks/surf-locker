
/*****************************************************************************************
 *                             Metodos do modulo display lcd
 ****************************************************************************************/

/*****************************************************************************************
 * Setup do hardware
 ****************************************************************************************/
void lcd_setup(void)
{
    lcd.begin(LCD_NUM_COLS, LCD_NUM_ROWS);   // inicia o modulo lcd_i2c
    lcd_show_menu_initial();
}

/*****************************************************************************************
 * Inicializacao das variaveis
 ****************************************************************************************/
void lcd_init(void)
{
    vLcd.menu_actual  = MENU_0;
    vLcd.menu_last    = MENU_0;
    vLcd.submenu      = 1;
    vLcd.need_update  = true;
    vLcd.update_credit_value = false;

    vLcd.key_pressed  = 0xFF;
}

/*****************************************************************************************
 * Apresenta menu inicial
 ****************************************************************************************/
void lcd_show_menu_initial(void)
{
    char msg[(LCD_NUM_COLS + 1)];
	byte value;

    lcd.setBacklight(HIGH);                     // liga backlight
    lcd.clear();
    lcd.home();

    lcd.print(Menu0[LCD_MENU0_CUSTOM_MSG1]);    // Imprime msg linha 1
    lcd.setCursor(0, ROW2);
    lcd.print(Menu0[LCD_MENU0_CUSTOM_MSG2]);    // Imprime msg linha 2
    lcd.setCursor(0, ROW3);
    lcd.print(row_blank);                       // Imprime msg linha 3

	// monta versao e revisao
    for (byte i = 0; i < (LCD_NUM_COLS + 1); i++)
    {
        msg[i] = Menu0[LCD_MENU0_FW_VERSION][i];
    }
    if (FW_VERSION < 100)
    {
		value = FW_VERSION;
        msg[15] = ((value / 10) + '0'); value %= 10;
        msg[16] =  (value       + '0');
    }
	if (FW_REVISION < 100)
    {
		value = FW_REVISION;
        msg[18] = ((value / 10) + '0'); value %= 10;
        msg[19] =  (value       + '0');
    }
    lcd.setCursor(0, ROW4);
    lcd.print(msg);                             // Imprime msg linha 4
}

/*****************************************************************************************
 * DOCUMENTAR
 ****************************************************************************************/
void lcd_show_wifi_status(bool _wifi_on)
{
    lcd.setCursor(0, ROW3);
    lcd.print(Menu0[LCD_MENU0_WIFI_ON]);
//    if (_wifi_on)
//    {
//    }
//    else
//    {
//        lcd.print(Menu0[LCD_MENU0_WIFI_OFF]);
//    }
}

/*****************************************************************************************
 * Apresenta menu default
 ****************************************************************************************/
void lcd_show_menu_default(void)
{
    static int new_credit;
    byte key_pressed_local;

    // Apresenta o menu default no display
    lcd.clear();
    lcd.home();

    lcd_menu01_row1_update();

    // apresenta menu para selecao de produto
    lcd.setCursor(0, ROW2);
    lcd_menu01_row2_update(0xFF);

    //  se houver credito, a informacao deve ser diferente
    if (credit_get_value() == 0)
    {
        lcd.setCursor(0, ROW3);
        lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);        // imprime mensagem para inserir dinheiro 
    }

    lcd.setCursor(0, ROW4);
    lcd.print(Menu1[LCD_MENU1_WEB_BUY]);

#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("lcd     : Apresenta menu default."));
#endif
}

/*****************************************************************************************
 * Apresenta menu default
 ****************************************************************************************/
void lcd_show_menu_web(void)
{
    // Apresenta o menu default no display
    lcd.clear();
    lcd.home();

    lcd.print(Menu1Web[LCD_MENU1WEB_SHOW_MESSAGE]); // linha 1
    lcd.setCursor(0, ROW2);
    lcd.print(Menu1Web[LCD_MENU1WEB_ASKFORCODE]);   // linha 2
    lcd.setCursor(0, ROW3);
    lcd.print(row_blank);                           // linha 3
    lcd.setCursor(0, ROW4);
    lcd.print(Menu1[LCD_MENU1_CANCEL_PRODUCT]);     // linha 4
#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("lcd     : Apresenta menu web."));
#endif
}

/*****************************************************************************************
 * Apresenta menu de recarga
 ****************************************************************************************/
void lcd_show_menu_recharge(void)
{
    // Apresenta o menu default no display
    lcd.clear();
    lcd.home();

    lcd.print(Menu2[LCD_MENU2_PRESENTATION]);   // linha 1
    lcd_menu02_row2_update(0);                  // linha 2
    lcd.setCursor(0, ROW3);
    lcd.print(Menu2[LCD_MENU2_CONFIRM]);        // linha 3
    lcd.setCursor(0, ROW4);
    lcd.print(Menu2[LCD_MENU2_CANCEL]);         // linha 4
#if (FW_SERIAL_DEBUG == 1)
    Serial.println(F("lcd     : Apresenta menu recarga."));
#endif
}

/*****************************************************************************************
 * Informa o menu que esta apresentando
 ****************************************************************************************/
byte lcd_get_actual_menu(void)
{
    return (vLcd.menu_actual);
}

/*****************************************************************************************
 * Informa o ultimo menu existente
 ****************************************************************************************/
byte lcd_get_menu_max_num(void)
{
    return (MENU_NUMOF);
}

/*****************************************************************************************
 * Informa o ultimo menu existente
 ****************************************************************************************/
void lcd_update_menu_to(byte menu)
{
    vLcd.menu_actual = menu;
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu01_row1_update(void)
{
    char msg[(LCD_NUM_COLS + 1)];

    // texto para variavel local
    for (byte i = 0; i < (LCD_NUM_COLS + 1); i++)
    {
        msg[i] = Menu1[LCD_MENU1_SHOW_CREDIT][i];
    }

    int_to_hex_array(credit_get_value(), &msg[10]);
    lcd.home();
    lcd.print(msg);
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu01_row2_update(byte option_select)
{
    byte num_of_dispenser;
    char msg[(LCD_NUM_COLS + 1)];

    // texto para variavel local
    for (byte i = 0; i < (LCD_NUM_COLS + 1); i++)
    {
        //msg[i] = Menu1Row2[1][i];
        msg[i] = Menu1[LCD_MENU1_SELECTPRODUCT][i];
    }

    num_of_dispenser = product_num_of_dispenser();
    if ((num_of_dispenser > 0) &&
        (num_of_dispenser < 10))
    {
        msg[14] = (0x30 + num_of_dispenser);
    }

    if (option_select <= '9')
    {
        msg[18] = option_select;
    }

    lcd.setCursor(0, ROW2);
    lcd.print(msg);
    lcd.setCursor(18, ROW2);
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu1web_row2_update_code(char _value, byte _position)
{
    lcd.setCursor((10 + _position), ROW2);
    lcd.print(_value);
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu1web_row3_update_retries(char _value)
{
    char value[3] = {' ', 0};

    lcd.setCursor(15, ROW3);
    value[0] = _value;
    lcd.print(value);
}



/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu02_row2_update(byte product_index)
{
    byte num_of_dispenser;
    byte product_qty;
    char msg[(LCD_NUM_COLS + 1)];

    // texto para variavel local
    for (byte i = 0; i < (LCD_NUM_COLS + 1); i++)
    {
        msg[i] = Menu2[LCD_MENU2_SHOW_QTY][i];
    }

    num_of_dispenser = product_num_of_dispenser();
    if (((num_of_dispenser > 0)   &&
         (num_of_dispenser < 10)) &&
        (product_index < num_of_dispenser))
    {
        msg[6] = (('0' + product_index) + 1);
        product_qty = product_get_quantity(product_index);
        if (product_qty < 100)
        {
            msg[15] = ((product_qty / 10) + '0'); product_qty %= 10;
            msg[16] =  (product_qty       + '0');
        }
#if (FW_SERIAL_DEBUG == 1)
        Serial.print(F("lcd     : "));
        Serial.println(msg);
#endif
    }
    else
    {
#if (FW_SERIAL_DEBUG == 1)
        Serial.println(F("lcd     : Produto invalido ou quantidade de dispensers superior ao esperado."));
#endif
    }

    lcd.setCursor(0, ROW2);
    lcd.print(msg);
}

/*****************************************************************************************
 * COMENTAR
 ****************************************************************************************/
void lcd_menu02_row2_update_qty(char _value, byte _position)
{
    char value[3] = {' ', ' ', 0};
    // TODO: avaliar o numero de posicoes possiveis
    if (_position == 0)
    {
        lcd.setCursor(15, ROW2);
        value[0] = _value;
        lcd.print(value);
    }
    else
    {
        lcd.setCursor(16, ROW2);
        lcd.print(_value);
    }
}
