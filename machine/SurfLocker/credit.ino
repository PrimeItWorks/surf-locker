
/*****************************************************************************************
 *                             Metodos do modulo credito
 ****************************************************************************************/

/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/
#define CREDIT_INITIAL_VALUE    0

typedef struct
{
    unsigned int  value;
    bool need_update;
} credit_config_t;

static credit_config_t vCredit;

/***************************************************************************************************
 * Adquire o credito verificado pelo noteiro
 **************************************************************************************************/
void credit_init(void)
{
	vCredit.value = CREDIT_INITIAL_VALUE;
}

/***************************************************************************************************
 * COMENTAR
 **************************************************************************************************/
void credit_update_value(void)
{
	/* TODO: avaliar limites */
	vCredit.value += noteiro_update_value();
	if (vCredit.value >= 10000)
	{
		vCredit.value = 10000;
	}
}

/***************************************************************************************************
 * COMENTAR
 **************************************************************************************************/
int credit_get_value(void)
{
	return (vCredit.value);
}

/***************************************************************************************************
 * COMENTAR
 * recebe o indice do produto e verifica se tem credito suficiente
 **************************************************************************************************/
bool credit_verify_balance(byte product_index)
{
	return (vCredit.value >= product_get_value(product_index));
}

/***************************************************************************************************
 * COMENTAR
 * recebe o indice do produto e subtrai este valor do credito total
 **************************************************************************************************/
void credit_calculate_remainder(byte product_index)
{
	vCredit.value -= product_get_value(product_index);
}

