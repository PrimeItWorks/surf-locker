
/***************************************************************************************************
 *                                      Aplicacao
 **************************************************************************************************/

/***************************************************************************************************
 * DEFINICOES
 **************************************************************************************************/
#define MSG_TIMEOUT                 1000
#define SET_INTERRUPT_WAIT_TIMER    500

static byte product_selected;
static bool app_update_menu;

static bool app_menu1_webcredit;
static byte app_menu1_web_digit_index;
static char app_menu1_web_code[4];
static bool app_product_code_editing;

// Rever posicao destas variaveis
// Utilizados na recarga do produto
static char app_new_product_quantity[MAX_NUM_DISPENSER][2];  // previsto ate dois digitos
static byte _app_product_index;
static byte _app_digit = 0;
static bool _app_product_qty_editing;
/***************************************************************************************************
 * Prototipos de funcoes locais
 **************************************************************************************************/

void _app_process_change_menu(void);
void _app_process_key_pressed(char key);
void _app_update_menu01(char key);
void _app_update_menu02(char key);

void _app_menu1_treat_key_nums(char key);
void _app_menu1_treat_key_hash(void);
void _app_menu1_treat_key_asterisk(void);
void _app_menu1web_treat_key_nums(char _key);
void _app_menu1web_treat_key_hash(void);
void _app_menu1web_treat_key_asterisk(void);
void _app_menu2_treat_key_nums(char key);
void _app_menu2_treat_key_hash(void);
void _app_menu2_treat_key_asterisk(void);

/***************************************************************************************************
 * Setup do hardware
 **************************************************************************************************/
void app_setup(void)
{
    comm_debug_setup();
    comm_esp8266_setup();
    keypad_setup();
    lcd_setup();
    leds_setup();
    motor_setup();
    noteiro_setup();
}

/***************************************************************************************************
 * Inicializacao
 **************************************************************************************************/
void app_init(void)
{
    // hardware init
    motor_init();
    leds_init();

    comm_debug_init();

    keypad_init();
    lcd_init();
    noteiro_init();
    comm_esp8266_init();
    lcd_show_wifi_status(esp8266_wifi_on());    // mostra status do wifi na tela

    product_selected = 0xFF;
    app_update_menu = true;
    app_menu1_webcredit = false;

#if (FW_SOFT_DEBUG == 0)
    delay(3000);
#else
    delay(500);
#endif

// software init
    credit_init();
    product_init();
}

/***************************************************************************************************
 * App running
 **************************************************************************************************/
void app_run(void)
{
    char keypad_key;

    // Avalia a funcao do noteiro
    if (noteiro_run() == true)
    {
        credit_update_value();    // atualiza valor do credito
        lcd_menu01_row1_update();
        if (product_selected != 0xFF)  // TODO: verificar se o menu eh compativel antes
        {
            // se foi digitado valor de produto valido anteriormente
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_CONFIRM_PRODUCT]);
            lcd.setCursor(0, ROW4);
            lcd.print(Menu1[LCD_MENU1_CANCEL_PRODUCT]);
        }
        else
        {
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
            lcd.setCursor(0, ROW4);
            lcd.print(row_blank);
        }
    }

    if (app_update_menu == true)
    {
        app_update_menu = false;
        _app_process_change_menu();
    }

    // Avalia se tecla foi pressionada
    if (keypad_read_key() == true)
    {
        _app_process_key_pressed(keypad_get_key());
    }

    // Toggle o LED de STATUS
    led_status_handle();
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_process_change_menu(void)
{
    byte next_menu;

    next_menu = lcd_get_actual_menu();
    next_menu++;

    if (next_menu > (lcd_get_menu_max_num() - 1))
    {
        next_menu = 0;
    }
    lcd_update_menu_to(next_menu);

    switch (next_menu)
    {
        // menu LockerPro
        case MENU_0:
            lcd_show_menu_initial();
            app_update_menu = true;  // requisita nova atualizacao do display
            break;

        // menu recarga
        case MENU_2:
            _app_product_index = 0;
            // le os valores das variaveis que contem as quantidades ???
            lcd_show_menu_recharge();
            break;

        // menu de apresentacao
        default:
        case MENU_1:
            lcd_show_menu_default();
            app_menu1_webcredit = false;
            break;
    }
}

/***************************************************************************************************
 * Valida a tecla pressionada de acordo com o menu atual
 *
 * As teclas validas para esta tela sao:
 *  1. valores numericos de 0 a 9. Elas serao avaliadas para o numero de dispenser
 *  2. a tecla '#' para confirmar o pedido
 *  3. a tecla '*' para trocar para o modo cliente/servidor
 **************************************************************************************************/
void _app_process_key_pressed(char _key)
{
    switch (lcd_get_actual_menu())
    {
        // menu de apresentacao
        case 1:
            _app_update_menu01(_key);
            break;

        // menu de recarga
        case 2:
            _app_update_menu02(_key);
            break;

        // menu LockerPro
        case 0:
        default:
            break;
    }
}

/***************************************************************************************************
 * Valida a tecla pressionada de acordo com o menu atual
 *
 * As teclas validas para esta tela sao:
 *  1. valores numericos de 0 a 9. Elas serao avaliadas para o numero de dispenser
 *  2. a tecla '#' para confirmar o pedido
 *  3. a tecla '*' para trocar para o modo cliente/servidor
 **************************************************************************************************/
void _app_update_menu01(char _key)
{
    switch (_key)
    {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if (app_menu1_webcredit == false)
            {
                _app_menu1_treat_key_nums(_key);
            }
            else
            {
                _app_menu1web_treat_key_nums(_key);
            }
            break;

        case 'A':
        case 'B':
        case 'C':
        case 'D':
            if (app_menu1_webcredit == true)
            {
                _app_menu1web_treat_key_nums(_key);
            }
            break;

        case '#':
            if (app_menu1_webcredit == false)
            {
                _app_menu1_treat_key_hash();
            }
            else
            {
                _app_menu1web_treat_key_hash();
            }
            break;

        case '*':
            if (app_menu1_webcredit == false)
            {
                _app_menu1_treat_key_asterisk();
            }
            else
            {
                _app_menu1web_treat_key_asterisk();
            }
            break;

        case 'e':
            app_update_menu = true;
            break;

        default:
            // nenhuma outra tecla eh valida neste menu
            break;
    }
}

/***************************************************************************************************
 * Valida a tecla pressionada de acordo com o menu atual
 *
 * As teclas validas para esta tela sao:
 *  1. valores numericos de 0 a 9. Elas serao avaliadas para o numero de dispenser
 *  2. a tecla '#' para confirmar o pedido
 *  3. a tecla '*' para trocar para o modo cliente/servidor
 **************************************************************************************************/
void _app_update_menu02(char _key)
{
    byte _product_quantity;
    int _produtc_value;

    switch (_key)
    {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            _app_menu2_treat_key_nums(_key);
            break;

        case '#':
            _app_menu2_treat_key_hash();
            break;

        case '*':
            _app_menu2_treat_key_asterisk();
            break;

        case 'e':
            app_update_menu = true;
            break;

        default:
            // nenhuma outra tecla eh valida neste menu
            break;
    }
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1_treat_key_nums(char _key)
{
    if ((_key == '0') || ((_key - 0x30) > product_num_of_dispenser()))
    {
        lcd.setCursor(0, ROW3);
        lcd.print(Menu1[LCD_MENU1_ERROR_OPTION]);
        lcd.setCursor(0, ROW4);
        lcd.print(row_blank);
        delay(MSG_TIMEOUT);
        lcd_menu01_row2_update(0xFF);
        product_selected = 0xFF;
        if (credit_get_value() != 0)
        {
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
        }
        else
        {
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
        }
        lcd.setCursor(0, ROW4);
        lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
    }
    else
    {
        product_selected = ((_key - 0x30) - 1);  // o indice dos dispensar comecam do 0.
        if (product_get_quantity(product_selected) == 0)
        {
            // mostra erro indisponivel
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ERROR_NOITEM]);
            lcd.setCursor(0, ROW4);
            lcd.print(row_blank);
            delay(MSG_TIMEOUT);
            lcd_menu01_row2_update(0xFF);
            product_selected = 0xFF;
            if (credit_get_value() != 0)
            {
                lcd.setCursor(0, ROW3);
                lcd.print(row_blank);
            }
            else
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
            }
            lcd.setCursor(0, ROW4);
            lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
        }
        else
        {
            lcd_menu01_row2_update(_key);
            if (credit_verify_balance(product_selected) == true)
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_CONFIRM_PRODUCT]);
                lcd.setCursor(0, ROW4);
                lcd.print(Menu1[LCD_MENU1_CANCEL_PRODUCT]);
            }
            else
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
                lcd.setCursor(0, ROW4);
                lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
            }
        }
    }
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1_treat_key_hash(void)
{
    byte _product_quantity;
    int _produtc_value;

    if (product_selected != 0xFF)
    {
        // dispensa o produto
        _product_quantity = product_get_quantity(product_selected);
        if (_product_quantity > 0)
        {
            // verifica se credito e suficiente
            if (credit_verify_balance(product_selected) == true)
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_RELEASE_PRODUCT]);
                lcd.setCursor(0, ROW4);
                lcd.print(row_blank);

                noteiro_detachInterrupt();  // desabilita interrupcao do noteiro

                if (motor_turnon(product_selected) == false)
                {
                    credit_calculate_remainder(product_selected);
                    product_delivery_count(product_selected);

                    lcd.setCursor(0, ROW3);
                    lcd.print(row_blank);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_THANKS]);
                    delay(MSG_TIMEOUT);
                    
                    esp8266_resume_money(products[product_selected].code);

                    lcd_menu01_row1_update();

                    if (credit_get_value() != 0)
                    {
                        lcd.setCursor(0, ROW3);
                        lcd.print(row_blank);
                    }
                    else
                    {
                        lcd.setCursor(0, ROW3);
                        lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
                    }
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
                }
                else
                {
                    // ocorreu erro. Deve voltar credito + quantidade
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_ERROR_MOTOR]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(row_blank);
                    delay(MSG_TIMEOUT);
                    lcd_menu01_row1_update();
                    lcd_menu01_row2_update(0xFF);
                    product_selected = 0xFF;
                    if (credit_get_value() != 0)
                    {
                        lcd.setCursor(0, ROW3);
                        lcd.print(row_blank);
                    }
                    else
                    {
                        lcd.setCursor(0, ROW3);
                        lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
                    }
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
                }

                 delay(SET_INTERRUPT_WAIT_TIMER);
                 noteiro_attachInterrupt();  // habilita interrupcao do noteiro
            }
            else
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_ERROR_NOMONEY]);
                lcd.setCursor(0, ROW4);
                lcd.print(row_blank);
                delay(MSG_TIMEOUT);
                if (credit_get_value() != 0)
                {
                    lcd.setCursor(0, ROW3);
                    lcd.print(row_blank);
                }
                else
                {
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
                }
                lcd.setCursor(0, ROW4);
                lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
            }

            lcd_menu01_row2_update(0xFF);
            product_selected = 0xFF;
        }
        else
        {
            // mostra erro indisponivel
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ERROR_NOITEM]);
            delay(MSG_TIMEOUT);
            lcd_menu01_row2_update(0xFF);
            product_selected = 0xFF;
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
            lcd.setCursor(0, ROW4);
            lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
        }
    }
    else
    {
        // mostra erro: falta selecionar item
        lcd.setCursor(0, ROW3);
        lcd.print(Menu1[LCD_MENU1_ERROR_NULLENTRY]);
        lcd.setCursor(0, ROW4);
        lcd.print(row_blank);
        delay(MSG_TIMEOUT);
        lcd_menu01_row2_update(0xFF);
        product_selected = 0xFF;
        if (credit_get_value() != 0)
        {
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
        }
        else
        {
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
        }
        lcd.setCursor(0, ROW4);
        lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
    }
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1_treat_key_asterisk(void)
{
    if (product_selected == 0xFF)
    {
        // Nao havia produto selecionado. Trata-se de uma requisicao na WEB
        app_menu1_webcredit = true;
        app_menu1_web_digit_index = 0;  // aponta para o primeiro digito
        lcd_show_menu_web();
    }
    else
    {
        // havia produto selecionado anteriormente. Apenas limpa a tela para nova edica
        lcd_menu01_row2_update(0xFF);
        if (credit_get_value() != 0)
        {
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
        }
        else
        {
            lcd.setCursor(0, ROW3);
            lcd.print(Menu1[LCD_MENU1_ASKFORMONEY]);
        }
        lcd.setCursor(0, ROW4);
        lcd.print(Menu1[LCD_MENU1_WEB_BUY]);
    }

    product_selected = 0xFF;
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1web_treat_key_nums(char _key)
{
    // a partir daqui entrara em edicao do codigo
    // havera um flag sinalizando que o numero entrou em edicao
    // sempre que pressionado corrige, volta ao menu1 - compra com dinheiro

    app_product_code_editing = true;

    switch (app_menu1_web_digit_index)
    {
        case 0:
            lcd_menu1web_row2_update_code(_key, app_menu1_web_digit_index);
            app_menu1_web_code[app_menu1_web_digit_index] = _key;
            app_menu1_web_digit_index++;
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
            lcd.setCursor(0, ROW4);
            lcd.print(Menu1[LCD_MENU1_CANCEL_PRODUCT]);
            break;

        case 1:
        case 2:
            lcd_menu1web_row2_update_code(_key, app_menu1_web_digit_index);
            app_menu1_web_code[app_menu1_web_digit_index] = _key;
            app_menu1_web_digit_index++;
            break;

        case 3:
            lcd_menu1web_row2_update_code(_key, app_menu1_web_digit_index);
            app_menu1_web_code[app_menu1_web_digit_index] = _key;
            app_menu1_web_digit_index++;
            lcd.setCursor(0, ROW3);
            lcd.print(Menu2[LCD_MENU2_CONFIRM]);
            break;

        default:
            break;
    }
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1web_treat_key_hash(void)
{   
    String codeToSend;
    char product_recv_code[36];
    esp8266_errors_e esp_ans;

    bool ready_to_release = false;

    codeToSend.reserve(4);

    if (app_product_code_editing)
    {
        if (app_menu1_web_digit_index > 3)
        {
            for (byte i = 0; i < 4; i++)
            {
                codeToSend += app_menu1_web_code[i];
            }
            for (byte i = 0; i < 3; i++)
            {
                lcd.setCursor(0, ROW3);
                lcd.print(Menu1[LCD_MENU1_WEB_CONNETING_SERVER]);
                // lcd_menu1web_row3_update_retries(0x30 + i + 1);
                lcd.setCursor(0, ROW4);
                lcd.print(row_blank);

                if (codeToSend == secretKey1) {
                  strcpy(product_recv_code, products[0].code);  
                  esp_ans = ESP_OK;
                } 
                else if (codeToSend == secretKey2) {
                  strcpy(product_recv_code, products[1].code);  
                  esp_ans = ESP_OK;
                }
                else if (codeToSend == secretKey3) {
                  strcpy(product_recv_code, products[2].code);  
                  esp_ans = ESP_OK;
                }
                else {
                  esp_ans = esp8266_verify_code(codeToSend, product_recv_code);
                  if ((esp_ans == ESP_OK) ||
                      (esp_ans == ESP_ERROR_NOT_CONNECTED))
                  {
                      break;
                  }
                }
            }
            
            switch (esp_ans)
            {
                case ESP_OK:
                    // procura por codigo nos produtos cadastrados
                    for (byte i = 0; i <  product_num_of_dispenser(); i++)
                    {
                        if (utils_is_chars_equals(product_recv_code, products[i].code, 4))
                        {
                            // verifica se ha quantidade suficiente. Se nao, continua a busca. 
                            if (product_get_quantity(i) > 0) 
                            {
                                product_selected = i;
                                break;
                            }
                        }
                    }

                    // tentativa de liberar o produto
                    if (product_selected != 0xFF)
                    {
                        lcd.setCursor(0, ROW3);
                        lcd.print(Menu1[LCD_MENU1_RELEASE_PRODUCT]);
                        lcd.setCursor(0, ROW4);
                        lcd.print(row_blank);

                        noteiro_detachInterrupt();  // desabilita interrupcao do noteiro

                        if (motor_turnon(product_selected) == false)
                        {
                            product_delivery_count(product_selected);
                            lcd.setCursor(0, ROW3);
                            lcd.print(row_blank);
                            lcd.setCursor(0, ROW4);
                            lcd.print(Menu1[LCD_MENU1_THANKS]);
                            delay(MSG_TIMEOUT);

                            esp8266_resume_code(codeToSend);
                        }
                        else
                        {
                            // ocorreu erro. Deve voltar credito + quantidade
                            lcd.setCursor(0, ROW3);
                            lcd.print(row_blank);
                            lcd.setCursor(0, ROW4);
                            lcd.print(Menu1[LCD_MENU1_ERROR_MOTOR]);
                            delay(MSG_TIMEOUT);
                        }

                        product_selected = 0xFF;                        
                        delay(SET_INTERRUPT_WAIT_TIMER);
                        noteiro_attachInterrupt();  // habilita interrupcao do noteiro
                    }
                    else {
                        lcd.setCursor(0, ROW4);
                        lcd.print(Menu1[LCD_MENU1_ERROR_MACHINE_WITHOUT_PRODUCT]);
                        delay(MSG_TIMEOUT);
                    }
                    break;

                case ESP_ERROR_NOT_CONNECTED:
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_WEB_TRY_AGAIN]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_ERROR_NOT_CONNECTED]);
                    delay(MSG_TIMEOUT);
                    break;

                case ESP_ERROR_ON_OPEN_TCP_CONN:
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_WEB_TRY_AGAIN]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_ERROR_ON_OPEN_TCP_CONN]);
                    delay(MSG_TIMEOUT);
                    break;

                case ESP_ERROR_ON_OPEN_REQUEST:
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_WEB_TRY_AGAIN]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_ERROR_ON_OPEN_REQUEST]);
                    delay(MSG_TIMEOUT);
                    break;

                case ESP_ERROR_USER_CODE:
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_WEB_TRY_AGAIN]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_ERROR_USER_CODE]);
                    delay(MSG_TIMEOUT);
                    break;
    
                case ESP_ERROR_UNEXPECTED:
                default:
                    lcd.setCursor(0, ROW3);
                    lcd.print(Menu1[LCD_MENU1_WEB_TRY_AGAIN]);
                    lcd.setCursor(0, ROW4);
                    lcd.print(Menu1[LCD_MENU1_ERROR_UNEXPECTED]);
                    delay(MSG_TIMEOUT);
                    break;
            }
        }
    }

    app_product_code_editing = false;
    app_menu1_webcredit = false;
    lcd_show_menu_default();
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu1web_treat_key_asterisk(void)
{
    app_product_code_editing = false;
    app_menu1_webcredit = false;
    lcd_show_menu_default();
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu2_treat_key_nums(char _key)
{

    // a partir daqui entrara em edicao dos valores dos numeros
    // havera um flag sinalizando que o numero entrou em edicao
    // sempre que pressionado corrige, este numero sera reescrito do valor interno

    _app_product_qty_editing = true;

    switch (_app_digit)
    {
        case 0:
            lcd_menu02_row2_update_qty(_key, _app_digit);
            app_new_product_quantity[_app_product_index][_app_digit] = _key;
            _app_digit++;
            lcd.setCursor(0, ROW3);
            lcd.print(row_blank);
            break;

        case 1:
            lcd_menu02_row2_update_qty(_key, _app_digit);
            app_new_product_quantity[_app_product_index][_app_digit] = _key;
            _app_digit++;
            lcd.setCursor(0, ROW3);
            lcd.print(Menu2[LCD_MENU2_CONFIRM]);
            break;

        default:
            break;
    }
}

/***************************************************************************************************
 * Trata da atualizacao dos valores individuais de cada dispenser.
 * Sera avaliado se houve alguma alteracao na quantidade e, se sim, avalia se o numero digitado
 *  esta correto.
 * Caso ocorra validacao OK, atualiza o valor e pula para o proximo dispenser.
 * Toda vez que chegar ate o ultimo dispenser, volta para o primeiro.
 * Para sair desta funcinalidade deve-se apertar o botao "CONFIG".
 **************************************************************************************************/
void _app_menu2_treat_key_hash(void)
{
    byte qty = 0;

    if (_app_product_qty_editing == true)
    {
        if (_app_digit > 1)
        {
            qty = ((app_new_product_quantity[_app_product_index][0] - '0') * 10) +
                   (app_new_product_quantity[_app_product_index][1] - '0');
            (void)product_update_quantity(_app_product_index, qty);
            lcd.setCursor(0, ROW3);
            lcd.print(Menu2[LCD_MENU2_SAVE_OK]);
            lcd.setCursor(0, ROW4);
            lcd.print(row_blank);
            delay(2000);
 
            _app_digit = 0;
            _app_product_index++;
            if (_app_product_index > 2)    // MAGIC NUMBER
            {
                _app_product_index = 0;
            }

            lcd_menu02_row2_update(_app_product_index);
            lcd.setCursor(0, ROW3);
            lcd.print(Menu2[LCD_MENU2_CONFIRM]);
            lcd.setCursor(0, ROW4);
            lcd.print(Menu2[LCD_MENU2_CANCEL]);

            _app_product_qty_editing = false;
        }
    }
    else
    {
        // nao foi atualizado o valor anterior
        lcd.setCursor(0, ROW3);
        lcd.print(Menu2[LCD_MENU2_SAVE_OK]);
        lcd.setCursor(0, ROW4);
        lcd.print(row_blank);
        delay(2000);
        _app_product_index++;
        if (_app_product_index > 2)    // MAGIC NUMBER
        {
            _app_product_index = 0;
        }
        lcd_menu02_row2_update(_app_product_index);
        lcd.setCursor(0, ROW3);
        lcd.print(Menu2[LCD_MENU2_CONFIRM]);
        lcd.setCursor(0, ROW4);
        lcd.print(Menu2[LCD_MENU2_CANCEL]);
    }
}

/***************************************************************************************************
 * DOCUMENTAR
 **************************************************************************************************/
void _app_menu2_treat_key_asterisk(void)
{
    // trata a correcao dos valores
    byte qty = 0;

    if (_app_product_qty_editing == true)
    {
        _app_digit = 0;

        lcd_menu02_row2_update(_app_product_index);
        lcd.setCursor(0, ROW3);
        lcd.print(Menu2[LCD_MENU2_CONFIRM]);
        lcd.setCursor(0, ROW4);
        lcd.print(Menu2[LCD_MENU2_CANCEL]);

        _app_product_qty_editing = false;
    }
}
