import React from "react";
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from "react-navigation";
import { View } from 'react-native'
import { Badge } from 'react-native-elements'
import { FontAwesome, Ionicons } from '@expo/vector-icons';

import AccountScreen from "./screens/AccountScreen";
import MachineScreen from "./screens/MachineScreen";
import HomeScreen from "./screens/HomeScreen";
import IntroScreen from "./screens/IntroScreen";
import LoginScreen from "./screens/LoginScreen";
import ProductScreen from "./screens/ProductScreen";
import VideoScreen from "./screens/VideoScreen";
import TicketScreen from "./screens/TicketScreen";

const TabAppNavigator = createBottomTabNavigator({
	Home: { 
		screen: HomeScreen,
		navigationOptions:  ({ screenProps, navigation }) => ({
			title :  "Produtos",
			tabBarOptions: {
				activeTintColor: '#F6921E',
			},
			tabBarIcon: ({ focused })  => <View>
				<Ionicons name="ios-list" size={22} color={focused ? '#F6921E' :'#82D1E2' } />
			</View>
		})
	},
	Machines: { 
		screen: MachineScreen,
		navigationOptions:  ({ screenProps, navigation }) => ({
			title : "Máquinas",
			tabBarOptions: {
				activeTintColor: '#F6921E',
			},
			tabBarIcon: ({ focused })  => {
				return (
					<View>
						<FontAwesome name="film" size={22} color={focused ? '#F6921E' :'#82D1E2' } />
					</View>
				)
			}
		})
	}, 
	Ticket: {
		screen: TicketScreen,
		navigationOptions: ({ screenProps, navigation }) => ({
			title : "Códigos",
			tabBarOptions: {
				activeTintColor: '#F6921E',
			},
			tabBarIcon:  ({ focused })  => {
				return (
					<View>
						<FontAwesome name="ticket" size={22} color={focused ? '#F6921E' :'#82D1E2' } />
						{ screenProps.ticketsBadge && screenProps.ticketsBadge > 0
							? <Badge
								badgeStyle={{backgroundColor:'#F6921E'}}
								value={screenProps.ticketsBadge}
								containerStyle={{ position: 'absolute', top: -10, right: -10 }}
								/>
							: null
						}
						
					</View>
				)
			}
		})
	}, 
	// Shopping: { 
	// 	screen: ShoppingScreen,
	// 	navigationOptions:  ({ screenProps, navigation }) => ({
	// 		title :  "Loja",
	// 		tabBarOptions: {
	// 			activeTintColor: '#F6921E',
	// 		},
	// 		tabBarIcon: ({ focused })  => {
	// 			return (
	// 				<View>
	// 					<FontAwesome name="shopping-bag" size={22} color={focused ? '#F6921E' :'#82D1E2' } />
	// 				</View>
	// 			)
	// 		}
			
	// 	})
	// },
	Account: { 
		screen: AccountScreen,
		navigationOptions: ({ screenProps, navigation }) => ({
			title : "Meus dados",
			tabBarOptions: {
				activeTintColor: '#F6921E',
			},
			tabBarIcon:  ({ focused })  => {
				return (
					<View>
						<FontAwesome name="user-circle" size={22} color={focused ? '#F6921E' :'#82D1E2' } />
					</View>
				)
			}
		})
	}
})

TabAppNavigator.navigationOptions = {
	header: null,
};

const AppNavigator = createStackNavigator({
	Tab: TabAppNavigator,
	Intro: { screen: IntroScreen,},
	Login: { screen: LoginScreen},
	Product: { screen: ProductScreen},
	Video: { screen: VideoScreen},
},
{ 
	initialRouteName: "Login",
});

export default createAppContainer(AppNavigator);