import { AsyncStorage } from 'react-native';
import jwtDecode from 'jwt-decode';
const AUTH_DATA_KEY = "AUTH_DATA";

/**
 * Responsável pelo armazenamento das credenciais do usuário.
**/
class Storage {

  async store(authData) {
    return await AsyncStorage.setItem(AUTH_DATA_KEY, JSON.stringify(authData))
  }

  async clear() {
    return await AsyncStorage.removeItem(AUTH_DATA_KEY);
  }

  async getData() {
    return JSON.parse(await AsyncStorage.getItem(AUTH_DATA_KEY));
  }

  async getUseId() {
    let tokenObj = JSON.parse(await AsyncStorage.getItem(AUTH_DATA_KEY))    
    return tokenObj.user;
  }

  async hasData() {
    return (await AsyncStorage.getItem(AUTH_DATA_KEY)) !== null;
  }
}

export default Storage;
