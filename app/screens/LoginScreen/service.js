'use strict'

import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'
import Toast from 'react-native-root-toast';

import * as Facebook from 'expo-facebook';

export default class {
    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async loginEmail(data) {
        let resp = await this.apiClient.postApi('/SignIn/email', data);    
        if(resp.status && resp.status !== 200) {
            return resp;
        }
        
        if(resp.statusCode === 200){            
            this.storage.store(resp.response)
        }        
        return resp
    }   
     
    async getUserByEmail(email){
        let resp   = await this.apiClient.getApi(`/accounts/byemail/${email}`)
        return resp
    }

    async createUser(user) {
        let resp   = await this.apiClient.postApi(`/accounts`, user)
        if(resp.statusCode === 200){            
            this.storage.store(resp.response)
        }  
        return resp
    }

    async loginFacebook() {
        let resp = {};

        try {
            const r = await Facebook.initializeAsync()
            const { type, token } = await Facebook.logInWithReadPermissionsAsync('2145604769072053', {
                permissions: ['public_profile', 'email']
            });

            if (type === 'success') {
                const response = await fetch(`https://graph.facebook.com/me?fields=email,name,birthday&access_token=${token}`);
                resp = await response.json();
            }

        } catch (err) {
            console.log('erro:::::', err);
        }

        if(!resp.name){
            Toast.show('Ops! Ocorreu um erro no seu login.', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});
            return null
        }

        const user = {
            name: resp.name,
            email: resp.email,
            facebookId: resp.id
        };
        
        const result = await this.apiClient.postApi('/SignIn', user);    
        if(result.status && result.status !== 200){
            return result;
        }
        
        if(result.statusCode === 200){            
            this.storage.store(result.response)
        }        

        return result
    }
}