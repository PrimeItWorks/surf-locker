import React, { useState, useEffect} from 'react';
import { Animated,Dimensions, UIManager, View, Text, TouchableOpacity, Image, Linking, TextInput } from "react-native"
import { useKeyboard } from 'react-native-hooks'
import { Ionicons, FontAwesome } from '@expo/vector-icons'
import Toast from 'react-native-root-toast';

import Service from './service'
import styles from './styles';

const { State: TextInputState } = TextInput;
const service = new Service()

const loginScreens = {
    register: 1,
    initial: 2,
    password: 3
}

LoginScreen.navigationOptions = {
	header: null
};

export default function LoginScreen(props) {
    const [email, setemail] = useState('')
    const [screen, setscreen] = useState(loginScreens.initial)
    const [password, setpassword] = useState('')
    const [name, setname] = useState('')
    const [passwordCreate, setpasswordCreate] = useState('')
    const [shift, setshift] = useState(new Animated.Value(0))
    const [ready, setready] = useState(true)

    const keyboard = useKeyboard()

    function keyboardDidShow (event) {
        const { height: windowHeight } = Dimensions.get('window')
        const keyboardHeight = event.endCoordinates.height
        const currentlyFocusedField = TextInputState.currentlyFocusedField()

        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
          const fieldHeight = height
          const fieldTop = pageY
          const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight)    
          if (gap >= 0) {
            return
          }

          Animated.timing(
            shift,
            {
              toValue: gap,
              duration: 500,
              useNativeDriver: true,
            }
          ).start()
        })
    }
    
    function keyboardDidHide(){
        Animated.timing(
          shift,
          {
            toValue: 0,
            duration: 500,
            useNativeDriver: true,
          }
        ).start()
    }

    useEffect(() => {
        keyboard.Keyboard.addListener('keyboardDidShow', keyboardDidShow)
        keyboard.Keyboard.addListener('keyboardDidHide', keyboardDidHide)
    }, [])

    async function login() {
        setready(false)
        if (password) {
            let user = {
                email: email,
                password: password
            }
            
            let loginResp = await service.loginEmail(user)   
            setready(true)
            
            if(loginResp.status && loginResp.status !== 200 || loginResp.statusCode !== 200) {
                Toast.show('Ops! Ocorreu um erro no seu login.', {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.BOTTOM,
                    shadow: true,
                    animation: true,
                    hideOnPress: true});

                return null
            }        
            
            if(loginResp.response.firstAccess) {
                props.navigation.navigate("Intro")
            }
            
            props.navigation.navigate("Home")
            return true
        }

        const loginResp = await service.loginFacebook()
        if (loginResp === null) {
            setready(true)
            return null
        }

        if(loginResp.status && loginResp.status !== 200 || loginResp.statusCode !== 200) {
            Toast.show('Ops! Ocorreu um erro no seu login.', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});

            setready(true)
            return null
        }        
        
        if(loginResp.response.firstAccess){
            props.navigation.navigate("Intro")
            setready(true)
            return true
        }

        setready(true)
        props.navigation.navigate("Home")
    }

    async function getUserByEmail() {
        if (!email) {
            return
        }

        if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            .test(email)) {
            Toast.show('Informe um e-mail válido', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});
            return
        }

        const user = await service.getUserByEmail(email)

        if (!user.response) {
            setscreen(loginScreens.register)
        }
        else {
            screen(loginScreens.password)
        }
    }

    async function createUserByLogin() {
        if(!(email && passwordCreate && name))
            return

        const request = {
            "name": name,
            "email": email,
            "password": passwordCreate,
            "isAdmin": false
        }

        const loginResp = await service.createUser(request)

        if(loginResp.status && loginResp.status !== 200 || loginResp.statusCode !== 200){
            Toast.show('Ops! Ocorreu um erro no seu login.', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});
            return null
        }        
        
        if(loginResp.response.firstAccess){
            props.navigation.navigate("Intro")
            return true
        }

        props.navigation.navigate("Home")
    }

    function renderSocialLinks() {
        return (
            <View style={styles.socialLinks}>
                <Text style={styles.socialLinks__Text}>NOSSAS REDES SOCIAS</Text>
                <View style={styles.socialLinks__ListIcons}>
                    <TouchableOpacity onPress={()=> Linking.openURL('https://www.facebook.com/surflockerbrasil/')}>
                        <View style={styles.socialLinks__ListIcons__Icon} >
                            <Text>
                                <FontAwesome name="facebook" size={18} color="#82D1E2" />
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> Linking.openURL('https://www.instagram.com/surflockerbrasil/')}>
                        <View style={styles.socialLinks__ListIcons__Icon}>
                            <Text>
                                <Ionicons name="logo-instagram" size={18} color="#82D1E2" />
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> Linking.openURL('https://api.whatsapp.com/send?phone=555198057588&text=Fala%20Leandro,%20sou%20teu%20cliente%20da%20Surf%20Locker,%20pode%20me%20ajudar?&source=&data=')}>
                        <View style={styles.socialLinks__ListIcons__Icon}>
                            <Text>
                                <FontAwesome name="whatsapp" size={18} color="#82D1E2" />
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    function renderBackground() {
        return (
            <>
                <Image source={require("../../assets/bg-login.png")} style={styles.imgBackground}/>
                <Image source={require("../../assets/logo.png")} style={styles.imgLogon}/>
            </>
        )
    }


    if(screen == loginScreens.initial) {
        return (
            <Animated.View style={[styles.mainContent, { transform: [{translateY: shift}] }]}>
                {renderBackground()}
                {ready 
                    ? <TouchableOpacity onPress={()=> login()} style={styles.buttonLogin}>
                        <Text style={styles.buttonLoginText} >ENTRE COM FACE </Text>
                    </TouchableOpacity>

                    :<TouchableOpacity style={styles.buttonLogin}>
                        <Text style={styles.buttonLoginText} >ENTRE COM FACE </Text>
                    </TouchableOpacity>}

                <View style={styles.email}>

                    <Text style={styles.email__Text}>ou digita aí teu e-mail</Text>

                    <TextInput
                        style={styles.email__Input}
                        placeholder="E-mail"
                        onChangeText={(text) => setemail(text)}
                        onBlur={(e) =>getUserByEmail()}
                        value={email}
                    />

                </View>
                {renderSocialLinks()}
            </Animated.View>
        )
    }

    else if(screen == loginScreens.register) {
        return (
            <Animated.View style={[styles.mainContent, { transform: [{translateY: shift}] }]}>
                 {renderBackground()}                
                <View style={styles.email}>
                    <Text style={styles.email__Text}>Perfeito! Agora diga seu nome e escolha uma senha para acessar o app</Text>
                    <TextInput
                        style={styles.email__Input}
                        placeholder="Nome"
                        onChangeText={text => setname(text)}
                        onBlur={(e) => createUserByLogin()}
                        value={name}
                    />
                    <TextInput
                        placeholder="Senha"
                        style={styles.email__Input}
                        onChangeText={text => setpasswordCreate(text)}
                        onBlur={(e) => createUserByLogin()}
                        value={passwordCreate}
                        secureTextEntry={true} 
                    />
                </View>
                {renderSocialLinks()}
            </Animated.View>
        )
    }

    else if(screen == loginScreens.password) {
        return (
            <Animated.View style={[styles.mainContent, { transform: [{translateY: shift}] }]}>
                {renderBackground()}                
                <View style={styles.email}>
                    <Text style={styles.email__Text}>Perfeito! Agora digite sua senha</Text>
                    <TextInput
                        placeholder="Senha"
                        style={styles.email__Input}
                        onChangeText={text => setpassword(text)}
                        onBlur={(e) => login()}
                        value={password}
                        secureTextEntry={true} 
                    />
                </View>
                {renderSocialLinks()}
            </Animated.View>
        )
    }

    return null
}
