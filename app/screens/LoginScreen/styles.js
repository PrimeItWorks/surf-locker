import { StyleSheet} from "react-native";

export default StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        backgroundColor: "#05072D"
    },
    imgLogon:{
        width: 200,
        height: 116,
        marginTop: -140,
        marginBottom: 20
    },
    imgBackground:{
        position: "absolute",
        top: 0,
        width: "100%",
        height: "100%",
    },
    buttonLogin:{
        backgroundColor: "#00A2B4",    
        width: 278,
        borderRadius: 10,
        height: 60 ,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    buttonLoginText:{
        color: "#FFF",
        fontSize: 16,
        textAlign:"center"
    },
    socialLinks:{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 0,
        paddingTop: 20,
        paddingBottom: 20
    },
    socialLinks__Text:{
        color: "#FFF"
    },
    socialLinks__ListIcons: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",        
        width: 288
    },
    socialLinks__ListIcons__Icon:{
        width:57,
        height: 57,
        borderWidth: 1,
        borderColor: "#82D1E2",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 57,
        marginTop: 20,
        marginBottom: 20
    },        
    email:{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        paddingTop: 20,
        paddingBottom: 20
    },
    email__Text:{
        color: "#FFF",
        marginTop:40,
        textAlign: 'center'
    },
    email__Input:{
        height: 60, 
        borderColor: '#CCC', 
        color: '#CCC', 
        borderWidth: 1, 
        marginTop: 20, 
        padding: 10, 
        width: 278,
        borderRadius: 10,
        height: 60 ,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },
    
    buttonLoginAlt:{
        backgroundColor: "transparent",    
        width: 278,
        borderRadius: 10,
        borderColor: "#CCC",
        borderWidth: 1,
        height: 60 ,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    }
})
