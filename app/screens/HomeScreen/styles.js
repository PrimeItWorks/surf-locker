import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",        
        width: "100%",        
    },
    imgLogon:{
        width: 106,
        height: 150,
        position: "relative",
        top: -5
    },
    prodImg:{
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: -20,
        left: 10,
        borderRadius: 6,
    },
    imgBackground:{
        position: "absolute",
        top: 0,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    headerProducts:{
        width: "100%",
        height: 280,
        display : "flex",
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden"
    },
    bodyProducts:{      
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        top: -20,
        zIndex: 99
    },
    bgProducts:{
        backgroundColor: "#E5E5E5",
        width: "100%",
        position: "absolute",
        top: 0,

    },
    grid:{
        backgroundColor: "#E5E5E5",
        display: "flex",
        width: "100%",
        height: Dimensions.get("screen").height,
        position: "relative",
    },
    gridProducts:{  
        flexDirection:"row",
    },
    gridProducts__mainProd:{
        display: "flex",
        flex: 2,
        flexDirection:"row",
        justifyContent : "center",
        alignItems : "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 12,
        paddingTop: 70,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        marginTop: 0,
        marginLeft: 15,
        marginRight: 15,
        minHeight: 210,
        maxWidth: 155  
    },
    gridProducts__othersProd:{        
        display: "flex",
        minHeight: 120,
        flexDirection:"row",
        justifyContent : "center",
        alignItems : "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 16,
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        marginTop: 20,
        marginLeft: 40,
        marginRight: 20
    },
    gridProducts__othersProdCombo:{        
        display: "flex",
        minHeight: 210,
        flexDirection:"row",
        justifyContent : "center",
        alignItems : "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 16,
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        marginTop: 20,
        marginLeft: 40,
        marginRight: 20
    },
    gridProducts__othersProd_img:{
        position: "relative",
        left: -30,
        width: "30%"
    },
    gridProducts__othersProd_imgCombo:{
        position: "relative",
        left: -30,
        width: "30%",
        height: 170

    },
    prodImg__othersProd:{
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: 0,
        left: -10,
        borderRadius: 6
    },
    gridProducts__othersProd_info: {
        position: "relative",
        width: "70%",
        position: "relative",
        left: -20
    },
    prodImg__othersProdTwo:{
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: 92,
        left: -10,
        borderRadius: 6
    },
    prodCategory: {
        color :"black",
        fontSize : 12,
        fontWeight: "bold",
    },
    prodTitle: {
        color :"#64B9CC",
        fontSize : 18,
        fontWeight: "bold",
    },
    prodTitleHighlight :{
        color: "#F6921E",
    },    
    prodQdt: {
        color: "#9B9B9B",
        fontSize : 12,
        marginTop: 5,
        marginBottom: 20
    },
    prodPrice:{
        color:"black",
        fontSize : 20,
        fontWeight: "bold",
    },
    viewEmpty:{
        display: 'flex',
        flex: 1,
        height: 150,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }

})