import React, { useState, useEffect } from 'react'
import { View, ScrollView, Image, ActivityIndicator, Text } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import {ProductsTypes} from '../../store/ducks/products'
import {AccountsTypes} from '../../store/ducks/accounts'
import styles from './styles'
import Service from './service'

const service = new Service()

HomeScreen.navigationOptions = {
	header: null
};

export default function HomeScreen(props) {
	const dispatch = useDispatch()
	const productsMachine = useSelector(state => state.products.machine)
	
	const [isRefreshing, setisRefreshing] = useState(true)
	const [isLoaded, setisLoaded] = useState(false)
	const [page, setpage] = useState(false)
	
	useEffect(async () => {
		const data = await service.getStoredData()
		if (!data) {
            props.navigation.navigate('Login');
        }

		await getProducts()
		await getAccount()
	}, [])

	async function getProducts() {
		const products = await service.getAllProduct()
		dispatch({type: ProductsTypes.MACHINE, productsMachine: products})
		setisRefreshing(false)
		setisLoaded(true)
	}

	async function getAccount() {
		const account = await service.getAccount()
		dispatch({type: AccountsTypes.ACCOUNT, account})
	}

	async function getMore() {
		if(productsMachine.hasNextPage) {

			const appendToMachineProducts = await service.getAllProduct(page +1)
			dispatch({type: ProductsTypes.APPENT_TO_MACHINE, appendToMachineProducts})
			setpage(page +1)
        }
	}

	async function refresh() {
		setisRefreshing(true)
		await getProducts()
    }

	if(!isLoaded){
		return (
			<View style={styles.mainContent}>            
				<ScrollView style={styles.grid}>                                    
					<View style={styles.headerProducts}>
						<Image source={require("../../assets/bg-login.png")} style={styles.imgBackground}/>
						<Image source={require("../../assets/logo-horizontal.png")} style={styles.imgLogon}/>
					</View>     
					<View style={styles.mainContent}>                    
						<ActivityIndicator size="large" color="#0000ff" />
					</View>
				</ScrollView>
			</View>
		)
	}
	
	if(productsMachine.items.length === 0){
		return(
			<View style={styles.mainContent}>            
				<ScrollView style={styles.grid}>                                    
					<View style={styles.headerProducts}>
						<Image source={require("../../assets/bg-login.png")} style={styles.imgBackground}/>
						<Image source={require("../../assets/logo-horizontal.png")} style={styles.imgLogon}/>
					</View>                      
					<View style={styles.bodyProducts}>
						<View style={styles.bgProducts}></View>
						<View style={styles.gridProducts}>
							<View style={styles.viewEmpty}>
								<Text>Nenhum produto cadastrado ...</Text>
							</View>                                
						</View>
					</View>
				</ScrollView>
			</View>
		)
	}

	let secondItems = []
	for(let i = 2; i < productsMachine.items.length; i++){
		secondItems.push(productsMachine.items[i])
	}

	return (
		<View style={styles.mainContent}>            
			<ScrollView style={styles.grid}>                                    
				<View style={styles.headerProducts}>
					<Image source={require("../../assets/bg-login.png")} style={styles.imgBackground}/>
					<Image source={require("../../assets/logo-horizontal.png")} style={styles.imgLogon}/>
				</View>                      
				<View style={styles.bodyProducts}>
					<View style={styles.bgProducts}></View>

					{productsMachine.items.map((ele, index) => {                            
						if(index > 1) {
							return null
						}

						return ( 
							<View style={styles.gridProducts} key={index}>
								<TouchableOpacity style={styles.gridProducts__mainProd} onPress={()=> this.props.navigation.navigate("Product", {prod : ele})}>                            
									<Image source={{uri : ele.thumbnail[0]}} style={styles.prodImg}/>
									<View>                                    
										<Text style={styles.prodCategory}>{ele.trademark}</Text>
										<Text style={styles.prodTitle}>{ele.title}</Text>
										<Text style={styles.prodQdt}>{`${ele.unit} unidade`}</Text>
										<Text style={styles.prodPrice}>R$ {ele.price.toFixed(2).replace('.',',')}</Text>
									</View>
								</TouchableOpacity>
							</View>
						)
					})}

					<FlatList
						data={secondItems}
						renderItem={({ item }) =>
							<TouchableOpacity 
								style={styles.gridProducts__othersProdCombo} 
								onPress={()=> props.navigation.navigate("Product", {prod : item})}>

								<View style={item.thumbnail.length <= 1 ? styles.gridProducts__othersProd_img : styles.gridProducts__othersProd_imgCombo}>
									<Image source={{uri : item.thumbnail[0]}}  style={styles.prodImg__othersProd}/>
									{item.thumbnail.length > 1 && <Image source={{uri : item.thumbnail[0]}}  style={styles.prodImg__othersProdTwo}/>}                                        
								</View>

								<View style={styles.gridProducts__othersProd_info}>
									<Text style={styles.prodCategory}>{item.trademark}</Text>
									<Text style={styles.prodTitle}>{item.title}</Text>
									<Text style={styles.prodQdt}>{`${item.unit} unidade`}</Text>
									<Text style={styles.prodPrice}>R$ {item.price.toFixed(2).replace('.',',')}</Text>
								</View>

							</TouchableOpacity>
						}
						keyExtractor={(item, index) => index.toString()}
						numColumns={1}
						refreshing={isRefreshing}
						onRefresh={refresh}
						onEndReached={getMore}
						onEndThreshold={0}
						style={{width: '100%'}}
					/>
				</View>
			</ScrollView>
		</View>
	)
}

