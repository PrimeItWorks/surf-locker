'use strict'

import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'

export default class {

    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    getStoredDate = async () => {
        return await this.storage.getData()
    }

    async getAllProduct(page = 1){
        let resp = await this.apiClient.getApi(`/Products/machine?Page=${page}`, true);

        return resp;
    }

    async getAccount(){
        let user  = await this.storage.getUseId()                   
        let resp  = await this.apiClient.getApi(`/accounts/${user.code}`, true)
        return resp
    }
}