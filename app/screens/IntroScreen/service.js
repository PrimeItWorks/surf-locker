'use strict'

import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'

export default class {
    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async getAccount(){
        let user  = await this.storage.getUseId()                   
        let resp  = await this.apiClient.getApi(`/accounts/${user.code}`, true)
        return resp
    }

    async SendPhoneSmS(data){
        let resp = await this.apiClient.postApi('/Accounts/AskSmsPhoneConfirmation', data, true)
        return resp
    }

    async gift(data){
        let resp   = await this.apiClient.postApi("/vouchers/gift", data, true);
        return resp;
    }

    async confirmPhone(data){
        let resp   = await this.apiClient.postApi("/accounts/confirmphone", data, true)
        return resp
    }
}