import React, { useState, useEffect, useRef} from 'react'
import { Animated,Dimensions, UIManager, View, Text, TouchableOpacity, Image } from "react-native"
import { useKeyboard } from 'react-native-hooks'
import AppIntroSlider from 'react-native-app-intro-slider'

import styles from './styles'
import Service from './service'

const service = new Service()

IntroScreen.navigationOptions = {
	header: null
};

export default function IntroScreen(props) {
  const appIntroSlider = useRef(null)
  const phoneField = useRef(null)
  const codeSmSField = useRef(null)
  const [machineKey, setmachineKey] = useState('')
  const [phone, setphone] = useState('')
  const [shift] = useState(new Animated.Value(0))
  const [codeSmS, setcodeSmS] = useState('')
  const [errors, seterrors] = useState({})
  const [slides, setslides] = useState([
      {
        key: 'somethun',
        title: 'Fala brother! ',
        text: 'É tua primeira visita aqui no app da Surf Locker? Seja bem-vindo à família!!',
        img : require("../../assets/intro-1.png"),
      },
      {
        key: 'somethun1',
        text: 'Beleza!! Agora vamos precisar do teu cel pra fazermos teu cadastro!',
        img : require("../../assets/intro-2.png"),
        voucher: '',
      }
  ])

  const keyboard = useKeyboard()
  
  function keyboardDidShow (event) {
      const { height: windowHeight } = Dimensions.get('window')
      const keyboardHeight = event.endCoordinates.height
      const currentlyFocusedField = TextInputState.currentlyFocusedField()

      UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
        const fieldHeight = height
        const fieldTop = pageY
        const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight)    
        if (gap >= 0) {
          return
        }

        Animated.timing(
          shift,
          {
            toValue: gap,
            duration: 500,
            useNativeDriver: true,
          }
        ).start()
      })
  }

  function keyboardDidHide(){
      Animated.timing(
        shift,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }
      ).start()
  }

  useEffect(() => {
      keyboard.Keyboard.addListener('keyboardDidShow', keyboardDidShow)
      keyboard.Keyboard.addListener('keyboardDidHide', keyboardDidHide)
  }, [])

  function handleValidatePhone(){
    let err = {}
    if(phone === ''){
        err.phone = "Telefone obrigatório"            
    }

    if(!/^\d{11}$/i.test(phoneField.getRawValue())){
        err.phone = "Telefone deve conter somente números"
    }        

    return err
  }

  function handleMsgErrorApi(data){        
    if(data.hasOwnProperty('status') && data.status !== 200){
        data.statusCode = data.status
    }

    if(data.hasOwnProperty('statusCode') && data.statusCode !== 200 && data.statusCode === 401 || data.statusCode === 403){
        props.navigation.navigate('Login')
        return null
    }

    if(data.hasOwnProperty('statusCode')  && data.statusCode !== 200){
        Toast.show('Ops! Ocorreu algum erro de carregamento', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true})
        return null
    }

    if(data.response === null || !data.hasOwnProperty('response')){
        Toast.show('Ops! Ocorreu algum erro de carregamento', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true})
        return null
    }

    return true
  }

  async function submitPhone() {
    let err = handleValidatePhone()
    seterrors(err)
    if(err.phone) {
        Toast.show(err.phone, {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true})
        return null
    }

    let data = await service.getAccount()

    if(!handleMsgErrorApi(data)){
        return null            
    }

    const ticket = await service.gift(data.response.code)

    if(!handleMsgErrorApi(ticket)){
      return null            
    }

    const confirmed = await service.confirmPhone(data.response.code)

    if(!handleMsgErrorApi(confirmed)){
      return null             
    }

    setslides([{
          key: 'somethun2',
          text: "Irado, man!! Anota esse código aí que ele vale uma parafa em qualquer uma das nossas máquinas! Alohaaa",
          img : require("../../assets/intro-3.png"),
          voucher: ticket.response.machineKey,
      }])

    setmachimachineKey(ticket.response.machineKey)

    appIntroSlider.goToSlide(0)
    return true
  }


  function renderItem (props) {
    return (
        <View style={[styles.mainContent, {
                paddingTop: props.topSpacer,
                paddingBottom: props.bottomSpacer,
                width: props.width,
                height: props.height,
            }]} >   
            
            <View style={styles.SlideBody}>
              <Image source={require("../../assets/logo.png")} style={styles.imageLogo} />
              <View style={styles.contentSlider}>
                <Image source={props.img} style={styles.imgSlide} />
                {props.title ? <Text style={styles.title}>{props.title}</Text> : null }
                <Text style={styles.text}>{props.text}</Text>
                {props.key === "somethun2" && props.voucher !== '' ? 
                    <View style={styles.btn}>
                        <Text style={styles.btn__text}>{props.voucher}</Text>
                    </View>
                    : null
                }
                {props.key === "somethun1" && props.voucher === ''
                  ? 
                    <View>
                        <TextInputMask type={'cel-phone'} options={{maskType: 'BRL', withDDD: true,dddMask: '(99) '}} 
                            style={styles.dataUserField} 
                            value={phone} 
                            onChangeText={() => {
                                const phone = `55${phoneField.getRawValue()}`
                                setphone(phone)
                            }}
                            ref={phoneField}
                        /> 
                        <Text style={styles.errors}>{errors && errors.phone}</Text>                  
                        <TouchableHighlight style={styles.buy}  onPress={() => submitPhone()}>
                            <View style={styles.btn}>
                                <Text style={styles.btn__text}>{'SALVAR'}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                :
                    props.key === "somethun1" && props.voucher !== ''? 
                        <View>
                            <TextInputMask type={'only-numbers'} 
                                style={styles.dataUserField} 
                                value={codeSmS}
                                onChangeText={codeSmS => setcodeSmS(codeSmS)}
                                ref={codeSmSField}
                            /> 
                            <Text style={styles.errors}>{errors && errors.phone}</Text>                  
                            <TouchableHighlight style={styles.buy} onPress={() => submitPhone()}>
                                <View style={styles.btn}>
                                    <Text style={styles.btn__text}>{'VALIDAR CÓDIGO'}</Text>
                                </View>
                            </TouchableHighlight>
                        </View> 
                    : null                   
                }
            </View>
            <View style={styles.slideFooter}></View>
            </View>
        </View>
    )
  }

  function renderDoneButton()  {
    if (!machineKey)
        return null    

    return (
        <View>
            <TouchableOpacity onPress={()=> props.navigation.navigate('Home')}>
                <Text style={styles.buttonLabel}>Entrar</Text>
            </TouchableOpacity>
        </View>
    )
  }

  function renderNextButton() {
    return (
        <View>
            <Text style={styles.buttonLabel}>                    
                Próximo
                <Entypo name="chevron-small-right" size={10} color="#82D1E2" />
            </Text>
        </View>
    )
  }

  function renderPrevButton() {
    return (
        <View>
            <Text style={styles.buttonLabel}>
                <Entypo name="chevron-small-left" size={10} color="#82D1E2" />
                Anterior
            </Text>
        </View>
    )
}

  return (
    <Animated.View style={[styles.mainContent, { transform: [{translateY: shift}] }]}>
      <Image source={require('../../assets/bg-intro.png')} style={styles.bgSlider}/>
      <AppIntroSlider
          ref={appIntroSlider}
          slides={slides}
          renderItem={renderItem}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          renderPrevButton={renderPrevButton}
          showPrevButton={true}
          showNextButton={true}
      />
  </Animated.View>
  )
}
