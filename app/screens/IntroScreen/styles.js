import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
    mainContent: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-around',
    },
    text: {
      color: '#979797',
      backgroundColor: 'transparent',
      textAlign: 'center',
      paddingHorizontal: 16,
      fontWeight: "bold",
      fontSize: 16,
    },
    title: {
      fontSize: 22,
      color: '#00A2B4',
      backgroundColor: 'transparent',
      textAlign: 'center',
      marginBottom: 16,
      fontWeight: "bold",
      marginLeft: 30,
      marginRight: 30,
    },
    bgSlider:{
        position: "absolute",
        top: 0,
        width: "100%"
    },
    SlideBody:{
        display: "flex",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: Dimensions.get("screen").height,
    },
    contentSlider:{
        backgroundColor: "#FFF",
        width: 280,
        height: 360,
        borderRadius: 10,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: 30
    },
    imgSlide:{
        width: 100,
        height: 100,
        marginBottom: 5
    },
    imageLogo:{
        width: 192,
        height: 112,   
        marginBottom: 10,
        marginTop: 10     
    },
    slideFooter: {
        width: "100%",
        height : 100,
        backgroundColor : "#05072D",
        position: "absolute",
        bottom: -80
    },
    buttonLabel:{
        color: "#82D1E2"
    },
    dataUserLabel: {
        fontSize: 13,
        color: "#9B9B9B",
        fontWeight: "bold",
        marginBottom: 10
    },
    dataUserField: {
        fontSize: 16,
        color: "#000",
        fontWeight: "bold",
        borderWidth: 1,
        borderColor: "#00A2B4",
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 10,
        borderRadius: 6,
        width: '100%',
        minWidth: 200,
    },
    btn: {
        width: 200,
        height: 46,
        borderRadius: 8,
        backgroundColor: "#00A2B4",
        justifyContent: "center",
        alignItems: "center"
    },
    btn__text: {
        color: "#FFF",
        fontWeight: "bold",
        fontSize: 16
    },
})