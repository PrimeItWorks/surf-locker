import React, { useState, useRef, useEffect } from 'react';
import { View } from 'react-native';

import { useSelector, useDispatch } from 'react-redux';
import Service from './service'
import styles from './styles';
import { TicketsTypes } from '../../store/ducks/tickets';

const service = new Service()

ProductScreen.navigationOptions = {
	header: null
};

export default function ProductScreen(props) {
    const dispatch = useDispatch()
    const account = useSelector(state => state.accounts.account)

    const cpfField = useRef()
    const birthdayField = useRef()

    const [modalVisible, setmodalVisible] = useState(false)
    const [loading, setloading] = useState(false)
    const [code, setcode] = useState('')
    const [cpf, setcpf] = useState('')
    const [birthday, setbirthday] = useState('')
    const [name, setname] = useState('')
    const [email, setemail] = useState('')
    const [phone, setphone] = useState('')
    const [cardToken, setcardToken] = useState('')
    const [cardholderName, setcardholderName] = useState('')
    const [cardNumber, setcardNumber] = useState('')
    const [expirationDate_month, setexpirationDate_month] = useState('')
    const [expirationDate_year, setexpirationDate_year] = useState('')
    const [securityCode, setsecurityCode] = useState('')
    const [cardErrors, setcardErrors] = useState({})
    const [quantity, setquantity] = useState(1)
    const [success, setsuccess] = useState(false)
    const [voucher, setvoucher] = useState('')
    const [latestDigitCard, setlatestDigitCard] = useState('')

    useEffect(() => {
        setAccountToState()
    }, [])

    function handleMsgErrorApi(data) {        
        if(data.hasOwnProperty('status') && data.status !== 200) {
            data.statusCode = data.status
        }

        if(data.hasOwnProperty('statusCode') 
            && data.statusCode !== 200 
            && data.statusCode === 401 
            || data.statusCode === 403) {
            setloading(false)
            setmodalVisible(false)
            props.navigation.navigate('Login')
            return null
        }

        if(data.hasOwnProperty('statusCode')  && data.statusCode !== 200) {
            Toast.show('Ops! Ocorreu algum erro de carregamento', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true
            })

            setloading(false)
            setmodalVisible(false)
            return null
        }

        if(data.response === null || !data.hasOwnProperty('response')) {
            if(data.notifications) {
                const notification = data.notifications[0]
                if (notification.code == "CREATE_TRANSACTION") {
                    const gatewayNotification = JSON.parse(notification.message)
                    const message = MESSAGES[gatewayNotification.error.code]
                    if(message) {
                        Toast.show(" teste", {
                            duration: Toast.durations.LONG,
                            position: Toast.positions.BOTTOM,
                            shadow: true,
                            animation: true,
                            hideOnPress: true})
                    }

                    setloading(false)
                    setmodalVisible(false)
                    return null
                }

                const message = MESSAGES[notification.code]
                if(message) {
                    Toast.show(message, {
                        duration: Toast.durations.LONG,
                        position: Toast.positions.BOTTOM,
                        shadow: true,
                        animation: true,
                        hideOnPress: true
                    })
                }

                setloading(false)
                setmodalVisible(false)
                return null
            }

            Toast.show('Ops! Ocorreu algum erro de carregamento', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true
            })
            setloading(false)
            setmodalVisible(false)
            return null
        }

        return true
    }

    function validateCreditCard() {
        let err = {}

        if(cardholderName === '') {
            err.cardholderName = "Nome do titular do cartão é obrigatório"   
        }

        if(cardNumber === '') {
            err.cardNumber = "Número do cartão é obrigatório"   
        }

        if(birthday === '') {
            err.birthday = "Data de nascimento do titular do cartão é obrigatório"   
        }

        if(cpf === '') {
            err.cpf = "CPF do titular do cartão é obrigatório"   
        }
        
        if(!/^\d{11}$/i.test(cpfField.getRawValue())){
            err.cpf = "CPF do titular deve conter somente números"
        }

        if(expirationDate_month ==='' || expirationDate_year ==='' ){
            err.expirationDate = "Data e validade obrigatória"   
        }

        if(expirationDate_month !=='' && expirationDate_month > 12){
            err.expirationDate = "Informe um mês válido"   
        }

        let today = new Date()
        let currentMonth = today.getMonth()+ 1
        let currentYear =  today.getFullYear().toString().substr(-2)
        if(currentYear == expirationDate_year && currentMonth > expirationDate_month){
            err.expirationDate = "Informe um a data de validate válida"       
        }

        if(securityCode ===''){
            err.securityCode = "codigo de segurança obrigatória"   
        }
        
        return err
    }

    async function setAccountToState(){
        setcode(account.code)
        setbirthday(`${splitedDate[2]}/${splitedDate[1]}/${splitedDate[0]}`)
        setfacebookId(data.facebookId)
        setname(account.name)
        setemail(ata.response.email)
        setaddress(account.address)
        setphone(account.phone)
        setwaves(account.waves)
        setcpf(account.cpf)
        setcardToken(account.cardToken)
        setlatestDigitCard(account.latestDigitCard)
    }

    async function submitCreditCard() {
        const prod = props.navigation.getParam('prod')
        setloading(true)
        try {

            let err = validateCreditCard()
            if(Object.keys(err).length !== 0) {
                setcardErrors(err)
                setTimeout(() => {
                    setcardErrors(cardErrors)
                }, 5000)       
                
                Toast.show('Informe os dados obrigatórios', {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.BOTTOM,
                    shadow: true,
                    animation: true,
                    hideOnPress: true
                })

                return null
            }       
            
            setloading(true)
            let accessKey = await service.GetAcessToken()
            if(handleMsgErrorApi(accessKey) === null) {
                return null
            }

            let cardData = {
                "accessKey": accessKey.response.accessKey,
                "cardData": {
                    "type": 1,
                    "cardholderName": cardholderName,
                    "cardNumber": cardNumber.replace(/ /g,''),
                    "expirationDate": expirationDate_month+expirationDate_year,
                    "securityCode": securityCode
                }
            }

            let resp = await service.postCreditCard(cardData)
            if(resp.error){            
                Toast.show(resp.error.message, {
                    duration: Toast.durations.LONG,
                    position: Toast.positions.BOTTOM,
                    shadow: true,
                    animation: true,
                    hideOnPress: true
                })
                return null
            }       

            let respPUblicKey = await service.PublicApiToken()
            if(handleMsgErrorApi(respPUblicKey) === null) {
                return null
            }

            let createMyCreditCard = {
                "userCode": code,
                "gatewayRequest": {
                "merchantKey": respPUblicKey.response.merchantKey,
                "cardNonce": resp.cardNonce,
                "waitForToken": true
                }
            }

            let card = await service.CreateCreditCard(createMyCreditCard)
            if(handleMsgErrorApi(card) === null){
                return null
            }       

            const splitedDate = birthday.split('/')
            
            let createMyTransaction = {
                "merchantKey": respPUblicKey.response.merchantKey,
                "metaId": "",
                "softDescriptor": "",
                "paymentMethod": [
                {
                    "cardNonce": resp.cardNonce,
                    "cardBrandId": card.response.brandId,
                    "cardToken": card.response.cardToken,
                    "installmentType": 1,
                    "installments": 1,
                    "amount": prod.price * quantity
                }
                ],
                "customerInfo": {
                "userCode": code,
                "productCode": prod.code,
                "quantity": quantity,
                "fullName": name,
                "cpf": cpf,
                "birthday": `${splitedDate[2]}-${splitedDate[1]}-${splitedDate[0]}`,
                "phoneNumber": phone,
                "emailAddress": email
                },
                "amount": prod.price * quantity
            }
            
            let transaction = await service.CreateTransaction(createMyTransaction)
            if(handleMsgErrorApi(transaction) === null){
                return null
            }

            let createMyTicket = {
                "productCode": prod.code,
                "userCode": code,
                "title": prod.title,
                "trademark": prod.trademark,
                "thumbnail": prod.thumbnail,
                "quantity": quantity,
                "image": prod.image,
                "machineKey": "",
                "description": prod.description,
                "urlVideo": prod.urlVideo,
                "price": prod.price,
                "active": true
            }

            let respTicket = await service.createTicket(createMyTicket)
            if(handleMsgErrorApi(respTicket) === null) {
                return null
            }

            const tickets = service.getTickets(1)
            dispatch({type: TicketsTypes.TICKETS, tickets})
            dispatch({type: TicketsTypes.TICKETS_BADGE, ticketsBadge: 1})

            setsuccess(success)
            setloading(loading)
            setvoucher(voucher)
            
            setTimeout(() => {
                props.navigation.navigate("Home")
                setloading(false)
            }, 9000)
        }
        catch (err) {
            setloading(false)
        }

    }

    async function submitTransaction(){
        const prod = props.navigation.getParam('prod')
        setloading(true)

        try {
            let respPUblicKey = await service.PublicApiToken()
            if(handleMsgErrorApi(respPUblicKey) === null) {
                return null
            }

            const splitedDate = birthday.split('/')
            let createMyTransaction = {
                "merchantKey": respPUblicKey.response.merchantKey,
                "metaId": "string",
                "softDescriptor": "string",
                "paymentMethod": [
                    {                
                        "cardToken": cardToken,
                        "installmentType": 1,
                        "installments": 1,
                        "amount": prod.price * quantity
                    }
                ],
                "customerInfo": {
                    "userCode": code,
                    "productCode": prod.code,
                    "quantity": quantity,
                    "fullName": name,
                    "cpf": cpf,
                    "birthday": `${splitedDate[2]}-${splitedDate[1]}-${splitedDate[0]}`,
                    "phoneNumber": phone,
                    "emailAddress": email
                },
                "amount": prod.price * quantity
            }
            
            let transaction = await service.CreateTransaction(createMyTransaction)
            
            if(handleMsgErrorApi(transaction) === null) {
                return null
            }
            
            let createMyTicket = {
                "productCode": prod.code,
                "userCode": code,
                "title": prod.title,
                "trademark": prod.trademark,
                "thumbnail": prod.thumbnail,
                "quantity": quantity,
                "image": prod.image,
                "machineKey": "",
                "description": prod.description,
                "urlVideo": prod.urlVideo,
                "price": prod.price,
                "active": true
            }
            
            let respTicket = await service.createTicket(createMyTicket)
            if(handleMsgErrorApi(respTicket) === null){
                return null
            }
        
            setloading(false)
            setsuccess(true)
            setvoucher(respTicket.response.machineKey)

            setsuccess(success)
            setloading(loading)
            setvoucher(voucher)
            
            setTimeout(() => {
                props.navigation.navigate("Home")
                setloading(false)
            }, 9000)
        } catch (err) {
            setloading(false)
        }
    }
    
    function handleVideo(urlVideo) {
        props.navigation.navigate('Video', { url: urlVideo })
    }

    const prod = props.navigation.getParam('prod')

    if (loading) {
        return (
            <View style={styles.mainContent}>
                <TouchableHighlight style={{position: "absolute", top: 25, left: 5, width: 40, height: 40, display: "flex", justifyContent: "center", alignItems: "center", zIndex: 99}} onPress={()=> props.navigation.navigate("Home")}><Ionicons name="ios-arrow-back" size={32} color="#00A2B4"  /></TouchableHighlight>
            <ScrollView style={styles.grid}>
                <View style={styles.content}>
                    <View style={styles.headerProduct}>
                        <Image source={{uri: prod.image}} style={styles.prodImg}/>
                    </View>
                    <View style={styles.bodyProduct}>
                        <Text style={styles.prod__title__span}>{prod.trademark.toUpperCase()}</Text>
                        <Text style={styles.prod__title}>{prod.title.toUpperCase()}</Text>
                        <Text style={styles.prod__text}>{prod.description}</Text>

                        {prod.urlVideo ?
                            <TouchableHighlight onPress={()=> handleVideo(prod.urlVideo)}>
                                <Text style={styles.prod__link}><FontAwesome name="chevron-circle-right" size={18} color="#00A2B4"  />{' VÍDEO DO PRODUTO'}</Text>
                            </TouchableHighlight>
                        : null}
                        <View style={styles.prod_qtd}>
                            <Text style={styles.prod_qtd__text}>Quantidade:</Text>
                            <Picker
                                selectedValue={quantity}
                                style={{height: 50, width: 140}}
                                onValueChange={itemValue => setquantity(itemValue)}>
                                <Picker.Item label="1 unidade" value={1} />
                                <Picker.Item label="2 unidades" value={2} />
                                <Picker.Item label="3 unidades" value={3} />
                                <Picker.Item label="4 unidades" value={4} />
                                <Picker.Item label="5 unidades" value={5} />
                            </Picker>
                        </View>
                        <View style={styles.prod_total}>
                            <Text style={styles.prod_total__text}>Total:</Text>
                            <Text style={styles.prod_total__price}>R$ {(quantity * prod.price).toFixed(2).replace(".",',')}</Text>
                        </View>
                        <TouchableHighlight style={styles.buy}  onPress={() => setmodalVisible(true)}>
                            <View style={styles.prod_btn}>
                                <Text style={styles.prod_btn__text}>{'Comprar'.toUpperCase()}</Text>
                            </View>
                        </TouchableHighlight>
                        <Modal
                            animationType="fade"
                            transparent={true}
                            visible={modalVisible}
                            onRequestClose={() => {
                                console.log('Modal has been closed.')
                            }}>
                            <View style={{marginTop: 0, backgroundColor: "rgba(0,0,0,0.7)", padding: 20, height: "100%", width: "100%"}}>
                                <View style={{backgroundColor: "#fff", height: "96%", width: "90%", margin: 20 }}>
                                    <View style={{position: "relative", height: 170, backgroundColor : "#05072D", padding: 20, paddingTop: 40, display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center"}}>
                                        <TouchableHighlight style={{position: "absolute", top: 5, right: 5}} onPress={() => setModalVisible(!modalVisible) }>
                                            <AntDesign name="closecircle" size={32} color="rgba(255,255,255, 0.5)"  />
                                        </TouchableHighlight>
                                        <View style={{width: "30%",  justifyContent: "center", alignItems: "center"}}>
                                            <Image source={{uri: prod.image}} style={{width: 50, height: 50}}/>
                                        </View>
                                        <View style={{width: "70%", padding: 10}}>
                                            <Text style={{fontSize: 14, fontWeight: "bold", color: "#fff"}}>{`${prod.trademark} ${prod.title}`}</Text>
                                            <Text style={{fontSize: 32, fontWeight: "bold", color: "#fff"}}>R$ {(quantity * prod.price).toFixed(2).replace(".",',')}</Text>
                                        </View>
                                    </View>
                                    <ScrollView>    
                                        <View style={{padding:20}}>  
                                            {cardToken === "" || cardToken === null && success === false ? 
                                            <View>
                                                <View>
                                                    <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 30}}>
                                                        Pague com seu Cartão de Crédito
                                                    </Text>
                                                </View>                                                
                                                <View>                              
                                                    <View style={{ marginBottom: 15}}>
                                                        <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Nome Impresso no Cartão</Text> 
                                                        <TextInput
                                                                style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4}}
                                                                onChangeText={text => setcardholderName(text)}
                                                                value={text}
                                                            />
                                                        <Text style={styles.errors}>{cardErrors && cardErrors.cardholderName}</Text>     
                                                    </View>
                                                    
                                                    <View style={{ marginBottom: 15}}>
                                                        <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Número do Cartão</Text> 
                                                        <TextInputMask
                                                            type={'credit-card'}
                                                            maxLength={19}
                                                            style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4}}
                                                            onChangeText={text => setcardNumber(text)}
                                                            value={cardNumber}
                                                        />
                                                        <Text style={styles.errors}>{cardErrors && cardErrors.cardNumber}</Text>     
                                                    </View>

                                                    <View style={{ marginBottom: 15}}>
                                                        <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Validade do Cartão</Text> 
                                                        <View style={{display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                                                            <TextInputMask
                                                                    type={'datetime'}
                                                                    options={{format: 'MM' }}
                                                                    maxLength={2}
                                                                    style={{height: 40, width: "45%", borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                    onChangeText={expirationDate_month => setexpirationDate_month(expirationDate_month)}
                                                                    value={expirationDate_month}
                                                                />
                                                            <TextInputMask
                                                                    type={'datetime'}
                                                                    options={{format: 'YY'}}
                                                                    maxLength = {2}
                                                                    style={{height: 40, width: "45%", borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                    onChangeText={expirationDate_year => setexpirationDate_year(expirationDate_year)}
                                                                    value={expirationDate_year}
                                                                />
                                                        </View>
                                                        <Text style={styles.errors}>{cardErrors && cardErrors.expirationDate}</Text>     
                                                    </View>

                                                    <View style={{ marginBottom: 15}}>
                                                        <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Código de Segurança</Text> 
                                                        <TextInputMask
                                                            maxLength = {4}
                                                            type={'only-numbers'}
                                                            style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                            onChangeText={(text) => setsecurityCode(text)}
                                                            value={securityCode} />

                                                        <Text style={styles.errors}>{cardErrors && cardErrors.securityCode}</Text>     
                                                    </View>
                                                    <View style={{marginTop:30}}>
                                                        <TouchableHighlight style={styles.buy} onPress={() => { console.log('carregando') }}>
                                                            <View style={styles.btn}>
                                                                <ActivityIndicator size="large" color="#fff" />
                                                            </View>
                                                        </TouchableHighlight>
                                                        <TouchableHighlight  onPress={() => { console.log('carregando') }}>
                                                            <View style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                                                <ActivityIndicator size="large" color="#fff" />
                                                            </View>
                                                        </TouchableHighlight>
                                                    </View>                                                    
                                                </View>
                                            </View>                                                    
                                                : success === true ?
                                                <View>
                                                    <View>
                                                        <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 10}}>
                                                            Compra Efetuada com Sucesso!
                                                        </Text>
                                                    </View>                                                
                                                    <View>
                                                        <Text style={{color: "#9B9B9B", fontSize: 16}}>
                                                            Digite * para acessar o menu de aplicativo, dige o código abaixo e # para confirmar! Na sua página de Tickets contém todo o histórico de compras.
                                                        </Text>                                                            
                                                        <Text style={styles.prod__link}>Código de Liberação</Text>
                                                        <View style={{backgroundColor:'#F5F5F5', borderRadius: 5, height: 86, display: 'flex', flexDirection: 'row', marginBottom: 10, alignItems: 'center', justifyContent: 'center'}}>
                                                            <Text style={{color: '#05072D', fontSize: 16, fontWeight: 'bold'}}>{voucher}</Text>
                                                        </View>
                                                        <TouchableHighlight style={styles.buy} onPress={() => { console.log('carregando') }}>
                                                            <View style={styles.btn}>
                                                                <ActivityIndicator size="large" color="#fff" />
                                                            </View>
                                                        </TouchableHighlight>
                                                    </View>
                                                </View>
                                                :
                                                <View>
                                                    <View>
                                                        <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 30}}>
                                                            Pague com seu Cartão de Crédito
                                                        </Text>
                                                    </View>                                                
                                                    <View style={styles.collapseViewBuy}>                
                                                        <View style={styles.brandCredit}>
                                                            <FontAwesome name="credit-card-alt" size={28} color="#666" style={{position: "relative"}}/>
                                                        </View>
                                                        <View style={styles.infoCredit}>
                                                            <Text style={styles.dataCredit}>{}</Text>
                                                            <Text style={styles.dataCreditNumber}>XXXX - XXXX - XXXX - XXX</Text>                                                                            
                                                        </View>
                                                    </View>
                                                    <TouchableHighlight style={styles.buy}  onPress={() => console.log('carregando')}>
                                                        <View style={styles.btn}>
                                                            <ActivityIndicator size="large" color="#fff" />
                                                        </View>
                                                    </TouchableHighlight>
                                                </View>
                                            }                                  
                                        </View>                                              
                                    </ScrollView>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </View>
            </ScrollView>
        </View>
        )
    }


    return (
            <View style={styles.mainContent}>
                <TouchableHighlight style={{position: "absolute", top: 25, left: 5, width: 40, height: 40, display: "flex", justifyContent: "center", alignItems: "center", zIndex: 99}} onPress={()=> props.navigation.navigate("Home")}><Ionicons name="ios-arrow-back" size={32} color="#00A2B4"  /></TouchableHighlight>
                <ScrollView style={styles.grid}>
                    <View style={styles.content}>
                        <View style={styles.headerProduct}>
                            <Image source={{uri: prod.image}} style={styles.prodImg}/>
                        </View>
                        <View style={styles.bodyProduct}>
                            <Text style={styles.prod__title__span}>{prod.trademark.toUpperCase()}</Text>
                            <Text style={styles.prod__title}>{prod.title.toUpperCase()}</Text>
                            <Text style={styles.prod__text}>{prod.description}</Text>

                            {prod.urlVideo ?
                                <TouchableHighlight onPress={()=> handleVideo(prod.urlVideo)}>
                                    <Text style={styles.prod__link}><FontAwesome name="chevron-circle-right" size={18} color="#00A2B4"  />{' VÍDEO DO PRODUTO'}</Text>
                                </TouchableHighlight>
                            : null}
                            <View style={styles.prod_qtd}>
                                <Text style={styles.prod_qtd__text}>Quantidade:</Text>
                                <Picker
                                    selectedValue={quantity}
                                    style={{height: 50, width: 140}}
                                    onValueChange={itemValue => setquantity(itemValue)}>
                                    <Picker.Item label="1 unidade" value={1} />
                                    <Picker.Item label="2 unidades" value={2} />
                                    <Picker.Item label="3 unidades" value={3} />
                                    <Picker.Item label="4 unidades" value={4} />
                                    <Picker.Item label="5 unidades" value={5} />
                                </Picker>
                            </View>
                            <View style={styles.prod_total}>
                                <Text style={styles.prod_total__text}>Total:</Text>
                                <Text style={styles.prod_total__price}>R$ {(quantity * prod.price).toFixed(2).replace(".",',')}</Text>
                            </View>
                            <TouchableHighlight style={styles.buy}  onPress={() => {
                                    setmodalVisible(true)
                                }}>
                                <View style={styles.prod_btn}>
                                    <Text style={styles.prod_btn__text}>{'COMPRAR'}</Text>
                                </View>
                            </TouchableHighlight>
                            <Modal
                                animationType="fade"
                                transparent={true}
                                visible={modalVisible}
                                onRequestClose={() => console.log('Modal has been closed.')}>
                                <View style={{marginTop: 0, backgroundColor: "rgba(0,0,0,0.7)", padding: 20, height: "100%", width: "100%"}}>
                                    <View style={{backgroundColor: "#fff", height: "96%", width: "90%", margin: 20 }}>
                                        <View style={{position: "relative", height: 170, backgroundColor : "#05072D", padding: 20, paddingTop: 40, display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center"}}>
                                            {success === true 
                                                ? null 
                                                : <TouchableHighlight style={{position: "absolute", top: 5, right: 5}} onPress={() => setmodalVisible(!modalVisible)}>
                                                <AntDesign name="closecircle" size={32} color="rgba(255,255,255, 0.5)"  />
                                            </TouchableHighlight>}
                                            <View style={{width: "30%",  justifyContent: "center", alignItems: "center"}}>
                                                <Image source={{uri: prod.image}} style={{width: 50, height: 50}}/>
                                            </View>
                                            <View style={{width: "70%", padding: 10}}>
                                                <Text style={{fontSize: 14, fontWeight: "bold", color: "#fff"}}>{`${prod.trademark} ${prod.title}`}</Text>
                                                <Text style={{fontSize: 32, fontWeight: "bold", color: "#fff"}}>R$ {(quantity * prod.price).toFixed(2).replace(".",',')}</Text>
                                            </View>
                                        </View>
                                        <ScrollView>    
                                            <View style={{padding:20}}>                                                
                                                {cardToken === "" || cardToken === null && success === false ? 
                                                <View>
                                                    <View>
                                                        <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 30}}>
                                                            Pague com seu Cartão de Crédito
                                                        </Text>
                                                    </View>                                                
                                                    <View>                              
                                                        <View style={{ marginBottom: 15}}>
                                                            <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Nome Impresso no Cartão</Text> 
                                                            <TextInput
                                                                style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4}}
                                                                onChangeText={(text) => setcardholderName(text)}
                                                                value={cardholderName}
                                                                />
                                                            <Text style={styles.errors}>{cardErrors && cardErrors.cardholderName}</Text>     
                                                        </View>

                                                        <View style={{ marginBottom: 15}}>
                                                            <View style={{display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                                                                <View style={{width: "45%"}}>
                                                                    <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>CPF do Titular</Text> 
                                                                    <TextInputMask
                                                                        type={'cpf'}
                                                                        maxLength = {14}
                                                                        style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                        onChangeText={text => setcpf(text)}
                                                                        value={cpf}
                                                                        ref={cpfField}
                                                                    />
                                                                    <Text style={styles.errors}>{cardErrors && cardErrors.cpf}</Text>
                                                                </View>
                                                                <View  style={{width: "45%"}}>
                                                                    <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Data de Nascimento</Text> 
                                                                    <TextInputMask
                                                                        type={'datetime'}
                                                                        maxLength = {10}
                                                                        style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                        onChangeText={(text) => setbirthday(text)}
                                                                        value={birthday}
                                                                        ref={birthdayField}
                                                                    />
                                                                    <Text style={styles.errors}>{cardErrors && cardErrors.birthday}</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                        
                                                        <View style={{ marginBottom: 15}}>
                                                            <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Número do Cartão</Text> 
                                                            <TextInputMask
                                                                type={'credit-card'}
                                                                maxLength={19}
                                                                style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4}}
                                                                onChangeText={text => setcardNumber(text)}
                                                                value={cardNumber}
                                                            />
                                                            <Text style={styles.errors}>{cardErrors && cardErrors.cardNumber}</Text>     
                                                        </View>

                                                        <View style={{marginBottom: 15}}>
                                                            <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Validade do Cartão</Text> 
                                                            <View style={{display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "center"}}>
                                                                <TextInputMask
                                                                        type={'datetime'}
                                                                        options={{
                                                                            format: 'MM'
                                                                        }}
                                                                        maxLength={2}
                                                                        style={{height: 40, width: "45%", borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                        onChangeText={expirationDate_month => setexpirationDate_month(expirationDate_month)}
                                                                        value={expirationDate_month}
                                                                    />
                                                                <TextInputMask
                                                                        type={'datetime'}
                                                                        options={{
                                                                            format: 'YY'
                                                                        }}
                                                                        maxLength={2}
                                                                        style={{height: 40, width: "45%", borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                        onChangeText={expirationDate_year => setexpirationDate_year(expirationDate_year)}
                                                                        value={expirationDate_year}
                                                                />
                                                            </View>
                                                            <Text style={styles.errors}>{cardErrors && cardErrors.expirationDate}</Text>     
                                                        </View>

                                                        <View style={{ marginBottom: 15}}>
                                                            <Text style={{color: "#9B9B9B", fontSize: 13, fontWeight: "bold", marginBottom: 6}}>Código de Segurança</Text> 
                                                            <TextInputMask
                                                                maxLength = {4}
                                                                type={'only-numbers'}
                                                                style={{height: 40, borderColor: '#CCC', borderWidth: 1, borderRadius:4, paddingLeft:5, paddingRight: 5}}
                                                                onChangeText={text => setsecurityCode(text)}
                                                                value={securityCode}
                                                                    />
                                                            <Text style={styles.errors}>{cardErrors && cardErrors.securityCode}</Text>     
                                                        </View>
                                                        <View style={{marginTop:30}}>
                                                            <TouchableHighlight style={styles.buy}  onPress={() => submitCreditCard()}>
                                                                <View style={styles.btn}>
                                                                    <Text style={styles.btn__text}>{'CONFIRMAR COMPRA'}</Text>
                                                                </View>
                                                            </TouchableHighlight>
                                                            <TouchableHighlight  onPress={() => setmodalVisible(!modalVisible)}>
                                                                <View style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                                                    <Text style={styles.prod__link}>Cancelar Compra</Text>
                                                                </View>
                                                            </TouchableHighlight>
                                                        </View>                                                    
                                                    </View>
                                                </View>                                                    
                                                : success === true ?
                                                    <View>
                                                        <View>
                                                            <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 10}}>
                                                                Compra Efetuada com Sucesso!
                                                            </Text>
                                                        </View>                                                
                                                        <View>
                                                            <Text style={{color: "#9B9B9B", fontSize: 16}}>
                                                                Confira abaixo seu código de liberação. Na sua página de Tickets contém todo o histórico de compras.
                                                            </Text>                                                            
                                                            <Text style={styles.prod__link}>Código de Liberação</Text>
                                                            <View style={{backgroundColor:'#F5F5F5', borderRadius: 5, height: 86, display: 'flex', flexDirection: 'row', marginBottom: 10, alignItems: 'center', justifyContent: 'center'}}>
                                                                <Text style={{color: '#05072D', fontSize: 16, fontWeight: 'bold'}}>{voucher}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                : <View>
                                                        <View>
                                                            <Text style={{fontSize: 22, fontWeight: "bold", color: "#05072D", marginBottom: 30}}>
                                                                Pague com seu Cartão de Crédito
                                                            </Text>
                                                        </View>                                                
                                                        <View style={styles.collapseViewBuy}>                
                                                            <View style={styles.brandCredit}>
                                                                <FontAwesome name="credit-card-alt" size={28} color="#666" style={{position: "relative"}}/>
                                                            </View>
                                                            <View style={styles.infoCredit}>
                                                                <Text style={styles.dataCredit}>{}</Text>
                                                                <Text style={styles.dataCreditNumber}>XXXX - XXXX - XXXX - {latestDigitCard ? latestDigitCard :'XXXX'}</Text>                                                                            
                                                            </View>

                                                        </View>
                                                        {prod.type === 2 ? 
                                                            <View style={styles.collapseViewBuy}>                
                                                                <View>
                                                                    <Text style={styles.dataCreditNumber}>Nos entraremos em contato com você para fazer a entrega</Text> 
                                                                </View>
                                                            </View>
                                                        : null}

                                                        <TouchableHighlight style={styles.buy}  onPress={() => submitTransaction()}>
                                                            <View style={styles.btn}>
                                                                <Text style={styles.btn__text}>{'CONFIRMAR COMPRA'}</Text>
                                                            </View>
                                                        </TouchableHighlight>
                                                    </View>
                                                }                                  
                                            </View>                                              
                                        </ScrollView>
                                    </View>
                                </View>
                            </Modal>
                        </View>
                    </View>
                </ScrollView>
            </View>
    )
}
