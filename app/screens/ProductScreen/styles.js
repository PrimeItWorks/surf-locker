import { StyleSheet } from "react-native";

export default StyleSheet.create({
    mainContent: {
        backgroundColor: "#E5E5E5",
        display: "flex",
        alignItems: 'center',
        justifyContent: "center",
    },
    grid:{        
        width: "100%",
        height:"100%",
    },
    content: {
        display: "flex",
        alignItems: 'center',
        justifyContent: "center",
    },
    headerProduct :{
        display: "flex",
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: "center",
        width: "100%",
        height: 340,
        backgroundColor: "#05072D",
        position: "absolute",
        top: 0
    },
    bodyProduct: {
        display: "flex",
        marginTop: 300,         
        marginBottom: 20,
        backgroundColor: "#FFF",
        width: "85%",
        minHeight: 300,
        borderRadius: 4,
        padding: 20
    },
    prodImg:{
        width: 190,
        height: 190
    },
    prod__title: {
        color: "#000",
        fontSize: 22,
        fontWeight: "bold",
        marginBottom: 12
    },
    prod__text: {
        color: "#9B9B9B",
        fontSize: 16,
    },
    prod__link: {
        color: "#00A2B4",
        fontSize: 14,
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 20
    },
    prod__title__span: {
        color: "#000",
        fontSize: 14,
        fontWeight: "bold"
    },
    prod_qtd:{
        borderWidth: 3,
        borderColor: '#d6d7da',
        borderStyle: 'dotted',
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 0.1,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    prod_qtd__text:{
        fontSize: 16,
        color: "#9B9B9B",
        fontWeight: "bold"
    },
    prod_total:{
        paddingLeft: 10,
        paddingRight: 10,
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 20,
        paddingBottom: 20,
    },
    prod_total__text:{
        fontSize: 16,
        color: "#9B9B9B",
        fontWeight: "bold"
    },
    prod_total__price:{
        fontSize: 31,
        color: "#000",
        fontWeight: "bold"
    },
    buy: {
        flex: 1,
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },  
    prod_btn: {
        width: 260,
        height: 66,
        borderRadius: 8,
        backgroundColor: "#00A2B4",
        justifyContent: "center",
        alignItems: "center"
    },
    prod_btn__text: {
        color: "#FFF",
        fontWeight: "bold",
        fontSize: 16
    },
    btn: {
        width: 260,
        height: 56,
        borderRadius: 8,
        backgroundColor: "#00A2B4",
        justifyContent: "center",
        alignItems: "center"
    },
    btn__text: {
        color: "#FFF",
        fontWeight: "bold",
        fontSize: 16
    },
    collapseViewBuy: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20, 
        display: "flex",
        flex: 2,
        flexDirection: "row"
    },
    iconView: {
        borderRadius : 10,
        padding: 20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor:'#ffffff',
        width: "100%"
    },
    iconViewTitle: {
        fontSize : 16,
        fontWeight: "bold"
    },
    dataUserLabel: {
        fontSize: 13,
        color: "#9B9B9B",
        fontWeight: "bold",
        marginBottom: 10
    },
    dataUserField: {
        fontSize: 16,
        color: "#000",
        fontWeight: "bold",
        borderWidth: 1,
        borderColor: "#00A2B4",
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 30,
        borderRadius: 6
    },
    brandCredit:{
        width: "30%",
        paddingLeft: 10,
        paddingTop: 10
    },
    infoCredit:{
        width: "70%",
        paddingLeft: 20
    },
    dataCredit: {
        color: "#000",
        fontSize: 16,
        fontWeight: "bold"
    },
    dataCreditNumber: {
        color: "#000",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 4,
    },
    dataCreditLink: {
        color: "#00A2B4",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 10,
        marginBottom: 0,
        fontWeight: "bold"
    },
    errors: {
        fontSize: 12,
        paddingLeft: 4
    }
})