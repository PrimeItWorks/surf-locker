import React, { useState, useEffect } from 'react'
import { View } from 'react-native'

import Service from './service'
import styles from './styles'
import { useDispatch, useSelector } from 'react-redux'
import { TicketsTypes } from '../../store/ducks/tickets'

const service = new Service()

TicketScreen.navigationOptions = {
	header: null
};

export default function TicketScreen() {
	const dispatch = useDispatch()
	const tickets = useSelector(state => state.tickets.tickets)

	const [isLoaded, setisLoaded] = useState(false)
	const [isRefreshing, setisRefreshing] = useState(false)
	const [page, setpage] = useState(false)
	
	useEffect(() => {
		getTickets()
	}, [])

	useEffect(() => {
		getTickets()
	}, [page])

	function getTickets() {
		const ticks = service.getTickets()
		
		dispatch({ type: TicketsTypes.TICKETS, tickets: ticks })
		dispatch({ type: TicketsTypes.SET_ZERO_TICKETS_BADGE })
		setisLoaded(true)
		setisRefreshing(false)
	}

	getMore = () => {
		setisLoaded(false)
		const tickets = service.getTickets(page +1)
		if(tickets.hasNextPage) {
			setpage(page +1)
		}
    }
	
	refresh = () => {
		setisRefreshing(true)
		setpage(1)
    }

	if(isLoaded){
		return(
			<View style={styles.screen}>
				<View style={styles.header}>
					<Text style={styles.header__title}>Meus Tickets</Text>
				</View>
				<View style={styles.mainContent}>                    
					<ActivityIndicator size="large" color="#0000ff" />
				</View>
			</View>
		)
	}

	if(tickets.items.length === 0) {
		return(
			<View style={styles.screen}>
				<View style={styles.header}>
					<Text style={styles.header__title}>Meus Tickets</Text>
				</View>
				<ScrollView style={styles.screen}>
					<View style={styles.mainContent}>
						<Text>Nenhum ticket encontrado.</Text>
					</View>  
				</ScrollView>                 
			</View>         
		)
	}

	return (
		<View style={styles.screen}>
			<View style={styles.header}>
				<Text style={styles.header__title}>Meus Tickets</Text>
			</View>
			<ScrollView style={styles.screen}>
				<View style={styles.mainContent}>    
						<FlatList
							data={tickets.items}
							renderItem={({ item, index }) => <TicketAccordion collapse={false}  item={tickets.items}/>}                                            
							keyExtractor={(item, index) => index.toString()}
							numColumns={1}
							refreshing={isRefreshing}
							onRefresh={refresh}
							onEndReached={getMore}
							onEndThreshold={0}
							style={{width: '100%'}}
						/>
					</View>                       
			</ScrollView>                 
		</View>
	)
}
