import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'

export default class {
    constructor() {
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async getTickets(page){
        let user  = await this.storage.getUseId()           
        let resp   = await this.apiClient.getApi(`/Vouchers?UserCode=${user.code}&Page=${page}&PageSize=12&OrderBy=BuyDate&Desc=true`)        
        return resp
    }

    async clearStorage(page){
        this.storage.clear()
    }
}
