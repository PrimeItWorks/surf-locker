'use strict'

import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'

export default class {
    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async getAllMachine(page = 1){
        let resp = await this.apiClient.getApi(`/machines?Page=${page}&PageSize=999`, true)
        return resp
    }

    async clearStorage(){
        this.storage.clear()
    }

    calculateDistance(lat2, lon2) {
        if (this.state.coords) {
            var { latitude, longitude } = this.state.coords;
            if (latitude && longitude) {
                var lat = [latitude, lat2]
                var lng = [longitude, lon2]
                var R = 6378137;
                var dLat = (lat[1] - lat[0]) * Math.PI / 180;
                var dLng = (lng[1] - lng[0]) * Math.PI / 180;
                var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                    Math.cos(lat[0] * Math.PI / 180) * Math.cos(lat[1] * Math.PI / 180) *
                    Math.sin(dLng / 2) * Math.sin(dLng / 2);
                var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                var d = R * c;
                return Math.round(d / 1000)
            } else {
                return 0;
            }
        }
    }
}