import React, {useState, useEffect} from "react"
import { View, Text, ScrollView, TouchableOpacity, FlatList, ActivityIndicator } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import {MachineTypes} from '../../store/ducks/machines'

import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'

import { FontAwesome } from '@expo/vector-icons'

import styles from './styles'
import Service from './service'

const service = new Service()

MachineScreen.navigationOptions = {
	header: null
};

export default function MachineScreen() {
    const dispatch = useDispatch()
    const machines = useSelector(state => state.machines.machines)
    useEffect(async () => {
        await askLocation()
        await getMachines()
    }, [])
	
	const [isRefreshing, setisRefreshing] = useState(true)
	const [isLoaded, setisLoaded] = useState(false)
	const [page] = useState(false)
	const [locationResult, setlocationResult] = useState(false)
    const [coords, setcoords] = useState(false)
    
    async function askLocation() {
        try {
            const { status } = await Permissions.askAsync(Permissions.LOCATION)
            if (status !== 'granted') {
                setlocationResult('Sem permissão para acessar localização atual')
                return
            }

            const enabled = await Location.hasServicesEnabledAsync()
            if (!enabled) {
                setlocationResult('Ative a sua localização')
                return
            }

            Location.watchPositionAsync({ enableHighAccuracy: true, timeout: 1000 }, (location) => {
                const { coords } = location
                setcoords(coords)
                return location
            })
            .catch(err => console.log('err::::', err))
        } catch (error) {
            console.log('err::::', err)
        }
    }

    async function getMachines() {
        let resp = await service.getAllMachine(page)
        if (resp.status && resp.status !== 200) {
            resp.statusCode = resp.status
        }
        if (resp.statusCode !== 200 && resp.statusCode === 401 || resp.statusCode == 403) {
            service.clearStorage()
            props.navigation.navigate('Login')

            return null
        }

        const items = resp.response.map(i => {
            i.distance = service.calculateDistance(Number(i.lat), Number(i.long))
            return i
        })

        const withDistance = items.map(i => {
            i.distance = service.calculateDistance(Number(i.lat), Number(i.long))

            return i
        })
        
        const sorted = withDistance.sort((a, b) => a.distance > b.distance ? 1 : -1)

        const machines = {
            items: sorted,
            hasNextPage: resp.response.hasNextPage,
            count: resp.response.count

        }
        
        dispatch({type: MachineTypes, machines})
        setisLoaded(true)
    }

    function getMore() {
	}

    async function refresh() {
		setisRefreshing(true)
		await getMachines()
    }

    async function openMap(lat2, long2) {
        const locationToGo = { latitude: Number(lat2), longitude: Number(long2) }
        const placeToGo = await Location.reverseGeocodeAsync(locationToGo)

        const { latitude, longitude } = coords
        const locationBegin = { latitude: Number(latitude), longitude: Number(longitude) }
        const placeBegin = await Location.reverseGeocodeAsync(locationBegin)

        const options = { start: placeBegin[0].postalCode, end: placeToGo[0].postalCode }
        openMap(options)
    }

    if (!isLoaded) {
        return (
            <View style={styles.screen}>
                <View style={styles.header}>
                    <Text style={styles.header__title}>Nossas Máquinas</Text>
                </View>
                <View style={styles.mainContent}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    }

    if (locationResult) {
        return (
            <View style={styles.mainContent}>
                <Text>{locationResult}</Text>
            </View>
        )
    }

    if (machines.items.length === 0) {
        return (
            <View style={styles.screen}>
                <View style={styles.header}>
                    <Text style={styles.header__title}>Nossas Máquinas</Text>
                </View>
                <ScrollView style={styles.grid}>
                    <View style={styles.headerMachines}>
                        <Ionicons name="map" size={30} color="#82D1E2" />
                    </View>
                    <View style={styles.bodyMachines}>
                        <View style={styles.bgMachines}></View>
                        <View style={styles.gridMachines}>
                            <View style={styles.viewEmpty}>
                                <Text>Nenhuma máquina encontrada...</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }

    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <Text style={styles.header__title}>Nossas Máquinas</Text>
            </View>

            <ScrollView style={styles.grid}>
                <View style={styles.bodyMachines}>
                    <View style={styles.bgMachines}></View>

                    <FlatList
                        data={machines.items}
                        renderItem={({ item }) =>
                            <TouchableOpacity style={styles.gridMachines__othersProd} onPress={async () => await openMap(item.lat, item.long)}>
                                <View style={styles.gridMachines__othersProd_imgCombo}>
                                    <FontAwesome name="map-marker" size={70} color="#82D1E2" />
                                    <Text style={styles.prodTitle}>{service.calculateDistance(Number(item.lat), Number(item.long))}km</Text>
                                </View>
                                <View style={styles.gridMachines__othersProd_info}>
                                    <Text style={styles.prodCategory}>{item.title}</Text>
                                    <Text style={styles.prodTitle}>{item.place}</Text>
                                    <Text style={styles.prodQdt}>{item.prodName1}</Text>
                                    <Text style={styles.prodQdt}>{item.prodName2}</Text>
                                    <Text style={styles.prodQdt}>{item.prodName3}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => index.toString()}
                        numColumns={1}
                        refreshing={isRefreshing}
                        onRefresh={refresh}
                        onEndReached={getMore}
                        onEndThreshold={0}
                        style={{ width: '100%', marginTop: 20 }}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
