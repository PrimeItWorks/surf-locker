import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: "#E5E5E5"
    },
    header: {
        height: 70,
        backgroundColor: "#05072D",
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        flexDirection: "row",
        paddingTop: 16
    },
    header__title: {
        color: "#FFF",
        fontSize: 21,
        fontWeight: "bold"
    },
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "flex-start",
        justifyContent: "center",
        width: "100%",
    },
    imgLogon: {
        width: 106,
        height: 150,
        position: "relative",
        top: -5
    },
    prodImg: {
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: -20,
        left: 10,
        borderRadius: 6,
    },
    imgBackground: {
        position: "absolute",
        top: 0,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    headerMachines: {
        width: "100%",
        height: 280,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden"
    },
    bodyMachines: {
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        top: -20,
        zIndex: 99
    },
    bgMachines: {
        backgroundColor: "#E5E5E5",
        width: "100%",
        position: "absolute",
        top: 0,

    },
    grid: {
        backgroundColor: "#E5E5E5",
        display: "flex",
        width: "100%",
        height: Dimensions.get("screen").height,
        position: "relative",
    },
    gridMachines: {
        flexDirection: "row",
    },
    gridMachines__mainProd: {
        display: "flex",
        flex: 2,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 12,
        paddingTop: 70,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        marginTop: 0,
        marginLeft: 15,
        marginRight: 15,
        minHeight: 210,
        maxWidth: 155
    },
    gridMachines__othersProd: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 16,
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        marginTop: 20,
        marginRight: 20
    },
    gridMachines__othersProdCombo: {
        display: "flex",
        minHeight: 210,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-start",
        backgroundColor: "#fff",
        borderRadius: 10,
        margin: 16,
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10,
        marginTop: 20,
        marginLeft: 40,
        marginRight: 20
    },
    gridMachines__othersProd_img: {
        position: "relative",
        left: -30,
        width: "30%"
    },
    gridMachines__othersProd_imgCombo: {
        position: "relative",
        width: "40%",
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15
    },
    prodImg__othersProd: {
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: 0,
        left: -10,
        borderRadius: 6
    },
    gridMachines__othersProd_info: {
        position: "relative",
        width: "60%",
    },
    prodImg__othersProdTwo: {
        width: 77,
        height: 77,
        marginBottom: 20,
        position: "absolute",
        top: 92,
        left: -10,
        borderRadius: 6
    },
    prodCategory: {
        color: "black",
        fontSize: 12,
        fontWeight: "bold",
    },
    prodTitle: {
        color: "#64B9CC",
        fontSize: 18,
        fontWeight: "bold",
    },
    prodTitleHighlight: {
        color: "#F6921E",
    },
    prodQdt: {
        color: "#9B9B9B",
        fontSize: 12,
        marginTop: 5,
        marginBottom: 5
    },
    prodPrice: {
        color: "black",
        fontSize: 20,
        fontWeight: "bold",
    },
    viewEmpty: {
        display: 'flex',
        flex: 1,
        height: 150,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }

})