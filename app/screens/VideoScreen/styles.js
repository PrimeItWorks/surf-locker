import {StyleSheet} from "react-native";

export default StyleSheet.create({
    screen: { flex: 1 },
    web_view: {marginTop: 20}
})