import React from 'react'
import { View } from 'react-native'
import styles from './styles'

export default function VideoScreen() {
    const url = this.props.navigation.state.params.url

    return (
        <View style={styles.screen}>
            <WebView source={{uri: url}} style={styles.web_view} />
        </View>
    )
}
