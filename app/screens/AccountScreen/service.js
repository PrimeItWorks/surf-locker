'use strict'

import ApiClient from '../../services/apiClient'
import Storage from '../../helpers/Storage'

export default class {

    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async getAccount(){
        let user  = await this.storage.getUseId()                   
        let resp  = await this.apiClient.getApi(`/accounts/${user.code}`, true)
        return resp
    }

    async putAccount(data){
        let user = await this.apiClient.putApi('/Accounts', data, true)
        return user
    }

    async postCreditCard(data, Authorization = false){               
        if(Authorization){
            this.authToken = this.storage.getData().value        
        }

        return fetch("https://gateway.api.4all.com/prepareCard",
        // return fetch("https://gateway.homolog-interna.4all.com/prepareCard",
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            return response.json()
        })
        .then(responseJson => { 
            return responseJson            
        })
        .catch(err => err)
    }

    async GetAcessToken(){
        let resp   = await this.apiClient.postApi('/Payments/RequestVaultKey', true)        
        return resp
    }

    async PublicApiToken(){
        let resp   = await this.apiClient.getApi('/Payments/publicApiToken', true)        
        return resp
    }

    async CreateCreditCard(data){
        let resp = await this.apiClient.postApi('/Payments/CreateCardToken', data, true)        
        return resp
    }
}