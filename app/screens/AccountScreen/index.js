import React, { useState } from 'react';
import { View } from 'react-native';
import Toast from 'react-native-root-toast';

import Service from './service'
import styles from "./styles";
import { useDispatch } from 'react-redux';
import { Types } from '../../store/ducks/accounts';

const service = new Service()

AccountScreen.navigationOptions = {
	header: null
};

export default function AccountScreen(props) {
    const dispatch = useDispatch()

    const [isLoading, setisLoading] = useState(true)
    const [modalVisible, setmodalVisible] = useState(false)
    const [code, setcode] = useState('')
    const [facebookId, setfacebookId] = useState('')
    const [name, setname] = useState('')
    const [email, setemail] = useState('')
    const [address, setaddress] = useState('')
    const [cpf, setcpf] = useState('')
    const [birthday, setbirthday] = useState('')
    const [phone, setphone] = useState('')
    const [waves, setwaves] = useState([])
    const [cardToken, setcardToken] = useState('')
    const [cardholderName, setcardholderName] = useState('')
    const [cardNumber, setcardNumber] = useState('')
    const [expirationDate_month, setexpirationDate_month] = useState('')
    const [expirationDate_year, setexpirationDate_year] = useState('')
    const [securityCode, setsecurityCode] = useState('')
    const [titleModal, settitleModal] = useState('Modifique')
    const [errors, seterrors] = useState({})
    const [cardErrors, setcardErrors] = useState({})
    const phoneField = useRef(null)
    const cpfField = useRef(null)
    const birthdayField = useRef(null)

    useEffect(async () => {
        await getAccount()

        return () => { }
    }, [])
    
    async function getAccount() {
        setisLoading(true)
        const data = await service.getAccount();    
        if (!handleMsgErrorApi(data)) {
            setisLoading(false);
            return null
        }

        dispatch({type: Types.ACCOUNT, account: data.response})
        setcode(data.response.code)
        setfacebookId(data.response.facebookId)
        setname(data.response.name)
        setemail(data.response.email)
        setcpf(data.response.cpf)
        setbirthday(data.response.birthday)
        setaddress(data.response.address)
        setphone(data.response.phone)
        setwaves(data.response.waves)
        setcardToken(data.response.cardToken)
        setisLoading(false)        
    }

    async function submitAccount() {
        let err = validateAccount();
        if (Object.keys(errors).length !== 0) {
            seterrors(err)
            setTimeout(() => {
                seterrors({})
            }, 5000);

            Toast.show('Informe os dados obrigatórios', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true
            });

            return null
        }

        let user = {
            code: code,
            facebookId: facebookId,
            name: name,
            email: email,
            address: address,
            phone: phone,
            waves: waves,
            cpf: cpf,
            birthday: birthday,
            cardToken: cardToken
        };

        const resp = await service.putAccount(user);
        handleMsgErrorApi(resp);
        getAccount();

        Toast.show('Dados Atualizados com sucesso!', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true
        });
    }

    function validateAccount() {
        let err = {};
        if (address === '') 
            err.address = "Endereço obrigatório"

        if (phone === '') 
            err.phone = "Telefone obrigatório";

        if (!/^\d{11}$/i.test(phoneField.getRawValue())) 
            err.phone = "Telefone deve conter somente números";

        if (waves.length === 0) 
            err.waves = "Onde você pega onda obrigatório"

        return err;
    }

    function handleMsgErrorApi(data) {
        if (data.hasOwnProperty('status') && data.status !== 200) {
            data.statusCode = data.status;
        }

        if (data.hasOwnProperty('statusCode') && data.statusCode !== 200 && data.statusCode === 401 || data.statusCode === 403) {
            props.navigation.navigate('Login');
            return null
        }

        if (data.hasOwnProperty('statusCode') && data.statusCode !== 200) {
            Toast.show('Ops! Ocorreu algum erro de carregamento', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});
            return null
        }

        if (data.response === null || !data.hasOwnProperty('response')) {
            Toast.show('Ops! Ocorreu algum erro de carregamento', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});
            return null;
        }

        return true;

    }

    function validateCreditCard() {
        let err = {};

        if (cardholderName === '') 
            err.cardholderName = "Nome do titular do cartão é obrigatório"
        
        if (birthday === '') 
            err.birthday = "Data de nascimento do titular do cartão é obrigatório"

        if (cpf === '') 
            err.cpf = "CPF do titular do cartão é obrigatório"

        if (birthday === '') 
            err.cpf = "CPF do titular do cartão é obrigatório"

        if (!/^\d{11}$/i.test(cpfField.getRawValue())) 
            err.cpf = "CPF do titular deve conter somente números"

        if (cardNumber === '') 
            err.cardNumber = "Número do cartão é obrigatório"

        if (expirationDate_month === '' || expirationDate_year === '') 
            err.expirationDate = "Data e validade obrigatória"

        if (expirationDate_month !== '' && expirationDate_month > 12) 
            err.expirationDate = "Informe um mês válido"

        let today = new Date();
        let currentMonth = today.getMonth() + 1;
        let currentYear = today.getFullYear().toString().substr(-2);
        if (currentYear == expirationDate_year && currentMonth > expirationDate_month) {
            err.expirationDate = "Informe um a data de validate válida"
        }

        if (securityCode === '') 
            err.securityCode = "Codigo de segurança obrigatória"

        return err;
    }

    async function submitCreditCard() {
        let err = validateCreditCard();
        if (Object.keys(err).length !== 0) {
            setcardErrors(err)
            setTimeout(() => {
                setcardErrors({})
            }, 5000);

            Toast.show('Informe os dados obrigatórios', {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});

            return null
        }

        let accessKey = await service.GetAcessToken();

        if (handleMsgErrorApi(accessKey) === null) {
            return null
        }

        const cardData = {
            "accessKey": accessKey.response.accessKey,
            "cardData": {
                "type": 1,
                "cardholderName": cardholderName,
                "cardNumber": cardNumber.replace(/ /g, ''),
                "expirationDate": expirationDate_month + expirationDate_year,
                "securityCode": securityCode
            }
        }

        const resp = await service.postCreditCard(cardData);
        if (resp.error) {
            Toast.show(resp.error.message, {
                duration: Toast.durations.LONG,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true});

            return null;
        }

        let respPUblicKey = await service.PublicApiToken();
        if (handleMsgErrorApi(respPUblicKey) === null) {
            return null
        }
        const splitedDate = birthday.split('/')
        const createMyCreditCard = {
            "userCode": code,
            "birthday": `${splitedDate[2]}-${splitedDate[1]}-${splitedDate[0]}`,
            "cpf": cpf,
            "gatewayRequest": {
                "merchantKey": respPUblicKey.response.merchantKey,
                "cardNonce": resp.cardNonce,
                "waitForToken": true
            }
        }

        const card = await service.CreateCreditCard(createMyCreditCard)
        if (handleMsgErrorApi(card) === null) {
            return null
        }

        Toast.show('Dados inseridos com sucesso!', {
            duration: Toast.durations.LONG,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true
        });
    }

    function setModalVisibility(visible, type) {
        if (type === 'add') {
            settitleModal('Adicione')
        }
        setmodalVisible(visible)
    }
    
    function renderHeaderAccount(collapse) {
        return (
            <View style={styles.iconView}>
                <Text style={styles.iconViewTitle}>MEUS DADOS</Text>
                <View>
                    {collapse 
                            ? <Entypo name="chevron-small-up" size={14} color="#000" /> 
                            : <Entypo name="chevron-small-down" size={14} color="#000" />}
                </View>
            </View>
        )
    }

    function renderBodyAccount() {
        return (
            <View style={styles.collapseView}>
                <View>
                    <Text style={styles.dataUserLabel}>Nome completo</Text>
                    <Text style={styles.dataUserField}>{name}</Text>
                </View>
                <View>
                    <Text style={styles.dataUserLabel}>E-mail</Text>
                    <Text style={styles.dataUserField}>{email}</Text>
                </View>
                <View>
                    <Text style={styles.dataUserLabel}>Telefone</Text>
                    <TextInputMask 
                        onChangeText={(phone) => setphone(phone)}
                        ref={phoneField}
                        options={{ maskType: 'BRL', withDDD: true, dddMask: '(99) ' }}
                        style={styles.dataUserField}
                        value={phone}
                        type={'cel-phone'} 
                    />
                    <Text style={styles.errors}>{errors && errors.phone}</Text>
                </View>
                <View>
                    <Text style={styles.dataUserLabel}>Endereço</Text>
                    <TextInput style={styles.dataUserField} value={address} onChangeText={address => setaddress(address)} />
                    <Text style={styles.errors}>{errors && errors.address}</Text>
                </View>
                <View>
                    <Text style={styles.dataUserLabel}>Onde você pega onda?</Text>
                    <TextInput 
                        style={styles.dataUserField} 
                        value={waves[0]} 
                        onChangeText={(waves) => {
                            let waves_tmp = [];
                            waves_tmp.push(waves);
                            setwaves(waves_tmp)
                        }} />
                    <Text style={styles.errors}>{errors && errors.waves}</Text>
                </View>
                <View>
                    <TouchableHighlight 
                        style={styles.buy} 
                        onPress={() => { submitAccount() }}>
                        <View style={styles.btn}>
                            <Text style={styles.btn__text}>{'SALVAR'}</Text>
                        </View>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }

    function renderHeaderCard(collapse) {
        return (
            <View style={styles.iconView}>
                <Text style={styles.iconViewTitle}>MEUS CARTÕES</Text>
                <View>
                    {collapse 
                        ? <Entypo name="chevron-small-up" size={14} color="#000" /> 
                        : <Entypo name="chevron-small-down" size={14} color="#000" />
                    }
                </View>
            </View>
        )
    }

    function renderBodyCard () {
        if (cardToken !== '') {
            return (
                <View style={styles.collapseViewC}>
                    <View style={styles.brandCredit}>
                        <FontAwesome name="credit-card-alt" size={28} color="#666" style={{ position: "relative" }} />
                    </View>
                    <View style={styles.infoCredit}>
                        <Text style={styles.dataCredit}>{}</Text>
                        <Text style={styles.dataCreditNumber}>XXXX - XXXX - XXXX - </Text>
                        <TouchableHighlight onPress={() => { setModalVisibility(true, 'edit') }}>
                            <Text style={styles.dataCreditLink}>modificar</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            )
        }

        return (
                <View style={styles.collapseView}>
                    <View>
                        <TouchableHighlight style={styles.buy} onPress={() => { setModalVisibility(true, 'add'); }}>
                            <View style={styles.btn}>
                                <Text style={styles.btn__text}>{'ADICIONAR'}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                </View>
        )
    }

    if (isLoading) {
        return (
            <View style={styles.screen}>
                <View style={styles.header}>
                    <Text style={styles.header__title}>Meus Dados</Text>
                </View>
                <View style={styles.mainContent}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            </View>
        )
    }
    
    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <Text style={styles.header__title}>Meus Dados</Text>
            </View>
            <ScrollView style={styles.screen}>
                <View style={styles.mainContent}>
                    <View style={styles.userData}>
                        <Collapse>
                            <CollapseHeader>
                                {renderHeaderAccount()}
                            </CollapseHeader>
                            <CollapseBody>
                                {renderBodyAccount()}
                            </CollapseBody>
                        </Collapse>
                    </View>
                    <View style={styles.userData}>
                        <Collapse>
                            <CollapseHeader>
                                {renderHeaderCard()}
                            </CollapseHeader>
                            <CollapseBody>
                                {renderBodyCard()}
                            </CollapseBody>
                        </Collapse>
                    </View>
                </View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        console.log('closed')
                    }}>
                    <View style={styles.modalBody}>
                        <View style={styles.modalBodyContainer}>
                            <ScrollView>
                                <View style={styles.modalBodyContainerScrollable}>
                                    <View>
                                        <Text style={modalTitle}>{titleModal} seu Cartão de Crédito</Text>
                                    </View>
                                    <View>
                                        <View style={styles.fieldGroup}>
                                            <Text style={styles.fieldLabel}>Nome Impresso no Cartão</Text>
                                            <TextInput
                                                style={styles.fieldText}
                                                onChangeText={(cardholderName) => setcardholderName(cardholderName)}
                                                value={cardholderName}
                                            />
                                            <Text style={styles.errorsCard}>{cardErrors && cardErrors.cardholderName}</Text>
                                        </View>

                                        <View style={styles.fieldGroup}>
                                            <View style={styles.fieldCardDate}>
                                                <View style={styles.fieldGroupTwoColumns}>
                                                    <Text style={styles.fieldLabel}>CPF do Titular</Text>
                                                    <TextInputMask
                                                        type={'cpf'}
                                                        maxLength={14}
                                                        style={styles.fieldText}
                                                        onChangeText={(text) => setcpf(text)}
                                                        value={cpf}
                                                        ref={cpfField}
                                                    />
                                                    <Text style={styles.errors}>{cardErrors && cardErrors.cpf}</Text>
                                                </View>
                                                <View style={styles.fieldGroupTwoColumns}>
                                                    <Text style={styles.fieldLabel}>Data de Nascimento</Text>
                                                    {birthday 
                                                        ? <TextInputMask
                                                            type={'datetime'}
                                                            maxLength={10}
                                                            style={styles.fieldText}
                                                            onChangeText={text => setbirthday(text)}
                                                            value={new Date(birthday).toLocaleString("pt-BR")}
                                                            ref={birthdayField}
                                                        />
                                                        : <TextInputMask
                                                            type={'datetime'}
                                                            maxLength={10}
                                                            style={styles.fieldText}
                                                            onChangeText={text => setbirthday(text)}
                                                            value={birthday}
                                                            ref={birthdayField}
                                                        />}
                                                    
                                                    <Text style={styles.errors}>{cardErrors && cardErrors.birthday}</Text>
                                                </View>
                                            </View>
                                        </View>

                                        <View style={styles.fieldGroup}>
                                            <Text style={styles.fieldLabel}>Número do Cartão</Text>
                                            <TextInputMask
                                                type={'credit-card'}
                                                maxLength={19}
                                                style={styles.fieldText}
                                                onChangeText={(cardNumber) => setcardNumber(cardNumber)}
                                                value={cardNumber}
                                            />
                                            <Text style={styles.errorsCard}>{cardErrors && cardErrors.cardNumber}</Text>
                                        </View>

                                        <View style={styles.fieldGroup}>
                                            <Text style={styles.fieldLabel}>Validade do Cartão</Text>
                                            <View style={styles.fieldCardDate}>
                                                <TextInputMask
                                                    type={'datetime'}
                                                    options={{format: 'MM'}}
                                                    maxLength={2}
                                                    style={styles.fieldCardDateColumn}
                                                    onChangeText={(expirationDate_month) => setexpirationDate_month(expirationDate_month)}
                                                    value={expirationDate_month}
                                                />
                                                <TextInputMask
                                                    type={'datetime'}
                                                    options={{
                                                        format: 'YY'
                                                    }}
                                                    maxLength={2}
                                                    style={styles.fieldCardDateColumn}
                                                    onChangeText={(expirationDate_year) => setexpirationDate_year(expirationDate_year)}
                                                    value={expirationDate_year}
                                                />
                                            </View>
                                            <Text style={styles.errorsCard}>{cardErrors && cardErrors.expirationDate}</Text>
                                        </View>
                                        
                                        <View style={styles.fieldGroup}>
                                            <Text style={styles.fieldLabel}>Código de Segurança</Text>
                                            <TextInputMask
                                                type={'only-numbers'}
                                                maxLength={3}
                                                style={styles.fieldText}
                                                onChangeText={(securityCode) => setsecurityCode(securityCode)}
                                                value={securityCode}
                                            />
                                            <Text style={styles.errorsCard}>{cardErrors && cardErrors.securityCode}</Text>
                                        </View>
                                        
                                        <View style={{ marginTop: 30 }}>
                                            <TouchableHighlight style={styles.buy} onPress={() => submitCreditCard()}>
                                                <View style={styles.btn}>
                                                    <Text style={styles.btn__text}>{'SALVAR'}</Text>
                                                </View>
                                            </TouchableHighlight>
                                            <TouchableHighlight onPress={() =>  setModalVisibility(!modalVisible)}>
                                                <View style={styles.cancelBtn}>
                                                    <Text style={styles.prod__link}>Cancelar</Text>
                                                </View>
                                            </TouchableHighlight>
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        </View>
    )
}
