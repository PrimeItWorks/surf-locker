import { StyleSheet} from "react-native";

export default StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: "#E5E5E5"
    },
    header: {
        height: 70,
        backgroundColor: "#05072D",
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
        flexDirection: "row",
        paddingTop: 16
    },
    header__title: {
        color: "#FFF",
        fontSize: 21,
        fontWeight: "bold"
    },
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "flex-start",
        backgroundColor: "#E5E5E5",
        padding: 20,
    },
    userData: {
        borderRadius: 10,
        width: "100%",
        backgroundColor: '#ffffff',
        marginBottom: 20
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    view: {
        borderRadius: 10,
        height: 50,
        padding: 20,
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
    collapseView: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 80,
    },
    collapseViewC: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 80,
        display: "flex",
        flex: 2,
        flexDirection: "row"
    },
    iconView: {
        borderRadius: 10,
        padding: 20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#ffffff',
        width: "100%"
    },
    iconViewTitle: {
        fontSize: 16,
        fontWeight: "bold"
    },
    dataUserLabel: {
        fontSize: 13,
        color: "#9B9B9B",
        fontWeight: "bold",
        marginBottom: 10
    },
    dataUserField: {
        fontSize: 16,
        color: "#000",
        fontWeight: "bold",
        borderWidth: 1,
        borderColor: "#00A2B4",
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 30,
        borderRadius: 6
    },
    brandCredit: {
        width: "30%",
        paddingLeft: 10,
        paddingTop: 10
    },
    infoCredit: {
        width: "70%",
        paddingLeft: 20
    },
    dataCredit: {
        color: "#000",
        fontSize: 16,
        fontWeight: "bold"
    },
    dataCreditNumber: {
        color: "#000",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 4,
    },
    dataCreditLink: {
        color: "#00A2B4",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 10,
        marginBottom: 0,
        fontWeight: "bold"
    },
    btn: {
        width: 260,
        height: 56,
        borderRadius: 8,
        backgroundColor: "#00A2B4",
        justifyContent: "center",
        alignItems: "center"
    },
    btn__text: {
        color: "#FFF",
        fontWeight: "bold",
        fontSize: 16
    },
    prod__link: {
        color: "#00A2B4",
        fontSize: 14,
        fontWeight: "bold",
        marginTop: 20,
        marginBottom: 20
    },
    errors: {
        position: 'absolute',
        top: 80,
        fontSize: 12,
        paddingLeft: 4
    },
    errorsCard: {
        fontSize: 12,
        paddingLeft: 4
    },

    modalBody: { 
        marginTop: 0, 
        backgroundColor: "rgba(0,0,0,0.7)", 
        padding: 20, 
        height: "100%", 
        width: "100%" 
    },

    modalBodyContainer: { 
        backgroundColor: "#fff", 
        height: "96%", 
        width: "90%", 
        margin: 20 
    },
    modalBodyContainerScrollable: { 
        padding: 20 
    },
    
    modalTitle: { 
        fontSize: 22, 
        fontWeight: "bold", 
        color: "#05072D", 
        marginBottom: 30 
    },

    fieldGroup: { 
        marginBottom: 15
    },

    fieldLabel: { 
        color: "#9B9B9B", 
        fontSize: 13, 
        fontWeight: "bold", 
        marginBottom: 6 
    },

    fieldText: { 
        height: 40, 
        borderColor: '#CCC', 
        borderWidth: 1,
        borderRadius: 4,
        paddingLeft: 5,
        paddingRight: 5 
    },

    fieldGroupTwoColumns: {
         width: "45%"
    },

    fieldCardDate: { 
        display: "flex", 
        flexDirection: "row", 
        justifyContent: "space-between", 
        alignItems: "center" 
    },

    fieldCardDateColumn:{ 
        height: 40, 
        width: "45%", 
        borderColor: '#CCC', 
        borderWidth: 1, 
        borderRadius: 4, 
        paddingLeft: 5, 
        paddingRight: 5 
    },

    cancelBtb: { 
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center" 
    }

})