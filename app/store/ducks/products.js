import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

export const { Types, Creators } = createActions({
    machine: ['machine'],
    appentToMachine: ['appentToMachine'],
});

export const ProductsTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    machine: {
        items: [],
        hasNextPage: false,
        count: 0
    },
});

export const machine = (state = INITIAL_STATE, action) => ({...state, machine: action.productsMachine });
export const appendToMachine = (state = INITIAL_STATE, action) => ({
    ...state, 
    machine: {
        items: [...state.machine.items, ...action.appendToMachineProducts.items],
        hasNextPage: action.appendToMachineProducts.hasNextPage,
        count: action.appendToMachineProducts.count
    }  
});

export const products = createReducer(INITIAL_STATE, {
    [Types.MACHINE]: machine,
    [Types.APPENT_TO_MACHINE]: appendToMachine,
});
