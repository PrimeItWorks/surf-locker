import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

export const { Types, Creators } = createActions({
    tickets: ['tickets'],
    appendToTickets: ['appendToTickets'],
    ticketsBadge: ['ticketsBadge'],
    setZeroTicketsBadge: ['setZeroTicketsBadge'],
})

export const TicketsTypes = Types
export default Creators

export const INITIAL_STATE = Immutable({
    tickets: false,
    ticketsBadge: 0,
})

export const tickets = (state = INITIAL_STATE, action) => ({...state, tickets: action.tickets })
export const ticketsBadge = (state = INITIAL_STATE, action) => ({...state, ticketsBadge: state.ticketsBadge + action.ticketsBadge })
export const setZeroTicketsBadge = (state = INITIAL_STATE, action) => ({...state, ticketsBadge: 0 })
export const appendToTickets = (state = INITIAL_STATE, action) => ({
    ...state, 
    tickets: {
        items: [...state.tickets.items, ...action.appendToTickets.items],
        hasNextPage: action.appendToTickets.hasNextPage,
        count: action.appendToTickets.count
    }  
});

export const banners = createReducer(INITIAL_STATE, {
    [Types.TICKETS]: tickets,
    [Types.TICKETS_BADGE]: ticketsBadge,
    [Types.SET_ZERO_TICKETS_BADGE]: setZeroTicketsBadge,
    [Types.APPEND_TO_TICKETS]: appendToTickets,
})
