import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

export const { Types, Creators } = createActions({
    machines: ['machines'],
});

export const MachineTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    machines: {
        items: [],
        hasNextPage: false,
        count: 0
    },
});

export const machines = (state = INITIAL_STATE, action) => ({...state, machines: action.machines });

export const products = createReducer(INITIAL_STATE, {
    [Types.MACHINES]: machines,
});
