import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

export const { Types, Creators } = createActions({
    account: ['account'],
});

export const AccountsTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
    account: {},
});

export const account = (state = INITIAL_STATE, action) => ({...state, account: action.account });

export const accounts = createReducer(INITIAL_STATE, {
    [Types.ACCOUNT]: account,
});
