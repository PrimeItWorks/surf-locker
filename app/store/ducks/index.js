import {combineReducers} from 'redux'

import {tickets} from './tickets'
import {products} from './products'
import {accounts} from './accounts'
import {machines} from './machines'

export default combineReducers({
    tickets,
    accounts,
    products,
    machines
})

