'use strict';

import ApiClient from './apiClient';
import Storage from '../helpers/Storage'

class TicketService{

    constructor(){
        this.apiClient = new ApiClient();
        this.storage = new Storage();
    }

    async getTickets(page){
        let user  = await this.storage.getUseId();           
        let resp   = await this.apiClient.getApi(`/Vouchers?UserCode=${user.code}&Page=${page}&PageSize=12&OrderBy=BuyDate&Desc=true`);        
        return resp;
    }

    async createTicket(data){
        let resp   = await this.apiClient.postApi("/Vouchers", data, true);
        return resp;
    }

    async gift(data){
        let resp   = await this.apiClient.postApi("/vouchers/gift", data, true);
        return resp;
    }
    
}

export default TicketService;
