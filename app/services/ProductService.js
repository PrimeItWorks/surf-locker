'use strict';

import ApiClient from './apiClient';

class ProductService{
    constructor(){
        this.apiClient = new ApiClient();
    }

    async getAllProduct(page = 1){
        let resp = await this.apiClient.getApi(`/Products/machine?Page=${page}`, true);

        return resp;
    }
}

export default ProductService;