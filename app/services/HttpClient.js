import Auth from "./AuthCliente";

class HttpClient {
  async refreshToken(init) {
    const data = Auth.getData();

    if (data !== null) {
      const newData = await Auth.refresh(data.Username, data.RefreshToken);
      data.AccessToken = newData.AccessToken;
      data.IdToken = newData.IdToken;
      Auth.updateData(data);
      init.headers.delete("Authorization");
      init.headers.append("Authorization", `Bearer ${data.AccessToken}`);
      return init;
    }

    return null;
  }

  async request(input, init = { useAuthorization: false }, firstTry = true) {
    if (init.useAuthorization) {
      const data = Auth.getData() || {};

      if (!init.headers) {
        init.headers = new Headers();
      }

      init.headers.delete("Authorization");
      init.headers.append("Authorization", `Bearer ${data.AccessToken}`);
    }

    if (init === null) {
      throw new Error("Parâmetro init não informado.");
    }

    return fetch(input, init).then(async response => {
      if (response.status === 401 && firstTry && init.useAuthorization) {
        init = await this.refreshToken(init);

        if (init !== null) {
          return await this.request(input, init, false);
        }

        return response;
      }

      if (response.status === 401 && !firstTry) {
        throw new Error("unauthorized-invalid-jwt");
      }

      return response;
    });
  }
}

export default HttpClient;
