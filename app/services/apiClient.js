'use strict';

import Storage from '../helpers/Storage';
const BASE_URL = "https://surflocker-app.azurewebsites.net/api";
// const BASE_URL = "https://83b4312a.ngrok.io/api";

class ApiClient {
  constructor() {
    this.BASE_URL = BASE_URL;
    this.storage = new Storage();
    this.authToken = null;
  }

  postApi(endpoint, data, Authorization = false) {
    const URL = this.BASE_URL + endpoint;       
    if(Authorization){
        this.authToken = this.storage.getData().value;        
    }

    return fetch(URL,
      {
        method: "POST",
        headers: {
          'Authorization': this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(response => {
          return response.json()
       })
      .then(responseJson => {          
          return responseJson;
      })
      .catch(err =>{ 
        return err
      });
  }

  getApi(endpoint, Authorization = false) {
    const URL = this.BASE_URL + endpoint;
    if(Authorization){
        this.authToken = this.storage.getData().value;        
    }

    return fetch(URL,
       {
          method: "GET",
          headers: {
            'Authorization': this.authToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
       })
       .then(response => {
          return response.json()
       })
      .then(responseJson => responseJson)
      .catch(err => err);
  }

  putApi(endpoint, data, Authorization = false) {
    const URL = this.BASE_URL + endpoint; 
    if(Authorization){
        this.authToken = this.storage.getData().value;        
    }

    return fetch(URL,
      {
        method: "PUT",
        headers: {
          'Authorization': this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
       .then(response => {
          return response.json()
       })
      .then(responseJson => responseJson)
      .catch(err => err);
  }


}

export default ApiClient;