'use strict'

import ApiClient from './apiClient'

class MachineService{
    constructor(){
        this.apiClient = new ApiClient()
    }

    async getAllMachine(page = 1){
        let resp = await this.apiClient.getApi(`/machines?Page=${page}&PageSize=999`, true)
        return resp
    }
}

export default MachineService