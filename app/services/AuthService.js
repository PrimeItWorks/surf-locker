'use strict';

import ApiClient from './apiClient';
import Storage from '../helpers/Storage'

class AuthService{
    constructor(){
        this.apiClient = new ApiClient();
        this.storage = new Storage();
    }

    async login(data){
        let resp = await this.apiClient.postApi('/SignIn', data);    
        if(resp.status && resp.status !== 200){
            return resp;
        }
        
        if(resp.statusCode === 200){            
            this.storage.store(resp.response)
        }        
        return resp
    }    

    async loginEmail(data){
        let resp = await this.apiClient.postApi('/SignIn/email', data);    
        if(resp.status && resp.status !== 200){
            return resp;
        }
        
        if(resp.statusCode === 200){            
            this.storage.store(resp.response)
        }        
        return resp
    }    
}

export default AuthService;