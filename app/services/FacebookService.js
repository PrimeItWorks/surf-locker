'use strict'

class Facebook{
    
    constructor(){
        this.KEY_FACEBOOK = "2145604769072053";
    }

    conect = async() =>{
        try {
            const {
              type,
              token
            } = await Expo.Facebook.logInWithReadPermissionsAsync(this.KEY_FACEBOOK, {
              permissions: ['public_profile', 'email'],
            });

            if (type === 'success') {            
              const response = await fetch(`https://graph.facebook.com/me?fields=email,name,birthday&access_token=${token}`);
              const resp = await response.json();            
              return resp;              
            } else {
              return type;
             
            }
          } catch (err) {
            //alert(`Facebook Login Error: ${message}`);
            return { err };
          }
    }
}

export default Facebook;