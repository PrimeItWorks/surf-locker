'use strict'

import ApiClient from './apiClient'
import Storage from '../helpers/Storage'

class AccountService{

    constructor(){
        this.apiClient = new ApiClient()
        this.storage = new Storage()
    }

    async getAccount(){
        let user  = await this.storage.getUseId()                   
        let resp  = await this.apiClient.getApi(`/Accounts/${user.code}`, true)
        return resp
    }

    async putAccount(data){
        let user = await this.apiClient.putApi('/Accounts', data, true)
        return user
    }

    async postCreditCard(data, Authorization = false){               
        if(Authorization){
            this.authToken = this.storage.getData().value        
        }

        return fetch("https://gateway.api.4all.com/prepareCard",
        // return fetch("https://gateway.homolog-interna.4all.com/prepareCard",
        {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            return response.json()
        })
        .then(responseJson => { 
            return responseJson            
        })
        .catch(err => err)
    }

    async GetAcessToken(){
        let resp   = await this.apiClient.postApi('/Payments/RequestVaultKey', true)        
        return resp
    }

    async PublicApiToken(){
        let resp   = await this.apiClient.getApi('/Payments/publicApiToken', true)        
        return resp
    }

    async CreateCreditCard(data){
        let resp = await this.apiClient.postApi('/Payments/CreateCardToken', data, true)        
        return resp
    }

    async SetCustumerInfo(data){
        let resp = await this.apiClient.postApi('/Payments/SetCustumerInfo', data, true)        
        return resp
    }

    async CreateTransaction(data){
        let resp = await this.apiClient.postApi('/Payments/CreateTransaction', data, true)
        return resp
    }

    async SendPhoneSmS(data){
        let resp = await this.apiClient.postApi('/Accounts/AskSmsPhoneConfirmation', data, true)
        return resp
    }

    async confirmPhone(data){
        let resp   = await this.apiClient.postApi("/accounts/confirmphone", data, true)
        return resp
    }

    async getUserByEmail(email){
        let resp   = await this.apiClient.getApi(`/accounts/byemail/${email}`)
        return resp
    }

    async createUser(user) {
        let resp   = await this.apiClient.postApi(`/accounts`, user)
        if(resp.statusCode === 200){            
            this.storage.store(resp.response)
        }  
        return resp
    }
}

export default AccountService
