import React from "react";
import {StyleSheet, View, Text, ScrollView, Image, Modal, TouchableHighlight, FlatList} from "react-native";
import {Collapse,CollapseHeader, CollapseBody} from 'accordion-collapse-react-native';
import { Entypo } from '@expo/vector-icons';


const TicketAccordion = (props) => {
    const {item} = props 
    _renderCollapseView = () => {
        return(
            props.item.active ? 
            <View style={styles.collapseView}>
                <View style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center"       
                }}>
                    <View style={{width: "70%", padding: 10}}>
                        <Text style={{fontSize: 14, fontWeight: "bold", color: "#000"}}>{`${props.item.trademark} ${props.item.title}`.toUpperCase()}</Text>
                        <Text style={{fontSize: 13, fontWeight: "bold", color: "#F6921E"}}>código disponível</Text>
                    </View>
                </View>
                <View style={{width: "100%", padding: 0, display: "flex", flexDirection: "row"}}>
                    <View style={{width: "45%",  justifyContent: "center", alignItems: "flex-end"}}>
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#979797"}}>Data da Compra:</Text>
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#979797"}}>Produtos:</Text>                        
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#979797"}}>Total da compra:</Text>                        
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#979797"}}>Tickets gerados:</Text>                        
                    </View>
                    <View style={{width: "55%", padding: 10}}>
                    <Text style={{fontSize: 13, textAlign: "left",color: "#979797"}}>10/01/2019 às 08:30</Text>
                        <Text style={{fontSize: 13, textAlign: "left", color: "#979797"}}>{props.item.quantity} no total</Text>                        
                        <Text style={{fontSize: 13, textAlign: "left", color: "#979797"}}>R$ {props.item.price}</Text>                        
                        <Text style={{fontSize: 13, textAlign: "left", color: "#979797"}}>{props.item.quantity}</Text>                        
                    </View>
                </View>
            </View>    
            :
            <View style={styles.collapseView}>
                <View style={{
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center"       
                }}>
                    {/*<View style={{width: "30%",  justifyContent: "center", alignItems: "center"}}>
                         {item.thumbnail && item.thumbnail.lenght > 0 ?
                            <Image source={item.thumbnail[0]} style={{width: 50, height: 50}}/>
                        :null}


                    </View> */}
                    <View style={{width: "70%", padding: 10}}>
                        <Text style={{fontSize: 14, fontWeight: "bold", color: "#666"}}>{`${props.item.trademark} ${props.item.title}`.toUpperCase()}</Text>
                        <Text style={{fontSize: 13, fontWeight: "bold", color: "#666"}}>código indisponível</Text>
                    </View>
                </View>
                <View style={{width: "100%", padding: 0, display: "flex", flexDirection: "row"}}>
                    <View style={{width: "45%",  justifyContent: "center", alignItems: "flex-end"}}>
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#666"}}>Data da Compra:</Text>
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#666"}}>Produtos:</Text>                        
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#666"}}>Total da compra:</Text>                        
                        <Text style={{fontSize: 13, textAlign: "right", fontWeight: "bold", color: "#666"}}>Tickets gerados:</Text>                        
                    </View>
                    <View style={{width: "55%", padding: 10}}>
                    <Text style={{fontSize: 13, textAlign: "left",color: "#666"}}>10/01/2019 às 08:30</Text>
                        <Text style={{fontSize: 13, textAlign: "left", color: "#666"}}>{props.item.quantity} no total</Text>                        
                        <Text style={{fontSize: 13, textAlign: "left", color: "#666"}}>R$ {props.item.price}</Text>                        
                        <Text style={{fontSize: 13, textAlign: "left", color: "#666"}}>{props.item.quantity}</Text>                        
                    </View>
                </View>
            </View>    
            
            
        )
    }
    
    _renderIconView = () => {
        return(
            props.item.active ? 
            <View style={styles.iconView}>
                <View style={styles.iconViewTitle}>
                    <Text style={{display: 'flex', flexDirection: 'column', fontSize:13, color: "#00A2B4", width: '100%', fontWeight: 'bold'}}>{`${props.item.trademark} ${props.item.title}`.toUpperCase() }</Text>
                    <Text style={{display: 'flex', flexDirection: 'column', width:'100%', fontWeight: 'bold'}}>{`digite *  ${props.item.machineKey} e # para confirmar`}</Text>
                </View>
                <View>
                    <Entypo name="chevron-small-down" size={14} color="#000" />
                </View>
            </View>
            : 
            <View style={styles.iconView}>
                <View style={styles.iconViewTitle}>
                    <Text style={{display: 'flex', flexDirection: 'column', fontSize:13, color: "#666", width: '100%', fontWeight: 'bold'}}>{`${props.item.trademark} ${props.item.title}`.toUpperCase()}</Text>
                    <Text style={{display: 'flex', flexDirection: 'column', width:'100%',color: "#666", fontWeight: 'bold'}}>{`digite *  ${props.item.machineKey} e # para confirmar`}</Text>
                </View>
                <View>
                    <Entypo name="chevron-small-down" size={14} color="#000" />
                </View>
            </View>
        )
    }
    
    
    return <View style={styles.userData}>
        <Collapse>
            <CollapseHeader>
                {this._renderIconView()}
            </CollapseHeader>
            <CollapseBody>
                {this._renderCollapseView()}
            </CollapseBody>
        </Collapse>
    </View>
}


export default TicketAccordion;

const styles = StyleSheet.create({    
    userData : {        
        borderRadius : 10,
        width: "100%",
        backgroundColor: '#ffffff',
        marginBottom: 20
    },
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    view: {
        borderRadius : 10,
        padding: 20,
        justifyContent:'center',
        backgroundColor:'#ffffff',
    },
    collapseView: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 10
        
    },
    collapseViewC: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 0,
        display: "flex",
        flex: 2,
        flexDirection: "row"
    },
    iconView: {
        borderRadius : 10,
        padding: 20,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor:'#ffffff',
        width: "100%"
    },
    iconViewTitle: {
        fontSize : 16,
        fontWeight: "bold",
        display: 'flex',
        flexDirection: 'column'
    },
    dataUserLabel: {
        fontSize: 13,
        color: "#9B9B9B",
        fontWeight: "bold",
        marginBottom: 10
    },
    dataUserField: {
        fontSize: 16,
        color: "#000",
        fontWeight: "bold",
        borderWidth: 1,
        borderColor: "#00A2B4",
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 30,
        borderRadius: 6
    },
    brandCredit:{
        width: "30%",
        paddingLeft: 10,
        paddingTop: 10
    },
    infoCredit:{
        width: "70%",
        paddingLeft: 20
    },
    dataCredit: {
        color: "#000",
        fontSize: 16,
        fontWeight: "bold"
    },
    dataCreditNumber: {
        color: "#000",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 4,
    },
    dataCreditLink: {
        color: "#00A2B4",
        fontSize: 13,
        fontWeight: "normal",
        marginTop: 10,
        marginBottom: 0,
        fontWeight: "bold"
    }

})