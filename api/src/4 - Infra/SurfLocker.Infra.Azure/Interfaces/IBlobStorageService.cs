﻿using System.Threading.Tasks;
using System.IO;
using System;

namespace SurfLocker.Infra.Azure.Services
{
    public interface IBlobStorageService
    {
        Task<string> UploadAsync(Stream stream, string fileName);
        Task DeleteAsync(string fileName);
        Task<string> GetAsBase64(string azureUri);
    }
}