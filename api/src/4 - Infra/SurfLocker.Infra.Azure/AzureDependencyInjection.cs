﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Infra.Azure.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class AzureDependencyInjection
    {
        public static IServiceCollection AddAzureDependency(this IServiceCollection services)
        {
            services.TryAddTransient<IBlobStorageService, BlobStorageService>();
            return services;
        }
    }
}
