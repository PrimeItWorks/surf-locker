﻿using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using System.Threading.Tasks;
using System.IO;
using System;

namespace SurfLocker.Infra.Azure.Services
{
    internal class BlobStorageService : IBlobStorageService
    {
        public Uri BaseUri { get; private set; }

        private readonly CloudBlobClient _cloudBlobClient;
        private readonly CloudBlobContainer _container;
        public BlobStorageService(IConfiguration configuration)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(configuration["BlobStorageAccountStringConnection"]);
            _cloudBlobClient = storageAccount.CreateCloudBlobClient();
            _container = _cloudBlobClient.GetContainerReference(configuration["BlobStorageContainner"]);
            BaseUri = _container.StorageUri.PrimaryUri;
        }

        public async Task DeleteAsync(string fileName)
        {
            CloudBlockBlob blockBlob = _container.GetBlockBlobReference(fileName);
            await blockBlob.DeleteIfExistsAsync();
        }

        public async Task<string> GetAsBase64(string path)
        {
            if (string.IsNullOrEmpty(path))
                return string.Empty;

            CloudBlockBlob blob = new CloudBlockBlob(new Uri(path), _cloudBlobClient);
            await blob.FetchAttributesAsync();

            byte[] bytes = new byte[blob.Properties.Length];
            await blob.DownloadToByteArrayAsync(bytes, 0);
            var azureBase64 = Convert.ToBase64String(bytes);

            return azureBase64;
        }

        public async Task<string> UploadAsync(Stream stream, string fileName)
        {
            if (stream == null)
                return string.Empty;

            var blockBlob = _container.GetBlockBlobReference(fileName);

            if (stream.CanSeek)
                stream.Seek(0, SeekOrigin.Begin);

            await blockBlob.UploadFromStreamAsync(stream);

            return blockBlob.Uri.AbsoluteUri;
        }
    }
}
