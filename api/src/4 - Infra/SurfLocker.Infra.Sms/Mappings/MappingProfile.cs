﻿using static Nexmo.Api.SMS;
using SurfLocker.Dto.Sms;
using AutoMapper;

namespace SurfLocker.Domain.Mapper
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<SMSResponse, SMSResponseDTo>();
            CreateMap<SMSResponseDetail, SMSResponseDetailDto>();
        }
    }
}
