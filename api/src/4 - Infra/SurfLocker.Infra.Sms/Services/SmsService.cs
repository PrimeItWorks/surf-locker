﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Nexmo.Api;
using Nexmo.Api.Request;
using SurfLocker.Dto.Sms;
using SurfLocker.Infra.Sms.Interfaces;
using static Nexmo.Api.SMS;

namespace SurfLocker.Infra.Sms.Services
{
    internal class SmsService : ISmsService
    {
        private readonly Client _client;
        private readonly IMapper _mapper;
        private readonly string _apiSecret;

        public SmsService(IConfiguration configuration
            , IMapper mapper)
        {
            var credentials = new Credentials()
            {
                ApiKey = configuration["NexmoApiKey"],
                ApiSecret = configuration["NexmoApiSecret"]
            };

            _client = new Client(credentials);
            _mapper = mapper;
        }

        public SMSResponseDTo Send(SMSRequestDto dto)
        {
            var request = new SMSRequest() {
                from = dto.From,
                to = dto.To,
                text = dto.Text
            };

            var results = _client.SMS.Send(request);
            return _mapper.Map<SMSResponseDTo>(results);
        }
    }
}
