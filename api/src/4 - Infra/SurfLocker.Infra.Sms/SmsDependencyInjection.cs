﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Infra.Sms.Interfaces;
using SurfLocker.Infra.Sms.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class SmsDependencyInjection
    {
        public static IServiceCollection AddSmsDependency(this IServiceCollection services)
        {
            services.TryAddTransient<ISmsService, SmsService>();
            return services;
        }
    }
}
