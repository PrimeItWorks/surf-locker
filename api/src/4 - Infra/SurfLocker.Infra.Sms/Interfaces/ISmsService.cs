﻿using SurfLocker.Dto.Sms;

namespace SurfLocker.Infra.Sms.Interfaces
{
    public interface ISmsService
    {
        SMSResponseDTo Send(SMSRequestDto request);
    }
}
