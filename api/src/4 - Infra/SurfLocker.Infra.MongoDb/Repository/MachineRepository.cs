using SurfLocker.Domain.Machines.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Domain.Machines;
using System.Collections.Generic;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Linq;
using AutoMapper;
using System;

namespace SurfLocker.Infra.MongoDb.Repository
{
    public class MachineRepository : IMachineRepository
    {
        private readonly IMongoDatabase  _db;
        private readonly IMapper _mapper;

        public MachineRepository(IConfiguration configuration
            , IMapper mapper)
        {
            var client = new MongoClient(configuration.GetConnectionString("MongoDb"));
            _db = client.GetDatabase(DatabasesConst.Machine);
            _mapper = mapper;
        }

        public async Task<MachineDto> GetByCodeAsync(string code)
        {
            var filter = Builders<Machine>.Filter
                .Eq(nameof(Machine.Code), code);

            var machine = await _db
                .GetCollection<Machine>(nameof(Machine))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<MachineDto>(machine);
        }

        public async Task<List<MachineDto>> GetByCodesAsync(string[] codes)
        {
            var filter = Builders<Machine>.Filter
                .In(nameof(Machine.Code), codes);

            var machine = await _db
                .GetCollection<Machine>(nameof(Machine))
                .Find(filter)
                .ToListAsync();

            return _mapper.Map<List<MachineDto>>(machine);
        }

        public bool Exists(string code)
        {
            var filter = Builders<Machine>.Filter
               .Eq(nameof(Machine.Code), code);

            var machines = _db
                .GetCollection<Machine>(nameof(Machine))
                .Find(filter)
                .CountDocuments();

            return machines > 0;
        }

        public async Task<ListResponseDto<MachineDto>> GetAllPagingAsync(MachineRequestAllDto request)
        {
            var filterBuilder = Builders<Machine>.Filter;

            var filter = filterBuilder.Empty;

            if (!string.IsNullOrEmpty(request.Search))
            {
                filter = filterBuilder.Or(
                    filterBuilder.Regex(nameof(Machine.Title), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.Place), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdName1), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdName2), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdName3), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdCod1), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdCod2), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(Machine.ProdCod3), new BsonRegularExpression($".*{request.Search}.*", "-i"))
                );
            }


            var sortBuilder = Builders<Machine>.Sort;
            var result = await _db.GetCollection<Machine>(nameof(Machine))
               .Find(filter)
               .Sort(request.Desc ? $"{{{request.OrderBy}: -1}}" : $"{{{request.OrderBy}: 1}}")
               .Skip((request.Page - 1) * request.PageSize)
               .Limit(request.PageSize + 1)
               .ToListAsync();

            var count = await _db.GetCollection<Machine>(nameof(Machine))
               .Find(filter)
               .CountDocumentsAsync();

            var mappedMachines = _mapper.Map<List<MachineDto>>(result);
            return new ListResponseDto<MachineDto>()
            {
                Items = mappedMachines.Take(request.PageSize).ToList(),
                Count = Convert.ToInt32(count),
                HasNextPage = result.Count > request.PageSize
            };
        }

        public async Task AddAsync(Machine Machine)
        {
            await _db
                .GetCollection<Machine>(nameof(Machine))
                .InsertOneAsync(Machine);
        }

        public async Task DeleteAsync(string code)
        {
            var productCollection = _db
                .GetCollection<Machine>(nameof(Machine));

            await productCollection.DeleteOneAsync(item => item.Code == code);
        }
        public async Task<Machine> UpdateAsync(Machine machine)
        {
            var options = new FindOneAndUpdateOptions<Machine, Machine> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Machine.Code), machine.Code);
            var update = Builders<Machine>.Update
                .Set(nameof(Machine.Title), machine.Title)
                .Set(nameof(Machine.Place), machine.Place)
                .Set(nameof(Machine.Lat), machine.Lat)
                .Set(nameof(Machine.Long), machine.Long)
                .Set(nameof(Machine.ProdCod1), machine.ProdCod1)
                .Set(nameof(Machine.ProdName1), machine.ProdName1)
                .Set(nameof(Machine.ProdQtd1), machine.ProdQtd1)
                .Set(nameof(Machine.ProdValue1), machine.ProdValue1)
                .Set(nameof(Machine.ProdCod2), machine.ProdCod2)
                .Set(nameof(Machine.ProdName2), machine.ProdName2)
                .Set(nameof(Machine.ProdQtd2), machine.ProdQtd2)
                .Set(nameof(Machine.ProdValue2), machine.ProdValue2)
                .Set(nameof(Machine.ProdCod3), machine.ProdCod3)
                .Set(nameof(Machine.ProdName3), machine.ProdName3)
                .Set(nameof(Machine.ProdQtd3), machine.ProdQtd3)
                .Set(nameof(Machine.ProdValue3), machine.ProdValue3);

            var result = await _db
                .GetCollection<Machine>(nameof(Machine))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }

        public async Task SetProductValue1ValueByProduct(string productCode, decimal value)
        {
            var options = new FindOneAndUpdateOptions<Machine, Machine> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Machine.ProdCod1), productCode);
            var update = Builders<Machine>.Update
                .Set(nameof(Machine.ProdValue1), value);

            var result = await _db
                .GetCollection<Machine>(nameof(Machine))
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task SetProductValue2ValueByProduct(string productCode, decimal value)
        {
            var options = new FindOneAndUpdateOptions<Machine, Machine> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Machine.ProdCod2), productCode);
            var update = Builders<Machine>.Update
                .Set(nameof(Machine.ProdValue2), value);

            var result = await _db
                .GetCollection<Machine>(nameof(Machine))
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task SetProductValue3ValueByProduct(string productCode, decimal value)
        {
            var options = new FindOneAndUpdateOptions<Machine, Machine> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Machine.ProdCod3), productCode);
            var update = Builders<Machine>.Update
                .Set(nameof(Machine.ProdValue3), value);

            var result = await _db
                .GetCollection<Machine>(nameof(Machine))
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task DecrementProdQtd(string code, string fieldName)
        {
            var options = new FindOneAndUpdateOptions<Machine, Machine> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Machine.Code), code);
            var update = Builders<Machine>.Update
                .Inc(fieldName, -1);

            var result = await _db
                .GetCollection<Machine>(nameof(Machine))
                .FindOneAndUpdateAsync(filter, update, options);
        }
    }   
}