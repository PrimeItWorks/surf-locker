using SurfLocker.Domain.Store.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Domain.Store.Sales;
using System.Collections.Generic;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Linq;
using AutoMapper;
using System;

namespace SurfLocker.Infra.MongoDb.Repository
{
    public class SalesRepository : ISalesRepository
    {
        private readonly IMongoDatabase  _db;
        private readonly IMapper _mapper;

        public SalesRepository(IConfiguration configuration
            , IMapper mapper)
        {
            var client = new MongoClient(configuration.GetConnectionString("MongoDb"));
            _db = client.GetDatabase(DatabasesConst.Store);
            _mapper = mapper;
        }

        public async Task<Sale> AddAsync(Sale sale)
        {
            sale.BuyDate = DateTime.Now;

            var collection = _db.GetCollection<Sale>(nameof(Sale));
            await collection
                .InsertOneAsync(sale);

            var filter = Builders<Sale>.Filter.Eq(nameof(Sale.Code), sale.Code);
            var result = await collection
                .Find(filter)
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<ListResponseDto<SalesDto>> GetAllPagingAsync(SalesRequestAllDto request)
        {
            var filterBuilder = Builders<Sale>.Filter;
            var filter = filterBuilder.Empty;

            if (!string.IsNullOrEmpty(request.UserCode))
            {
                filter = filterBuilder.Eq(nameof(Sale.UserCode), request.UserCode);

            }

            if (!string.IsNullOrEmpty(request.MachineCode))
            {
                filter = filterBuilder.And(
                    filter,
                    filterBuilder.Eq(nameof(Sale.MachineCode), request.MachineCode)
                );
            }
            
            if (!string.IsNullOrEmpty(request.ProductCode))
            {
                filter = filterBuilder.And(
                    filter,
                    filterBuilder.Eq(nameof(Sale.ProductCode), request.ProductCode)
                );
            }
            
            if (!string.IsNullOrEmpty(request.Trademark))
            {
                filter = filterBuilder.And(
                    filter,
                    filterBuilder.Regex(nameof(Sale.Trademark), new BsonRegularExpression($".*{request.Trademark}.*", "-i"))
                );
            }

            if (request.Start.HasValue)
            {
                filter = filterBuilder.And(
                    filter,
                    filterBuilder.Gte(nameof(Sale.BuyDate), request.Start.Value)
                );
            }

            if (request.End.HasValue)
            {
                filter = filterBuilder.And(
                    filter,
                    filterBuilder.Lte(nameof(Sale.BuyDate), request.End.Value)
                );
            }

            var sortBuilder = Builders<Sale>.Sort;
            var result = await _db.GetCollection<Sale>(nameof(Sale))
               .Find(filter)
               .Sort(request.Desc ? $"{{{request.OrderBy}: -1}}" : $"{{{request.OrderBy}: 1}}")
               .Skip((request.Page - 1) * request.PageSize)
               .Limit(request.PageSize + 1)
               .ToListAsync();

            var count = await _db.GetCollection<Sale>(nameof(Sale))
               .Find(filter)
               .CountDocumentsAsync();

            var mappedSales = _mapper.Map<List<SalesDto>>(result);
            return new ListResponseDto<SalesDto>()
            {
                Items = mappedSales.Take(request.PageSize).ToList(),
                Count = Convert.ToInt32(count),
                HasNextPage = result.Count > request.PageSize
            };
        }

        public async Task<List<SalesDto>> GetAllAsync()
        {
            var sortBuilder = Builders<Sale>.Sort;
            var result = await _db.GetCollection<Sale>(nameof(Sale))
               .Find(Builders<Sale>.Filter.Empty)
               .Sort($"{{{nameof(Sale.BuyDate)}: -1}}")
               .ToListAsync();

            var mappedSales = _mapper.Map<List<SalesDto>>(result);
            return mappedSales;
        }

        public async Task<SalesDto> GetByVoucherCodeAsync(string voucher)
        {
            var filter = Builders<Sale>.Filter
                .Eq(nameof(Sale.Code), voucher);

            var sale = await _db
                .GetCollection<Sale>(nameof(Sale))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<SalesDto>(sale);
        }
    }   
}