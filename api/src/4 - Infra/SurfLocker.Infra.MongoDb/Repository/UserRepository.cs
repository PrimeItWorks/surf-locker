using System;
using Microsoft.Extensions.Configuration;
using SurfLocker.Domain.Users.Interfaces;
using MongoDB.Driver;
using SurfLocker.Domain.Users;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurfLocker.Dto.Response;
using SurfLocker.Dto;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;

namespace SurfLocker.Infra.MongoDb.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoDatabase  _db;
        private readonly IMapper _mapper;

        public UserRepository(IConfiguration configuration
            , IMapper mapper)
        {
            var client = new MongoClient(configuration.GetConnectionString("MongoDb"));
            _db = client.GetDatabase(DatabasesConst.User);
            _mapper = mapper;
        }

        public async Task<ListResponseDto<UserDto>> GetAllPagingAsync(UserRequestAllDto request)
        {
             var filterBuilder = Builders<User>.Filter;

            var filter = filterBuilder.Empty;

            if (!string.IsNullOrEmpty(request.Search))
            {
                filter = filterBuilder.Or(
                    filterBuilder.Regex(nameof(User.Name), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(User.Cpf), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(User.Email), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                    filterBuilder.Regex(nameof(User.Phone), new BsonRegularExpression($".*{request.Search}.*", "-i"))
                );
            }

            var sortBuilder = Builders<User>.Sort;
            var result = await _db.GetCollection<User>(nameof(User))
               .Find(filter)
               .Sort(request.Desc ? $"{{{request.OrderBy}: -1}}" : $"{{{request.OrderBy}: 1}}")
               .Skip((request.Page -1) * request.PageSize)
               .Limit(request.PageSize + 1)
               .ToListAsync();

            var count = await _db.GetCollection<User>(nameof(User))
               .Find(filter)
               .CountDocumentsAsync();

            var mappedUsers = _mapper.Map<List<UserDto>>(result);
            return new ListResponseDto<UserDto>()
            {
                Items = mappedUsers.Take(request.PageSize).ToList(),
                Count = Convert.ToInt32(count),
                HasNextPage = result.Count > request.PageSize
            };
        }

        public async Task AddAsync(User user)
        {
            var UserCollection = _db
                .GetCollection<User>(nameof(User));

            await UserCollection.InsertOneAsync(user);
        }

        public async Task<UserDto> GetByCodeAsync(string code)
        {
            var filter = Builders<User>.Filter
                .Eq(nameof(User.Code), code);

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<UserDto>(user);
        }

        public async Task<List<UserDto>> GetByCodesAsync(string[] codes)
        {
            var filter = Builders<User>.Filter
                .In(nameof(User.Code), codes);

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .ToListAsync();

            return _mapper.Map<List<UserDto>>(user);
        }

        public async Task<UserDto> GetByFacebookId(string facebookId)
        {
            var filter = Builders<User>.Filter
               .Eq(nameof(User.FacebookId), facebookId);

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<UserDto>(user);
        }

        public async Task<UserDto> GetByEmail(string email)
        {
            var filter = Builders<User>.Filter
               .Eq(nameof(User.Email), email);

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<UserDto>(user);
        }

        public async Task<bool> ValidateLogin(string facebookId)
        {
            var filter = Builders<User>.Filter
                .Eq(nameof(User.FacebookId), facebookId);

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .FirstOrDefaultAsync();

            return user != null;
        }

        public async Task<bool> ValidateLogin(string email, string password, bool isAdmin = false)
        {
            var builder = Builders<User>.Filter;

            var filter =builder.And(
                builder.Eq(nameof(User.Email), email),
                builder.Eq(nameof(User.Password), password.Encrypt())
            );

            var user = await _db
                .GetCollection<User>(nameof(User))
                .Find(filter)
                .FirstOrDefaultAsync();

            if(!isAdmin)
                return user != null;

            return user != null && user.IsAdmin;
        }

        public async Task<User> UpdateAsync(User user)
        {
            var options = new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(User.Code), user.Code);
            var update = Builders<User>.Update
                .Set(nameof(User.Email), user.Email)
                .Set(nameof(User.Name), user.Name)
                .Set(nameof(User.Address), user.Address)
                .Set(nameof(User.Waves), user.Waves)
                .Set(nameof(User.CardToken), user.CardToken)
                .Set(nameof(User.CardBrand), user.CardBrand)
                .Set(nameof(User.ConfirmedNumber), user.ConfirmedNumber)
                .Set(nameof(User.LatestDigitCard), user.LatestDigitCard)
                .Set(nameof(User.Cpf), user.Cpf)
                .Set(nameof(User.Birthday), user.Birthday)
                .Set(nameof(User.Phone), user.Phone);

            var result = await _db
                .GetCollection<User>(nameof(User))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }

        public async Task MakeAdminAsync(User user)
        {
            var options = new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(User.Code), user.Code);
            var update = Builders<User>.Update
                .Set(nameof(User.IsAdmin), true);

            await _db
                .GetCollection<User>(nameof(User))
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task MakeNotAdminAsync(User user)
        {
            var options = new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(User.Code), user.Code);
            var update = Builders<User>.Update
                .Set(nameof(User.IsAdmin), false);

            await _db
                .GetCollection<User>(nameof(User))
                .FindOneAndUpdateAsync(filter, update, options);
        }

        public async Task<User> UpdateFieldAsync(string userCode, string column, object value)
        {
            var options = new FindOneAndUpdateOptions<User, User> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(User.Code), userCode);
            var update = Builders<User>.Update
                .Set(column, value);

            var result = await _db
                .GetCollection<User>(nameof(User))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }
    }
}