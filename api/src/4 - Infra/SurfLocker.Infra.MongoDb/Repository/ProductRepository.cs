using SurfLocker.Domain.Store.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Domain.Store.Products;
using System.Collections.Generic;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Linq;
using AutoMapper;
using System;

namespace SurfLocker.Infra.MongoDb.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly IMongoDatabase  _db;
        private readonly IMapper _mapper;

        public ProductRepository(IConfiguration configuration
            , IMapper mapper)
        {
            var client = new MongoClient(configuration.GetConnectionString("MongoDb"));
            _db = client.GetDatabase(DatabasesConst.Store);
            _mapper = mapper;
        }

        public async Task<ProductDto> GetByCodeAsync(string code)
        {
             var filter = Builders<Product>.Filter
                .Eq(nameof(Product.Code), code);

            var product = await _db
                .GetCollection<Product>(nameof(Product))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<ProductDto>(product);
        }

        public async Task<List<ProductDto>> GetByCodesAsync(string[] codes)
        {
            var filter = Builders<Product>.Filter
                .In(nameof(Product.Code), codes);

            var product = await _db
                .GetCollection<Product>(nameof(Product))
                .Find(filter)
                .ToListAsync();

            return _mapper.Map<List<ProductDto>>(product);
        }

        public async Task<ListResponseDto<ProductDto>> GetAllPagingAsync(ProductRequestAllDto request)
        {
            var filterBuilder = Builders<Product>.Filter;

            var filter = Builders<Product>.Filter.Empty;

            if(request.Type != ProductTypeDto.None)
                filter = filterBuilder.Eq(nameof(Product.Type), (int)request.Type);

            if (!string.IsNullOrEmpty(request.Search))
            {
                filter = filterBuilder.And(
                    filterBuilder.Eq(nameof(Product.Type), (int)request.Type),
                    filterBuilder.Or(
                        filterBuilder.Regex(nameof(Product.Title), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                        filterBuilder.Regex(nameof(Product.Trademark), new BsonRegularExpression($".*{request.Search}.*", "-i")),
                        filterBuilder.Regex(nameof(Product.Description), new BsonRegularExpression($".*{request.Search}.*", "-i"))
                    ));
            }

            var sortBuilder = Builders<Product>.Sort;
            var result = await _db.GetCollection<Product>(nameof(Product))
               .Find(filter)
               .Sort(request.Desc ? $"{{{request.OrderBy}: -1}}" : $"{{{request.OrderBy}: 1}}")
               .Skip((request.Page - 1) * request.PageSize)
               .Limit(request.PageSize + 1)
               .ToListAsync();

            var count = await _db.GetCollection<Product>(nameof(Product))
               .Find(filter)
               .CountDocumentsAsync();

            var mappedProducts = _mapper.Map<List<ProductDto>>(result);
            return new ListResponseDto<ProductDto>()
            {
                Items = mappedProducts.Take(request.PageSize).ToList(),
                Count = Convert.ToInt32(count),
                HasNextPage = result.Count > request.PageSize
            };
        }

        public async Task AddAsync(Product product)
        {
            var productCollection = _db
                .GetCollection<Product>(nameof(Product));

            await productCollection.InsertOneAsync(product);
        }

        public async Task DeleteAsync(string code)
        {
            var productCollection = _db
                .GetCollection<Product>(nameof(Product));

            await productCollection.DeleteOneAsync(item => item.Code == code);
        }

        public bool Exists(string code)
        {
            var filter = Builders<Product>.Filter
               .Eq(nameof(Product.Code), code);

            var products = _db
                .GetCollection<Product>(nameof(Product))
                .Find(filter)
                .CountDocuments();

            return products > 0;
        }

        public async Task<Product> UpdateAsync(Product user)
        {
            var options = new FindOneAndUpdateOptions<Product, Product> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Product.Code), user.Code);
            var update = Builders<Product>.Update
                .Set(nameof(Product.Description), user.Description)
                .Set(nameof(Product.Image), user.Image)
                .Set(nameof(Product.Price), user.Price)
                .Set(nameof(Product.Thumbnail), user.Thumbnail)
                .Set(nameof(Product.Title), user.Title)
                .Set(nameof(Product.Trademark), user.Trademark)
                .Set(nameof(Product.Unit), user.Unit)
                .Set(nameof(Product.Type), user.Type)
                .Set(nameof(Product.UrlVideo), user.UrlVideo);

            var result = await _db
                .GetCollection<Product>(nameof(Product))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }
    }   
}