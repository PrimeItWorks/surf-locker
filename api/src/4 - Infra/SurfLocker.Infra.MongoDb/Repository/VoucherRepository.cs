using SurfLocker.Domain.Store.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Domain.Store.Vouchers;
using System.Collections.Generic;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Linq;
using AutoMapper;
using System;

namespace SurfLocker.Infra.MongoDb.Repository
{
    public class VoucherRepository : IVoucherRepository
    {
        private readonly IMongoDatabase  _db;
        private readonly IMapper _mapper;

        public VoucherRepository(IConfiguration configuration
            , IMapper mapper)
        {
            var client = new MongoClient(configuration.GetConnectionString("MongoDb"));
            _db = client.GetDatabase(DatabasesConst.Store);
            _mapper = mapper;
        }

        public bool CheckMachineKey(string machineKey)
        {
            var filterBuilder = Builders<Voucher>.Filter;

            var filter = filterBuilder.And(
               filterBuilder.Eq(nameof(Voucher.MachineKey), machineKey), 
               filterBuilder.Eq(nameof(Voucher.Active), true)
            );

            var vouchers = _db
                .GetCollection<Voucher>(nameof(Voucher))
                .Find(filter)
                .CountDocuments();

            return vouchers > 0;
        }

        public async Task<VoucherDto> GetByMachineKey(string machineKey)
        {
            var filterBuilder = Builders<Voucher>.Filter;

            var filter = filterBuilder.And(
               filterBuilder.Eq(nameof(Voucher.MachineKey), machineKey),
               filterBuilder.Eq(nameof(Voucher.Active), true)
            );

            var voucher = await _db
                .GetCollection<Voucher>(nameof(Voucher))
                .Find(filter)
                .FirstOrDefaultAsync();

            return _mapper.Map<VoucherDto>(voucher);
        }

        public async Task<ListResponseDto<VoucherDto>> GetAllPagingAsync(VoucherRequestAllDto request)
        {
            var filterBuilder = Builders<Voucher>
                .Filter;

            FilterDefinition<Voucher> filter;
            if (string.IsNullOrEmpty(request.UserCode))
                filter = filterBuilder.Empty;
            else
                filter = filterBuilder.Eq(nameof(Voucher.UserCode), request.UserCode);

            var sortBuilder = Builders<Voucher>.Sort;
            var result = await _db.GetCollection<Voucher>(nameof(Voucher))
               .Find(filter)
               .Sort(request.Desc ? $"{{{request.OrderBy}: -1}}" : $"{{{request.OrderBy}: 1}}")
               .Skip((request.Page - 1) * request.PageSize)
               .Limit(request.PageSize + 1)
               .ToListAsync();

            var count = await _db.GetCollection<Voucher>(nameof(Voucher))
               .Find(filter)
               .CountDocumentsAsync();

            var mappedVouchers = _mapper.Map<List<VoucherDto>>(result);
            return new ListResponseDto<VoucherDto>()
            {
                Items = mappedVouchers.Take(request.PageSize).ToList(),
                Count = Convert.ToInt32(count),
                HasNextPage = result.Count > request.PageSize
            };
        }

        public async Task<Voucher> AddAsync(Voucher voucher)
        {
            voucher.BuyDate = DateTime.Now;
            voucher.Active = true;

            var collection = _db.GetCollection<Voucher>(nameof(Voucher));
            await collection
                .InsertOneAsync(voucher);

            var filter = Builders<Voucher>.Filter.Eq(nameof(Voucher.MachineKey), voucher.MachineKey);
            var result = await collection
                .Find(filter)
                .FirstOrDefaultAsync();

            return result;
        }

        public async Task<Voucher> ResumeAsync(Voucher voucher)
        {
            voucher.DispenseDate = DateTime.Now;
            voucher.Active = false;

            var options = new FindOneAndUpdateOptions<Voucher, Voucher> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Voucher.Code), voucher.Code);
            var update = Builders<Voucher>.Update
                .Set(nameof(Voucher.Active), voucher.Active)
                .Set(nameof(Voucher.MachineCode), voucher.MachineCode)
                .Set(nameof(Voucher.DispenseDate), voucher.DispenseDate);

            var result = await _db
                .GetCollection<Voucher>(nameof(Voucher))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }

        public async Task<Voucher> DeactivateAsync(string code)
        {
            var options = new FindOneAndUpdateOptions<Voucher, Voucher> { ReturnDocument = ReturnDocument.After };
            var filter = new BsonDocument(nameof(Voucher.Code), code);
            var update = Builders<Voucher>.Update
                .Set(nameof(Voucher.Active), false);

            var result = await _db
                .GetCollection<Voucher>(nameof(Voucher))
                .FindOneAndUpdateAsync(filter, update, options);

            return result;
        }
    }   
}