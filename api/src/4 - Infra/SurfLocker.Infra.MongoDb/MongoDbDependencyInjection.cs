﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Domain.Machines.Interfaces;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Infra.MongoDb.Repository;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MongoDbDependencyInjection
    {
        public static IServiceCollection AddMongoDbDependency(this IServiceCollection services)
        {
            services.TryAddScoped<IProductRepository, ProductRepository>();
            services.TryAddScoped<IUserRepository, UserRepository>();
            services.TryAddScoped<IVoucherRepository, VoucherRepository>();
            services.TryAddScoped<IMachineRepository, MachineRepository>();
            services.TryAddScoped<ISalesRepository, SalesRepository>();

            return services;
        }
    }
}
