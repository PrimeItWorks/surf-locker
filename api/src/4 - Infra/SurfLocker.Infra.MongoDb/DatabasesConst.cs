﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Infra.MongoDb
{
    public static class DatabasesConst
    {
        public const string Store = "DBStore";
        public const string Machine = "DBMachines";
        public const string User = "DBUser";
    }
}
