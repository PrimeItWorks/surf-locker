﻿using SurfLocker.Infra.GatewayPayment.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Dto.Payment.Response;
using SurfLocker.Domain.Notifications;
using SurfLocker.Dto.Payment.Request;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System;

namespace SurfLocker.Infra.GatewayPayment.Services
{
    internal class PaymentService : IPaymentService
    {
        private readonly string _publicApiToken;
        private readonly string _merchantKey;
        private readonly string _baseUrl;
        private readonly INotificationHandler _notificationHandler;
        private static readonly HttpClient _client = new HttpClient();

        public PaymentService(IConfiguration configuration
            , INotificationHandler notificationHandler)
        {
            _publicApiToken = configuration["4AllPublicApiKey"];
            _merchantKey = configuration["4AllMerchantKey"];
            _baseUrl = configuration["4AllApi"];

            _notificationHandler = notificationHandler;
        }

        public string GetPublicApiToken() => _publicApiToken;
        public string GetMerchantKey() => _merchantKey;

        public async Task<RequestVaultKeyResponseDto> RequestVaultKeyAsync()
        {
            var request = new RequestVaultKeyRequestDto() { merchantKey = _merchantKey };

            var json = JsonConvert.SerializeObject(request);
            using (var content = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                using (HttpResponseMessage response = await _client.PostAsync($"{_baseUrl}/requestVaultKey", content))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        _notificationHandler.DefaultBuilder()
                            .WithCode("REQUEST_VAULT_KEY")
                            .WithMessage(result)
                            .RaiseNotification();

                        return null;
                    }

                    return await response.Content.ReadAsAsync<RequestVaultKeyResponseDto>();
                }
            }
        }

        public async Task<CreateCardTokenResponseDto> CreateCardToken(CreateCardTokenRequestDto request)
        {
            request.merchantKey = _merchantKey;

            var json = JsonConvert.SerializeObject(request);
            using (var content = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                using (HttpResponseMessage response = await _client.PostAsync($"{_baseUrl}/createCardToken", content))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        _notificationHandler.DefaultBuilder()
                            .WithCode("CREATE_CARD_TOKEN")
                            .WithMessage(result)
                            .RaiseNotification();

                        return null;
                    }

                    return await response.Content.ReadAsAsync<CreateCardTokenResponseDto>();
                }
            }
        }

        public async Task SetCustomerInfo(SetCustomerInfoRequestDto request)
        {
            request.merchantKey = _merchantKey;

            var json = JsonConvert.SerializeObject(request);
            using (var content = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                using (HttpResponseMessage response = await _client.PostAsync($"{_baseUrl}/setCustomerInfo", content))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        _notificationHandler.DefaultBuilder()
                            .WithCode("SET_CUSTOMER_INFO")
                            .WithMessage(result)
                            .RaiseNotification();
                    }

                }
            }
        }

        public async Task<CreateTransactionResponseDto> CreateTransaction(CreateTransactionRequestDto request)
        {
            request.metaId = Guid.NewGuid().ToString();
            request.softDescriptor = $"C{request.customerInfo.cpf}";
            request.amount = request.amount * 100;
            request.paymentMethod.ForEach(p => p.amount = p.amount * 100);

            request.merchantKey = _merchantKey;
            

            var json = JsonConvert.SerializeObject(request);
            using (var content = new StringContent(json, Encoding.UTF8, "application/json"))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                using (HttpResponseMessage response = await _client.PostAsync($"{_baseUrl}/createTransaction", content))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        _notificationHandler.DefaultBuilder()
                            .WithCode("CREATE_TRANSACTION")
                            .WithMessage(result)
                            .RaiseNotification();

                        return null;
                    }

                    return await response.Content.ReadAsAsync<CreateTransactionResponseDto>();
                }
            }
        }
    }
}
