﻿using SurfLocker.Dto.Payment.Request;
using SurfLocker.Dto.Payment.Response;
using System.Threading.Tasks;

namespace SurfLocker.Infra.GatewayPayment.Interfaces
{
    public interface IPaymentService
    {
        string GetPublicApiToken();
        string GetMerchantKey();

        Task<RequestVaultKeyResponseDto> RequestVaultKeyAsync();

        Task SetCustomerInfo(SetCustomerInfoRequestDto request);

        Task<CreateCardTokenResponseDto> CreateCardToken(CreateCardTokenRequestDto request);

        Task<CreateTransactionResponseDto> CreateTransaction(CreateTransactionRequestDto request);
    }
}
