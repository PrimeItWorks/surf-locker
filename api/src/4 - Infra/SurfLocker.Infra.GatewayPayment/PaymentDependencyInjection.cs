﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Infra.GatewayPayment.Interfaces;
using SurfLocker.Infra.GatewayPayment.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class PaymentDependencyInjection
    {
        public static IServiceCollection AddPaymentDependency(this IServiceCollection services)
        {
            services.TryAddTransient<IPaymentService, PaymentService>();

            return services;
        }
    }
}
