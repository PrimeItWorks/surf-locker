﻿using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SurfLocker.Application.Blob
{
    public class UploadBlobStorageRequest : IRequest<string>
    {
        public Stream File { get; }
        public string Name { get; }

        public UploadBlobStorageRequest(Stream file, string name)
        {
            File = file;
            Name = name;
        }
    }
}
