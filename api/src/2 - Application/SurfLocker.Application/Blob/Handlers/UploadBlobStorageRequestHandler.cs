﻿using MediatR;
using SurfLocker.Infra.Azure.Services;
using System.Threading.Tasks;
using System.Threading;

namespace SurfLocker.Application.Blob.Handlers
{
    public class UploadBlobStorageRequestHandler : IRequestHandler<UploadBlobStorageRequest, string>
    {
        private readonly IBlobStorageService _service;

        public UploadBlobStorageRequestHandler(IBlobStorageService service)
        {
            _service = service;
        }

        public async Task<string> Handle(UploadBlobStorageRequest request, CancellationToken cancellationToken)
        {
            var result = await _service.UploadAsync(request.File, request.Name);
            return result;
        }
    }
}
