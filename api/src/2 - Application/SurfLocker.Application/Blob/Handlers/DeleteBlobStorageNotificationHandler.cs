﻿using SurfLocker.Infra.Azure.Services;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace SurfLocker.Application.Blob.Handlers
{
    public class DeleteBlobStorageNotificationHandler : INotificationHandler<DeleteBlobStorageNotification>
    {
        private readonly IBlobStorageService _service;

        public DeleteBlobStorageNotificationHandler(IBlobStorageService service)
        {
            _service = service;
        }

        public async Task Handle(DeleteBlobStorageNotification notification, CancellationToken cancellationToken)
        {
            await _service.DeleteAsync(notification.Name);
        }
    }
}
