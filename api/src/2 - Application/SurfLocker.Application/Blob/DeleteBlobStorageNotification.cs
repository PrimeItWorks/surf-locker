﻿using MediatR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SurfLocker.Application.Blob
{
    public class DeleteBlobStorageNotification : INotification
    {
        public string Name { get; }

        public DeleteBlobStorageNotification(string name)
        {
            Name = name;
        }
    }
}
