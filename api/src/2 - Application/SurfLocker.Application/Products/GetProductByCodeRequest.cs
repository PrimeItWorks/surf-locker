﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Products
{
    public class GetProductByCodeRequest
        : IRequest<ProductDto>
    {
        public string Code { get; }
        public GetProductByCodeRequest(string code) =>
            Code = code;
    }
}
