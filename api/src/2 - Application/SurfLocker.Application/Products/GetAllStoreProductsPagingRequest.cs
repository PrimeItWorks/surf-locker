﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Products
{
    public class GetAllStoreProductPagingRequest
        : IRequest<ListResponseDto<ProductDto>>
    {
        public ProductRequestAllDto Request { get; }
        public GetAllStoreProductPagingRequest(ProductRequestAllDto request) =>
            Request = request;
    }
}
