﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;

namespace SurfLocker.Application.Products.Handlers
{
    public class CreateProductNotificationHandler
        : INotificationHandler<CreateProductNotification>
    {
        private readonly IProductFactory _factory;
        public CreateProductNotificationHandler(IProductFactory factory)
        {
            _factory = factory;
        }

        public async Task Handle(CreateProductNotification notification, CancellationToken cancellationToken)
        {
            await _factory
                .DefaultBuilder()
                .WithCode(Guid.NewGuid().ToString())
                .WithDescription(notification.Product.Description)
                .WithImage(notification.Product.Image)
                .WithPrice(notification.Product.Price)
                .WithThumbnail(notification.Product.Thumbnail)
                .WithTitle(notification.Product.Title)
                .WithTrademark(notification.Product.Trademark)
                .WithType(notification.Product.Type)
                .WithUnit(notification.Product.Unit)
                .WithVideo(notification.Product.UrlVideo)
                .Raise()
                .AddAsync();
        }
    }
}
