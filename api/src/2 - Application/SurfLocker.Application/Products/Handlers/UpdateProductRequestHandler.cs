﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;
using SurfLocker.Domain.Machines.Interfaces.Factory;

namespace SurfLocker.Application.Products.Handlers
{
    public class UpdateProductRequestHandler
        : IRequestHandler<UpdateProductRequest, ProductDto>
    {
        private readonly IProductFactory _factory;
        private readonly IMachineFactory _machineFactory;

        public UpdateProductRequestHandler(
            IProductFactory factory
            , IMachineFactory machineFactory) 
        {
             _factory = factory;
             _machineFactory = machineFactory;
        }

        public async Task<ProductDto> Handle(UpdateProductRequest request, CancellationToken cancellationToken)
        {
            var updated = await _factory
                .DefaultBuilder()
                .WithCode(request.Product.Code)
                .WithDescription(request.Product.Description)
                .WithImage(request.Product.Image)
                .WithPrice(request.Product.Price)
                .WithThumbnail(request.Product.Thumbnail)
                .WithTitle(request.Product.Title)
                .WithTrademark(request.Product.Trademark)
                .WithType(request.Product.Type)
                .WithVideo(request.Product.UrlVideo)
                .WithUnit(request.Product.Unit)
                .Raise()
                .UpdateAsync();

            var machine = _machineFactory
                .DefaultBuilder()
                .WithProdCode1(request.Product.Code)
                .WithProdValue1(request.Product.Price)
                .WithProdCode2(request.Product.Code)
                .WithProdValue2(request.Product.Price)
                .WithProdCode3(request.Product.Code)
                .WithProdValue3(request.Product.Price)
                .Raise();

            await machine.SetProdValue1();
            await machine.SetProdValue2();
            await machine.SetProdValue3();

            return updated;
        }
    }
}
