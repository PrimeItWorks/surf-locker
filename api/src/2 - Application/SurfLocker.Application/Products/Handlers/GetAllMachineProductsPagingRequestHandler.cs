﻿using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using SurfLocker.Domain.Store.Interfaces;

namespace SurfLocker.Application.Products.Handlers
{
    public class GetAllMachineProductsPagingRequestHandler
        : IRequestHandler<GetAllMachineProductPagingRequest, ListResponseDto<ProductDto>>
    {
        private readonly IProductRepository _repository;
        public GetAllMachineProductsPagingRequestHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<ProductDto>> Handle(GetAllMachineProductPagingRequest request, CancellationToken cancellationToken)
        {
            request.Request.Type = ProductTypeDto.Machine;
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}
