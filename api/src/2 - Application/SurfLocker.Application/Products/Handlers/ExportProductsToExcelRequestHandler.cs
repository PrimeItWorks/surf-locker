﻿using SurfLocker.Domain.Store.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Threading;
using System.IO;
using MediatR;
using System;

namespace SurfLocker.Application.Products.Handlers
{
    internal class ExportProductsToExcelRequestHandler
        : IRequestHandler<ExportProductsToExcelRequest, MemoryStream>
    {
        private readonly IProductRepository _productRepository;
        private readonly IHostingEnvironment _env;

        public ExportProductsToExcelRequestHandler(
            IProductRepository productRepository
            , IHostingEnvironment env)
        {
            _productRepository = productRepository;
            _env = env;
        } 
                

        public async Task<MemoryStream> Handle(ExportProductsToExcelRequest request, CancellationToken cancellationToken)
        {
            request.Request.PageSize = 999999;
            request.Request.Page = 1;
            var users = await _productRepository.GetAllPagingAsync(request.Request);

            string sWebRootFolder = _env.WebRootPath;
            string sFileName = $@"{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            var filePath = Path.Combine(sWebRootFolder, sFileName);
            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Produtos das Máquinas");
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("CODIGO");
                row.CreateCell(1).SetCellValue("NOME");
                row.CreateCell(2).SetCellValue("DESCRICAO");
                row.CreateCell(3).SetCellValue("MARCA");
                row.CreateCell(4).SetCellValue("PRECO");
                row.CreateCell(5).SetCellValue("TIPO");
                row.CreateCell(6).SetCellValue("UNIDADES");
                row.CreateCell(7).SetCellValue("VIDEO");

                var usersList = users.Items;
                for (int i = 0; i < usersList.Count; i++)
                {
                    var item = usersList[i];

                    row = excelSheet.CreateRow(i + 1);
                    row.CreateCell(0).SetCellValue(item.Code);
                    row.CreateCell(1).SetCellValue(item.Title);
                    row.CreateCell(2).SetCellValue(item.Description);
                    row.CreateCell(3).SetCellValue(item.Trademark);
                    row.CreateCell(4).SetCellValue(Convert.ToDouble(item.Price));
                    row.CreateCell(5).SetCellValue(item.Type.ToString());
                    row.CreateCell(6).SetCellValue(Convert.ToDouble(item.Unit));
                    row.CreateCell(7).SetCellValue(item.UrlVideo);
                }

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            File.Delete(filePath);

            memory.Position = 0;

            return memory;
        }
    }
}
