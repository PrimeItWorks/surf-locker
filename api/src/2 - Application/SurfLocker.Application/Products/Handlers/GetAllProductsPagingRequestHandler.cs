using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Products.Handlers
{
    public class GetAllProductsPagingRequestHandler
        : IRequestHandler<GetAllProductsPagingRequest, ListResponseDto<ProductDto>>
    {
        private readonly IProductRepository _repository;
        public GetAllProductsPagingRequestHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<ProductDto>> Handle(
            GetAllProductsPagingRequest request
            , CancellationToken cancellationToken)
        {
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}