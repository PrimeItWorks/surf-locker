﻿using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using SurfLocker.Domain.Store.Interfaces;

namespace SurfLocker.Application.Products.Handlers
{
    public class GetAllStoreProductsPagingRequestHandler
        : IRequestHandler<GetAllStoreProductPagingRequest, ListResponseDto<ProductDto>>
    {
        private readonly IProductRepository _repository;
        public GetAllStoreProductsPagingRequestHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<ProductDto>> Handle(GetAllStoreProductPagingRequest request, CancellationToken cancellationToken)
        {
            request.Request.Type = ProductTypeDto.Store;
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}
