﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Domain.Store.Interfaces;

namespace SurfLocker.Application.Products.Handlers
{
    public class DeleteProductNotificationHandler
        : INotificationHandler<DeleteProductNotification>
    {
        private readonly IProductRepository _repo;
        public DeleteProductNotificationHandler(IProductRepository repo)
        {
            _repo = repo;
        }

        public async Task Handle(DeleteProductNotification notification, CancellationToken cancellationToken)
        {
            await _repo.DeleteAsync(notification.Code);
        }
    }
}
