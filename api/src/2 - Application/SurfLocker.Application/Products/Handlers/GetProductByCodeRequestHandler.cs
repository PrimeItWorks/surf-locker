﻿using SurfLocker.Domain.Store.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Products.Handlers
{
    public class GetProductByCodeRequestHandler
        : IRequestHandler<GetProductByCodeRequest, ProductDto>
    {
        private readonly IProductRepository _repository;
        public GetProductByCodeRequestHandler(IProductRepository repository)
        {
            _repository = repository;
        }

        public async Task<ProductDto> Handle(GetProductByCodeRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetByCodeAsync(request.Code);
        }
    }
}
