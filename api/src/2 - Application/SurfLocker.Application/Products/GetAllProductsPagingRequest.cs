﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Products
{
    public class GetAllProductsPagingRequest
        : IRequest<ListResponseDto<ProductDto>>
    {
        public ProductRequestAllDto Request { get; }
        public GetAllProductsPagingRequest(ProductRequestAllDto request) =>
            Request = request;
    }
}
