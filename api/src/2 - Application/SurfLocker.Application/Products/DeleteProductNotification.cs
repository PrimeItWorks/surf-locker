﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Products
{
    public class DeleteProductNotification
        : INotification
    {
        public string Code { get; }
        public DeleteProductNotification(string code) =>
            Code = code;
    }
}
