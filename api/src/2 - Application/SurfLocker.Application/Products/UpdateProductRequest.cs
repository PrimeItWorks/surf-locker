﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Products
{
    public class UpdateProductRequest
        : IRequest<ProductDto>

    {
        public ProductDto Product { get; }
        public UpdateProductRequest(ProductDto product) =>
            Product = product;
    }
}
