﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Products
{
    public class CreateProductNotification
        : INotification

    {
        public ProductDto Product { get; }
        public CreateProductNotification(ProductDto product) =>
            Product = product;
    }
}
