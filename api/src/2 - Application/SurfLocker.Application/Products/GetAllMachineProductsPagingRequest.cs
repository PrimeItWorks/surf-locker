﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Products
{
    public class GetAllMachineProductPagingRequest
        : IRequest<ListResponseDto<ProductDto>>
    {
        public ProductRequestAllDto Request { get; }
        public GetAllMachineProductPagingRequest(ProductRequestAllDto request) =>
            Request = request;
    }
}
