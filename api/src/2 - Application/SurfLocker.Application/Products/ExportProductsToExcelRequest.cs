using System.IO;
using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Products
{
    public class ExportProductsToExcelRequest
        : IRequest<MemoryStream>
    {
        public ProductRequestAllDto Request { get; }
        public ExportProductsToExcelRequest(ProductRequestAllDto request)
        {
            this.Request = request;
        }
    }
}