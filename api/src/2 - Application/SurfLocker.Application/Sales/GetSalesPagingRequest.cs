using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Sales
{
    public class GetSalesPagingRequest
        : IRequest<ListResponseDto<SalesDto>>
    {
        public SalesRequestAllDto Request { get; }
        public GetSalesPagingRequest(SalesRequestAllDto request)
        {
            Request = request;
        }
    }
}