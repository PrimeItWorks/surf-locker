using System.IO;
using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Sales
{
    public class ExportSalesToExcelRequest
        : IRequest<MemoryStream>
    {
        public SalesRequestAllDto Request { get; }
        public ExportSalesToExcelRequest(SalesRequestAllDto request)
        {
            this.Request = request;
        }
    }
}