﻿using SurfLocker.Domain.Store.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using System.Linq;
using MediatR;

namespace SurfLocker.Application.Sales.Handlers
{
    public class GetTotalsRequestHandler
        : IRequestHandler<GetTotalsRequest, TotalsDto>
    {
        private readonly ISalesRepository _repository;
        public GetTotalsRequestHandler(ISalesRepository repository)
        {
            _repository = repository;
        }

        public async Task<TotalsDto> Handle(GetTotalsRequest request, CancellationToken cancellationToken)
        {
            var sales = await _repository.GetAllAsync();
            var totals = new TotalsDto(){
                SalesApp = sales.Count(sale => sale.Online),
                SalesMachine = sales.Count(sale => !sale.Online),
                Sales = sales.Count,
                Billed = sales.Sum(sale => sale.Value)
            };

            totals.SalesAppPercent = (totals.SalesApp / totals.Sales) * 100;
            totals.SalesMachinePercent = (totals.SalesMachine / totals.Sales) * 100;

            return totals;
        }
    }
}
