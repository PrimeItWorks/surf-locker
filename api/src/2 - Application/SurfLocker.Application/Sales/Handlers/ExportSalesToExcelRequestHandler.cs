﻿using SurfLocker.Domain.Store.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System.Threading.Tasks;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using System.Threading;
using System.IO;
using MediatR;
using System;
using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Dto;
using SurfLocker.Domain.Machines.Interfaces;
using System.Linq;

namespace SurfLocker.Application.Sales.Handlers
{
    internal class ExportSalesToExcelRequestHandler
        : IRequestHandler<ExportSalesToExcelRequest, MemoryStream>
    {
        private readonly ISalesRepository _saleRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMachineRepository _machineRepository;
        private readonly IProductRepository _productRepository;
        private readonly IHostingEnvironment _env;

        public ExportSalesToExcelRequestHandler(
            ISalesRepository saleRepository
            , IUserRepository userRepository
            , IMachineRepository machineRepository
            , IProductRepository productRepository
            , IHostingEnvironment env)
        {
            _saleRepository = saleRepository;
            _userRepository = userRepository;
            _machineRepository = machineRepository;
            _productRepository = productRepository;
            _env = env;
        } 

        public async Task<MemoryStream> Handle(ExportSalesToExcelRequest request, CancellationToken cancellationToken)
        {
            request.Request.PageSize = 999999;
            request.Request.Page = 1;
            var sales = await _saleRepository.GetAllPagingAsync(request.Request);

            var userCodes = sales.Items
                    .Where(i => !string.IsNullOrEmpty(i.UserCode))
                    .Select(i => i.UserCode);

            var users = await _userRepository.GetByCodesAsync(userCodes.ToArray());

            var machineCodes = sales.Items
                .Where(i => !string.IsNullOrEmpty(i.MachineCode))
                .Select(i => i.MachineCode);

            var machines = await _machineRepository.GetByCodesAsync(machineCodes.ToArray());

            var productCodes = sales.Items
                .Where(i => !string.IsNullOrEmpty(i.ProductCode))
                .Select(i => i.ProductCode);

            var products = await _productRepository.GetByCodesAsync(productCodes.ToArray());

            var salesList = sales.Items;

            string sWebRootFolder = _env.WebRootPath;
            string sFileName = $@"{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            var filePath = Path.Combine(sWebRootFolder, sFileName);

            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Relatório de Vendas");
                IRow row = excelSheet.CreateRow(0);

                IDataFormat dataFormatCustom = workbook.CreateDataFormat();

                row.CreateCell(0).SetCellValue("CODIGO");
                row.CreateCell(1).SetCellValue("MARCA");
                row.CreateCell(2).SetCellValue("PRODUTO");
                row.CreateCell(3).SetCellValue("DATA");
                row.CreateCell(4).SetCellValue("QUANTIDADE");
                row.CreateCell(5).SetCellValue("VALOR UNITÁRIO");
                row.CreateCell(6).SetCellValue("VALOR DA TRANSACAO");
                row.CreateCell(7).SetCellValue("IDENTIFICADOR DA TRANSACAO");
                row.CreateCell(8).SetCellValue("COD CLIENTE");
                row.CreateCell(9).SetCellValue("NOME CLIENTE");
                row.CreateCell(10).SetCellValue("COD MAQUINA");
                row.CreateCell(11).SetCellValue("NOME MAQUINA");
                row.CreateCell(12).SetCellValue("LUGAR MAQUINA");
                row.CreateCell(13).SetCellValue("COD PRODUTO");
                row.CreateCell(14).SetCellValue("MARCA ATUALIZADO PRODUTO");
                row.CreateCell(15).SetCellValue("NOME ATUALIZADO PRODUTO");
                row.CreateCell(16).SetCellValue("VALOR ATUALIZADO PRODUTO");

                for (int i = 0; i < salesList.Count; i++)
                {
                    var item = salesList[i];

                    var user = users.FirstOrDefault(u => u.Code == item.UserCode);                    
                    if(user == null)
                        user = new UserDto();

                    var machine = machines.FirstOrDefault(u => u.Code == item.MachineCode);                    
                    if(machine == null)
                        machine = new MachineDto();
                    
                    var product = products.FirstOrDefault(u => u.Code == item.ProductCode);                    
                    if(product == null)
                        product = new ProductDto();

                    row = excelSheet.CreateRow(i + 1);
                    row.CreateCell(0).SetCellValue(item.Code);
                    row.CreateCell(1).SetCellValue(item.Trademark);
                    row.CreateCell(2).SetCellValue(item.Title);
                    row.CreateCell(3).SetCellValue(item.BuyDate.Value);
                    row.CreateCell(4).SetCellValue(item.Quantity);
                    row.CreateCell(5).SetCellValue(Convert.ToDouble(item.Value / item.Quantity));
                    row.CreateCell(6).SetCellValue(Convert.ToDouble(item.Value));
                    row.CreateCell(7).SetCellValue(item.TransactionId);
                    row.CreateCell(8).SetCellValue(user.Code);
                    row.CreateCell(9).SetCellValue(user.Name);
                    row.CreateCell(10).SetCellValue(machine.Code);
                    row.CreateCell(11).SetCellValue(machine.Title);
                    row.CreateCell(12).SetCellValue(machine.Place);
                    row.CreateCell(13).SetCellValue(product.Code);
                    row.CreateCell(14).SetCellValue(product.Trademark);
                    row.CreateCell(15).SetCellValue(product.Title);
                    row.CreateCell(16).SetCellValue(Convert.ToDouble(product.Price));
                }

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            File.Delete(filePath);

            memory.Position = 0;

            return memory;
        }
    }
}
