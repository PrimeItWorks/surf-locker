using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Sales.Handlers
{
    public class GetSalesPagingRequestHandler
        : IRequestHandler<GetSalesPagingRequest, ListResponseDto<SalesDto>>
    {
        private ISalesRepository _salesRepository;
        public GetSalesPagingRequestHandler(ISalesRepository salesRepository)
        {
            _salesRepository = salesRepository;
        }

        public async Task<ListResponseDto<SalesDto>> Handle(GetSalesPagingRequest request, CancellationToken cancellationToken)
        {
            var sales = await _salesRepository.GetAllPagingAsync(request.Request);
            return sales;
        }
    }
}