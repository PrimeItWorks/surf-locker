using System.IO;
using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Users
{
    public class ExportUsersToExcelRequest
        : IRequest<MemoryStream>
    {
        public UserRequestAllDto Request { get; }
        public ExportUsersToExcelRequest(UserRequestAllDto request)
        {
            this.Request = request;
        }
    }
}