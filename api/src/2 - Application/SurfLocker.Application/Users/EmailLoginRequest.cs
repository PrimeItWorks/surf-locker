﻿using MediatR;
using SurfLocker.Domain.Users.Jwt;

namespace SurfLocker.Application.Users
{
    public class EmailLoginRequest
         : IRequest<JwtToken>
    {
        public EmailLoginRequest(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; }
        public string Password { get; }
    }
}
