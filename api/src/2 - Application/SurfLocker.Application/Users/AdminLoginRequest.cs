﻿using MediatR;
using SurfLocker.Domain.Users.Jwt;

namespace SurfLocker.Application.Users
{
    public class AdminLoginRequest
         : IRequest<JwtToken>
    {
        public AdminLoginRequest(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; }
        public string Password { get; }
    }
}
