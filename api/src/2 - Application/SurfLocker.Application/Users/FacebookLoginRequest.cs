﻿using MediatR;
using SurfLocker.Domain.Users.Jwt;

namespace SurfLocker.Application.Users
{
    public class FacebookLoginRequest
         : IRequest<JwtToken>
    {
        public string FacebookId { get; }
        public FacebookLoginRequest(string facebookId) => FacebookId = facebookId;
    }
}
