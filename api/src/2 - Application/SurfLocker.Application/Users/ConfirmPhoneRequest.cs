﻿using MediatR;
using SurfLocker.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Application.Users
{
    public class ConfirmPhoneRequest : IRequest<string>
    {
        public ConfirmPhoneRequest(ConfirmPhoneDto request)
        {
            Request = request;
        }

        public ConfirmPhoneDto Request { get; }
    }
}
