﻿using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users
{
    public class GetUserByCodeRequest
        : IRequest<UserDto>

    {
        public string Code { get; }
        public GetUserByCodeRequest(string code) =>
            Code = code;
    }
}
