﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Users
{
    public class UpdateUserRequest
        : IRequest<UserDto>

    {
        public UserDto User { get; }
        public UpdateUserRequest(UserDto user) =>
            User = user;
    }
}
