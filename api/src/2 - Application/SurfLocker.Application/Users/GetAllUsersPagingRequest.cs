﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Users
{
    public class GetAllUsersPagingRequest
        : IRequest<ListResponseDto<UserDto>>

    {
        public UserRequestAllDto Request { get; }
        public GetAllUsersPagingRequest(UserRequestAllDto request) =>
            Request = request;
    }
}
