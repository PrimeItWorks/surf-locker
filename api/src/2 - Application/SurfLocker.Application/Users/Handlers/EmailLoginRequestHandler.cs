﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Jwt;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    internal class EmailLoginRequestHandler
        : IRequestHandler<EmailLoginRequest, JwtToken>
    {
        private readonly IUserRepository _userRepository;
        public EmailLoginRequestHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<JwtToken> Handle(EmailLoginRequest request, CancellationToken cancellationToken)
        {
            var isValid = await _userRepository.ValidateLogin(request.Email, request.Password);
            if (!isValid)
                return null;

            var token = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                .AddSubject(request.Email)
                .AddIssuer("issuerTest")
                .AddAudience("bearerTest")
                .AddClaim("MembershipId", "111")
                .AddExpiry(1)
                .Build();

            return token;
        }
    }
}
