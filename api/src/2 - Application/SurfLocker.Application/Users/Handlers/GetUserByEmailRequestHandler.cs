﻿using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    public class GetUserByEmailRequestHandler
        : IRequestHandler<GetUserByEmailRequest, UserDto>
    {
        private readonly IUserRepository _repository;
        public GetUserByEmailRequestHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserDto> Handle(GetUserByEmailRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetByEmail(request.Email);
        }
    }
}
