﻿using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Dto;

namespace SurfLocker.Application.Users.Handlers
{
    public class UpdateUserRequestHandler
        : IRequestHandler<UpdateUserRequest, UserDto>
    {
        private readonly IUserFactory _factory;
        private readonly IUserRepository _repository;

        public UpdateUserRequestHandler(IUserFactory factory,IUserRepository repository)
        {
            _factory = factory;
            _repository = repository;
        }

        public async Task<UserDto> Handle(UpdateUserRequest notification, CancellationToken cancellationToken)
        {
            var toUpdate = await _repository.GetByCodeAsync(notification.User.Code);
            var updated = await _factory
                .DefaultBuilder()
                .WithCardToken(toUpdate.CardToken)
                .WithCardBrand((int)toUpdate.CardBrand)
                .WithLatestDigitCard(toUpdate.LatestDigitCard)
                .WithAddress(notification.User.Address)
                .WithCode(notification.User.Code)
                .WithCpf(toUpdate.Cpf)
                .WithBirthday(toUpdate.Birthday)
                .WithEmail(notification.User.Email)
                .WithFacebookId(toUpdate.FacebookId)
                .WithName(notification.User.Name)
                .WithPhone(notification.User.Phone)
                .WithWaves(notification.User.Waves)
                .Raise()
                .UpdateAsync();

            return updated;
        }
    }
}
