﻿using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Users.Handlers
{
    public class GetAllUsersPagingRequestHandler
        : IRequestHandler<GetAllUsersPagingRequest, ListResponseDto<UserDto>>
    {
        private readonly IUserRepository _repository;
        public GetAllUsersPagingRequestHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<UserDto>> Handle(GetAllUsersPagingRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}
