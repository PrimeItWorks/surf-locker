﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Jwt;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System.Security.Claims;
using SurfLocker.Domain.Users.Users;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace SurfLocker.Application.Users.Handlers
{
    internal class ExportUsersToExcelRequestHandler
        : IRequestHandler<ExportUsersToExcelRequest, MemoryStream>
    {
        private readonly IUserRepository _userRepository;
        private readonly IHostingEnvironment _env;

        public ExportUsersToExcelRequestHandler(IUserRepository userRepository
            , IHostingEnvironment env)
        {
            _userRepository = userRepository;
            _env = env;
        } 
                

        public async Task<MemoryStream> Handle(ExportUsersToExcelRequest request, CancellationToken cancellationToken)
        {
            request.Request.PageSize = 999999;
            request.Request.Page = 1;
            var users = await _userRepository.GetAllPagingAsync(request.Request);

            string sWebRootFolder = _env.WebRootPath;
            string sFileName = $@"{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            var filePath = Path.Combine(sWebRootFolder, sFileName);
            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Usuários");
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("CODIGO");
                row.CreateCell(1).SetCellValue("NOME");
                row.CreateCell(2).SetCellValue("E-MAIL");
                row.CreateCell(3).SetCellValue("TELEFONE");
                row.CreateCell(4).SetCellValue("CPF");
                row.CreateCell(5).SetCellValue("FACEBOOK ID");
                row.CreateCell(6).SetCellValue("ENDEREÇO");
                row.CreateCell(7).SetCellValue("PRAIAS");

                var usersList = users.Items;
                for (int i = 0; i < usersList.Count; i++)
                {
                    var item = usersList[i];

                    row = excelSheet.CreateRow(i + 1);
                    row.CreateCell(0).SetCellValue(item.Code);
                    row.CreateCell(1).SetCellValue(item.Name);
                    row.CreateCell(2).SetCellValue(item.Email);
                    row.CreateCell(3).SetCellValue(item.Phone);
                    row.CreateCell(4).SetCellValue(item.Cpf);
                    row.CreateCell(5).SetCellValue(item.FacebookId);
                    row.CreateCell(6).SetCellValue(item.Address);

                    if (item.Waves.Length > 0) 
                        row.CreateCell(6).SetCellValue(string.Join(" | ", item.Waves));
                }

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            File.Delete(filePath);

            memory.Position = 0;

            return memory;
        }
    }
}
