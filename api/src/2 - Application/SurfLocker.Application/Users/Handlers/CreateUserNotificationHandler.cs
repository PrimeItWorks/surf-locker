﻿using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;

namespace SurfLocker.Application.Users.Handlers
{
    public class CreateUserNotificationHandler
        : INotificationHandler<CreateUserNotification>
    {
        private readonly IUserFactory _factory;
        public CreateUserNotificationHandler(IUserFactory factory)
        {
            _factory = factory;
        }

        public async Task Handle(CreateUserNotification notification, CancellationToken cancellationToken)
        {
            await _factory
                .DefaultBuilder()
                .WithCode(Guid.NewGuid().ToString())
                .WithFacebookId(notification.User.FacebookId)
                .WithEmail(notification.User.Email)
                .WithIsAdmin(notification.User.IsAdmin)
                .WithPassword(notification.User.Password)
                .WithName(notification.User.Name)
                .WithPhone(notification.User.Phone)
                .WithAddress(notification.User.Address)
                .WithCardToken(notification.User.CardToken)
                .WithWaves(notification.User.Waves)
                .Raise()
                .AddAsync();
        }
    }
}
