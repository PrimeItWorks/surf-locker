﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Jwt;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    internal class FacebookLoginRequestHandler
        : IRequestHandler<FacebookLoginRequest, JwtToken>
    {
        private readonly IUserRepository _userRepository;
        public FacebookLoginRequestHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<JwtToken> Handle(FacebookLoginRequest request, CancellationToken cancellationToken)
        {
            var isValid = await _userRepository.ValidateLogin(request.FacebookId);
            if (!isValid)
                return null;

            var token = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                .AddSubject(request.FacebookId)
                .AddIssuer("issuerTest")
                .AddAudience("bearerTest")
                .AddClaim("MembershipId", "111")
                .AddExpiry(1)
                .Build();

            return token;
        }
    }
}
