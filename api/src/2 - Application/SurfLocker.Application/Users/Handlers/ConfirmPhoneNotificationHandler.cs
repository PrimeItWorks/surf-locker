﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    public class ConfirmPhoneNotificationHandler
        : INotificationHandler<ConfirmPhoneNotification>
    {
        private readonly IUserRepository _repo;
        public ConfirmPhoneNotificationHandler(IUserRepository repo)
        {
            _repo = repo;
        }

        public async Task Handle(ConfirmPhoneNotification notification, CancellationToken cancellationToken)
        {
            await _repo.UpdateFieldAsync(notification.UserCode, nameof(User.ConfirmedNumber), true);
        }
    }
}
