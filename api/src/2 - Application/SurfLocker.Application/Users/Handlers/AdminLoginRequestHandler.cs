﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Jwt;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System.Security.Claims;
using SurfLocker.Domain.Users.Users;

namespace SurfLocker.Application.Users.Handlers
{
    internal class AdminLoginRequestHandler
        : IRequestHandler<AdminLoginRequest, JwtToken>
    {
        private readonly IUserRepository _userRepository;
        public AdminLoginRequestHandler(IUserRepository userRepository) 
            => _userRepository = userRepository;

        public async Task<JwtToken> Handle(AdminLoginRequest request, CancellationToken cancellationToken)
        {
            var isValid = await _userRepository.ValidateLogin(request.Email, request.Password, true);
            if (!isValid)
                return null;

            var token = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                .AddSubject(request.Email)
                .AddIssuer("issuerTest")
                .AddAudience("bearerTest")
                .AddClaim(ClaimTypes.Role, UserRoles.ADMIN)
                .AddExpiry(1)
                .Build();

            return token;
        }
    }
}
