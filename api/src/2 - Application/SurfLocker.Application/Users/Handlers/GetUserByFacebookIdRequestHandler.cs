﻿using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    public class GetUserByFacebookIdRequestHandler
        : IRequestHandler<GetUserByFacebookIdRequest, UserDto>
    {
        private readonly IUserRepository _repository;
        public GetUserByFacebookIdRequestHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserDto> Handle(GetUserByFacebookIdRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetByFacebookId(request.FacebookId);
        }
    }
}
