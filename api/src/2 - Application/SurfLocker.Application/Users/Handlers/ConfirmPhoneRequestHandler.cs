﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Infra.Sms.Interfaces;
using Microsoft.Extensions.Configuration;
using SurfLocker.Dto.Sms;

namespace SurfLocker.Application.Users.Handlers
{
    internal class ConfirmPhoneRequestHandler
        : IRequestHandler<ConfirmPhoneRequest, string>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserFactory _factory;

        private readonly ISmsService _service;
        private readonly string _sender;

        public ConfirmPhoneRequestHandler(IUserRepository userRepository
            , IUserFactory factory
            , ISmsService service
            , IConfiguration configuration)
        {
            _userRepository = userRepository;
            _factory = factory;
            _service = service;
            _sender = configuration["SMSSender"];
        }

        public async Task<string> Handle(ConfirmPhoneRequest request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetByCodeAsync(request.Request.UserCode);
            user.Phone = request.Request.Phone;

            var userToUpdate =  _factory.DefaultBuilder()
                .WithCode(user.Code)
                .WithAddress(user.Address)
                .WithCardBrand((int)user.CardBrand)
                .WithCardToken(user.CardToken)
                .WithConfirmedNumber(false)
                .WithCpf(user.Cpf)
                .WithEmail(user.Email)
                .WithFacebookId(user.FacebookId)
                .WithLatestDigitCard(user.LatestDigitCard)
                .WithName(user.Name)
                .WithPhone(user.Phone)
                .WithWaves(user.Waves)
                .Raise();

            var updated = await userToUpdate
                .UpdateAsync();

            var code = userToUpdate.GeneratePhoneConfirmation();
            //var sms = new SMSRequestDto()
            //{
            //    From = _sender,
            //    To = request.Request.Phone,
            //    Text = $"Seu codigo de verificacao e {code}"
            //};
            //var response = _service.Send(sms);

            return code;
        }
    }
}
