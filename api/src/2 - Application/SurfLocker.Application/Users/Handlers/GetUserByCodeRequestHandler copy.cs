﻿using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users.Handlers
{
    public class GetUserByCodeRequestHandler
        : IRequestHandler<GetUserByCodeRequest, UserDto>
    {
        private readonly IUserRepository _repository;
        public GetUserByCodeRequestHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<UserDto> Handle(GetUserByCodeRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetByCodeAsync(request.Code);
        }
    }
}
