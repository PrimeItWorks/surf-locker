﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Users
{
    public class CreateUserNotification
        : INotification

    {
        public UserDto User { get; }
        public CreateUserNotification(UserDto user) =>
            User = user;
    }
}
