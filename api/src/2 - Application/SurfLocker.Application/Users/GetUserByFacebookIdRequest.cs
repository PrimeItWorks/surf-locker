﻿using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users
{
    public class GetUserByFacebookIdRequest
        : IRequest<UserDto>

    {
        public string FacebookId { get; }
        public GetUserByFacebookIdRequest(string facebookid) =>
            FacebookId = facebookid;
    }
}
