﻿using MediatR;

namespace SurfLocker.Application.Users
{
    public class ConfirmPhoneNotification : INotification
    {
        public ConfirmPhoneNotification(string userCode)
        {
            UserCode = userCode;
        }

        public string UserCode { get; }
    }
}
