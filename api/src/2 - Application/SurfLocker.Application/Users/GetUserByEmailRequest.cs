﻿using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Users
{
    public class GetUserByEmailRequest
        : IRequest<UserDto>

    {
        public string Email { get; }
        public GetUserByEmailRequest(string email) 
            => Email = email;
    }
}
