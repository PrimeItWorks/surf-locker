﻿using MediatR;
using SurfLocker.Dto.Payment.Request;
using SurfLocker.Dto.Payment.Response;

namespace SurfLocker.Application.Payment
{
    public class RequestVaultKeyRequest
        : IRequest<RequestVaultKeyResponseDto>
    {
    }
}
