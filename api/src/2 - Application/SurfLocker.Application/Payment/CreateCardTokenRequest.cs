﻿using MediatR;
using SurfLocker.Dto.Payment.Request;
using SurfLocker.Dto.Payment.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Application.Payment
{
    public class CreateCardTokenRequest : IRequest<CreateCardTokenResponseDto>
    {
        public CreateCardTokenRequestApiDto Request { get; }

        public CreateCardTokenRequest(CreateCardTokenRequestApiDto request)
        {
            Request = request;
        }
    }
}
