﻿using MediatR;
using SurfLocker.Dto.Payment;

namespace SurfLocker.Application.Payment
{
    public class GetPublicApiTokenRequest 
        : IRequest<TokensDto>
    {
    }
}
