﻿using MediatR;
using SurfLocker.Dto.Payment.Request;

namespace SurfLocker.Application.Payment
{
    public class CreateTransactiontNotification : INotification
    {
        public CreateTransactionRequestDto Request { get; }

        public CreateTransactiontNotification(CreateTransactionRequestDto request)
        {
            Request = request;
        }
    }
}
