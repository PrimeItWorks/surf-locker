﻿using SurfLocker.Infra.GatewayPayment.Interfaces;
using SurfLocker.Dto.Payment.Response;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Interfaces.Factory;

namespace SurfLocker.Application.Payment.Handlers
{
    public class CreateCardTokenRequestRequestHandler
        : IRequestHandler<CreateCardTokenRequest, CreateCardTokenResponseDto>
    {
        private readonly IPaymentService _paymentService;
        private readonly IUserRepository _userRepository;
        private readonly IUserFactory _userFactory;

        public CreateCardTokenRequestRequestHandler(IPaymentService paymentService
            , IUserRepository userRepository
            , IUserFactory userFactory)
        {
            _paymentService = paymentService;
            _userRepository = userRepository;
            _userFactory = userFactory;
        }

        public async Task<CreateCardTokenResponseDto> Handle(CreateCardTokenRequest request, CancellationToken cancellationToken)
        {
            var response = await _paymentService.CreateCardToken(request.Request.GatewayRequest);

            if (response == null || string.IsNullOrEmpty(request.Request.UserCode))
                return response;

            var toUpdate = await _userRepository.GetByCodeAsync(request.Request.UserCode);
            await _userFactory
                .DefaultBuilder()
                .WithCardToken(response.cardToken)
                .WithCardBrand(response.brandId)
                .WithLatestDigitCard(response.lastDigits)
                .WithAddress(toUpdate.Address)
                .WithCode(toUpdate.Code)
                .WithCpf(request.Request.Cpf)
                .WithBirthday(request.Request.Birthday)
                .WithEmail(toUpdate.Email)
                .WithFacebookId(toUpdate.FacebookId)
                .WithName(toUpdate.Name)
                .WithPhone(toUpdate.Phone)
                .WithWaves(toUpdate.Waves)
                .Raise()
                .UpdateAsync();

            return response;
        }
    }
}
