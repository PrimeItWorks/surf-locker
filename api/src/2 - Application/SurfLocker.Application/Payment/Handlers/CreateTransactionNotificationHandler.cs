﻿using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Infra.GatewayPayment.Interfaces;
using SurfLocker.Domain.Users.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Domain.Store.Interfaces.Factory;
using System;
using SurfLocker.Domain.Store.Interfaces;

namespace SurfLocker.Application.Payment.Handlers
{
    public class CreateTransactionNotificationHandler : INotificationHandler<CreateTransactiontNotification>
    {
        private readonly IPaymentService _paymentService;
        private readonly IProductRepository _productRepository;
        private readonly IUserFactory _userFactory;
        private readonly ISaleFactory _saleFactory;
        private readonly INotification _notification;

        public CreateTransactionNotificationHandler(
            IPaymentService paymentService
            , IProductRepository productRepository
            , IUserFactory userFactory
            , ISaleFactory saleFactory
            , INotification notification)
        {
            _paymentService = paymentService;
            _productRepository = productRepository;
            _userFactory = userFactory;
            _saleFactory = saleFactory;
            _notification = notification;
        }

        public async Task Handle(CreateTransactiontNotification notification, CancellationToken cancellationToken)
        {
            var paymentResponse = await _paymentService.CreateTransaction(notification.Request);
            if (paymentResponse == null)
                return;

            switch (paymentResponse.status)
            {
                case 1:
                case 10:
                case 15:
                    _notification
                       .DefaultBuilder()
                       .WithCode("PAYMENT_TRANSACTION_REFUSED")
                       .WithMessage("Transação recusada.")
                       .WithDetailMessage("")
                       .RaiseNotification();
                    return;
                default:
                    break;
            }   

            var toUpdate = _userFactory
                .DefaultBuilder()
                .WithCode(notification.Request.customerInfo.UserCode)
                .WithBirthday(Convert.ToDateTime(notification.Request.customerInfo.birthday))
                .WithCpf(notification.Request.customerInfo.cpf)
                .Raise();

            await toUpdate.UpdateCpfAsync();
            await toUpdate.UpdateBirthdayAsync();

            var product = await _productRepository.GetByCodeAsync(notification.Request.customerInfo.ProductCode);
            
            await _saleFactory
                .DefaultBuilder()
                .WithCode(Guid.NewGuid().ToString())
                .WithTransactonId(paymentResponse.transactionId)
                .WithUserCode(notification.Request.customerInfo.UserCode)
                .WithProductCode(product.Code)
                .WithTitle(product.Title)
                .WithTrademark(product.Trademark)
                .WithValue(notification.Request.amount/100)
                .WithOnline(true)
                .WithQuantity(notification.Request.customerInfo.Quantity)
                .Raise()
                .AddAsync();
        }
    }
}
