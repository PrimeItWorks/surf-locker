﻿using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Infra.GatewayPayment.Interfaces;

namespace SurfLocker.Application.Payment.Handlers
{
    public class SetCustomerInfoNotificationHandler : INotificationHandler<SetCustomerInfoNotification>
    {
        private readonly IPaymentService _paymentService;

        public SetCustomerInfoNotificationHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task Handle(SetCustomerInfoNotification notification, CancellationToken cancellationToken)
        {
            await _paymentService.SetCustomerInfo(notification.Request);
        }
    }
}
