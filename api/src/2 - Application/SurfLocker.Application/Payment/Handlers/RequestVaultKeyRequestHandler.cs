﻿using SurfLocker.Infra.GatewayPayment.Interfaces;
using SurfLocker.Dto.Payment.Response;
using System.Threading.Tasks;
using System.Threading;
using MediatR;

namespace SurfLocker.Application.Payment.Handlers
{
    public class RequestVaultKeyRequestHandler 
        : IRequestHandler<RequestVaultKeyRequest, RequestVaultKeyResponseDto>
    {
        private readonly IPaymentService _service;

        public RequestVaultKeyRequestHandler(IPaymentService service)
        {
            _service = service;
        }

        public async Task<RequestVaultKeyResponseDto> Handle(RequestVaultKeyRequest request, CancellationToken cancellationToken)
        {
            var response = await _service.RequestVaultKeyAsync();
            return response;
        }
    }
}
