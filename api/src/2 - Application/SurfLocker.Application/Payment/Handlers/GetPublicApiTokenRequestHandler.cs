﻿using SurfLocker.Infra.GatewayPayment.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Dto.Payment;

namespace SurfLocker.Application.Payment.Handlers
{
    public class GetPublicApiTokenRequestHandler : IRequestHandler<GetPublicApiTokenRequest, TokensDto>
    {
        private readonly IPaymentService _service;
        public GetPublicApiTokenRequestHandler(IPaymentService service)
        {
            _service = service;
        }

        public Task<TokensDto> Handle(GetPublicApiTokenRequest request, CancellationToken cancellationToken)
        {
            var publicApiToken = _service.GetPublicApiToken();
            var merchantKey = _service.GetMerchantKey();
            var tokens = new TokensDto()
            {
                MerchantKey = merchantKey,
                PublicApiToken = publicApiToken
            };

            return Task.FromResult(tokens);
        }
    }
}
