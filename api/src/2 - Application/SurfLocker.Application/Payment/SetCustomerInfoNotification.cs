﻿using SurfLocker.Dto.Payment.Request;
using MediatR;

namespace SurfLocker.Application.Payment
{
    public class SetCustomerInfoNotification : INotification
    {
        public SetCustomerInfoRequestDto Request { get; }

        public SetCustomerInfoNotification(SetCustomerInfoRequestDto request)
        {
            Request = request;
        }
    }
}
