﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machine
{
    public class ResumeSaleAfterBuyMoneyNotification
        : INotification
    {
        public string MachineCode { get; }
        public string ProductCode { get; }
        public ResumeSaleAfterBuyMoneyNotification(string machineCode, string productCode)
        {
            MachineCode = machineCode;
            ProductCode = productCode;
        }

    }
}
