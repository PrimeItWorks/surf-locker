﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machines
{
    public class UpdateMachineRequest
        : IRequest<MachineDto>
    {
        public MachineDto Machine { get; }
        public UpdateMachineRequest(MachineDto user) =>
            Machine = user;
    }
}
