﻿using SurfLocker.Domain.Machines.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Machines.Handlers
{
    public class GetMachineByCodeRequestHandler
        : IRequestHandler<GetMachineByCodeRequest, MachineDto>
    {
        private readonly IMachineRepository _repository;
        public GetMachineByCodeRequestHandler(IMachineRepository repository)
        {
            _repository = repository;
        }

        public async Task<MachineDto> Handle(GetMachineByCodeRequest request, CancellationToken cancellationToken)
        {
            var machine = await _repository.GetByCodeAsync(request.Code);
         
            return machine;
        }
    }
}
