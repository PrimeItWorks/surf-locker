﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Machines.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;

namespace SurfLocker.Application.Machines.Handlers
{
    public class DeleteMachineNotificationHandler
        : INotificationHandler<DeleteMachineNotification>
    {
        private readonly IMachineRepository _repo;
        public DeleteMachineNotificationHandler(IMachineRepository repo)
        {
            _repo = repo;
        }

        public async Task Handle(DeleteMachineNotification notification, CancellationToken cancellationToken)
        {
            await _repo.DeleteAsync(notification.Code);
        }
    }
}
