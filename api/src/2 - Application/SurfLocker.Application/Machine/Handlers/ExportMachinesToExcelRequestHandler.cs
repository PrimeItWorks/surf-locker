﻿using SurfLocker.Domain.Machines.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System.Security.Claims;
using SurfLocker.Domain.Machines;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace SurfLocker.Application.Machines.Handlers
{
    internal class ExportMachinesToExcelRequestHandler
        : IRequestHandler<ExportMachinesToExcelRequest, MemoryStream>
    {
        private readonly IMachineRepository _userRepository;
        private readonly IHostingEnvironment _env;

        public ExportMachinesToExcelRequestHandler(IMachineRepository userRepository
            , IHostingEnvironment env)
        {
            _userRepository = userRepository;
            _env = env;
        } 
                

        public async Task<MemoryStream> Handle(ExportMachinesToExcelRequest request, CancellationToken cancellationToken)
        {
            request.Request.PageSize = 999999;
            request.Request.Page = 1;
            var users = await _userRepository.GetAllPagingAsync(request.Request);

            string sWebRootFolder = _env.WebRootPath;
            string sFileName = $@"{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var memory = new MemoryStream();
            var filePath = Path.Combine(sWebRootFolder, sFileName);
            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook;
                workbook = new XSSFWorkbook();
                ISheet excelSheet = workbook.CreateSheet("Máquinas");
                IRow row = excelSheet.CreateRow(0);

                row.CreateCell(0).SetCellValue("CODIGO");
                row.CreateCell(1).SetCellValue("NOME");
                row.CreateCell(2).SetCellValue("LUGAR");
                row.CreateCell(3).SetCellValue("PRODUTO 1 CÓDIGO");
                row.CreateCell(4).SetCellValue("PRODUTO 1 NOME");
                row.CreateCell(5).SetCellValue("PRODUTO 1 QUANTIDADE");
                row.CreateCell(6).SetCellValue("PRODUTO 1 VALOR");
                row.CreateCell(7).SetCellValue("PRODUTO 2 CÓDIGO");
                row.CreateCell(8).SetCellValue("PRODUTO 2 NOME");
                row.CreateCell(9).SetCellValue("PRODUTO 2 QUANTIDADE");
                row.CreateCell(10).SetCellValue("PRODUTO 2 VALOR");
                row.CreateCell(11).SetCellValue("PRODUTO 3 CÓDIGO");
                row.CreateCell(12).SetCellValue("PRODUTO 3 NOME");
                row.CreateCell(13).SetCellValue("PRODUTO 3 QUANTIDADE");
                row.CreateCell(14).SetCellValue("PRODUTO 3 VALOR");
                row.CreateCell(15).SetCellValue("LATITUDE");
                row.CreateCell(16).SetCellValue("LONGITUDE");

                var usersList = users.Items;
                for (int i = 0; i < usersList.Count; i++)
                {
                    var item = usersList[i];

                    row = excelSheet.CreateRow(i + 1);
                    row.CreateCell(0).SetCellValue(item.Code);
                    row.CreateCell(1).SetCellValue(item.Title);
                    row.CreateCell(2).SetCellValue(item.Place);
                    row.CreateCell(3).SetCellValue(item.ProdCod1);
                    row.CreateCell(4).SetCellValue(item.ProdName1);
                    row.CreateCell(5).SetCellValue(item.ProdQtd1);
                    row.CreateCell(6).SetCellValue(Convert.ToDouble(item.ProdValue1));
                    row.CreateCell(7).SetCellValue(item.ProdCod2);
                    row.CreateCell(8).SetCellValue(item.ProdName2);
                    row.CreateCell(9).SetCellValue(item.ProdQtd2);
                    row.CreateCell(10).SetCellValue(Convert.ToDouble(item.ProdValue3));
                    row.CreateCell(11).SetCellValue(item.ProdCod3);
                    row.CreateCell(12).SetCellValue(item.ProdName3);
                    row.CreateCell(13).SetCellValue(item.ProdQtd3);
                    row.CreateCell(14).SetCellValue(Convert.ToDouble(item.ProdValue3));
                    row.CreateCell(15).SetCellValue(item.Lat);
                    row.CreateCell(16).SetCellValue(item.Long);
                }

                workbook.Write(fs);
            }

            using (var stream = new FileStream(Path.Combine(sWebRootFolder, sFileName), FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            File.Delete(filePath);

            memory.Position = 0;

            return memory;
        }
    }
}
