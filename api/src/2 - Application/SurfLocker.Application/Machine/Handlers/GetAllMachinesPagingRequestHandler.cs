﻿using SurfLocker.Domain.Machines.Interfaces;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Machines.Handlers
{
    public class GetAllMachinesPagingRequestHandler
        : IRequestHandler<GetAllMachinePagingRequest, ListResponseDto<MachineDto>>
    {
        private readonly IMachineRepository _repository;
        public GetAllMachinesPagingRequestHandler(IMachineRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<MachineDto>> Handle(GetAllMachinePagingRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}
