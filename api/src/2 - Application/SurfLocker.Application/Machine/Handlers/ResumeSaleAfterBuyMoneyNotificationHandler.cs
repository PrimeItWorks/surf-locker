﻿using SurfLocker.Domain.Machines.Interfaces.Factory;
using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Machines.Interfaces;
using SurfLocker.Domain.Store.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;

namespace SurfLocker.Application.Machine.Handlers
{
    public class ResumeSaleAfterBuyMoneyNotificationHandler : INotificationHandler<ResumeSaleAfterBuyMoneyNotification>
    {
        private readonly IProductRepository _productRepository;
        private readonly IMachineRepository _machineRepository;
        private readonly ISaleFactory _saleFactory;
        private readonly IMachineFactory _machineFactory;

        public ResumeSaleAfterBuyMoneyNotificationHandler(
            IProductRepository productRepository
            , IMachineRepository machineRepository
            , ISaleFactory saleFactory
            , IMachineFactory machineFactory)
        {
            _productRepository = productRepository;
            _machineRepository = machineRepository;
            _saleFactory = saleFactory;
            _machineFactory = machineFactory;
        }

        public async Task Handle(ResumeSaleAfterBuyMoneyNotification notification, CancellationToken cancellationToken)
        {
            var machine = await _machineRepository.GetByCodeAsync(notification.MachineCode);
            var product = await _productRepository.GetByCodeAsync(notification.ProductCode);
            
            var machineToIncrement = _machineFactory
                .DefaultBuilder()
                .WithCode(machine.Code)
                .Raise();

            if (machine.ProdCod1 == notification.ProductCode) 
                await machineToIncrement.DecrementProdQtd1();
            else if (machine.ProdCod2 == notification.ProductCode) 
                await machineToIncrement.DecrementProdQtd2();
            else if (machine.ProdCod3 == notification.ProductCode) 
                await machineToIncrement.DecrementProdQtd3();

            await _saleFactory
                .DefaultBuilder()
                .WithCode(Guid.NewGuid().ToString())
                .WithMachineCode(notification.MachineCode)
                .WithProductCode(notification.ProductCode)
                .WithTitle(product.Title)
                .WithTrademark(product.Trademark)
                .WithValue(product.Price)
                .WithOnline(false)
                .WithQuantity(1)
                .Raise()
                .AddAsync();
        }
    }
}
