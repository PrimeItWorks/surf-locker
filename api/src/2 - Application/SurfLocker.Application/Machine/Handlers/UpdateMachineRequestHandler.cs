﻿using SurfLocker.Domain.Machines.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;
using SurfLocker.Domain.Store.Interfaces;

namespace SurfLocker.Application.Machines.Handlers
{
    public class UpdateMachineRequestHandler
        : IRequestHandler<UpdateMachineRequest, MachineDto>
    {
        private readonly IMachineFactory _factory;
        private readonly IProductRepository _productRepository;

        public UpdateMachineRequestHandler(IMachineFactory factory
            , IProductRepository productRepository)
        {
            _factory = factory;
            _productRepository = productRepository;
        }

        public async Task<MachineDto> Handle(UpdateMachineRequest request, CancellationToken cancellationToken)
        {
            var machine = request.Machine;

            var builder = _factory
                .DefaultBuilder()
                .WithCode(machine.Code)
                .WithTitle(machine.Title)
                .WithPlace(machine.Place)
                .WithLat(machine.Lat)
                .WithLong(machine.Long);

            if (!string.IsNullOrEmpty(machine.ProdCod1))
            {
                var product = await _productRepository.GetByCodeAsync(machine.ProdCod1);
                builder
                    .WithProdCode1(machine.ProdCod1)
                    .WithProdName1($"{product.Trademark} {product.Title}")
                    .WithProdQtd1(machine.ProdQtd1)
                    .WithProdValue1(product.Price);
            }

            if (!string.IsNullOrEmpty(machine.ProdCod1))
            {
                var product = await _productRepository.GetByCodeAsync(machine.ProdCod2);
                builder
                    .WithProdCode2(machine.ProdCod2)
                    .WithProdName2($"{product.Trademark} {product.Title}")
                    .WithProdQtd2(machine.ProdQtd2)
                    .WithProdValue2(product.Price);
            }

            if (!string.IsNullOrEmpty(machine.ProdCod3))
            {
                var product = await _productRepository.GetByCodeAsync(machine.ProdCod3);
                builder
                    .WithProdCode3(machine.ProdCod3)
                    .WithProdName3($"{product.Trademark} {product.Title}")
                    .WithProdQtd3(machine.ProdQtd3)
                    .WithProdValue3(product.Price);
            }

            return await builder
                .Raise()
                .UpdateAsync();
        }
    }
}
