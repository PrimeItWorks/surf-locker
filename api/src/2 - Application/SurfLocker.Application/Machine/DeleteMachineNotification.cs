﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machines
{
    public class DeleteMachineNotification
        : INotification
    {
        public string Code { get; }
        public DeleteMachineNotification(string code) =>
            Code = code;
    }
}
