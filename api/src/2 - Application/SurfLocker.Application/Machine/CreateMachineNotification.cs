﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machines
{
    public class CreateMachineNotification
        : INotification

    {
        public MachineDto Machine { get; }
        public CreateMachineNotification(MachineDto machine) =>
            Machine = machine;
    }
}
