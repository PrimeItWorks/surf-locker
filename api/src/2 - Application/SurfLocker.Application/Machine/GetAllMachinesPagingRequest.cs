﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Machines
{
    public class GetAllMachinePagingRequest
        : IRequest<ListResponseDto<MachineDto>>
    {
        public MachineRequestAllDto Request { get; }
        public GetAllMachinePagingRequest(MachineRequestAllDto request) =>
            Request = request;
    }
}
