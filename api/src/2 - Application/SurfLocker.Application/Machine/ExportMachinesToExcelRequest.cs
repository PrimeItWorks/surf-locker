using System.IO;
using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machines
{
    public class ExportMachinesToExcelRequest
        : IRequest<MemoryStream>
    {
        public MachineRequestAllDto Request { get; }
        public ExportMachinesToExcelRequest(MachineRequestAllDto request)
        {
            this.Request = request;
        }
    }
}