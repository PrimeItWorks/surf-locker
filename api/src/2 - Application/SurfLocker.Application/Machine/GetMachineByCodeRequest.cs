﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Machines
{
    public class GetMachineByCodeRequest
        : IRequest<MachineDto>
    {
        public string Code { get; }
        public GetMachineByCodeRequest(string code) =>
            Code = code;
    }
}
