﻿using SurfLocker.Dto.Sms;
using MediatR;

namespace SurfLocker.Application.Sms
{
    public class SendSmsRequest : IRequest<SMSResponseDTo>
    {
        public SMSRequestDto Request { get; }

        public SendSmsRequest(SMSRequestDto request)
        {
            Request = request;
        }
    }
}
