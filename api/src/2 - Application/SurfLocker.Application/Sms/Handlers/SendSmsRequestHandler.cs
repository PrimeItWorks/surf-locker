﻿using SurfLocker.Infra.Sms.Interfaces;
using System.Threading.Tasks;
using SurfLocker.Dto.Sms;
using System.Threading;
using MediatR;
using System;
using Microsoft.Extensions.Configuration;

namespace SurfLocker.Application.Sms.Handlers
{
    public class SendSmsRequestHandler : IRequestHandler<SendSmsRequest, SMSResponseDTo>
    {
        private readonly ISmsService _service;
        private readonly string _sender;

        public SendSmsRequestHandler(ISmsService service
            , IConfiguration configuration)
        {
            _service = service;
            _sender = configuration["SMSSender"];
        }

        public Task<SMSResponseDTo> Handle(SendSmsRequest request, CancellationToken cancellationToken)
        {
            request.Request.From = _sender;
            var response = _service.Send(request.Request);

            return Task.FromResult(response);
        }
    }
}
