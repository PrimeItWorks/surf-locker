﻿using MediatR;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;

namespace SurfLocker.Application.Vouchers
{
    public class GetAllVoucherPagingRequest
        : IRequest<ListResponseDto<VoucherDto>>
    {
        public VoucherRequestAllDto Request { get; }
        public GetAllVoucherPagingRequest(VoucherRequestAllDto request) =>
            Request = request;
    }
}
