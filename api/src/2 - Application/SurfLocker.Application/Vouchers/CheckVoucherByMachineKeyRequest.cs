﻿using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Vouchers
{
    public class CheckVoucherByMachineKeyRequest
        : IRequest<CheckVoucherResponseDto>
    {
        public string MachineKey { get; }
        public CheckVoucherByMachineKeyRequest(string machineKey) =>
            MachineKey = machineKey;
    }
}
