﻿using MediatR;

namespace SurfLocker.Application.Vouchers
{
    public class ResumeVoucherAfterBuyNotification
        : INotification
    {
        public string MachineKey { get; }
        public string MachineCode { get; }

        public ResumeVoucherAfterBuyNotification(string machineKey, string machineCode)
        {
            MachineKey = machineKey;
            MachineCode = machineCode;
        }
    }
}
