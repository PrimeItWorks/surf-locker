﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Dto;

namespace SurfLocker.Application.Vouchers.Handlers
{
    public class CreateVoucherRequestHandler
        : IRequestHandler<CreateVoucherRequest, VoucherDto>
    {
        private readonly IVoucherFactory _factory;
        private readonly IProductRepository _productRepository;

        public CreateVoucherRequestHandler(IVoucherFactory factory
            , IProductRepository productRepository)
        {
            _factory = factory;
            _productRepository = productRepository;
        }

        public async Task<VoucherDto> Handle(CreateVoucherRequest notification, CancellationToken cancellationToken)
        {
            var product = await _productRepository.GetByCodeAsync(notification.Voucher.ProductCode);
            var result = new VoucherDto();

            for (int i = 0; i < notification.Voucher.Quantity; i++)
            {
                result = await _factory
                    .DefaultBuilder()
                    .WithCode(Guid.NewGuid().ToString())
                    .WithProductCode(notification.Voucher.ProductCode)
                    .WithQuantity(notification.Voucher.Quantity)
                    .WithUserCode(notification.Voucher.UserCode)
                    .WithDescription(product.Description)
                    .WithThumbnail(product.Thumbnail)
                    .WithPrice(product.Price)
                    .WithTitle(product.Title)
                    .WithTrademark(product.Trademark)
                    .GenerateMachineKey()
                    .Raise()
                    .AddAsync();
            }

            return result;
        }
    }
}
