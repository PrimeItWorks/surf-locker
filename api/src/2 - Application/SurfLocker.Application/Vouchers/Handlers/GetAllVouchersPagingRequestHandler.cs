﻿using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using System.Threading;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Vouchers.Handlers
{
    public class GetAllVouchersPagingRequestHandler
        : IRequestHandler<GetAllVoucherPagingRequest, ListResponseDto<VoucherDto>>
    {
        private readonly IVoucherRepository _repository;
        public GetAllVouchersPagingRequestHandler(IVoucherRepository repository)
        {
            _repository = repository;
        }

        public async Task<ListResponseDto<VoucherDto>> Handle(GetAllVoucherPagingRequest request, CancellationToken cancellationToken)
        {
            return await _repository.GetAllPagingAsync(request.Request);
        }
    }
}
