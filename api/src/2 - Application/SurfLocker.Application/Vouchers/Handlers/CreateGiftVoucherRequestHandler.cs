﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Dto;

namespace SurfLocker.Application.Vouchers.Handlers
{
    public class CreateGiftVoucherRequestHandler
        : IRequestHandler<CreateGiftVoucherRequest, VoucherDto>
    {
        private readonly IVoucherFactory _factory;
        private readonly IProductRepository _productRepository;

        public CreateGiftVoucherRequestHandler(IVoucherFactory factory
            , IProductRepository productRepository)
        {
            _factory = factory;
            _productRepository = productRepository;
        }

        public async Task<VoucherDto> Handle(CreateGiftVoucherRequest notification, CancellationToken cancellationToken)
        {
            var product = await _productRepository.GetByCodeAsync("4689");
            var result = await _factory
                    .DefaultBuilder()
                    .WithCode(Guid.NewGuid().ToString())
                    .WithProductCode(product.Code)
                    .WithQuantity(1)
                    .WithUserCode(notification.UserCode)
                    .WithDescription(product.Description)
                    .WithThumbnail(product.Thumbnail)
                    .WithPrice(product.Price)
                    .WithTitle(product.Title)
                    .WithTrademark(product.Trademark)
                    .GenerateMachineKey()
                    .Raise()
                    .AddAsync();

            return result;
        }
    }
}
