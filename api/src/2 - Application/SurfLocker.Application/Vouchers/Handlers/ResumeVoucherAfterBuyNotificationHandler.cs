﻿using System.Threading.Tasks;
using System.Threading;
using MediatR;
using System;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Machines.Interfaces.Factory;
using SurfLocker.Domain.Machines.Interfaces;

namespace SurfLocker.Application.Vouchers.Handlers
{
    public class ResumeVoucherAfterBuyNotificationHandler
        : INotificationHandler<ResumeVoucherAfterBuyNotification>
    {
        private readonly IVoucherRepository _repository;
        private readonly IMachineRepository _machineRepository;
        private readonly IMachineFactory _machineFactory;
        private readonly IVoucherFactory _factory;

        public ResumeVoucherAfterBuyNotificationHandler(
            IVoucherRepository repository
            , IMachineFactory machineFactory
            , IMachineRepository machineRepository
            , IVoucherFactory factory
            , ISaleFactory saleFactory)
        {
            _repository = repository;
            _machineRepository = machineRepository;
            _machineFactory = machineFactory;
            _factory = factory;
        }

        public async Task Handle(ResumeVoucherAfterBuyNotification notification, CancellationToken cancellationToken)
        {
            var voucher = await _repository.GetByMachineKey(notification.MachineKey);
            var machine = await _machineRepository.GetByCodeAsync(notification.MachineCode);

            await _factory.DefaultBuilder()
                .WithCode(voucher.Code)
                .WithMachineCode(notification.MachineCode)
                .Raise()
                .ResumeAsync();

            var machineToDecrement = _machineFactory.DefaultBuilder()
                    .WithCode(notification.MachineCode)
                    .Raise();

            if (machine.ProdCod1 == voucher.ProductCode)
                await machineToDecrement.DecrementProdQtd1();
            else if (machine.ProdCod2 == voucher.ProductCode)
                await machineToDecrement.DecrementProdQtd2();
            else if (machine.ProdCod2 == voucher.ProductCode)
                await machineToDecrement
                    .DecrementProdQtd2();
        }
    }
}
