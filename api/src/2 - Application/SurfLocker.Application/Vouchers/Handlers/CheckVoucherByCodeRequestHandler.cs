﻿using SurfLocker.Domain.Store.Interfaces;
using System.Threading.Tasks;
using System.Threading;
using MediatR;
using SurfLocker.Dto;

namespace SurfLocker.Application.Vouchers.Handlers
{
    public class CheckVoucherByCodeRequestHandler
        : IRequestHandler<CheckVoucherByMachineKeyRequest, CheckVoucherResponseDto>
    {
        private readonly IVoucherRepository _repository;
        public CheckVoucherByCodeRequestHandler(IVoucherRepository repository)
        {
            _repository = repository;
        }

        public async Task<CheckVoucherResponseDto> Handle(CheckVoucherByMachineKeyRequest request, CancellationToken cancellationToken)
        {
            var voucher = await _repository.GetByMachineKey(request.MachineKey);

            if (voucher == null)
                return new CheckVoucherResponseDto();

            return new CheckVoucherResponseDto()
            {
                Valid = true,
                ProductCode = voucher.ProductCode
            };
        }
    }
}
