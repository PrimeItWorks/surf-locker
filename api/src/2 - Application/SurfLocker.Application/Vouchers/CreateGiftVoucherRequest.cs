﻿using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Vouchers
{
    public class CreateGiftVoucherRequest
        : IRequest<VoucherDto>

    {
        public CreateGiftVoucherRequest(string userCode)
        {
            UserCode = userCode;
        }

        public string UserCode { get; }
    }
}
