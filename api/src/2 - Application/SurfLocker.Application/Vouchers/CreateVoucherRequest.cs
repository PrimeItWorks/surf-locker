﻿using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.Application.Vouchers
{
    public class CreateVoucherRequest
        : IRequest<VoucherDto>

    {
        public VoucherDto Voucher { get; }
        public CreateVoucherRequest(VoucherDto machine) =>
            Voucher = machine;
    }
}
