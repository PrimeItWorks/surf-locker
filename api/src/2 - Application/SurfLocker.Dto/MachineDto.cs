﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto
{
    public class MachineDto
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Place { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string ProdCod1 { get; set; }
        public decimal ProdValue1 { get; set; }
        public string ProdName1 { get; set; }
        public int ProdQtd1 { get; set; }
        public int ProdQtd2 { get; set; }
        public int ProdQtd3 { get; set; }
        public string ProdCod2 { get; set; }
        public string ProdName2 { get; set; }
        public decimal ProdValue2 { get; set; }
        public string ProdCod3 { get; set; }
        public string ProdName3 { get; set; }
        public decimal ProdValue3 { get; set; }
    }
}
