﻿namespace SurfLocker.Dto.Sms
{
    public class SMSRequestDto
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Text { get; set; }
    }
}
