﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto.Sms
{
    public class SMSResponseDetailDto
    {
        public string status { get; set; }
        public string message_id { get; set; }
        public string to { get; set; }
        public string client_ref { get; set; }
        public string remaining_balance { get; set; }
        public string message_price { get; set; }
        public string network { get; set; }
        public string error_text { get; set; }
    }
}
