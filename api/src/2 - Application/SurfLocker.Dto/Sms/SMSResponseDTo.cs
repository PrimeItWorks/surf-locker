﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto.Sms
{
    public class SMSResponseDTo
    {
        public string message_count { get; set; }
        public List<SMSResponseDetailDto> messages { get; set; }
    }
}
