﻿namespace SurfLocker.Dto
{
    public class VoucherDto
    {
        public string Code { get; set; }
        public string ProductCode { get; set; }
        public string UserCode { get; set; }
        public string Title { get; set; }
        public string Trademark { get; set; }
        public string[] Thumbnail { get; set; }
        public int Quantity { get; set; }
        public string Image { get; set; }
        public string MachineKey { get; set; }
        public string MachineCode { get; set; }
        public string Description { get; set; }
        public string UrlVideo { get; set; }
        public decimal Price { get; set; }
        public bool Active { get; set; }
    }
}
