﻿namespace SurfLocker.Dto.ResponsePatterns.Machine
{
    public class ConfigureMachineResponseDto
    {
        public string C1 { get; set; }
        public int Q1 { get; set; }
        public decimal V1 { get; set; }
        public string C2 { get; set; }
        public int Q2 { get; set; }
        public decimal V2 { get; set; }
        public string C3 { get; set; }
        public decimal V3 { get; set; }
        public int Q3 { get; set; }
    }
}
