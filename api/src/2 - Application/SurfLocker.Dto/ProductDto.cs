﻿namespace SurfLocker.Dto
{
    public class ProductDto
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Trademark { get; set; }
        public string[] Thumbnail { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string UrlVideo { get; set; }
        public decimal Price { get; set; }
        public int Unit { get; set; }
        public ProductTypeDto Type { get; set; }
    }
}
