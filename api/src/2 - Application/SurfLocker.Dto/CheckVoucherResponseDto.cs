﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto
{
    public class CheckVoucherResponseDto
    {
        public bool Valid { get; set; }
        public string ProductCode { get; set; }
    }
}
