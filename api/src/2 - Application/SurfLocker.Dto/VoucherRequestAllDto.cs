﻿using SurfLocker.Dto.RequestPatterns;

namespace SurfLocker.Dto
{
    public class VoucherRequestAllDto : RequestAllDto
    {
        public string UserCode { get; set; }
    }
}
