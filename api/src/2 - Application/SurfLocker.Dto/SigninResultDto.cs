﻿namespace SurfLocker.Dto
{
    public class SigninResultDto
    {
        public UserDto User { get; set; }
        public object Token { get; set; }
        public bool FirstAccess { get; set; }
    }
}
