using System;
using System.Collections.Generic;

namespace Sesc.Gamefing.Application.Dto.Response
{
    public class TokenResultDto
    {
        public string Id { get; set; }
        public string AccessToken { get; set; }
        public bool Authenticated { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Expiration { get; set; }
        public List<string> Roles { get; set; }
    }
}