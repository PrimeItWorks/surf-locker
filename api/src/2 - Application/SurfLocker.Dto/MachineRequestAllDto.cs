﻿using SurfLocker.Dto.RequestPatterns;

namespace SurfLocker.Dto
{
    public class MachineRequestAllDto : RequestAllDto
    {
        public string Search { get; set; }
    }
}
