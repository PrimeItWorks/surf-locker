﻿using SurfLocker.Dto.RequestPatterns;

namespace SurfLocker.Dto
{
    public class UserRequestAllDto : RequestAllDto
    {
        public string Search { get; set; }
    }
}
