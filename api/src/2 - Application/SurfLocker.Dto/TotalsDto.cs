namespace SurfLocker.Dto
{
    public class TotalsDto
    {
        public int SalesApp { get; set; }
        public int SalesMachine { get; set; }
        public int Sales { get; set; }
        public decimal Billed { get; set; }
        public int SalesAppPercent { get; set; }
        public int SalesMachinePercent { get; set; }
    }
}