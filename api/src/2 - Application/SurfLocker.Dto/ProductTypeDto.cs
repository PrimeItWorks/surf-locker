﻿namespace SurfLocker.Dto
{
    public enum ProductTypeDto
    {
        None = 0,
        Machine = 1,
        Store = 2,
    }
}
