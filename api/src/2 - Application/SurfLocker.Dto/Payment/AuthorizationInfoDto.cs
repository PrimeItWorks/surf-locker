﻿namespace SurfLocker.Dto.Payment
{
    public class AuthorizationInfoDto
    {
        public int acquirerId { get; set; }
        public string acquirer { get; set; }
        public string acquirerTransactionId { get; set; }
        public string acquirerNsu { get; set; }
        public string acquirerTimestamp { get; set; }
        public string authorizationCode { get; set; }
        public string authorizationReason { get; set; }
        public string authorizationReasonMessage { get; set; }
    }
}
