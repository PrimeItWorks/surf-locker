﻿namespace SurfLocker.Dto.Payment.Response
{
    public class CreateCardTokenResponseDto
    {
        public string cardToken { get; set; }
        public int status { get; set; }
        public int type { get; set; }
        public string lastDigits { get; set; }
        public string expirationDate { get; set; }
        public int brandId { get; set; }
    }
}
