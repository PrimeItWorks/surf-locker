﻿using System.Collections.Generic;

namespace SurfLocker.Dto.Payment.Response
{
    public class CreateTransactionResponseDto
    {
        public string transactionId { get; set; }
        public int status { get; set; }
        public List<PaymentMethodResponseDto> paymentMethods { get; set; }
    }
}
