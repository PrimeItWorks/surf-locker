﻿namespace SurfLocker.Dto.Payment.Response
{
    public class RequestVaultKeyResponseDto
    {
        public string accessKey { get; set; }
    }
}
