﻿namespace SurfLocker.Dto.Payment.Request
{
    public class SetCustomerInfoRequestDto
    {
        public string merchantKey{ get; set; }
        public string cardToken { get; set; }
        public CustomerInfoDto customerInfo { get; set; }
    }
}
