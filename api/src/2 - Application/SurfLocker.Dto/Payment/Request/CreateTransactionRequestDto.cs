﻿using System.Collections.Generic;

namespace SurfLocker.Dto.Payment.Request
{
    public class CreateTransactionRequestDto
    {
        public string merchantKey { get; set; }
        public string metaId { get; set; }
        public string softDescriptor { get; set; }
        public List<PaymentMethodRequestDto> paymentMethod { get; set; }
        public CustomerInfoDto customerInfo { get; set; }
        /// <summary>
        /// Total da transação em centavos
        /// </summary>
        public int amount { get; set; }
    }
}
