﻿namespace SurfLocker.Dto.Payment.Request
{
    public class CreateCardTokenRequestApiDto
    {
        public string UserCode { get; set; }
        public string Cpf { get; set; }
        public string Birthday { get; set; }
        public CreateCardTokenRequestDto GatewayRequest { get; set; }
    }
}
