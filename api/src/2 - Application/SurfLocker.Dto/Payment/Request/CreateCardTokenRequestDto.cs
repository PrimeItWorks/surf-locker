﻿namespace SurfLocker.Dto.Payment.Request
{
    public class CreateCardTokenRequestDto
    {
        public string merchantKey { get; set; }
        public string cardNonce { get; set; }
        public bool waitForToken { get; set; }
    }
}
