﻿namespace SurfLocker.Dto.Payment.Request
{
    public class RequestVaultKeyRequestDto
    {
        public string merchantKey { get; set; }
    }
}
