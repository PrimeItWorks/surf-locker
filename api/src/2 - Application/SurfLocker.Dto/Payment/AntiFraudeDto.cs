﻿namespace SurfLocker.Dto.Payment
{
    public class AntiFraudeDto
    {
        public bool success { get; set; }
        public string validator { get; set; }
        public string score { get; set; }
        public string recommendation { get; set; }
    }
}
