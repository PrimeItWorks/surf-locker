﻿namespace SurfLocker.Dto.Payment
{
    public class CustomerInfoDto
    {
        public string UserCode { get; set; }
        public string MachineCode { get; set; }
        public string ProductCode { get; set; }
        public int Quantity { get; set; }
        public string fullName { get; set; }
        public string birthday { get; set; }
        public string cpf { get; set; }
        public string phoneNumber { get; set; }
        public string emailAddress { get; set; }
    }
}
