﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto.Payment
{
    public class TokensDto
    {
        public string PublicApiToken { get; set; }
        public string MerchantKey { get; set; }
    }
}
