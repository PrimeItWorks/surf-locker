﻿namespace SurfLocker.Dto.Payment
{
    public class OperationsDto
    {
        public string id { get; set; }
        /// <summary>
        /// Em centavos
        /// </summary>
        public int amount { get; set; }
        public int type { get; set; }
        public int status { get; set; }
    }
}
