﻿namespace SurfLocker.Dto.Payment
{
    public class PaymentMethodRequestDto
    {
        public string cardToken { get; set; }
        public int installments { get; set; }
        /// <summary>
        /// Em centavos
        /// </summary>
        public int amount { get; set; }
    }
}
