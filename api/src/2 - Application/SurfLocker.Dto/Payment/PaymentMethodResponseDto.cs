﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto.Payment
{
    public class PaymentMethodResponseDto
    {
        public string id { get; set; }
        /// <summary>
        /// Em centavos
        /// </summary>
        public int amount { get; set; }
        /// <summary>
        /// Em centavos
        /// </summary>
        public int cancelledAmount { get; set; }
        public int type { get; set; }
        public int status { get; set; }
        public string mode { get; set; }
        public int modeType { get; set; }
        public int installments { get; set; }
        public int installmentType { get; set; }
        public string installmentTypeName { get; set; }
        public string cardBin { get; set; }
        public string cardExpirationDate { get; set; }
        public int cardBrandId { get; set; }
        public string cardBrand { get; set; }
        public string cardLastDigits { get; set; }
        public string createdAt { get; set; }
        public object metadata { get; set; }
        public object paymentSlip { get; set; }
        public string debitTransactionUrl { get; set; } 
        public string debitTransactionToken { get; set; }
        public int tax { get; set; }

        public AuthorizationInfoDto authorizationInfo { get; set; }
        public List<OperationsDto> operations { get; set; }
        
    }
}
