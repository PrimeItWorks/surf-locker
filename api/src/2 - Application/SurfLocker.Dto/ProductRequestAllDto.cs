﻿using SurfLocker.Dto.RequestPatterns;

namespace SurfLocker.Dto
{
    public class ProductRequestAllDto : RequestAllDto
    {
        public ProductTypeDto Type { get; set; }
        public string Search { get; set; }
    }
}
