﻿using System;

namespace SurfLocker.Dto
{
    public class SalesDto
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Trademark { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public string UserCode { get; set; }
        public string TransactionId { get; set; }
        public string ProductCode { get; set; }
        public string MachineCode { get; set; }
        public DateTime? BuyDate { get; set; }
        public DateTime? DispenseDate { get; set; }
        public bool Online { get; set; }
    }
}
