﻿namespace SurfLocker.Dto
{
    public class ConfirmPhoneDto
    {
        public string UserCode { get; set; }
        public string Phone { get; set; }
    }
}
