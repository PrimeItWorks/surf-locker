﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Dto.RequestPatterns
{
    public class RequestAllDto
    {
        public RequestAllDto()
        {
            Page = 1;
            PageSize = 4;
            OrderBy = "_id";
            Desc = false;
        }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public bool Desc { get; set; }
    }
}
