﻿namespace SurfLocker.Dto
{
    public class MachineProductDto
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPriceByMoney { get; set; }
        public int QuantityAvailable { get; set; }
        public int QuantitySold { get; set; }
    }
}
