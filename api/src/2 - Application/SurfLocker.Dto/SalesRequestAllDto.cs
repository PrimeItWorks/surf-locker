﻿
using System;
using SurfLocker.Dto.RequestPatterns;

namespace SurfLocker.Dto
{
    public class SalesRequestAllDto : RequestAllDto
    {
        public string MachineCode { get; set; }
        public string UserCode { get; set; }
        public string ProductCode { get; set; }
        public string Trademark { get; set; }

        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
    }
}
