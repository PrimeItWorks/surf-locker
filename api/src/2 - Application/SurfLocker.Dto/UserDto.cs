﻿using System;
using SurfLocker.Dto.Payment;

namespace SurfLocker.Dto
{
    public class UserDto
    {
        public string Code { get; set; }
        public string FacebookId { get; set; }
        public string Name { get; set; }
        public string Cpf { get; set; }
        public DateTime? Birthday { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string[] Waves { get; set; }
        public string CardToken { get; set; }
        public string LatestDigitCard { get; set; }
        public bool ConfirmedNumber { get; set; }
        public CardBrandDto CardBrand { get; set; }
    }
}
