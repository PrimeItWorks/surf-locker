﻿using SurfLocker.WebApi;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Add swagger to Web Api Documentation
    /// </summary>
    internal static class SwaggerServiceCollectionExtensions
    {
        /// <summary>
        /// Método responsável por adicionar configurações de documentação da Api
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddSwaggerConfigurations(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = WebApiConsts.WebApiName,
                        Version = WebApiConsts.WebApiVersion,
                        Description = WebApiConsts.WebApiDescription,
                        Contact = new Contact
                        {
                            Name = WebApiConsts.WebApiContactName,
                            Url = WebApiConsts.WebApiContactUrl
                        }
                    });

                var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                var commentsFileName = "Comments" + ".XML";
                var commentsFile = Path.Combine(baseDirectory, commentsFileName);

                //c.IncludeXmlComments(commentsFile);
            });

            return services;
        }
    }
}
