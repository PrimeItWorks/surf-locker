﻿using Microsoft.AspNetCore.Builder;

namespace SurfLocker.WebApi.Extensions
{
    internal static class SwaggerApplicationBuilderExtensions
    {
        /// <summary>
        /// Método responsável por adicionar documentação da api
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder AddSwaggerUsage(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", WebApiConsts.WebApiName);
            });

            return app;
        }
    }
}
