﻿using Microsoft.AspNetCore.Builder;
using SurfLocker.Domain.Notifications;
using SurfLocker.WebApi.Middleewares;

namespace SurfLocker.WebApi.Extensions
{
    internal static class ExceptionHandlerApplicationBuilder
    {
        /// <summary>
        /// Método responsável por adicionar gerenciador de erros
        /// </summary>
        /// <param name="app"></param>
        /// <param name="notificationHandler"></param>
        /// <returns></returns>
        public static IApplicationBuilder AddExceptionHandler(this IApplicationBuilder app
            , INotificationHandler notificationHandler)
        {
            app.UseExceptionHandler(new ExceptionHandlerOptions() {
                ExceptionHandler = new JsonExceptionMiddleware(notificationHandler).Invoke
            });

            return app;
        }
    }
}
