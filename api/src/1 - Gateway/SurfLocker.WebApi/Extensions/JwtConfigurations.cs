﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SurfLocker.Domain.Users.Jwt;
using SurfLocker.WebApi;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Add swagger to Web Api Documentation
    /// </summary>
    internal static class JwtConfigurations
    {
        /// <summary>
        /// Método responsável por adicionar configurações de documentação da Api
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddJwtConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                 .AddJwtBearer(options => {
                     options.TokenValidationParameters = new TokenValidationParameters
                     {
                         ValidateIssuer = true,
                         ValidateAudience = true,
                         ValidateLifetime = true,
                         ValidateIssuerSigningKey = true,

                         ValidIssuer = configuration.GetSection("Authentication:Issuer").Value,
                         ValidAudience = configuration.GetSection("Authentication:Audience").Value,
                         IssuerSigningKey = JwtSecurityKey.Create(configuration.GetSection("Authentication:SecurityKey").Value)
                     };

                     options.Events = new JwtBearerEvents
                     {
                         OnAuthenticationFailed = context =>
                         {
                             return Task.CompletedTask;
                         },
                         OnTokenValidated = context =>
                         {
                             return Task.CompletedTask;
                         }
                     };
                 });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Member",
                    policy => policy.RequireClaim("MembershipId"));
            });

            return services;
        }
    }
}
