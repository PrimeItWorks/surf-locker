﻿namespace SurfLocker.WebApi
{
    /// <summary>
    /// Classe de constantes da Web Api
    /// </summary>
    internal static class WebApiConsts
    {
        /// <summary>
        /// Nome da Web Api
        /// </summary>
        public const string WebApiName = "Surf Locker - A chave pro seu surf";
        /// <summary>
        /// Versão da Web Api
        /// </summary>
        public const string WebApiVersion = "v1";
        /// <summary>
        /// Descrição da Web Api
        /// </summary>
        public const string WebApiDescription = "Web Api usada para o projeto Surf Locker";
        /// <summary>
        /// Desenvolvido por
        /// </summary>
        public const string WebApiContactName = "Prime IT Works";
        /// <summary>
        /// Contato
        /// </summary>
        public const string WebApiContactUrl = "mailto:arthur.jardim@primeit.works";
    }
}
