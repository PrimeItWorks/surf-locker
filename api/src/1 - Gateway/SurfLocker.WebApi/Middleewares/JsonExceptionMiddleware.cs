﻿using Microsoft.AspNetCore.Diagnostics;
using SurfLocker.Domain.Notifications;
using Microsoft.AspNetCore.Http;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.IO;

namespace SurfLocker.WebApi.Middleewares
{
    /// <summary>
    /// Middleware para tratar erros inesperados na api
    /// </summary>
    public class JsonExceptionMiddleware
    {
        /// <summary>
        /// Gerenciador de notificações da api
        /// </summary>
        private readonly INotificationHandler _notificationHandler;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="localizer">Textos localizados</param>
        public JsonExceptionMiddleware(INotificationHandler notificationHandler)
        {
            _notificationHandler = notificationHandler;
        }

        /// <summary>
        /// Ação do Middleware
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>Task</returns>
        public async Task Invoke(HttpContext context)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex == null) return;

            var error = new ErrorDto()
            {
                Message = "Erro ao acessar a Api",
                InternalError = ex
            };

            context.Response.ContentType = "application/json";
            using (var writer = new StreamWriter(context.Response.Body))
            {
                new JsonSerializer().Serialize(writer, error);
                await writer.FlushAsync().ConfigureAwait(false);
            }
        }
    }
}