﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using SurfLocker.WebApi.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Domain.Notifications;
using SurfLocker.Domain.Users.Users.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace SurfLocker.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationDependency()
                .AddStoreDependency()
                .AddAzureDependency()
                .AddMachineDependency()
                .AddNotificationsDependency()
                .AddPaymentDependency()
                .AddUsersDependency()
                .AddMongoDbDependency()
                .AddSwaggerConfigurations()
                .AddSmsDependency()
                .AddAutoMapperDependency()
                .AddAuthorization(auth =>
                {
                    auth.AddPolicy(Polices.POLICE_SURF_LOCKER, new AuthorizationPolicyBuilder()
                        .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser().Build());
                })
                .AddJwtConfigurations(Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app
            , IHostingEnvironment env
            , INotificationHandler notificationHandler)
        {
            app.AddSwaggerUsage();
            app.AddExceptionHandler(notificationHandler);
            app.UseStaticFiles();
            
            if (string.IsNullOrWhiteSpace(env.WebRootPath))
            {
                env.WebRootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            }

            app.UseAuthentication();
            app.UseCors(options => options
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
            );
            app.UseMvc();
        }
    }
}
