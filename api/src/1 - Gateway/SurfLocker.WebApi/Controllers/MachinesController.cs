using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Machines;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MediatR;
using System;
using SurfLocker.Application.Machine;
using SurfLocker.Dto.ResponsePatterns.Machine;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Máquinas
    /// </summary>
    [Route("api/[controller]")]
    public class MachinesController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de máquinas
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public MachinesController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por listar máquinas
        /// </summary>
        /// <param name="request">filtro</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMachineMachines([FromQuery]MachineRequestAllDto request)
        {
            var machines = await _mediator.Send(new GetAllMachinePagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(machines));
        }

        /// <summary>
        /// Método responsável por detalhar máquina
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var machines = await _mediator.Send(new GetMachineByCodeRequest(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(machines));
        }

        /// <summary>
        /// Método responsável por configurar máquina ao ligar
        /// </summary>
        /// <param name="machine"></param>
        /// <returns>
        /// Objeto para leitura pelo esp
        /// </returns>
        [HttpGet]
        [Route("configure/{id}")]
        public async Task<IActionResult> Configure(string id)
        {
            var machine = await _mediator.Send(new GetMachineByCodeRequest(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            var response = new ConfigureMachineResponseDto()
            {
                C1 = machine.ProdCod1,
                C2 = machine.ProdCod2,
                C3 = machine.ProdCod3,
                Q1 = machine.ProdQtd1,
                Q2 = machine.ProdQtd2,
                Q3 = machine.ProdQtd3,
                V1 = machine.ProdValue1,
                V2 = machine.ProdValue2,
                V3 = machine.ProdValue3
            };

            return Ok(response);
            //return Ok($"{machine.ProdCod1}|{machine.ProdQtd1}|{machine.ProdValue1}|{machine.ProdCod2}|{machine.ProdQtd2}|{machine.ProdValue2}|{machine.ProdCod3}|{machine.ProdQtd3}|{machine.ProdValue3}");
        }

        /// <summary>
        /// Método responsável por detalhar máquina
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("export")]
        public async Task<IActionResult> Export(MachineRequestAllDto request)
        {
            var response = await _mediator.Send(new ExportMachinesToExcelRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return File(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $@"maquinas_{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx");
        }

        /// <summary>
        /// Método responsável por cadastrar máquina
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]MachineDto machine)
        {
            var result = await _mediator.Send(new UpdateMachineRequest(machine));          
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, machine));

            return Ok(new OkDto(result));
        }

        /// <summary>
        /// Método responsável por cadastrar máquina
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]MachineDto machine)
        {
            await _mediator.Publish(new CreateMachineNotification(machine));          
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, machine));

            return Ok(new OkDto(true));
        }

        /// <summary>
        /// Método responsável por cadastrar máquina
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("resumemoney/{id}/{prod}")]
        public async Task<IActionResult> ResumeMoney(string id, string prod)
        {
            await _mediator.Publish(new ResumeSaleAfterBuyMoneyNotification(id, prod));          
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, new {id, prod}));

            return Ok(new OkDto(true));
        }

        /// <summary>
        /// Método responsável por excluir máquina cadastrada
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]string id)
        {
            await _mediator.Publish(new DeleteMachineNotification(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(true));
        }
    }
}
