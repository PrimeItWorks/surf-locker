using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Vouchers;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MediatR;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Usuários
    /// </summary>
    [Route("api/[controller]")]
    public class VouchersController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de vouchers
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public VouchersController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por buscar todos os vouchers
        /// </summary>
        /// <param name="request">Paramêtros para filtrar</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery]VoucherRequestAllDto request)
        {
            var products = await _mediator.Send(new GetAllVoucherPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(products));
        }

        /// <summary>
        /// Método responsável por Verificar validade do código
        /// </summary>
        /// <param name="id">Chave do produto na máquina</param>
        /// <returns></returns>
        [HttpGet]
        [Route("check/{id}")]
        public async Task<IActionResult> CheckValidity(string id)
        {
            var response = await _mediator.Send(new CheckVoucherByMachineKeyRequest(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Método responsável por criar voucher
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]VoucherDto product)
        {
            var result = await _mediator.Send(new CreateVoucherRequest(product));          
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, product));

            return Ok(new OkDto(result));
        }

        /// <summary>
        /// Método responsável por criar voucher de brinde
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Gift")]
        public async Task<IActionResult> Gift([FromBody]string userCode)
        {
            var result = await _mediator.Send(new CreateGiftVoucherRequest(userCode));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, userCode));

            return Ok(new OkDto(result));
        }

        /// <summary>
        /// Método responsável por criar voucher
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Resume/{id}/{machineCode}")]
        public async Task<IActionResult> Resume(string id, string machineCode)
        {
            await _mediator.Publish(new ResumeVoucherAfterBuyNotification(id, machineCode));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(true));
        }
    }
}
