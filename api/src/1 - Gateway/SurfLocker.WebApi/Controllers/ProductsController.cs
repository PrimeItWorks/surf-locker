using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Products;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using SurfLocker.Domain.Users.Users.Jwt;
using SurfLocker.Domain.Users.Users;
using System;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Usuários
    /// </summary>
    [Route("api/[controller]")]
    public class ProductsController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de login
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public ProductsController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por criar produto a vendas nas máquinas
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("machine")]
        public async Task<IActionResult> GetMachineProducts([FromQuery]ProductRequestAllDto request)
        {
            var products = await _mediator.Send(new GetAllMachineProductPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(products));
        }

        /// <summary>
        /// Método responsável por buscar produtos
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        //[Authorize(Policy = Polices.POLICE_SURF_LOCKER, Roles = UserRoles.ADMIN)]
        public async Task<IActionResult> GetProducts([FromQuery]ProductRequestAllDto request)
        {
            var products = await _mediator.Send(new GetAllProductsPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(products));
        }

        /// <summary>
        /// Método responsável por criar produto a vendas na loja
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("store")]
        public async Task<IActionResult> GetStoreProducts([FromQuery]ProductRequestAllDto request)
        {
            var products = await _mediator.Send(new GetAllStoreProductPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(products));
        }

        /// <summary>
        /// Método responsável por criar produto a vendas na loja
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var products = await _mediator.Send(new GetProductByCodeRequest(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(products));
        }

        /// <summary>
        /// Método responsável por criar produto a vendas na loja
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("export")]
        public async Task<IActionResult> Export([FromQuery] ProductRequestAllDto request)
        {
            var response = await _mediator.Send(new ExportProductsToExcelRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return File(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $@"clientes_{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx");
        }

        /// <summary>
        /// Método responsável por criar produto do site
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]ProductDto product)
        {
            await _mediator.Publish(new CreateProductNotification(product));          
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, product));

            return Ok(new OkDto(true));
        }

        /// <summary>
        /// Método responsável por atualizar produto do site
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]ProductDto product)
        {
            var updated = await _mediator.Send(new UpdateProductRequest(product));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, product));

            return Ok(new OkDto(updated));
        }

        /// <summary>
        /// Método responsável por atualizar produto do site
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]string id)
        {
            await _mediator.Publish(new DeleteProductNotification(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(true));
        }
    }
}
