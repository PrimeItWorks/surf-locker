using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Users;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using MediatR;
using SurfLocker.Application.Payment;
using SurfLocker.Dto.Payment.Request;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a pagamentos
    /// </summary>
    [Route("api/[controller]")]
    public class PaymentsController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de pagamentos
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public PaymentsController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Busca chave pública da api pra chamadas de front-end
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("publicApiToken")]
        public async Task<IActionResult> Get()
        {
            var response = await _mediator.Send(new GetPublicApiTokenRequest());
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Busca Vault Key para o gateway de pagamento
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("RequestVaultKey")]
        public async Task<IActionResult> RequestVaultKey()
        {
            var response = await _mediator.Send(new RequestVaultKeyRequest());
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Cria token do cartão no gateway de pagamento
        /// </summary
        /// <param name="request">Dados da requisição</param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateCardToken")]
        public async Task<IActionResult> CreateCardToken([FromBody] CreateCardTokenRequestApiDto request)
        {
            var response = await _mediator.Send(new CreateCardTokenRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Atribui dados do usuáro ao gateway de pagamento
        /// </summary
        /// <param name="request">Dados da requisição</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetCustumerInfo")]
        public async Task<IActionResult> SetCustumerInfo([FromBody] SetCustomerInfoRequestDto request)
        {
            await _mediator.Publish(new SetCustomerInfoNotification(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            return Ok(new OkDto(true));
        }

        /// <summary>
        /// Atribui dados do usuáro ao gateway de pagamento
        /// </summary
        /// <param name="request">Dados da requisição</param>
        /// <returns></returns>
        [HttpPost]
        [Route("CreateTransaction")]
        public async Task<IActionResult> CreateTransaction([FromBody] CreateTransactionRequestDto request)
        {
            await _mediator.Publish(new CreateTransactiontNotification(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            return Ok(new OkDto(true));
        }
    }
}