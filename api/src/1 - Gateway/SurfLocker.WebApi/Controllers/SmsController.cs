using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using MediatR;
using SurfLocker.Dto;
using SurfLocker.Application.Users;
using SurfLocker.Dto.Response;
using SurfLocker.Domain.Users.Jwt;
using SurfLocker.Dto.Sms;
using SurfLocker.Application.Sms;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Usuários
    /// </summary>
    [Route("api/[controller]")]
    public class SmsController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de login
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public SmsController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por buscar todos os usuários 
        /// paginados
        /// </summary>
        /// <param name="request">filtros de paginação para usuários</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Send([FromBody] SMSRequestDto request)
        {
            var response = await _mediator.Send(new SendSmsRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(response));
        }
    }
}