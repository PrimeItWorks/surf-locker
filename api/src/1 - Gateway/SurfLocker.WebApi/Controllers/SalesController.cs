using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Sales;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MediatR;
using System;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Vendas
    /// </summary>
    [Route("api/[controller]")]

    public class SalesController
        : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de pagamentos
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="notification"></param>
        public SalesController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }
        
        /// <summary>
        /// Método responsável por buscar as vendas paginadas
        /// </summary>
        /// <param name="request"></param>
        
        [HttpGet]
        public async Task<IActionResult> GetMachineProducts([FromQuery]SalesRequestAllDto request)
        {
            var response = await _mediator.Send(new GetSalesPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Método responsável por buscar as vendas paginadas
        /// </summary>
        /// <param name="request"></param>
        
        [HttpGet]
        [Route("export")]
        public async Task<IActionResult> Export([FromQuery]SalesRequestAllDto request)
        {
            var response = await _mediator.Send(new ExportSalesToExcelRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return File(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $@"clientes_{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx");
        }

        [HttpGet]
        [Route("totals")]
        public async Task<IActionResult> Totals() 
        {
            var response = await _mediator.Send(new GetTotalsRequest());
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, null));

            return Ok(new OkDto(response));
        }
    }
}