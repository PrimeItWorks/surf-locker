using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using MediatR;
using SurfLocker.Dto;
using SurfLocker.Application.Users;
using SurfLocker.Dto.Response;
using SurfLocker.Domain.Users.Jwt;
using SurfLocker.Domain.Users.Users.Jwt;
using SurfLocker.Domain.Users.Users;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Usuários
    /// </summary>
    [Route("api/[controller]")]
    public class SignInController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de login
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public SignInController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por criar usuário/acessar o app pelo facebook
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SignIn([FromBody] UserDto user)
        {
            var firstaccess = false;
            var token = await _mediator.Send(new FacebookLoginRequest(user.FacebookId));
            if (token == null) {
                firstaccess = true;
                await _mediator.Publish(new CreateUserNotification(user));
            }

            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification));

            token = await _mediator.Send(new FacebookLoginRequest(user.FacebookId));
            if (token == null)
            {
                _notification.DefaultBuilder()
                    .WithCode(JwtNotificationsCodes.LOGIN_SHOULD_BE_VALID.ToString())
                    .WithMessage("Não foi possível fazer login.")
                    .RaiseNotification();
            }

            var resultUser = await _mediator.Send(new GetUserByFacebookIdRequest(user.FacebookId));
            if (string.IsNullOrEmpty(resultUser.Phone))
                firstaccess = true;

            return Ok(new OkDto(new SigninResultDto() {
                User = resultUser,
                Token = token,
                FirstAccess = firstaccess
            }));
        }

        /// <summary>
        /// Método responsável por verificar autenticação do admin
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("checkAuthAdmin")]
        [Authorize(Policy = Polices.POLICE_SURF_LOCKER, Roles = UserRoles.ADMIN)]
        public IActionResult CheckAuthAdmin()
        {
            return Ok();
        }

        [HttpPost]
        [Route("admin")]
        public async Task<IActionResult> SignInAdmin([FromBody] UserDto user)
        {
            var firstaccess = false;
            var token = await _mediator.Send(new AdminLoginRequest(user.Email, user.Password));
            if (token == null)
            {
                _notification.DefaultBuilder()
                    .WithCode(JwtNotificationsCodes.LOGIN_SHOULD_BE_VALID.ToString())
                    .WithMessage("Não foi possível fazer login.")
                    .RaiseNotification();

                return BadRequest(new BadRequestDto(_notification));
            }

            var resultUser = await _mediator.Send(new GetUserByEmailRequest(user.Email));
            return Ok(new OkDto(new SigninResultDto()
            {
                User = resultUser,
                Token = token,
                FirstAccess = firstaccess
            }));
        }

        [HttpPost]
        [Route("email")]
        public async Task<IActionResult> SignInEmail([FromBody] UserDto user)
        {
            var firstaccess = false;
            var token = await _mediator.Send(new EmailLoginRequest(user.Email, user.Password));
            if (token == null)
            {
                _notification.DefaultBuilder()
                    .WithCode(JwtNotificationsCodes.LOGIN_SHOULD_BE_VALID.ToString())
                    .WithMessage("Não foi possível fazer login.")
                    .RaiseNotification();

                return BadRequest(new BadRequestDto(_notification));
            }

            var resultUser = await _mediator.Send(new GetUserByEmailRequest(user.Email));
            return Ok(new OkDto(new SigninResultDto()
            {
                User = resultUser,
                Token = token,
                FirstAccess = firstaccess
            }));
        }
    }
}