using Microsoft.AspNetCore.Authorization;
using SurfLocker.Domain.Users.Jwt;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using MediatR;
using SurfLocker.Dto;
using SurfLocker.Application.Users;
using SurfLocker.Dto.Response;
using SurfLocker.Domain.Users.Jwt;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Usuários
    /// </summary>
    [Route("api/[controller]")]
    public class AccountsController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de login
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public AccountsController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Método responsável por buscar todos os usuários 
        /// paginados
        /// </summary>
        /// <param name="request">filtros de paginação para usuários</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] UserRequestAllDto request)
        {
            var response = await _mediator.Send(new GetAllUsersPagingRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Método responsável por criar usuários
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserDto request)
        {
            await _mediator.Publish(new CreateUserNotification(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));
            
            var resultUser = await _mediator.Send(new GetUserByEmailRequest(request.Email));

            var token = new JwtTokenBuilder()
                .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                .AddSubject(request.Email)
                .AddIssuer("issuerTest")
                .AddAudience("bearerTest")
                .AddClaim("MembershipId", "111")
                .AddExpiry(1)
                .Build();

            return Ok(new OkDto(new SigninResultDto()
            {
                User = resultUser,
                Token = token,
                FirstAccess = true
            }));
        }


        /// <summary>
        /// Método responsável por buscar detalhes do usuário
        /// </summary>
        /// <param name="request">filtros de paginação para usuários</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var response = await _mediator.Send(new GetUserByCodeRequest(id));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, id));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Método responsável por buscar detalhes do usuário
        /// </summary>
        /// <param name="request">filtros de paginação para usuários</param>
        /// <returns></returns>
        [HttpGet]
        [Route("byemail/{email}")]
        public async Task<IActionResult> ByEmail(string email)
        {
            var response = await _mediator.Send(new GetUserByEmailRequest(email));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, email));

            return Ok(new OkDto(response));
        }

        /// <summary>
        /// Método responsável por exportar usuários para excel
        /// </summary>
        /// <param name="request">filtros para usuários</param>
        /// <returns></returns>
        [HttpGet]
        [Route("export")]
        public async Task<IActionResult> Export([FromQuery] UserRequestAllDto request)
        {
            var response = await _mediator.Send(new ExportUsersToExcelRequest(request));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, request));

            return File(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", $@"clientes_{DateTime.Now.ToString("yyyyMMddHHss")}.xlsx");
        }

        /// <summary>
        /// Método responsável por atualizar usuário do site
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody]UserDto user)
        {
            var updated = await _mediator.Send(new UpdateUserRequest(user));           
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, user));

            return Ok(new OkDto(updated));
        }

        /// <summary>
        /// Método responsavel por pedir codigo por sms pra confirmar telefone do usuário
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AskSmsPhoneConfirmation")]
        public async Task<IActionResult> SendConfirmation([FromBody]ConfirmPhoneDto dto)
        {
            var code = await _mediator.Send(new ConfirmPhoneRequest(dto));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, dto));

            return Ok(new OkDto(code));
        }

        /// <summary>
        /// Método responsável por confirmar que o telefone é mesmo do usuário
        /// </summary>
        /// <param name="userCode">Código do usuário</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ConfirmPhone")]
        public async Task<IActionResult> ConfirmPhone([FromBody]string userCode)
        {
            await _mediator.Publish(new ConfirmPhoneNotification(userCode));
            if (_notification.HasNotification())
                return BadRequest(new BadRequestDto(_notification, userCode));

            return Ok(new OkDto(true));
        }

        
    }
}