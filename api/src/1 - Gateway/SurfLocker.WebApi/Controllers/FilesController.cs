using INotification = SurfLocker.Domain.Notifications.INotificationHandler;
using SurfLocker.Application.Blob;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using System.Linq;
using System.IO;
using MediatR;
using System;

namespace SurfLocker.WebApi.Controllers
{
    /// <summary>
    /// Entrada de ações referentes a Arquivos
    /// </summary>
    [Route("api/[controller]")]
    public class FilesController : BaseController
    {
        private readonly INotification _notification;
        private readonly IMediator _mediator;

        /// <summary>
        /// Gerenciador de Arquivos
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="notification"></param>
        public FilesController(IMediator mediator
            , INotification notification)
            : base(mediator)
        {
            _notification = notification;
            _mediator = mediator;
        }

        /// <summary>
        /// Armazena arquivo no Azure Blob Storage
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            if (Request.ContentLength <= 0)
            {
                _notification.DefaultBuilder()
                   .WithCode("UPLOADFILE_01")
                   .WithMessage("Não foi possível localizar o arquivo")
                   .RaiseNotification();

                return BadRequest(new BadRequestDto(_notification, null));
            }

            var urls = new List<string>();

            files = Request.Form.Files.ToList();
            long size = files.Sum(f => f.Length);
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var fileName = $"{Guid.NewGuid().ToString()}{Path.GetExtension(file.FileName)}";

                    var url = await _mediator.Send(
                        new UploadBlobStorageRequest(file.OpenReadStream(), fileName));
                    urls.Add(url);
                }
            }

            return Ok(new OkDto(urls));
        }

        /// <summary>
        /// Deleta arquivo do Azure Blob Storage
        /// </summary>
        /// <param name="name">Nome do arquivo no blob storage</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task Delete([FromQuery] string name) =>
            await _mediator.Publish(new DeleteBlobStorageNotification(name));
    }
}