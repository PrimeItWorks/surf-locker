using MediatR;
using Microsoft.AspNetCore.Mvc;
using SurfLocker.Dto;

namespace SurfLocker.WebApi.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly IMediator _mediator;
        private UserDto _currentUser;

        /// <summary>
        /// Construtor de controller base
        /// </summary>
        /// <param name="userAppService"></param>
        /// <param name="userManager"></param>
        public BaseController(IMediator mediator)
        {
            _mediator = mediator;
            _currentUser = null;
        }

        /// <summary>
        /// Usuário Logado
        /// </summary>
        public UserDto CurrentUser =>  GetCurrentUser();

        private UserDto GetCurrentUser()
        {
            //if (!User.Identity.IsAuthenticated)
            //    return null;

            //if (_currentUser != null)
            //    return _currentUser;

            //User.Identity.Name
            //var userByIdentity = _userService.GetByLogon(User.Identity.Name);
            //userByIdentity.Wait();
            //_currentUser = userByIdentity.Result;

            return _currentUser;
        }
    }
}