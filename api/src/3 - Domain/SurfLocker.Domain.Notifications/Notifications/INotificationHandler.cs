﻿using static SurfLocker.Domain.Notifications.NotificationHandler;
using System.Collections.Generic;

namespace SurfLocker.Domain.Notifications
{
    public interface INotificationHandler
    {
        NotificationBuilder DefaultBuilder();
        List<NotificationEvent> GetAllNotifications();
        void ClearNotifications();
        bool HasNotification();
    }
}
