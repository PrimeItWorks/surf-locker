﻿namespace SurfLocker.Domain.Machines
{
    public class MachineProduct
    {
        public int DispenserNumber { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPriceByMoney { get; set; }
        public int QuantityAvailable { get; set; }
        public int QuantitySold{ get; set; }
    }
}
