using SurfLocker.Domain.Machines.Interfaces;
using SurfLocker.Domain.Notifications;
using MongoDB.Bson;
using AutoMapper;
using System.Threading.Tasks;
using SurfLocker.Dto;
using System.Collections.Generic;
using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Machines.Specifications;

namespace SurfLocker.Domain.Machines
{
    public class Machine
    {
        private readonly IMachineRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        internal Machine() { }
        internal Machine(INotificationHandler notificationHandler
            , IMachineRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public ObjectId _id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Place { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string ProdCod1 { get; set; }
        public string ProdName1 { get; set; }
        public decimal ProdValue1 { get; set; }
        public int ProdQtd1 { get; set; }
        public string ProdCod2 { get; set; }
        public decimal ProdValue2 { get; set; }
        public string ProdName2 { get; set; }
        public int ProdQtd2 { get; set; }
        public string ProdCod3 { get; set; }
        public decimal ProdValue3 { get; set; }
        public string ProdName3 { get; set; }
        public int ProdQtd3 { get; set; }

        public bool IsValid => !_notificationHandler.HasNotification();

        public void Specify()
        {
            new MachineSpecifications()
                .ForEach(specification => SpecifyOne(specification));
        }

        public async Task AddAsync()
        {
            if (!IsValid)
                return;

            Code = this.GenerateCode();
            var duplicatedCode = _repository.Exists(Code);
            if (duplicatedCode)
            {
                Code = this.GenerateCode();
                await AddAsync();
            }

            await _repository.AddAsync(this);
        }

        public async Task<MachineDto> UpdateAsync()
        {
            if (!IsValid)
                return null;

            var product = await _repository.UpdateAsync(this);
            return _mapper.Map<MachineDto>(product);
        }

        public async Task DecrementProdQtd1()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode());

            if (!IsValid)
                return;

            await _repository.DecrementProdQtd(Code, nameof(Machine.ProdQtd1));
        }

        public async Task DecrementProdQtd2()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode());

            if (!IsValid)
                return;

            await _repository.DecrementProdQtd(Code, nameof(Machine.ProdQtd2));
        }

        public async Task DecrementProdQtd3()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode());

            if (!IsValid)
                return;

            await _repository.DecrementProdQtd(Code, nameof(Machine.ProdQtd2));
        }

        public async Task SetProdValue1()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode1());
            SpecifyOne(new MachineShouldHaveProdValue1());

            if (!IsValid)
                return;

            await _repository.SetProductValue1ValueByProduct(ProdCod1, ProdValue1);
        }

        public async Task SetProdValue2()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode2());
            SpecifyOne(new MachineShouldHaveProdValue2());

            if (!IsValid)
                return;

            await _repository.SetProductValue2ValueByProduct(ProdCod2, ProdValue2);
        }

        public async Task SetProdValue3()
        {   
            _notificationHandler.ClearNotifications();
            SpecifyOne(new MachineShouldHaveCode3());
            SpecifyOne(new MachineShouldHaveProdValue3());

            if (!IsValid)
                return;

            await _repository.SetProductValue3ValueByProduct(ProdCod3, ProdValue3);
        }

        private void SpecifyOne(SpecificationBase<Machine> specification)
        {
            var validation = specification.Condition();
            if (!validation(this))
                _notificationHandler
                    .DefaultBuilder()
                    .WithCode(specification.Code)
                    .WithMessage(specification.Message)
                    .WithDetailMessage(specification.DetailMessage)
                    .RaiseNotification();
        }
    }
}
