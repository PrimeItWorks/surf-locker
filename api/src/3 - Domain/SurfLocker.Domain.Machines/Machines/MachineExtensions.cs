﻿using System;

namespace SurfLocker.Domain.Machines
{
    internal static class MachineExtensions
    {
        private static readonly Random _random = new Random();
        public static string GenerateCode(this Machine machine)
        {
            return _random.Next(1000, 9999).ToString();
        }
    }
}
