﻿using SurfLocker.Domain.Machines.Interfaces.Factory;
using SurfLocker.Domain.Machines.Interfaces;
using SurfLocker.Domain.Notifications;
using AutoMapper;
using SurfLocker.Dto;
using System.Collections.Generic;

namespace SurfLocker.Domain.Machines
{
    public class MachineFactory: IMachineFactory
    {
        private readonly IMachineRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        public MachineFactory(INotificationHandler notificationHandler
            , IMachineRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public MachineBuilder DefaultBuilder()
            => new MachineBuilder(_notificationHandler, _repository, _mapper);

        public class MachineBuilder
        {
            private readonly Machine _product;
            private readonly INotificationHandler _notificationHandler;
            private readonly IMapper _mapper;
            private readonly IMachineRepository _repository;

            public MachineBuilder(INotificationHandler notificationHandler
                , IMachineRepository repository
                , IMapper mapper)
            {
                _notificationHandler = notificationHandler;
                _mapper = mapper;
                _repository = repository;
                _product = new Machine(notificationHandler, repository, _mapper);
            }

            public MachineBuilder WithCode(string value)
            {
                _product.Code = value;
                return this;
            }

            public MachineBuilder WithTitle(string value)
            {
                _product.Title = value;
                return this;
            }

            public MachineBuilder WithPlace(string value)
            {
                _product.Place = value;
                return this;
            }

            public MachineBuilder WithProdCode1(string value)
            {
                _product.ProdCod1 = value;
                return this;
            }

            public MachineBuilder WithProdName1(string value)
            {
                _product.ProdName1 = value;
                return this;
            }

            public MachineBuilder WithProdValue1(decimal value)
            {
                _product.ProdValue1 = value;
                return this;
            }

            public MachineBuilder WithProdQtd1(int value)
            {
                _product.ProdQtd1 = value;
                return this;
            }
            public MachineBuilder WithProdQtd2(int value)
            {
                _product.ProdQtd2 = value;
                return this;
            }
            public MachineBuilder WithProdQtd3(int value)
            {
                _product.ProdQtd3 = value;
                return this;
            }

            public MachineBuilder WithProdCode2(string value)
            {
                _product.ProdCod2 = value;
                return this;
            }
            
            public MachineBuilder WithProdName2(string value)
            {
                _product.ProdName2 = value;
                return this;
            }

            public MachineBuilder WithProdValue2(decimal value)
            {
                _product.ProdValue2 = value;
                return this;
            }

            public MachineBuilder WithProdCode3(string value)
            {
                _product.ProdCod3 = value;
                return this;
            }
            
            public MachineBuilder WithProdName3(string value)
            {
                _product.ProdName3 = value;
                return this;
            }

            public MachineBuilder WithProdValue3(decimal value)
            {
                _product.ProdValue3 = value;
                return this;
            }

            public MachineBuilder WithLat(string value)
            {
                _product.Lat = value;
                return this;
            }

            public MachineBuilder WithLong(string value)
            {
                _product.Long = value;
                return this;
            }

            public Machine Raise()
            {
                _product.Specify();
                return _product;
            }
        }
    }
}