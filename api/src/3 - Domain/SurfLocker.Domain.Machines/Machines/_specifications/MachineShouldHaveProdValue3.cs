﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveProdValue3 : SpecificationBase<Machine>
    {
        public override string Message => "Quantidade do produto 3 é obrigatória";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PROD3VALUE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => toValidate.ProdValue3 > 0;
    }
}   
