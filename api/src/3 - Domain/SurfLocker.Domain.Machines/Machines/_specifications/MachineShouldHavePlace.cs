﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHavePlace : SpecificationBase<Machine>
    {
        public override string Message => "Onde está a máquina é obrigatório";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PLACE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Place);
    }
}   
