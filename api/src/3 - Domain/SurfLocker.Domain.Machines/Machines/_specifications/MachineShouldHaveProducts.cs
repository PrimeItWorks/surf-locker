﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveProducts : SpecificationBase<Machine>
    {
        public override string Message => "Máquina precisa ter produtos com seus respectivos valores";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PRODUCTS.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => (!string.IsNullOrEmpty(toValidate.ProdCod1) && !string.IsNullOrEmpty(toValidate.ProdName1) && toValidate.ProdValue1 > 0)
                || (!string.IsNullOrEmpty(toValidate.ProdCod2) && !string.IsNullOrEmpty(toValidate.ProdName2) && toValidate.ProdValue2 > 0)
                || (!string.IsNullOrEmpty(toValidate.ProdCod3) && !string.IsNullOrEmpty(toValidate.ProdName3) && toValidate.ProdValue3 > 0);
    }
}   
