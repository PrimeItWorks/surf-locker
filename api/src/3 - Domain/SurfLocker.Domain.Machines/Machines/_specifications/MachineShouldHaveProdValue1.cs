﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveProdValue1 : SpecificationBase<Machine>
    {
        public override string Message => "Quantidade do produto 1 é obrigatória";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PROD1VALUE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => toValidate.ProdValue1 > 0;
    }
}   
