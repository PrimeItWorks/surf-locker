﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveTitle : SpecificationBase<Machine>
    {
        public override string Message => "Nome da máquina é obrigatório";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_TITLE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Title);
    }
}   
