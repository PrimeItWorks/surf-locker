﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveCode3 : SpecificationBase<Machine>
    {
        public override string Message => "Código do produto 3 é obrigatório";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PROD3CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.ProdCod3);
    }
}   
