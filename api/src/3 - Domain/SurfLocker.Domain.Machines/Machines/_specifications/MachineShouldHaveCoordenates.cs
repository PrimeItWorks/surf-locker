﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveCoordenates : SpecificationBase<Machine>
    {
        public override string Message => "Latitude e Longitude da máquina são obrigatórios";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_COORDENATES.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Lat)
                && !string.IsNullOrEmpty(toValidate.Long);
    }
}   
