﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Machines.Specifications
{
    internal class MachineShouldHaveCode1 : SpecificationBase<Machine>
    {
        public override string Message => "Código do produto 1 é obrigatório";
        public override string Code => MachineNotificationsCodes.PRODUCT_SHOULD_HAVE_PROD1CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Machine, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.ProdCod1);
    }
}   
