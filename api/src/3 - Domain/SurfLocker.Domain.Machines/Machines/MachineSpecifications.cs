﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Machines.Specifications;
using System.Collections.Generic;

namespace SurfLocker.Domain.Machines
{
    internal class MachineSpecifications : List<SpecificationBase<Machine>>
    {
        public MachineSpecifications()
        {
            AddRange(new List<SpecificationBase<Machine>>() {
                new MachineShouldHaveTitle(),
                new MachineShouldHaveProducts(),
                new MachineShouldHavePlace(),
                new MachineShouldHaveCoordenates()
            });
        }
    }
}