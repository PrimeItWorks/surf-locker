﻿using SurfLocker.Domain.Machines;
using SurfLocker.Dto;
using AutoMapper;

namespace SurfLocker.Domain.Mapper
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<Machine, MachineDto>();
            CreateMap<MachineProduct, MachineProductDto>();
        }
    }
}
