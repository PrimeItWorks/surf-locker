using SurfLocker.Dto.Response;
using System.Threading.Tasks;
using SurfLocker.Dto;
using System.Collections.Generic;

namespace SurfLocker.Domain.Machines.Interfaces
{
    public interface IMachineRepository
    {
        Task<ListResponseDto<MachineDto>> GetAllPagingAsync(MachineRequestAllDto request);
        Task<MachineDto> GetByCodeAsync(string code);
        Task<List<MachineDto>> GetByCodesAsync(string[] codes);
        bool Exists(string code);
        Task<Machine> UpdateAsync(Machine user);
        Task AddAsync(Machine request);
        Task SetProductValue1ValueByProduct(string productCode, decimal value);
        Task SetProductValue2ValueByProduct(string productCode, decimal value);
        Task SetProductValue3ValueByProduct(string productCode, decimal value);
        Task DecrementProdQtd(string code, string fieldName);
        Task DeleteAsync(string code);
    }
}
