﻿using static SurfLocker.Domain.Machines.MachineFactory;

namespace SurfLocker.Domain.Machines.Interfaces.Factory
{
    public interface IMachineFactory
    {
        MachineBuilder DefaultBuilder();
    }
}
