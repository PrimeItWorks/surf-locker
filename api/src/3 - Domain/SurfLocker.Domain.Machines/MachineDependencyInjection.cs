﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Domain.Machines.Interfaces.Factory;
using SurfLocker.Domain.Machines;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class MachineDependencyInjection
    {
        public static IServiceCollection AddMachineDependency(this IServiceCollection services)
        {
            services.TryAddTransient<IMachineFactory, MachineFactory>();

            return services;
        }
    }
}
