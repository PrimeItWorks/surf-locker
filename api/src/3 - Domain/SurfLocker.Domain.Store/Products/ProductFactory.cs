﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using AutoMapper;
using SurfLocker.Dto;

namespace SurfLocker.Domain.Store.Products
{
    public class ProductFactory: IProductFactory
    {
        private readonly IProductRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        public ProductFactory(INotificationHandler notificationHandler
            , IProductRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public ProductBuilder DefaultBuilder()
            => new ProductBuilder(_notificationHandler, _repository, _mapper);

        public class ProductBuilder
        {
            private readonly Product _product;
            private readonly INotificationHandler _notificationHandler;
            private readonly IMapper _mapper;
            private readonly IProductRepository _repository;

            public ProductBuilder(INotificationHandler notificationHandler
                , IProductRepository repository
                , IMapper mapper)
            {
                _notificationHandler = notificationHandler;
                _mapper = mapper;
                _repository = repository;
                _product = new Product(notificationHandler, repository, _mapper);
            }

            public ProductBuilder WithCode(string value)
            {
                _product.Code = value;
                return this;
            }

            public ProductBuilder WithTitle(string value)
            {
                _product.Title = value;
                return this;
            }

            public ProductBuilder WithTrademark(string value)
            {
                _product.Trademark = value;
                return this;
            }

            public ProductBuilder WithThumbnail(string[] value)
            {
                _product.Thumbnail = value;
                return this;
            }

            public ProductBuilder WithImage(string value)
            {
                _product.Image = value;
                return this;
            }

            public ProductBuilder WithDescription(string value)
            {
                _product.Description = value;
                return this;
            }

            public ProductBuilder WithPrice(decimal value)
            {
                _product.Price = value;
                return this;
            }

            public ProductBuilder WithUnit(int value)
            {
                _product.Unit = value;
                return this;
            }
            

            public ProductBuilder WithType(ProductTypeDto value)
            {
                _product.Type = _mapper.Map<ProductType>(value);
                return this;
            }

            public ProductBuilder WithVideo(string value)
            {
                _product.UrlVideo = value;
                return this;
            }

            public Product Raise()
            {
                _product.Specify();
                return _product;
            }
        }
    }
}