﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Store.Products.Specifications;
using System.Collections.Generic;

namespace SurfLocker.Domain.Store.Products
{
    internal class ProductSpecifications : List<SpecificationBase<Product>>
    {
        public ProductSpecifications()
        {
            AddRange(new List<SpecificationBase<Product>>() {
                new ProductShouldHaveCode(),
                new ProductShouldHaveDescription(),
                new ProductShouldHaveImage(),
                new ProductShouldHavePrice(),
                new ProductShouldHavePrice(),
                new ProductShouldHaveThumbnail(),
                new ProductShouldHaveTitle(),
                new ProductShouldHaveTrademark(),
                new ProductShouldHaveType()
            });
        }
    }
}