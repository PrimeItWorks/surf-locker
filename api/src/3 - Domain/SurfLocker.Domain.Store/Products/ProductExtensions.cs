﻿using System;

namespace SurfLocker.Domain.Store.Products
{
    internal static class MachineExtensions
    {
        private static readonly Random _random = new Random();
        public static string GenerateCode(this Product machine)
        {
            return _random.Next(1000, 9999).ToString();
        }
    }
}
