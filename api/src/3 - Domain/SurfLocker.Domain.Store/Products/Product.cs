using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using MongoDB.Bson;
using AutoMapper;
using System.Threading.Tasks;
using SurfLocker.Dto;
using System;

namespace SurfLocker.Domain.Store.Products
{
    public class Product
    {
        private readonly IProductRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        internal Product() { }
        internal Product(INotificationHandler notificationHandler
            , IProductRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
            Thumbnail = new string[] { };
            Type = ProductType.None;
        }

        public ObjectId _id { get; set; }
        public string Code { get; set; }
        public string Title{ get; set; }
        public string Trademark { get; set; }
        public string[] Thumbnail { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public int Unit { get; set; }
        public string UrlVideo { get; set; }
        public decimal Price { get; set; }
        public ProductType Type { get; set; }

        public bool IsValid => !_notificationHandler.HasNotification();

        public void Specify()
        {
            var specifications = new ProductSpecifications();
            foreach (var specification in specifications)
            {
                var validation = specification.Condition();
                if (!validation(this))
                    _notificationHandler
                        .DefaultBuilder()
                        .WithCode(specification.Code)
                        .WithMessage(specification.Message)
                        .WithDetailMessage(specification.DetailMessage)
                        .RaiseNotification();
            }
        }

        public async Task AddAsync()
        {
            if (!IsValid)
                return;

            Code = this.GenerateCode();
            var duplicatedCode = _repository.Exists(Code);
            if (duplicatedCode)
            {
                Code = this.GenerateCode();
                await AddAsync();
            }

            await _repository.AddAsync(this);
        }

        public async Task<ProductDto> UpdateAsync()
        {
            if (!IsValid)
                return null;

            var product = await _repository.UpdateAsync(this);
            return _mapper.Map<ProductDto>(product);
        }
    }
}
