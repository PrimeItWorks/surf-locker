namespace SurfLocker.Domain.Store.Products
{
    public enum ProductType
    {
        None = 0,
        Machine = 1,
        Store = 2
    }
}
