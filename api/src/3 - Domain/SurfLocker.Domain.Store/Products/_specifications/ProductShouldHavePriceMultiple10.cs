﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHavePriceMultiple10 : SpecificationBase<Product>
    {
        public override string Message => "Preço do produto deve ser múltiplo de 10";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_PRICE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => toValidate.Price > 0 && (toValidate.Price % 10) == 0;
    }
}
