﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHaveThumbnail : SpecificationBase<Product>
    {
        public override string Message => "Thumbnails do produto são obrigatórios";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_THUMBNAIL.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => toValidate.Thumbnail != null && toValidate.Thumbnail.Length > 0;
    }
}
