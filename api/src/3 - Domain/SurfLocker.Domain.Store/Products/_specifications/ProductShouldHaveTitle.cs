﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHaveTitle : SpecificationBase<Product>
    {
        public override string Message => "Título do produto é obrigatório";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_TITLE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Title);
    }
}
