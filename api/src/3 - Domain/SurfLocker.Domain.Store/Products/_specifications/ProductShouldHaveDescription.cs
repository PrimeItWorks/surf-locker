﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHaveDescription : SpecificationBase<Product>
    {
        public override string Message => "Descrição do produto é obrigatório";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_DESCRIPTION.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Description);
    }
}
