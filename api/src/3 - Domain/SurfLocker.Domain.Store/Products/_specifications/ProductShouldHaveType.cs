﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHaveType : SpecificationBase<Product>
    {
        public override string Message => "Tipo de produto é obrigatório";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_TYPE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => toValidate.Type != ProductType.None;
    }
}
