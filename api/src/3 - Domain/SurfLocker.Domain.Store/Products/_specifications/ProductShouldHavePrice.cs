﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHavePrice : SpecificationBase<Product>
    {
        public override string Message => "Preço do produto é obrigatório";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_PRICE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => toValidate.Price > 0;
    }
}
