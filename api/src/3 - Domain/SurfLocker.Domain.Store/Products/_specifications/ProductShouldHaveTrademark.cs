﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Products.Specifications
{
    internal class ProductShouldHaveTrademark : SpecificationBase<Product>
    {
        public override string Message => "Marca do produto é obrigatória";
        public override string Code => ProductNotificationsCodes.PRODUCT_SHOULD_HAVE_TRADEMARK.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Product, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Trademark);
    }
}
