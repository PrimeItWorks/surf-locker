﻿namespace SurfLocker.Domain.Store.Products
{
    public enum ProductNotificationsCodes
    {
        PRODUCT_SHOULD_HAVE_TITLE,
        PRODUCT_SHOULD_HAVE_CODE,
        PRODUCT_CODE_SHOULD_BE_GUID,
        PRODUCT_SHOULD_HAVE_TRADEMARK,
        PRODUCT_SHOULD_HAVE_THUMBNAIL,
        PRODUCT_SHOULD_HAVE_IMAGE,
        PRODUCT_SHOULD_HAVE_DESCRIPTION,
        PRODUCT_SHOULD_HAVE_PRICE,
        PRODUCT_SHOULD_HAVE_TYPE
    }
}
