﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Store.Vouchers.Specifications;
using System.Collections.Generic;

namespace SurfLocker.Domain.Store.Vouchers
{
    internal class VoucherSpecifications : List<SpecificationBase<Voucher>>
    {
        public VoucherSpecifications()
        {
            AddRange(new List<SpecificationBase<Voucher>>() {
                new VoucherShouldHaveCode(),
                new VoucherShouldHaveProductCode(),
                new VoucherShouldHaveUserCode(),
                new VoucherCodeShouldBeGuid(),
                new VoucherShouldHaveDescription(),
                new VoucherShouldHaveQuantity(),
                new VoucherShouldHavePrice(),
                new VoucherShouldHavePrice(),
                new VoucherShouldHaveThumbnail(),
                new VoucherShouldHaveTitle(),
                new VoucherShouldHaveTrademark()
            });
        }
    }
}