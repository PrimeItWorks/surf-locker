using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using MongoDB.Bson;
using AutoMapper;
using System.Threading.Tasks;
using SurfLocker.Dto;
using System;
using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Store.Vouchers.Specifications;

namespace SurfLocker.Domain.Store.Vouchers
{
    public class Voucher
    {
        private readonly IVoucherRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;
        internal Voucher() { }
        internal Voucher(INotificationHandler notificationHandler
            , IVoucherRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
            Thumbnail = new string[] { };
        }

        public ObjectId _id { get; set; }
        public string Code { get; set; }
        public string Title{ get; set; }
        public string Trademark { get; set; }
        public string[] Thumbnail { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string UserCode { get; set; }
        public string ProductCode { get; set; }
        public string MachineKey { get; set; }
        public string MachineCode { get; set; }
        public bool Active { get; set; }
        public int Quantity { get; set; }
        public DateTime BuyDate { get; set; }
        public DateTime DispenseDate { get; set; }
        public decimal Value { get; set; }

        public bool IsValid => !_notificationHandler.HasNotification();

        public void Specify()
        {
            var specifications = new VoucherSpecifications();
            foreach (var specification in specifications)
            {
                var validation = specification.Condition();
                if (!validation(this))
                    _notificationHandler
                        .DefaultBuilder()
                        .WithCode(specification.Code)
                        .WithMessage(specification.Message)
                        .WithDetailMessage(specification.DetailMessage)
                        .RaiseNotification();
            }
        }

        public async Task<VoucherDto> AddAsync()
        {
            if (!IsValid)
                return null;

            var invalidMachineKey = _repository.CheckMachineKey(MachineKey);
            if (invalidMachineKey)
            {
                MachineKey = this.GenerateMachineKey();
                await AddAsync();
            }

            var response = await _repository.AddAsync(this);
            var result = _mapper.Map<VoucherDto>(response);
            return result;
        }

        public async Task<VoucherDto> ResumeAsync()
        {
            _notificationHandler.ClearNotifications();
            SpecifyOne(new VoucherShouldHaveCode());
            SpecifyOne(new VoucherShouldHaveMachineCode());

            if (!IsValid)
                return null;

            var response = await _repository.ResumeAsync(this);
            var result = _mapper.Map<VoucherDto>(response);
            return result;
        }

        public async Task<VoucherDto> DeactiveAsync(string code)
        {
            var voucher = await _repository.DeactivateAsync(code);
            return _mapper.Map<VoucherDto>(voucher);
        }

        private void SpecifyOne(SpecificationBase<Voucher> specification)
        {
            var validation = specification.Condition();
            if (!validation(this))
                _notificationHandler
                    .DefaultBuilder()
                    .WithCode(specification.Code)
                    .WithMessage(specification.Message)
                    .WithDetailMessage(specification.DetailMessage)
                    .RaiseNotification();
        }
    }
}
