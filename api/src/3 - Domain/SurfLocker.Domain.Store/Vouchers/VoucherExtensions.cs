﻿using System;

namespace SurfLocker.Domain.Store.Vouchers
{
    internal static class VoucherExtensions
    {
        private static readonly Random _random = new Random();
        public static string GenerateMachineKey(this Voucher voucher)
        {
            return _random.Next(1000, 9999).ToString();
        }
    }
}
