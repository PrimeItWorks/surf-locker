﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherCodeShouldBeGuid : SpecificationBase<Voucher>
    {
        public override string Message => "Código do voucher deve ser um identificador único";
        public override string Code => VoucherNotificationsCodes.VOUCHER_CODE_SHOULD_BE_GUID.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition()
        {
            Guid validationGuid;
            return toValidate => Guid.TryParse(toValidate.Code, out validationGuid);
        }
    }
}
