﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveDescription : SpecificationBase<Voucher>
    {
        public override string Message => "Descrição do produto é obrigatório";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_DESCRIPTION.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Description);
    }
}
