﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHavePrice : SpecificationBase<Voucher>
    {
        public override string Message => "Preço do produto é obrigatório";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_PRICE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => toValidate.Price > 0;
    }
}
