﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveProductCode : SpecificationBase<Voucher>
    {
        public override string Message => "Código do produto é obrigatório";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_PRODUCT_CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.ProductCode);
    }
}   
