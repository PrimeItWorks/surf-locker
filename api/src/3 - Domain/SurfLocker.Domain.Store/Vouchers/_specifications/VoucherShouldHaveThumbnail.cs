﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveThumbnail : SpecificationBase<Voucher>
    {
        public override string Message => "Thumbnails do produto são obrigatórios";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_THUMBNAIL.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => toValidate.Thumbnail != null && toValidate.Thumbnail.Length > 0;
    }
}
