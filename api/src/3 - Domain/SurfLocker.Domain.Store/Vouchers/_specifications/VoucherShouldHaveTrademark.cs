﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveTrademark : SpecificationBase<Voucher>
    {
        public override string Message => "Marca do produto é obrigatória";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_TRADEMARK.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Trademark);
    }
}
