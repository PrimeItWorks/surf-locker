﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveCode : SpecificationBase<Voucher>
    {
        public override string Message => "Código do voucher é obrigatório";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Code);
    }
}   
