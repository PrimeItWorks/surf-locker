﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveMachineCode : SpecificationBase<Voucher>
    {
        public override string Message => "Código da máquina que produto foi dispensado é obrigatório";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_MACHINE_CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.MachineCode);
    }
}   
