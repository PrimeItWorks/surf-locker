﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Vouchers.Specifications
{
    internal class VoucherShouldHaveQuantity : SpecificationBase<Voucher>
    {
        public override string Message => "Quantidade de vouchers é obrigatória";
        public override string Code => VoucherNotificationsCodes.VOUCHER_SHOULD_HAVE_QUANTITY.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Voucher, bool> Condition() =>
            toValidate => toValidate.Quantity > 0;
    }
}
