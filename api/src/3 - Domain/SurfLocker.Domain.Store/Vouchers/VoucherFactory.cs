﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using AutoMapper;

namespace SurfLocker.Domain.Store.Vouchers
{
    public class VoucherFactory: IVoucherFactory
    {
        private readonly IVoucherRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        public VoucherFactory(INotificationHandler notificationHandler
            , IVoucherRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public VoucherBuilder DefaultBuilder()
            => new VoucherBuilder(_notificationHandler, _repository, _mapper);

        public class VoucherBuilder
        {
            private readonly Voucher _product;
            private readonly INotificationHandler _notificationHandler;
            private readonly IMapper _mapper;
            private readonly IVoucherRepository _repository;

            public VoucherBuilder(INotificationHandler notificationHandler
                , IVoucherRepository repository
                , IMapper mapper)
            {
                _notificationHandler = notificationHandler;
                _mapper = mapper;
                _repository = repository;
                _product = new Voucher(notificationHandler, repository, _mapper);
            }

            public VoucherBuilder WithCode(string value)
            {
                _product.Code = value;
                return this;
            }

            public VoucherBuilder WithTitle(string value)
            {
                _product.Title = value;
                return this;
            }

            public VoucherBuilder WithTrademark(string value)
            {
                _product.Trademark = value;
                return this;
            }

            public VoucherBuilder WithThumbnail(string[] value)
            {
                _product.Thumbnail = value;
                return this;
            }

            public VoucherBuilder WithDescription(string value)
            {
                _product.Description = value;
                return this;
            }

            public VoucherBuilder WithPrice(decimal value)
            {
                _product.Price = value;
                return this;
            }

            public VoucherBuilder WithQuantity(int value)
            {
                _product.Quantity = value;
                return this;
            }

            public VoucherBuilder WithUserCode(string value)
            {
                _product.UserCode = value;
                return this;
            }

            public VoucherBuilder WithMachineCode(string value)
            {
                _product.MachineCode = value;
                return this;
            }

            public VoucherBuilder WithProductCode(string value)
            {
                _product.ProductCode = value;
                return this;
            }

            public VoucherBuilder WithMachineKey(string value)
            {
                _product.MachineKey = value;
                return this;
            }

            public VoucherBuilder GenerateMachineKey()
            {
                _product.MachineKey = _product.GenerateMachineKey();
                return this;
            }

            public Voucher Raise()
            {
                _product.Specify();
                return _product;
            }
        }
    }
}