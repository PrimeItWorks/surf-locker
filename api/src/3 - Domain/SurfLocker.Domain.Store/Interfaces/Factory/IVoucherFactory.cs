﻿using static SurfLocker.Domain.Store.Vouchers.VoucherFactory;

namespace SurfLocker.Domain.Store.Interfaces.Factory
{
    public interface IVoucherFactory
    {
        VoucherBuilder DefaultBuilder();
    }
}
