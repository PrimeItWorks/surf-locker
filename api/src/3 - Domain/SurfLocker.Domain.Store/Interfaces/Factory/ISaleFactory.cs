﻿using static SurfLocker.Domain.Store.Sales.SaleFactory;

namespace SurfLocker.Domain.Store.Interfaces.Factory
{
    public interface ISaleFactory
    {
        SaleBuilder DefaultBuilder();
    }
}
