﻿using static SurfLocker.Domain.Store.Products.ProductFactory;

namespace SurfLocker.Domain.Store.Interfaces.Factory
{
    public interface IProductFactory
    {
        ProductBuilder DefaultBuilder();
    }
}
