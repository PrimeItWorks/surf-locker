using SurfLocker.Domain.Store.Products;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SurfLocker.Domain.Store.Interfaces
{
    public interface IProductRepository
    {
        Task<ListResponseDto<ProductDto>> GetAllPagingAsync(ProductRequestAllDto request);
        Task<ProductDto> GetByCodeAsync(string code);
        Task<List<ProductDto>> GetByCodesAsync(string[] codes);
        Task<Product> UpdateAsync(Product user);
        Task AddAsync(Product request);
        bool Exists(string code);
        Task DeleteAsync(string code);
    }
}
