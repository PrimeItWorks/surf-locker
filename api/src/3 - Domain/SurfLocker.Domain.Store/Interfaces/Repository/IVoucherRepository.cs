using SurfLocker.Domain.Store.Vouchers;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using System.Threading.Tasks;

namespace SurfLocker.Domain.Store.Interfaces
{
    public interface IVoucherRepository
    {
        Task<ListResponseDto<VoucherDto>> GetAllPagingAsync(VoucherRequestAllDto request);
        Task<Voucher> AddAsync(Voucher request);
        Task<Voucher> ResumeAsync(Voucher request);
        bool CheckMachineKey(string machineKey);
        Task<Voucher> DeactivateAsync(string code);
        Task<VoucherDto> GetByMachineKey(string machineKey);
    }
}
