using SurfLocker.Domain.Store.Sales;
using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SurfLocker.Domain.Store.Interfaces
{
    public interface ISalesRepository
    {
        Task<SalesDto> GetByVoucherCodeAsync(string voucher);
        Task<ListResponseDto<SalesDto>> GetAllPagingAsync(SalesRequestAllDto request);
        Task<List<SalesDto>> GetAllAsync();
        Task<Sale> AddAsync(Sale request);
    }
}
