﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Store.Products;
using SurfLocker.Domain.Store.Sales;
using SurfLocker.Domain.Store.Vouchers;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class StoreDependencyInjection
    {
        public static IServiceCollection AddStoreDependency(this IServiceCollection services)
        {
            services.TryAddTransient<IProductFactory, ProductFactory>();
            services.TryAddTransient<IVoucherFactory, VoucherFactory>();
            services.TryAddTransient<ISaleFactory, SaleFactory>();

            return services;
        }
    }
}
