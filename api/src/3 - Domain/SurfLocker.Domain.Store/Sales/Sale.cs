using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using MongoDB.Bson;
using AutoMapper;
using System.Threading.Tasks;
using SurfLocker.Dto;
using System;
using SurfLocker.Domain.Store.Sales.Specifications;
using SurfLocker.Domain.Specifications;

namespace SurfLocker.Domain.Store.Sales
{
    public class Sale
    {
        private readonly ISalesRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;
        internal Sale() { }
        internal Sale(INotificationHandler notificationHandler
            , ISalesRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public ObjectId _id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Trademark { get; set; }
        public decimal Value { get; set; }
        public int Quantity { get; set; }
        public string UserCode { get; set; }
        public string TransactionId { get; set; }
        public string ProductCode { get; set; }
        public string MachineCode { get; set; }
        public DateTime? BuyDate { get; set; }
        public DateTime? DispenseDate { get; set; }
        public bool Online { get; set; }

        public bool IsValid => !_notificationHandler.HasNotification();

        public void Specify()
        {
            new SalesSpecifications()
            .ForEach(specification => SpecifyOne(specification));
        }

        public async Task<SalesDto> AddAsync()
        {
            if (!IsValid)
                return null;

            var response = await _repository.AddAsync(this);
            var result = _mapper.Map<SalesDto>(response);
            return result;
        }

        private void SpecifyOne(SpecificationBase<Sale> specification)
        {
            var validation = specification.Condition();

            if (!validation(this))
                _notificationHandler
                    .DefaultBuilder()
                    .WithCode(specification.Code)
                    .WithMessage(specification.Message)
                    .WithDetailMessage(specification.DetailMessage)
                    .RaiseNotification();
        }
    }
}
