﻿namespace SurfLocker.Domain.Store.Sales
{
    public enum SalesNotificationsCodes
    {
        SALES_CODE_SHOULD_BE_GUID,
        SALES_SHOULD_HAVE_CODE,
        SALES_SHOULD_HAVE_TITLE,
        SALES_SHOULD_HAVE_PRODUCT_CODE,
        SALES_SHOULD_HAVE_VALUE,
        SALES_SHOULD_HAVE_QUANTITY,
        SALES_SHOULD_HAVE_TRADEMARK,
        SALES_SHOULD_HAVE_MACHINE_CODE
    }
}
