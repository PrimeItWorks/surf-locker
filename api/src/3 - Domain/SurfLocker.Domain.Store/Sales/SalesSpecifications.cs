﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Store.Sales.Specifications;
using System.Collections.Generic;

namespace SurfLocker.Domain.Store.Sales
{
    internal class SalesSpecifications : List<SpecificationBase<Sale>>
    {
        public SalesSpecifications()
        {
            AddRange(new List<SpecificationBase<Sale>>() {
                new SalesShouldHaveCode(),
                new SalesShouldHaveProductCode(),
                new SalesCodeShouldBeGuid(),
                new SalesShouldHaveTitle(),
                new SalesShouldHaveTrademark(),
                new SalesShouldHaveQuantity()
            });
        }
    }
}