﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveQuantity : SpecificationBase<Sale>
    {
        public override string Message => "Quantidade de itens vendidos é obrigatório";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_QUANTITY.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => toValidate.Quantity > 0;
    }
}
