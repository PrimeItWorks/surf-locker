﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveTitle : SpecificationBase<Sale>
    {
        public override string Message => "Título do produto é obrigatório";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_TITLE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Title);
    }
}
