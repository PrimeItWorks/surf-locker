﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesCodeShouldBeGuid : SpecificationBase<Sale>
    {
        public override string Message => "Código da venda deve ser um identificador único";
        public override string Code => SalesNotificationsCodes.SALES_CODE_SHOULD_BE_GUID.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition()
        {
            Guid validationGuid;
            return toValidate => Guid.TryParse(toValidate.Code, out validationGuid);
        }
    }
}
