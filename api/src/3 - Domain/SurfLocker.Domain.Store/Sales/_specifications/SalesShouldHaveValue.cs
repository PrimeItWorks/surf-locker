﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveValue : SpecificationBase<Sale>
    {
        public override string Message => "Valor da venda é obrigatório";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_VALUE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => toValidate.Value > 0;
    }
}
