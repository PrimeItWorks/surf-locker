﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveCode : SpecificationBase<Sale>
    {
        public override string Message => "Código do voucher é obrigatório";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Code);
    }
}   
