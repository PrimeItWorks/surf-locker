﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveMachineCode : SpecificationBase<Sale>
    {
        public override string Message => "Código do produto é obrigatório";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_MACHINE_CODE.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.MachineCode);
    }
}   
