﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Store.Sales.Specifications
{
    internal class SalesShouldHaveTrademark : SpecificationBase<Sale>
    {
        public override string Message => "Marca do produto é obrigatória";
        public override string Code => SalesNotificationsCodes.SALES_SHOULD_HAVE_TRADEMARK.ToString();
        public override string DetailMessage => string.Empty;

        public override Func<Sale, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Trademark);
    }
}
