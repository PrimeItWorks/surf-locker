﻿using SurfLocker.Domain.Store.Interfaces.Factory;
using SurfLocker.Domain.Store.Interfaces;
using SurfLocker.Domain.Notifications;
using AutoMapper;
using System;

namespace SurfLocker.Domain.Store.Sales
{
    public class SaleFactory: ISaleFactory
    {
        private readonly ISalesRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        public SaleFactory(INotificationHandler notificationHandler
            , ISalesRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public SaleBuilder DefaultBuilder()
            => new SaleBuilder(_notificationHandler, _repository, _mapper);

        public class SaleBuilder
        {
            private readonly Sale _product;
            private readonly INotificationHandler _notificationHandler;
            private readonly IMapper _mapper;
            private readonly ISalesRepository _repository;

            public SaleBuilder(INotificationHandler notificationHandler
                , ISalesRepository repository
                , IMapper mapper)
            {
                _notificationHandler = notificationHandler;
                _mapper = mapper;
                _repository = repository;
                _product = new Sale(notificationHandler, repository, _mapper);
            }

            public SaleBuilder WithCode(string value)
            {
                _product.Code = value;
                return this;
            }

            public SaleBuilder WithTitle(string value)
            {
                _product.Title = value;
                return this;
            }

            public SaleBuilder WithTrademark(string value)
            {
                _product.Trademark = value;
                return this;
            }

            public SaleBuilder WithValue(decimal value)
            {
                _product.Value = value;
                return this;
            }

            public SaleBuilder WithQuantity(int value)
            {
                _product.Quantity = value;
                return this;
            }

            public SaleBuilder WithUserCode(string value)
            {
                _product.UserCode = value;
                return this;
            }

            public SaleBuilder WithTransactonId(string value)
            {
                _product.TransactionId = value;
                return this;
            }

            public SaleBuilder WithMachineCode(string value)
            {
                _product.MachineCode = value;
                return this;
            }

            public SaleBuilder WithOnline(bool value)
            {
                _product.Online = value;
                return this;
            }

            public SaleBuilder WithProductCode(string value)
            {
                _product.ProductCode = value;
                return this;
            }

            public Sale Raise()
            {
                _product.Specify();
                return _product;
            }
        }
    }
}