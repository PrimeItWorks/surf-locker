﻿using SurfLocker.Domain.Store.Vouchers;
using SurfLocker.Domain.Store.Products;
using SurfLocker.Dto;
using AutoMapper;

namespace SurfLocker.Domain.Mapper
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<Product, ProductDto>();
            CreateMap<Voucher, VoucherDto>();
            CreateMap<ProductType, ProductTypeDto>();
        }
    }
}
