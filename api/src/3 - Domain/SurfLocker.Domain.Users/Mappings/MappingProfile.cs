﻿using SurfLocker.Domain.Users;
using SurfLocker.Dto;
using AutoMapper;
using SurfLocker.Dto.Payment;

namespace SurfLocker.Domain.Mapper
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(dest => dest.Password
                    , config => config.MapFrom(from => string.Empty));

            CreateMap<CardBrand, CardBrandDto>();
        }
    }
}
