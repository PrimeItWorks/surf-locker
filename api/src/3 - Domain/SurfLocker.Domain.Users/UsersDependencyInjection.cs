﻿using Microsoft.Extensions.DependencyInjection.Extensions;
using SurfLocker.Domain.Users;
using SurfLocker.Domain.Users.Interfaces.Factory;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class UsersDependencyInjection
    {
        public static IServiceCollection AddUsersDependency(this IServiceCollection services)
        {
            services.TryAddTransient<IUserFactory, UserFactory>();

            return services;
        }
    }
}   
