﻿using static SurfLocker.Domain.Users.UserFactory;

namespace SurfLocker.Domain.Users.Interfaces.Factory
{
    public interface IUserFactory
    {
        UserBuilder DefaultBuilder();
    }
}
