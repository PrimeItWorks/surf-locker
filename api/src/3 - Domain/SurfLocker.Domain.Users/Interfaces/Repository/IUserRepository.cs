using SurfLocker.Dto;
using SurfLocker.Dto.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SurfLocker.Domain.Users.Interfaces
{
    public interface IUserRepository
    {
        Task<UserDto> GetByCodeAsync(string code);
        Task<List<UserDto>> GetByCodesAsync(string[] codes);
        Task<ListResponseDto<UserDto>> GetAllPagingAsync(UserRequestAllDto request);
        Task<bool> ValidateLogin(string facebookId);
        Task<bool> ValidateLogin(string email, string password, bool isAdmin = false);
        Task<UserDto> GetByFacebookId(string facebookId);
        Task<UserDto> GetByEmail(string email);
        Task AddAsync(User request);
        Task MakeAdminAsync(User request);
        Task MakeNotAdminAsync(User request);
        Task<User> UpdateAsync(User code);
        Task<User> UpdateFieldAsync(string userCode, string column, object value);
    }
}
