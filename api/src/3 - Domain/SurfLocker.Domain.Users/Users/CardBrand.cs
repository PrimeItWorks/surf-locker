﻿namespace SurfLocker.Domain.Users
{
    public enum CardBrand
    {
        Unknow = 0,
        Visa = 1,
        Mastercard = 2,
        Diners = 3,
        Elo = 4,
        Amex = 5,
        Discover = 6,
        Aura = 7,
        JCB = 8,
        Hipercard = 9,
        Maestro = 10
    }
}
