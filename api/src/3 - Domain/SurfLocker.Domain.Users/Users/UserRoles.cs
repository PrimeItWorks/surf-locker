﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SurfLocker.Domain.Users.Users
{
    public static class UserRoles
    {
        public const string ADMIN = "Administrador";
        public const string APP = "Aplicativo";
    }
}
