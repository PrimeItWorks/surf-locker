﻿
using SurfLocker.Domain.Users.Interfaces.Factory;
using SurfLocker.Domain.Notifications;
using SurfLocker.Domain.Users.Interfaces;
using AutoMapper;
using System;

namespace SurfLocker.Domain.Users
{
    public class UserFactory: IUserFactory
    {
        private readonly IUserRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        public UserFactory(INotificationHandler notificationHandler
            , IUserRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public UserBuilder DefaultBuilder()
            => new UserBuilder(_notificationHandler, _repository, _mapper);

        public class UserBuilder
        {
            private readonly User _user;
            private readonly INotificationHandler _notificationHandler;
            private readonly IMapper _mapper;
            private readonly IUserRepository _repository;

            public UserBuilder(INotificationHandler notificationHandler
                , IUserRepository repository
                , IMapper mapper)
            {
                _notificationHandler = notificationHandler;
                _mapper = mapper;
                _repository = repository;
                _user = new User(notificationHandler, repository, _mapper);
            }

            public UserBuilder WithCode(string value)
            {
                _user.Code = value;
                return this;
            }

            public UserBuilder WithBirthday(DateTime? value)
            {
                if(!value.HasValue)
                    return this;

                _user.Birthday = value.Value;
                return this;
            }

            public UserBuilder WithBirthday(string value)
            {
                _user.Birthday = Convert.ToDateTime(value);
                return this;
            }

            public UserBuilder WithName(string value)
            {
                _user.Name = value;
                return this;
            }

            public UserBuilder WithEmail(string value)
            {
                _user.Email = value;
                return this;
            }

            public UserBuilder WithIsAdmin(bool value)
            {
                _user.IsAdmin = value;
                return this;
            }

            public UserBuilder WithPhone(string value)
            {
                _user.Phone = value;
                return this;
            }

            public UserBuilder WithPassword(string value)
            {
                if(string.IsNullOrEmpty(value))
                    return this;

                _user.Password = value.Encrypt();
                return this;
            }

            public UserBuilder WithFacebookId(string value)
            {
                _user.FacebookId = value;
                return this;
            }

            public UserBuilder WithCardToken(string value)
            {
                _user.CardToken = value;
                return this;
            }

            public UserBuilder WithLatestDigitCard(string value)
            {
                _user.LatestDigitCard = value;
                return this;
            }

            public UserBuilder WithCpf(string value)
            {
                _user.Cpf = value;
                return this;
            }

            public UserBuilder WithCardBrand(int value)
            {
                _user.CardBrand = (CardBrand) value;
                return this;
            }

            public UserBuilder WithAddress(string value)
            {
                _user.Address = value;
                return this;
            }

            public UserBuilder WithWaves(string[] value)
            {
                _user.Waves = value;
                return this;
            }

            public UserBuilder WithConfirmedNumber(bool value)
            {
                _user.ConfirmedNumber = value;
                return this;
            }


            public User Raise()
            {
                _user.Specify();
                return _user;
            }
        }
    }
}