﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Users.Specifications;
using System.Collections.Generic;

namespace SurfLocker.Domain.Users
{
    internal class UserSpecifications : List<SpecificationBase<User>>
    {
        public UserSpecifications(IUserRepository userRepository)
        {
            AddRange(new List<SpecificationBase<User>>() {
                new UserCodeShouldHaveValue(),
                new UserCodeShouldBeGuid(),
                new UserShouldHaveContact(),
                new UserShouldHaveName(),
                new UserShouldHaveEmailOrFacebook(),
                new UserEmailShouldNotBeDuplicated(userRepository)
            });
        }
    }
}