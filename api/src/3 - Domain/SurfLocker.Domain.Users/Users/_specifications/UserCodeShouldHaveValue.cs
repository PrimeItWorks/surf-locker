﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserCodeShouldHaveValue : SpecificationBase<User>
    {
        public override string Message => "Código do usuário é obrigatório";

        public override string Code => UserNotificationsCodes.USER_CODE_SHOULD_HAVE_VALUE.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<User, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Code);
    }
}
