﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserShouldHaveContact : SpecificationBase<User>
    {
        public override string Message => "Usuário deve ter e-mail ou telefone.";

        public override string Code => UserNotificationsCodes.USER_SHOULD_HAVE_CONTACT.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<User, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Email) 
                || !string.IsNullOrEmpty(toValidate.Phone);
    }
}
