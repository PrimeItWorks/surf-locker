﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserFieldToUpdateShouldHaveValue : SpecificationBase<string>
    {
        public override string Message => "Código do usuário é obrigatório";

        public override string Code => UserNotificationsCodes.USER_CODE_SHOULD_HAVE_VALUE.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<string, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate);
    }
}
