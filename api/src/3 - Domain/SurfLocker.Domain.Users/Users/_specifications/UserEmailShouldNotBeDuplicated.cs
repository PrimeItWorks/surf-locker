﻿using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Users.Interfaces;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserEmailShouldNotBeDuplicated : SpecificationBase<User>
    {
        private IUserRepository _userRepository;
        public UserEmailShouldNotBeDuplicated(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public override string Message => "Usuário deve ter e-mail único";

        public override string Code => UserNotificationsCodes.USER_SHOULD_HAVE_EMAIL_UNIQUE.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<User, bool> Condition() =>
            toValidate =>  !IsDuplicated(toValidate.Email, toValidate.Code);

        public bool IsDuplicated(string email, string code)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            var task = _userRepository.GetByEmail(email);
            task.Wait();

            

            return task.Result != null && task.Result.Code != code;
        }
    }
}
