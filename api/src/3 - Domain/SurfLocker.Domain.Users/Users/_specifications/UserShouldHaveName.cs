﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserShouldHaveName : SpecificationBase<User>
    {
        public override string Message => "Usuário deve estar vinculado ao facebook";

        public override string Code => UserNotificationsCodes.USER_SHOULD_HAVE_FACEBOOK_ID_OR_EMAIL.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<User, bool> Condition() =>
            toValidate => !string.IsNullOrEmpty(toValidate.Name);
    }
}
