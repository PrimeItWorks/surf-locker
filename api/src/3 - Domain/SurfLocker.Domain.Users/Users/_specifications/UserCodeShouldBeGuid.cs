﻿using SurfLocker.Domain.Specifications;
using System;

namespace SurfLocker.Domain.Users.Specifications
{
    internal class UserCodeShouldBeGuid : SpecificationBase<User>
    {
        public override string Message => "Código do usuário deve ser um identificador único";

        public override string Code => UserNotificationsCodes.USER_CODE_SHOULD_BE_GUID.ToString();

        public override string DetailMessage => string.Empty;

        public override Func<User, bool> Condition()
        {
            Guid validationGuid;
            return toValidate => Guid.TryParse(toValidate.Code, out validationGuid);
        }
    }
}
