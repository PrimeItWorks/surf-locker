﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace SurfLocker.Domain.Users
{
    public static class UserExtensions
    {
        private static readonly Random _random = new Random();
        private const string encryptKey = "KEY1";
        public static string GeneratePhoneConfirmationCode(this User user)
        {
            return _random.Next(1000, 9999).ToString();
        }

        public static string Encrypt(this string toEncrypt)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(toEncrypt);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    toEncrypt = Convert.ToBase64String(ms.ToArray());
                }
            }

            return toEncrypt;
        }
    }
}
