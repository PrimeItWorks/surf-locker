﻿using SurfLocker.Domain.Users.Interfaces;
using SurfLocker.Domain.Notifications;
using System.Threading.Tasks;
using SurfLocker.Dto;
using MongoDB.Bson;
using AutoMapper;
using SurfLocker.Domain.Specifications;
using SurfLocker.Domain.Users.Specifications;
using System;

namespace SurfLocker.Domain.Users
{
    public class User
    {
        private readonly IUserRepository _repository;
        private readonly INotificationHandler _notificationHandler;
        private readonly IMapper _mapper;

        internal User() { }
        internal User(INotificationHandler notificationHandler
            , IUserRepository repository
            , IMapper mapper)
        {
            _repository = repository;
            _notificationHandler = notificationHandler;
            _mapper = mapper;
        }

        public ObjectId _id { get; set; }
        public string Code { get; set; }
        public string FacebookId { get; set; }
        public string Name { get; set; }
        public string Cpf { get; set; }
        public DateTime? Birthday { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsAdmin { get; set; }
        public string Address { get; set; }
        public string[] Waves { get; set; }
        public string CardToken { get; set; }
        public string LatestDigitCard { get; set; }
        public bool ConfirmedNumber { get; set; }
        public CardBrand CardBrand { get; set; }
        public string Password { get; set; }

        public bool IsValid => !_notificationHandler.HasNotification();

        public void Specify()
        {
            var specifications = new UserSpecifications(_repository);
            foreach (var specification in specifications)
            {
                SpecifyOne(specification);
            }
        }

        public string GeneratePhoneConfirmation() => this.GeneratePhoneConfirmationCode();

        public async Task AddAsync()
        {
            if (!IsValid)
                return;

            await _repository.AddAsync(this);
        }

        public async Task MakeAdminAsync()
        {
            if (string.IsNullOrEmpty(Code))
            {
                _notificationHandler.ClearNotifications();

                SpecifyOne(new UserCodeShouldHaveValue());

                return;
            }

            await _repository.MakeAdminAsync(this);
        }

        public async Task MakeNotAdminAsync()
        {
            if (string.IsNullOrEmpty(Code))
            {
                _notificationHandler.ClearNotifications();

                SpecifyOne(new UserCodeShouldHaveValue());

                return;
            }

            await _repository.MakeNotAdminAsync(this);
        }

        public async Task<UserDto> UpdateAsync()
        {
            if (!IsValid)
                return null;

            var user = await _repository.UpdateAsync(this);
            return _mapper.Map<UserDto>(user);
        }

        public async Task UpdateCpfAsync() 
        {
            _notificationHandler.ClearNotifications();
            SpecifyOne(new UserCodeShouldHaveValue());
            SpecifyOne(new UserCpfShouldHaveValue());

            if(!IsValid)
                return;

            await _repository.UpdateFieldAsync(Code, nameof(Cpf), Cpf);
        }

        public async Task UpdateBirthdayAsync() 
        {
            _notificationHandler.ClearNotifications();
            SpecifyOne(new UserCodeShouldHaveValue());
            SpecifyOne(new UserBirthdayShouldHaveValue());

            if(!IsValid)
                return;

            await _repository.UpdateFieldAsync(Code, nameof(Birthday), Birthday);
        }
    
        private void SpecifyOne(SpecificationBase<User> specification)
        {
            var validation = specification.Condition();
            if (!validation(this))
                _notificationHandler
                    .DefaultBuilder()
                    .WithCode(specification.Code)
                    .WithMessage(specification.Message)
                    .WithDetailMessage(specification.DetailMessage)
                    .RaiseNotification();
        }
    }
}
