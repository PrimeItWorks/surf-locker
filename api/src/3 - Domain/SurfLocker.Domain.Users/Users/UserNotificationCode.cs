﻿namespace SurfLocker.Domain.Users
{
    public enum UserNotificationsCodes
    {
        USER_CODE_SHOULD_HAVE_VALUE,
        USER_CPF_SHOULD_HAVE_VALUE,
        USER_CODE_SHOULD_BE_GUID,
        USER_SHOULD_HAVE_FACEBOOK_ID_OR_EMAIL,
        USER_SHOULD_HAVE_EMAIL_UNIQUE,
        USER_NEEDS_EMAIL_TO_BE_ADMIN,
        USER_SHOULD_HAVE_CONTACT
    }
}
