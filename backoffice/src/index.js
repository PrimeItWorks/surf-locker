import React from "react"
import ReactDOM from "react-dom"
import { createBrowserHistory } from "history"
import { Router, Route, Switch, Redirect } from "react-router-dom"
import { Provider } from 'react-redux'

import AuthLayout from "layouts/Auth/Auth.jsx"
import AdminLayout from "layouts/Admin/Admin.jsx"

import {AUTHENTICATED} from './redux/actions/login'
import configureStore from './stores/configureStore'

import {unregisterFetchInterceptor} from './fetchInterceptor'

import "bootstrap/dist/css/bootstrap.css"
import "assets/scss/paper-dashboard.scss"
import "assets/demo/demo.css"
import { AUTH_DATA_KEY } from "./_helpers/AuthStorage"

const hist = createBrowserHistory()

const store = configureStore()
// const user = localStorage.getItem(AUTH_DATA_KEY)

// if(user) {
//   store.dispatch({ type: AUTHENTICATED })
// }

ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path="/auth" render={props => <AuthLayout {...props} />} />
        <Route path="/admin" render={props => <AdminLayout {...props} />} />
        <Redirect from="/" to="/auth/login" />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
)
