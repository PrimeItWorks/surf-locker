import React from "react"
import Login from "./views/pages/Login";
import ProductsMachine from "./views/products/machine/ProductsMachine";
import ProductsMachineForm from "./views/products/machine/ProductsMachineForm";
import Users from "./views/users/Users";
import ProductsStore from "./views/products/store/ProductsStore";
import ProductsStoreForm from "./views/products/store/ProductsStoreForm";
import Machines from "./views/machines/Machines";
import MachineForm from "./views/machines/MachineForm";
import SalesReport from "./views/reports/sales/SalesReport";
import Dash from "./views/dashboard/Dash";

const routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-36",
    component: Dash,
    layout: "/admin"
  },

  {
    path: "/clientes",
    name: "Clientes do App",
    icon: "nc-icon nc-user-run",
    component: Users,
    layout: "/admin"
  },

  {
    redirect: true,
    path: "/maquinas/editar",
    name: "Editar Máquina",
    component: MachineForm,
    layout: "/admin"
  },
  {
    redirect: true,
    path: "/maquinas/criar",
    name: "Criar Máquina",
    component: MachineForm,
    layout: "/admin"
  },

  {
    path: "/maquinas",
    name: "Máquinas",
    icon: "nc-icon nc-box-2",
    component: Machines,
    layout: "/admin"
  },

  {
    redirect: true,
    path: "/produtos-maquina/criar",
    name: "Criar Produto",
    component: ProductsMachineForm,
    layout: "/admin"
  },
 
  {
    redirect: true,
    path: "/produtos-maquina/editar",
    name: "Editar Produto",
    component: ProductsMachineForm,
    layout: "/admin"
  },
  {
    redirect: true,
    path: "/produtos-loja/criar",
    name: "Criar Produto",
    component: ProductsStoreForm,
    layout: "/admin"
  },
  {
    redirect: true,
    path: "/produtos-loja/editar",
    name: "Edirtar Produto",
    component: ProductsStoreForm,
    layout: "/admin"
  },
  {
    collapse: true,
    name: "Produtos",
    icon: "nc-icon nc-money-coins",
    state: "pagesCollapse",
    views: [
      {
        path: "/produtos-maquina",
        name: "Produtos de máquinas",
        mini: <i className="sidebar-mini-icon nc-icon nc-box-2 pl-3" />,
        component: ProductsMachine,
        layout: "/admin"
      },
      {
        path: "/produtos-loja",
        name: "Produtos de loja",
        mini: <i className="sidebar-mini-icon nc-icon nc-money-coins pl-3" />,
        component: ProductsStore,
        layout: "/admin"
      }
    ]
  },

  {
    collapse: true,
    name: "Relatórios",
    icon: "nc-icon nc-chart-bar-32",
    state: "reportsCollapse",
    views: [
      {
        path: "/relatorio-vendas",
        name: "Relatório de vendas",
        mini: <i className="sidebar-mini-icon nc-icon nc-credit-card pl-3" />,
        component: SalesReport,
        layout: "/admin"
      }
    ]
  },

  {
    redirect: true,
    path: "/login",
    name: "Login",
    component: Login,
    layout: "/auth"
  }
];

export default routes;
