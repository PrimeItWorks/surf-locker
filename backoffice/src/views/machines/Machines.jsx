import React from "react"
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import ReactBSAlert from "react-bootstrap-sweetalert"
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Table,
  Button,
  Row,
  UncontrolledTooltip,
  Collapse,
  UncontrolledCollapse,
  Pagination,
  PaginationItem,
  PaginationLink,
  Input,
  FormGroup,
  Col} from "reactstrap"
  

import './Machines.scss'
import MachineService from "../../services/MachineService"

import * as modalActions  from '../../redux/actions/modals'
import * as loadingActions  from '../../redux/actions/loading'
import * as machineActions  from '../../redux/actions/machines'
import { CURRENCY_OPTIONS } from "../../variables/currencyOptions";

class Machines extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            machines: null,
            alert: null,
            openActives: true,
            defaultFilter: {
                search:'',
                page:1,
                pageSize: 12,
                orderBy: '_id',
                onlyActive: true,
                onlyInactive:false,
                desc: false
            },
            activeFilter: {
                search:'',
                page:1,
                pageSize: 10,
                orderBy: '_id',
                orderByDir: {
                    title: 'sort',
                    price: 'sort',
                },
                onlyActive: true,
                onlyInactive:false,
                desc: false
            }
        }

        this.machineService = new MachineService()

        this.getPagingMachines()
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0)
        while (id--) {
            window.clearTimeout(id)
        }
        window.scrollTo(0, 0)
    }

    getPagingMachines = async () => {
        const {loading, loaded} = this.props
        loading()
        const { search, page, pageSize, orderBy, desc } = this.state.activeFilter
        const request = { search, page, pageSize, orderBy, desc}

        const response = await this.machineService.getMachines(request)
        this.setState({ machines: response})
        loaded()
        console.log("response >>>>>>>>>>>", response)
    }

    orderActive = (field) => {
        let activeFilter = this.state.activeFilter

        if (!field)
            activeFilter = this.state.defaultFilter

        if(activeFilter.orderByDir[field] === 'sort')
            activeFilter.orderByDir[field] = 'sort-up' 
        else if(activeFilter.orderByDir[field] === 'sort-up')
            activeFilter.orderByDir[field] = 'sort-down'
        else
            activeFilter.orderByDir[field] = 'sort'

        if (activeFilter.orderByDir[field] ==='sort-down')
            activeFilter.desc = true
        else if (activeFilter.orderByDir[field] ==='sort-up')
            activeFilter.desc = false
        else {
            activeFilter.desc = this.state.defaultFilter.desc
            activeFilter.orderBy = this.state.defaultFilter.orderBy
            this.setState({activeFilter:activeFilter})
            this.props.getPagingMachineMachines(activeFilter)
            return
        }

        switch (field) {
            case 'title':
                activeFilter.orderBy = 'Description'
                break
            case 'price':
                activeFilter.orderBy = 'Price'
                break

            default:
                activeFilter.orderBy = this.state.defaultFilter.orderBy
        }

        this.setState({activeFilter:activeFilter})
    }

    titles = () => (
        <tr>
            <th className="title" >Nome</th>
            <th className="text-right actions"></th>
        </tr>
    )

    register = (item, index) =>{
        let color = "rgba(40, 167, 125, 0.36)"
        if ((item.prodCod1 && item.prodQtd1 < 6)
            || (item.prodCod2 && item.prodQtd2 < 6)
            || (item.prodCod3 && item.prodQtd3 < 6)) {
            color = "rgba(167, 40, 40, 0.36)"
        }
        else if ((item.prodCod1 && item.prodQtd1 < 10)
            || (item.prodCod2 && item.prodQtd2 < 10)
            || (item.prodCod3 && item.prodQtd3 < 10)) {
            color = "#FFC907"
        }

        return <Col md="6" sm="6" lg="4" xs="12">
            <Card style={{
                boxShadow: "inset 0px 0px 250px " + color
            }}>
                <CardHeader className="machines__active__card__header">
                    <h4 className="mb-3">
                        {`${item.title} - ${item.place}`}
                    </h4>                    
                </CardHeader>
                <CardBody>
                        {!item.prodCod1 ? null : 
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    {item.prodName1}
                                    <Row>
                                        <Col>Quantidade:</Col>
                                        <Col>{item.prodQtd1}</Col>
                                    </Row>
                                    <Row>
                                        <Col>Preço:</Col>
                                        <Col>{item.prodValue1.toLocaleString('pt-BR', CURRENCY_OPTIONS)}</Col>
                                    </Row>
                                </Col>
                            </Row>
                        }
                        {!item.prodCod2 ? null : 
                            <Row className="mt-2">
                                <Col xs={12} sm={12} md={12} lg={12}>
                                            {item.prodName2}
                                            <Row>
                                                <Col>Quantidade:</Col>
                                                <Col>{item.prodQtd2}</Col>
                                            </Row>
                                            <Row>
                                                <Col>Preço:</Col>
                                                <Col>{item.prodValue2.toLocaleString('pt-BR', CURRENCY_OPTIONS)}</Col>
                                            </Row>
                                </Col>
                            </Row>
                        }
                        {!item.prodCod3 ? null : 
                            <Row className="mt-2">
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    {item.prodName3}
                                    <Row>
                                        <Col>Quantidade:</Col>
                                        <Col>{item.prodQtd3}</Col>
                                    </Row>
                                    <Row>
                                        <Col>Preço:</Col>
                                        <Col>{item.prodValue3.toLocaleString('pt-BR', CURRENCY_OPTIONS)}</Col>
                                    </Row>
                                </Col>
                            </Row>
                        }
                </CardBody>
                <CardFooter>
                    <div className="float-right">
                        <Button value={item.code} onClick={this.handleEdit} id={`edit-${index}`} className="btn-icon" color="success" >
                            <i value={item.code} className="fas fa-edit"></i>
                        </Button>{` `}
                        <Button className="btn-icon" color="danger"   id={`delete-${index}`} value={item.code} onClick={this.delete}  >
                            <i value={item.code} className="fas fa-trash"></i>
                        </Button>
                        <UncontrolledTooltip placement="bottom" target={`edit-${index}`} delay={0}>
                            Editar
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement="bottom" target={`delete-${index}`} delay={0}>
                            Excluir
                        </UncontrolledTooltip>
                    </div>
                </CardFooter>
            </Card>
        </Col>
        }
    delete = (e) => {
        this.setState({
            toDelete: e.target.attributes["value"].value,
            alert: (
                <ReactBSAlert
                    warning
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Tem certeza?"
                    onConfirm={async () => {
                        this.hideAlert()

                        const {toDelete} = this.state
                        const {openSuccessModal, loading, loaded} = this.props

                        loading()
                        const result = await this.machineService.deleteMachine(toDelete)
                        loaded()

                        if(result.response) {
                            const success = {title: 'Alohaa! . m/', description: `Esse produto não está mais disponível no app.`}
                            openSuccessModal(success)
                            this.getPagingMachines()
                        }
                    }}
                    onCancel={() => this.cancelDetele()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    showCancel
                    >
                    Essa ação não poderá ser revertida.
                </ReactBSAlert>
            )
          })
    }
    
    cancelDetele = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Cancelado"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
            >
              O produto não foi excluido :)
            </ReactBSAlert>
          )
        })
      }

    hideAlert = () => {
        this.setState({
          alert: null
        })
    }

    handleEdit = (e) => {
        e.preventDefault()

        const id = e.target.attributes["value"].value
        const {machines} = this.state
        
        let item = machines.items.find(c => c.code == id)
        
        if(item) {
            this.props.history.push(`/admin/maquinas/editar/${item.trademark} ${item.title}`)
            const {setMachineToEdit} = this.props
            setMachineToEdit(item)
        }
    }   

    handleSearch =  async () => {
        var activeFilter = this.state.activeFilter
        activeFilter.page = 1
        this.setState({search : activeFilter }, await this.getPagingMachines())
    } 

    handleChangePageSize = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.pageSize = e.value

        this.setState({ activeFilter: activeFilter }, await this.getPagingMachines())
    }

    searchBtn = () => {
        return (
            <button className="btn btn-primary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleSearch()
                }} >
                Pesquisar
            </button>
        )
    }

    list = () => {
        const {openActives, machines} = this.state
        const {search} = this.state.activeFilter

        return (<Row className="machines__active">
            <Col md="12">
                <Card className="machines__active__card effect4">
                    <CardHeader className="machines__active__card__header">
                        <h4 className="mb-3">
                            MÁQUINAS
                        </h4>
                        <hr/>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col xs={12} sm={4} md={4} lg={4} >
                                <FormGroup className="has-label">
                                    <label htmlFor="searchActive">Buscar rápida</label>
                                        <Input 
                                            id="search"
                                            name="search"
                                            type="text"
                                            placeholder="Buscar por..."
                                            value={search}
                                            onChange={e => {
                                                e.preventDefault()
                                                let {activeFilter} = this.state

                                                activeFilter.search = e.target.value
                                                
                                                this.setState({activeFilter: activeFilter})
                                            }}
                                        />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col className="text-right" xs={12} sm={12} md={12} lg={12}>
                                {this.exportBtn()}
                                {this.searchBtn()}
                            </Col>
                        </Row>
                    </CardBody>
                </Card>

                <Row>
                        {
                            machines 
                                && machines.items 
                                && machines.items.length > 0
                                    ? machines.items.map((item,index) => this.register(item,index))
                                    : null
                        }  
                </Row>

                <Row>
                    <Col  xs={12} sm={12} md={12} lg={12}>
                        <Pagination listClassName="mt-3 justify-content-end">
                        {machines
                            ? <PaginationItem disabled>
                                    <PaginationLink href="#">
                                        <i className="fas fa-list"></i>{"   "} Total: {machines.count}
                                    </PaginationLink>
                                </PaginationItem>
                            : null}

                            {this.state.activeFilter.page > 1
                                ? (<PaginationItem >
                                        <PaginationLink href="javascript:void(0)" onClick={this.handlePrevPage}>
                                            <i className="fas fa-arrow-left"></i>
                                        </PaginationLink>
                                    </PaginationItem>)

                                : (<PaginationItem disabled>
                                    <PaginationLink href="#">
                                        <i className="fas fa-arrow-left"></i>
                                    </PaginationLink>
                                </PaginationItem>)}
                            {machines 
                                && machines.hasNextPage
                                
                                ? (<PaginationItem >
                                        <PaginationLink href="javascript:void(0)" onClick={this.handleNextPage}>
                                            <i className="fas fa-arrow-right"></i>
                                        </PaginationLink>
                                    </PaginationItem>)

                                : (<PaginationItem disabled>
                                    <PaginationLink href="#">
                                        <i className="fas fa-arrow-right"></i>
                                    </PaginationLink>
                                </PaginationItem>)}
                        </Pagination>
                    </Col>
                </Row>
            </Col>

        </Row>)
    }

    handleNextPage = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.page++

        this.setState({ activeFilter: activeFilter }, await this.getPagingMachines())
    }

    handlePrevPage = async (e) => {
        e.preventDefault()
        let activeFilter = this.state.activeFilter
        activeFilter.page--
        this.setState({ activeFilter: activeFilter }, await this.getPagingMachines())
    }

    add = (e) => {
        e.preventDefault()
        this.props.history.push("/admin/maquinas/criar")
    }

    handleExport = async () => {
        const {activeFilter} = this.state
        await this.machineService.getExcel(activeFilter)
    }

    exportBtn = () => {
        return (
            <button className="btn btn-secondary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleExport()
                }} >
                Exportar
            </button>
        )
    }

    render() {
        return (
            <div className="content machines">
                <Row>
                    <Col className="text-right">
                        <Button className="mb-3 col-xs-12 col-md-2 machines__create align-self-right effect1" onClick={this.add} color="primary">
                            Adicionar
                        </Button>
                    </Col>
                </Row>
                {this.list()}
                {this.state.alert}
            </div>
        )
    }
}

function mapStateToProps() {
    return { 
    }
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions, ...machineActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Machines))
