import React from "react"

import ReactBSAlert from "react-bootstrap-sweetalert"

import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import NumberFormat from 'react-number-format'
import {insertBlobFile, deleteBlobFile} from '../../services/BlobStorageService'
import defaultImage from "assets/img/image_placeholder.jpg"
import defaultAvatar from "assets/img/placeholder.jpg"
import ReactGoogleMapLoader from "react-google-maps-loader"
import ReactGooglePlacesSuggest from "react-google-places-suggest"

import * as modalActions  from '../../redux/actions/modals'
import * as loadingActions  from '../../redux/actions/loading'
import * as machineActions  from '../../redux/actions/machines'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  CardTitle,
  Row,
  CardFooter,
  Form,
  FormGroup,
  UncontrolledTooltip,
  Label,
  Input,
  Col} from "reactstrap"

import BlobService from "../../services/BlobService"
import MachineService from "../../services/MachineService"
import { GOOGLE_API_KEY } from "../../variables/maps"
import ProductService from "../../services/ProductService"
import { CURRENCY_OPTIONS } from "../../variables/currencyOptions";

class MachineForm extends React.Component {
    constructor(props){
        super(props)

        this.machineService = new MachineService()
        this.productService = new ProductService()
        this.blobService = new BlobService()

        this.state = {
            id: '',
            alert: null,
            googleSearch:'',
            googleValue:'',
            title:'',
            place: '',
            lat: '',
            long: '',
            prodCod1: '',
            prodCod2: '',
            prodCod3: '',
            prodQtd1: 0,
            prodQtd2: 0,
            prodQtd3: 0,
            products: [],
            formErrors: { 
                title:'',
                place:'',
                prodCod1: '',
                prodCod2: '',
                prodCod3: '',
                prodQtd1: '',
                prodQtd2: '',
                prodQtd3: '',
            },
            formTouched: { 
                title:false,
                place:false,
                prodCod1: '',
                prodCod2: '',
                prodCod3: '',
                prodQtd1: '',
                prodQtd2: '',
                prodQtd3: '',
            },
            formValid: false,
            titleValid:false,
            placeValid:false,
            prodCod1Valid:false,
            prodCod2Valid:true,
            prodCod3Valid:true,
            prodQtd1Valid:true,
            prodQtd2Valid:true,
            prodQtd3Valid:true,
        }

        this.getProducts()
    }

    componentDidMount () {
        const {machineToEdit} = this.props
        console.log(machineToEdit)
        if(machineToEdit) {
            const { code, title, place, prodCod1, prodCod2, prodCod3, prodQtd1, prodQtd2, prodQtd3, lat, long } = machineToEdit
            this.setState({
                id: code,
                title: title,
                place: place,
                lat: lat,
                long: long,
                googleValue: place,
                prodCod1: prodCod1,
                prodCod2: prodCod2,
                prodCod3: prodCod3,
                prodQtd1: prodQtd1,
                prodQtd2: prodQtd2,
                prodQtd3: prodQtd3,
                titleValid: true,
                placeValid: true,
                prodCod1Valid: true,
                prodCod2Valid: true,
                prodCod3Valid: true
            })
        }

    }

    componentWillUnmount() {
        window.scrollTo(0, 0)
    }

    getProducts = async () => {
        const request = {
            page: 1,
            pageSize: 50,
            orderBy: 'Title'
        }
        const response = await this.productService.getMachineProducts(request)
        this.setState({ products: response.items})
    }

    place = () => {
        const {googleSearch, googleValue, place, formErrors, formTouched} = this.state

        return (
            <ReactGoogleMapLoader
                params={{
                    key: GOOGLE_API_KEY,
                    libraries: "places,geocode",
                }}
                render={googleMaps =>
                    googleMaps && (
                        <ReactGooglePlacesSuggest
                            googleMaps={googleMaps}
                            autocompletionRequest={{
                                input: googleSearch,
                            }}
                            onNoResult={this.handleNoResult}
                            onSelectSuggest={this.handleSelectSuggest}
                            textNoResults="Nenhum resultado encontrado"
                            customRender={prediction => (
                                <div className="customWrapper">
                                    {prediction
                                        ? prediction.description
                                        : "Nenhum resultado encontrado"}
                                </div>
                            )} >
                            <FormGroup className={`has-label ${this.errorClass(formErrors.place, formTouched.place)}`}>
                                <label className="w-100" htmlFor="place">Onde a máquina está localizada? *</label>
                                <Input
                                    id="place"
                                    type="text"
                                    value={googleValue}
                                    onChange={this.handleInputChange}
                                    onBlur={this.handleInputChange}
                                />
                                            
                                <span className='error'>{formErrors.place}</span>
                            </FormGroup>

                        </ReactGooglePlacesSuggest>
                    )
                }
            />
        )
    }

    handleInputChange = e => {
        this.setState({
            googleSearch: e.target.value, 
            googleValue: e.target.value,
            place: e.target.value
        }, this.validateField('place', e.target.value))
    }
 
    handleSelectSuggest = (geocodedPrediction, originalPrediction) => {
        this.setState({
            googleSearch: "", 
            googleValue: originalPrediction.structured_formatting.main_text,
            place: originalPrediction.structured_formatting.main_text, 
            lat: geocodedPrediction.geometry.location.lat(),
            long: geocodedPrediction.geometry.location.lng(),
        }, this.validateField('place', geocodedPrediction.formatted_address))
    }
    
    handleNoResult = () => {
    }

    errorClass(error, touched) {
        if(!touched)
            return ''

        return(error.length === 0 ? 'has-success' : 'has-danger')
    }

    cancel = (e) => {
        e.preventDefault()

        const {removeMachineToEdit} = this.props

        removeMachineToEdit()
        this.props.history.push('/admin/maquinas')
    }

    title = () => {
        const {title, formErrors, formTouched} = this.state
        
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.title, formTouched.title)}`}>
                <label className="w-100" htmlFor="title">Nome *</label>
                <Input
                    id="title"
                    maxLength={70}
                    name="title"
                    type="text"
                    value={ title}
                    onBlur={e => {
                        e.preventDefault()
                        this.setState({title: e.target.value}, this.validateField('title', e.target.value))
                    }}
                    onChange={e => {
                        e.preventDefault()
                        this.setState({title: e.target.value}, this.validateField('title', e.target.value))
                    }}
                />
                <span className='error'>{formErrors.title}</span>
            </FormGroup>
        )
    }

    prod1 = () => {
        const {products, prodCod1, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodCod1, formTouched.prodCod1)}`}>
                <Label for="exampleSelect">Produto 1 (Opção 1 da máquina) *</Label>
                <Input value={prodCod1} onChange={(e) => {
                        e.preventDefault()
                        this.setState({prodCod1: e.target.value}, this.validateField('prodCod1', e.target.value))
                    }} 
                    onBlur={(e) => {
                        e.preventDefault()
                        this.setState({prodCod1: e.target.value}, this.validateField('prodCod1', e.target.value))
                    }} 
                    type="select" name="select" id="exampleSelect">
                    <option value=""></option>
                    {products.map((i,index) => (
                        <option key={index} value={i.code} >{`${i.trademark} ${i.title} - ${i.price.toLocaleString('pt-BR', CURRENCY_OPTIONS)}`}</option>
                    ))}
                </Input>
                <span className='error'>{formErrors.prodCod1}</span>
            </FormGroup>
        )
    }

    prod2 = () => {
        const {products, prodCod2, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodCod2, formTouched.prodCod2)}`}>
                <Label for="exampleSelect">Produto 2 (Opção 2 da máquina) *</Label>
                <Input value={prodCod2} onChange={(e) => {
                        e.preventDefault()
                        this.setState({prodCod2: e.target.value}, this.validateField('prodCod2', e.target.value))
                    }} 
                    onBlur={(e) => {
                        e.preventDefault()
                        this.setState({prodCod2: e.target.value}, this.validateField('prodCod2', e.target.value))
                    }} 
                    type="select" name="select" id="exampleSelect">
                    <option value=""></option>
                    {products.map((i,index) => (
                        <option key={index} value={i.code} >{`${i.trademark} ${i.title} - ${i.price.toLocaleString('pt-BR', CURRENCY_OPTIONS)}`}</option>
                    ))}
                </Input>
                <span className='error'>{formErrors.prodCod2}</span>
            </FormGroup>
        )
    }

    prod3 = () => {
        const {products, prodCod3, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodCod3, formTouched.prodCod3)}`}>
                <Label for="exampleSelect">Produto 3 (Opção 3 da máquina) *</Label>
                <Input value={prodCod3} onChange={(e) => {
                        e.preventDefault()
                        this.setState({prodCod3: e.target.value}, this.validateField('prodCod3', e.target.value))
                    }} 
                    onBlur={(e) => {
                        e.preventDefault()
                        this.setState({prodCod3: e.target.value}, this.validateField('prodCod3', e.target.value))
                    }} 
                    type="select" name="select" id="exampleSelect">
                    <option value=""></option>
                    {products.map((i,index) => (
                        <option key={index} value={i.code} >{`${i.trademark} ${i.title} - ${i.price.toLocaleString('pt-BR', CURRENCY_OPTIONS)}`}</option>
                    ))}
                </Input>
                <span className='error'>{formErrors.prodCod3}</span>
            </FormGroup>
        )
    }

    quantityProd1 = () => {
        const {prodQtd1, formErrors, formTouched} = this.state
        console.log(Number(prodQtd1))
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodQtd1, formTouched.prodQtd1)}`}>
                <label className="w-100" htmlFor="prodQtd1">Quantidade de produtos na opção 1 *</label>
                <NumberFormat 
                    className="form-control"
                    decimalSeparator="," 
                    maxLength="7"
                    id="prodQtd1"
                    name="prodQtd1"
                    type="text"
                    decimalScale={2}
                    value={Number(prodQtd1)}
                    onValueChange={e => {
                        const {floatValue} = e
                        this.setState({prodQtd1: floatValue}, this.validateField('prodQtd1', floatValue))
                    }}
                    onBlur={e => {
                        let value = e.target.value 
                        if (!value) {
                            value = '0'
                        }

                        const floatValue = parseFloat(value)
                        this.setState({prodQtd1: floatValue}, this.validateField('prodQtd1', floatValue))
                    }}
                />
                
                <span className='error'>{formErrors.prodQtd1}</span>
            </FormGroup>
        )
    }

    quantityProd2 = () => {
        const {prodQtd2, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodQtd2, formTouched.prodQtd2)}`}>
                <label className="w-100" htmlFor="prodQtd2">Quantidade de produtos na opção 1 *</label>
                <NumberFormat 
                    className="form-control"
                    decimalSeparator="," 
                    maxLength="7"
                    id="prodQtd2"
                    name="prodQtd2"
                    type="text"
                    decimalScale={2}
                    value={Number(prodQtd2)}
                    onValueChange={e => {
                        const {floatValue} = e
                        this.setState({prodQtd2: floatValue}, this.validateField('prodQtd2', floatValue))
                    }}
                    onBlur={e => {
                        let value = e.target.value 
                        if (!value) {
                            value = '0'
                        }

                        const floatValue = parseFloat(value)
                        this.setState({prodQtd2: floatValue}, this.validateField('prodQtd2', floatValue))
                    }}
                />
                
                <span className='error'>{formErrors.prodQtd2}</span>
            </FormGroup>
        )
    }

    quantityProd3 = () => {
        const {prodQtd3, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.prodQtd3, formTouched.prodQtd3)}`}>
                <label className="w-100" htmlFor="prodQtd3">Quantidade de produtos na opção 1 *</label>
                <NumberFormat 
                    className="form-control"
                    decimalSeparator="," 
                    maxLength="7"
                    id="prodQtd3"
                    name="prodQtd3"
                    type="text"
                    decimalScale={3}
                    value={Number(prodQtd3)}
                    onValueChange={e => {
                        const {floatValue} = e
                        this.setState({prodQtd3: floatValue}, this.validateField('prodQtd3', floatValue))
                    }}
                    onBlur={e => {
                        let value = e.target.value 
                        if (!value) {
                            value = '0'
                        }

                        const floatValue = parseFloat(value)
                        this.setState({prodQtd3: floatValue}, this.validateField('prodQtd3', floatValue))
                    }}
                />
                
                <span className='error'>{formErrors.prodQtd3}</span>
            </FormGroup>
        )
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors
        let titleValid = this.state.titleValid
        let placeValid = this.state.placeValid
        let prodCod1Valid = this.state.prodCod1Valid
        let prodQtd1Valid = this.state.prodQtd1Valid
        let prodQtd2Valid = this.state.prodQtd2Valid
        let prodQtd3Valid = this.state.prodQtd3Valid
        let formTouched = this.state.formTouched
        switch(fieldName) {
            case 'title':
                formTouched.title = true
                titleValid = value.length > 0
                fieldValidationErrors.title = titleValid ? '' : ' Dê um nome para a máquina'
                break
            case 'place':
                formTouched.place = true
                placeValid = value.length > 0
                fieldValidationErrors.place = placeValid ? '' : ' Diga onde a máquina está localizada'
                break
            case 'prodCod1':
                formTouched.prodCod1 = true
                prodCod1Valid = value.length > 0
                fieldValidationErrors.prodCod1 = prodCod1Valid ? '' : ' Máquina precisa ter o produto nº1'
                break
            case 'prodQtd1':
                formTouched.prodQtd1 = true
                prodQtd1Valid = value > 0
                fieldValidationErrors.prodQtd1 = prodQtd1Valid ? '' : ' Diga quantos produtos estão disponíveis na opção nº1'
                break
            case 'prodQtd2':
                formTouched.prodQtd2 = true
                prodQtd2Valid = this.state.prodCod2.length == 0 || value > 0
                fieldValidationErrors.prodQtd2 = prodQtd2Valid ? '' : ' Diga quantos produtos estão disponíveis na opção nº1'
                break
            case 'prodQtd3':
                formTouched.prodQtd3 = true
                prodQtd3Valid = this.state.prodCod2.length == 0 || value > 0
                fieldValidationErrors.prodQtd3 = prodQtd3Valid ? '' : ' Diga quantos produtos estão disponíveis na opção nº1'
                break
          default:
            break
        }

        this.setState({
            formTouched: formTouched,
            formErrors: fieldValidationErrors,
            titleValid: titleValid,
            placeValid: placeValid,
            prodCod1Valid: prodCod1Valid,
            prodQtd1Valid: prodQtd1Valid,
            prodQtd2Valid: prodQtd2Valid,
            prodQtd3Valid: prodQtd3Valid,
        }, this.validateForm)
    }

    validateForm(){
        this.setState({
            formValid: this.state.titleValid
                && this.state.placeValid
                && this.state.prodCod1Valid
                && this.state.prodQtd1Valid
                && this.state.prodQtd2Valid
                && this.state.prodQtd3Valid
        })
    }

    async submit() {
        const { id, formValid, title, place, lat, long, prodCod1, prodCod2, prodCod3, prodQtd1, prodQtd2, prodQtd3   } = this.state

        if (!formValid) {
            return
        }
        debugger;
        const machine = {
            "code": id,
            "title": title,
            "place": place,
            "lat": lat,
            "long": long,
            "prodCod1": prodCod1,
            "prodCod2": prodCod2,
            "prodCod3": prodCod3,
            "prodQtd1": prodQtd1,
            "prodQtd2": prodQtd2,
            "prodQtd3": prodQtd3
        }

        const { loading, loaded, history, openSuccessModal, removeMachineToEdit } = this.props
        if(id) {
            loading()
            const result = await this.machineService.editMachine(machine)
            loaded()

            if (result.response) {
                const success = {title: 'Alohaa! . m/', description: `Máquina cadastrada já está disponível no app.`}
                openSuccessModal(success)
                removeMachineToEdit()
                history.push('/admin/maquinas')
            } 

            return
        }

        loading()
        await this.machineService.createMachine(machine)
        loaded()

        const success = {title: 'Alohaa! . m/', description: `Máquina cadastrada já está disponível no app.`}
        openSuccessModal(success)
        history.push('/admin/maquinas')
    }

    handleSubmit = async (e) => {
        e.preventDefault()
        await this.submit()
    }

    handleHideAlert = () => {
        this.setState({
          alert: null
        })
    }

    render() {
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Form id="categories" onSubmit={this.handleSubmit}>
                            <Card className="effect4">
                                <CardHeader>
                                    <CardTitle tag="h4">Máquina {this.props.categoryToEdit ? ` - ${this.props.categoryToEdit.description }` : ''}</CardTitle>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col lg="6" md="6" xs="12">
                                            {this.title()}
                                        </Col>
                                        <Col lg="6" md="6" xs="12">
                                            {this.place()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg="4" md="4" xs="12">
                                            {this.prod1()}
                                        </Col>
                                        <Col lg="4" md="4" xs="12">
                                            {this.prod2()}
                                        </Col>
                                        <Col lg="4" md="4" xs="12">
                                            {this.prod3()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg="4" md="4" xs="12">
                                            {this.quantityProd1()}
                                        </Col>
                                        <Col lg="4" md="4" xs="12">
                                            {this.quantityProd2()}
                                        </Col>
                                        <Col lg="4" md="4" xs="12">
                                            {this.quantityProd3()}
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter className="text-right">
                                    <Button className=" mt-2 col-xs-2 col-md-2 effect1" onClick={this.cancel} color="secondary">
                                        Cancelar
                                    </Button>

                                    <Button className="ml-md-2 mt-2 col-xs-2 col-md-2 effect1" color="primary" disabled={!this.state.formValid}>
                                        Salvar
                                    </Button>
                                </CardFooter>
                            </Card>
                        </Form>
                    </Col>
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { 
        machineToEdit: state.machines.machineToEdit
    }
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions, ...machineActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MachineForm))
