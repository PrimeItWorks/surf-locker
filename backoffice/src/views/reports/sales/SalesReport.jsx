import React from "react"
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import ReactDatetime from "react-datetime"
import ReactBSAlert from "react-bootstrap-sweetalert"
import {
  Card,
  CardTitle,
  CardHeader,
  CardBody,
  Table,
  Button,
  Row,
  UncontrolledTooltip,
  Collapse,
  UncontrolledCollapse,
  Pagination,
  PaginationItem,
  PaginationLink, 
  Input,
  InputGroup,
  FormGroup,
  Col} from "reactstrap"
  
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'

import './SalesReport.scss'
import SaleService from "../../../services/SaleService"

import * as modalActions  from '../../../redux/actions/modals'
import * as loadingActions  from '../../../redux/actions/loading'
import { CURRENCY_OPTIONS } from "../../../variables/currencyOptions"
import { PAGE_SIZES } from "../../../variables/tables"
import UserService from "../../../services/UserService"
import MachineService from "../../../services/MachineService"
import ProductService from "../../../services/ProductService"

import moment from "moment"

require('moment/locale/pt-br')

class SalesReport extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            sales: null,
            clients: [],
            machines: [],
            products: [],
            alert: null,
            openActives: true,
            defaultFilter: {
                userCode:'',
                machineCode:'',
                productCode:'',
                trademark:'',
                start: null,
                end: null,
                page:1,
                pageSize: 4,
                orderBy: '_id',
                onlyActive: true,
                onlyInactive:false,
                desc: true
            },
            activeFilter: {
                userCode:'',
                machineCode:'',
                trademark:'',
                page:1,
                pageSize: 10,
                orderBy: '_id',
                orderByDir: {
                    title: 'sort',
                    price: 'sort',
                },
                onlyActive: true,
                onlyInactive:false,
                desc: true
            }
        }

        this.saleService = new SaleService()
        this.userService = new UserService()
        this.machineService = new MachineService()
        this.productService = new ProductService()

        this.getUsers()
        this.getMachines()
        this.getProducts()
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0)
        while (id--) {
            window.clearTimeout(id)
        }
        window.scrollTo(0, 0)
    }

    getUsers = async () => {        
        const request = { 
            "page": 1, 
            "pageSize": 500, 
            "orderBy": "Name"
        }

        const result = await this.userService.getUsers(request)
        this.setState({ clients: result.items})
    }
    
    getProducts = async () => {        
        const request = { 
            "page": 1, 
            "pageSize": 500, 
            "orderBy": "Trademark"
        }

        const result = await this.productService.getProducts(request)
        this.setState({ products: result.items})
    }

    getMachines = async () => {        
        const request = { 
            "page": 1, 
            "pageSize": 500, 
            "orderBy": "Name"
        }

        const result = await this.machineService.getMachines(request)
        this.setState({ machines: result.items})
    }

    getPagingSales = async () => {
        const {loading, loaded} = this.props
        const { userCode, productCode, machineCode, page, pageSize, orderBy, desc, trademark, start, end } = this.state.activeFilter
        const request = { userCode, productCode, machineCode, page, pageSize, orderBy, desc, trademark, start, end}

        loading()
        console.log('request >>>>>>>>>>>>>>', request)
        const response = await this.saleService.getSales(request)
        loaded()

        this.setState({ sales: response})
    }

    titles = () => <tr>
            <th >DATA</th>
            <th >MARCA</th>
            <th >PRODUTO</th>
            <th >QUANTIDADE</th>
            <th >VALOR</th>
            <th >COD MÁQUINA</th>
        </tr>

    register = (item, index) => <tr key={index}>
            <td>
                {new Date(item.buyDate).toLocaleString("pt-BR")}
            </td>
            <td>
                {item.trademark}
            </td>
            <td>
                {item.title}
            </td>
            <td>
                {item.quantity}
            </td>
            <td>
                {item.value}
            </td>
            <td>
                {item.machineCode}
            </td>
        </tr>
        
    handleSearch =  async () => {
        var activeFilter = this.state.activeFilter
        activeFilter.page = 1
        this.setState({userCode : activeFilter }, await this.getPagingSales())
    } 

    handleChangePageSize = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.pageSize = e.target.value

        this.setState({ activeFilter: activeFilter }, await this.getPagingSales())
    }

    searchBtn = () => {
        return <button className="btn btn-primary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleSearch()
                }} >
                Pesquisar
            </button>
    }
    
    filters = () => {
        const {clients, machines, products, activeFilter} = this.state
        const {userCode, machineCode, productCode, trademark} = activeFilter

        return <Row className="sales__active">
                <Col md="12">
                    <Card className="sales__active__card effect4 mt-2">
                        <CardHeader className="sales__active__card__header">
                            <h4 className="mb-3">
                                FILTROS
                            </h4>
                            <hr/>
                        </CardHeader>
                        <CardBody>
                            <Row>
                                <Col xs={12} md={4} lg={4} sm={4} >
                                    {this.clientFilter(userCode, clients)}
                                </Col>
                                <Col xs={12} md={4} lg={4} sm={4} >
                                    {this.machineFilter(machineCode, machines)}
                                </Col>
                                <Col xs={12} md={4} lg={4} sm={4} >
                                    {this.productFilter(productCode, products)}
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} md={4} lg={4} sm={4} >
                                    {this.trademarkFilter(trademark)}
                                </Col>
                                <Col xs={12} md={8} lg={8} sm={8} >
                                    {this.periodFilter()}
                                </Col>
                            </Row>
                            <Row>
                                {this.actionButons()}
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
    }

    list = () => {
        const { sales, pageSize, activeFilter } = this.state
        const {start, end} = activeFilter
        if(!sales)
            return <Row>
                <Col>
                    <Card id="capture" className="card-plain">
                        <CardHeader>
                            <Row>
                                {this.list_header(start, end)}
                            </Row>
                        </CardHeader>
                    </Card>
                </Col>
            </Row>

        return <Row>
                <Col >
                    <Card id="capture" className="card-plain">
                        <CardHeader>
                            <Row>
                                {this.list_header(start, end)}
                                {this.pageSizeSelector(pageSize)}
                            </Row>
                        </CardHeader>
                        <CardBody>
                            <Table id="capture" className="mt-3 " responsive striped>
                                <thead>
                                    {this.titles()}
                                </thead>
                                <tbody>
                                    {sales.items.map((item,index) => this.register(item,index))}                                        
                                </tbody>
                            </Table>
                            <Pagination listClassName="mt-3 justify-content-end">
                            {sales
                                ? <PaginationItem disabled>
                                        <PaginationLink href="#">
                                            <i className="fas fa-list"></i>{"   "} Total: {sales.count}
                                        </PaginationLink>
                                    </PaginationItem>
                                : null}

                                {this.state.activeFilter.page > 1
                                    ? (<PaginationItem >
                                            <PaginationLink href="javascript:void(0)" onClick={this.handlePrevPage}>
                                                <i className="fas fa-arrow-left"></i>
                                            </PaginationLink>
                                        </PaginationItem>)

                                    : (<PaginationItem disabled>
                                        <PaginationLink href="#">
                                            <i className="fas fa-arrow-left"></i>
                                        </PaginationLink>
                                    </PaginationItem>)}
                                {sales 
                                    && sales.hasNextPage
                                    
                                    ? (<PaginationItem >
                                            <PaginationLink href="javascript:void(0)" onClick={this.handleNextPage}>
                                                <i className="fas fa-arrow-right"></i>
                                            </PaginationLink>
                                        </PaginationItem>)

                                    : (<PaginationItem disabled>
                                        <PaginationLink href="#">
                                            <i className="fas fa-arrow-right"></i>
                                        </PaginationLink>
                                    </PaginationItem>)}
                            </Pagination>
                        </CardBody>
                    </Card>
                    
                </Col>
            </Row>
    }

    handleNextPage = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.page++

        this.setState({ activeFilter: activeFilter }, await this.getPagingSales())
    }

    handlePrevPage = async (e) => {
        e.preventDefault()
        let activeFilter = this.state.activeFilter
        activeFilter.page--
        this.setState({ activeFilter: activeFilter }, await this.getPagingSales())
    }

    add = (e) => {
        e.preventDefault()
        this.props.history.push("/admin/maquinas/criar")
    }

    handleExport = async () => {
        const {activeFilter} = this.state
        const {loading, loaded} = this.props
        loading()
        await this.saleService.getExcel(activeFilter)
        loaded()
    }

    exportBtn = () => {
        return <button className="btn btn-secondary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleExport()
                }} >
                Exportar
            </button>
    }

    handlePrint = async  () => {
        const {loading, loaded} = this.props

        loading()
        await html2canvas(document.getElementById("capture"))
            .then(canvas => {
                const imgData = canvas.toDataURL('image/png')

                const pdf = new jsPDF('l', 'pt', [canvas.height, canvas.width])
                pdf.addImage(imgData, 'PNG', 0, 0)
                pdf.save(`SurfLocker_Vendas_${new Date().toISOString()}.pdf`)
            })
        loaded()
    }

    printBtn = () => {
        return <button className="btn btn-secondary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handlePrint()
                }} >
                Imprimir
            </button>
    }

   
    actionButons() {
        return <Col className="text-right">
            {this.printBtn()}
            {this.exportBtn()}
            {this.searchBtn()}
        </Col>
    }

    machineFilter(machineCode, machines) {
        return <FormGroup className="has-label">
            <label htmlFor="machinefilter">Máquina</label>
            <Input id="machinefilter" value={machineCode} onChange={e => {
                e.preventDefault()
                let { activeFilter } = this.state
                activeFilter.machineCode = e.target.value
                this.setState({ activeFilter: activeFilter })
            } } type="select">
                <option></option>
                {machines.map((i, index) => (<option key={index} value={i.code}>{i.title}</option>))}
            </Input>
        </FormGroup>
    }

    productFilter(productCode, products) {
        return <FormGroup className="has-label">
            <label htmlFor="productFilter">Produto</label>
            <Input id="productFilter" value={productCode} onChange={e => {
                e.preventDefault()
                let { activeFilter } = this.state
                activeFilter.productCode = e.target.value
                this.setState({ activeFilter: activeFilter })
            } } type="select" >
                <option></option>
                {products.map((i, index) => (<option key={index} value={i.code}>{`${i.trademark} ${i.title}`}</option>))}
            </Input>
        </FormGroup>
    }

    clientFilter(userCode, clients) {
        return <FormGroup className="has-label">
            <label htmlFor="userFilter">Cliente</label>
            <Input id="userFilter" value={userCode} onChange={e => {
                e.preventDefault()
                let { activeFilter } = this.state
                activeFilter.userCode = e.target.value
                this.setState({ activeFilter: activeFilter })
            } } type="select">
                <option></option>
                {clients.map((i, index) => (<option key={index} value={i.code}>{i.name}</option>))}
            </Input>
        </FormGroup>
    }
    
    trademarkFilter(trademark) {
        return <FormGroup className="has-label">
            <label htmlFor="trademarkFilter">Marca</label>
            <Input id="trademarkFilter" value={trademark} onChange={e => {
                    e.preventDefault()
                    let { activeFilter } = this.state
                    activeFilter.trademark = e.target.value
                    this.setState({ activeFilter: activeFilter })
                } } 
                type="text" />
        </FormGroup>
    }
    
    periodFilter() {
        return <FormGroup className="has-label">
            <label htmlFor="start">Período de criação</label>
            <InputGroup>
                <Col className="pl-0">
                    <ReactDatetime
                        inputProps={{
                            className: "form-control ",
                            placeholder: "De",
                            id:"start",
                            maxLength:0,
                            style: { 
                                borderLeft: "1px solid rgb(221, 221, 221)",
                                borderRight: "1px solid rgb(221, 221, 221)"
                            } 
                        }}
                        locale="pt-BR"
                        timeFormat={true}
                        onChange={(e) => {
                            let { activeFilter } = this.state
                            
                            if(!e) {
                                activeFilter.start = ''
                                return
                            }
                            activeFilter.start = e.toISOString()
                            this.setState({ activeFilter: activeFilter })
                        }}
                    />
                </Col>
                <Col className="pr-0">
                    <ReactDatetime
                        inputProps={{
                            className: "form-control",
                            placeholder: "Até",
                            id: "end",
                            maxLength: 0,
                            style: { 
                                borderLeft: "1px solid rgb(221, 221, 221)",
                                borderRight: "1px solid rgb(221, 221, 221)"
                            } 
                        }}
                        locale="pt-BR"  
                        timeFormat={true}
                        onChange={(e) => {
                            let { activeFilter } = this.state
                            
                            if(!e) {
                                activeFilter.end = ''
                                return
                            }
                            
                            activeFilter.end = e.toISOString()
                            this.setState({ activeFilter: activeFilter })
                        }}
                    />
                </Col>
            </InputGroup>
        </FormGroup>
    }

    pageSizeSelector(pageSize) {

        return <Col xs={4} md={3} md={3} lg={3}>
            <FormGroup className="has-label">
                <label htmlFor="itemsPerPage">Itens por página</label>
                <Input id="itemsPerPage" value={pageSize} onChange={this.handleChangePageSize} type="select" name="select" id="exampleSelect">
                    {PAGE_SIZES.map((i, index) => (<option key={index} value={i.value}>{i.label}</option>))}
                </Input>
            </FormGroup>
        </Col>
    }
    

    list_header(start,end) {
        if (start && end) {
            return <Col>
                <CardTitle tag="h4">Relatório de vendas</CardTitle>
                <p className="card-category">
                    Período: {new Date(start).toLocaleString("pt-BR")} - {new Date(end).toLocaleString("pt-BR")}.
                </p>
            </Col>
        } 
        else if (start && !end) {
            return <Col>
                <CardTitle tag="h4">Relatório de vendas</CardTitle>
                <p className="card-category">
                    Período: A partir de {new Date(start).toLocaleString("pt-BR")}.
                </p>
            </Col>
        }
        else if (!start && end) {
            return <Col>
                <CardTitle tag="h4">Relatório de vendas</CardTitle>
                <p className="card-category">
                    Período: Até {new Date(end).toLocaleString("pt-BR")}.
                </p>
            </Col>
        }

        return <Col>
            <CardTitle tag="h4">Relatório de vendas</CardTitle>
            <p className="card-category">
                Período: Desde o início.
            </p>
        </Col>
    }

    render() {
        return <div className="content salesreport">
                {this.filters()}
                {this.list()}
            </div>
    }
}

function mapStateToProps() {
    return { 
    }
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SalesReport))
