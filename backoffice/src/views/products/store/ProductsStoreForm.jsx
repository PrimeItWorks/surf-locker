import React from "react"

import ReactBSAlert from "react-bootstrap-sweetalert";

import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import NumberFormat from 'react-number-format'
import {insertBlobFile, deleteBlobFile} from '../../../services/BlobStorageService'
import defaultImage from "assets/img/image_placeholder.jpg"
import defaultAvatar from "assets/img/placeholder.jpg"

import * as modalActions  from '../../../redux/actions/modals'
import * as loadingActions  from '../../../redux/actions/loading'
import * as productActions  from '../../../redux/actions/products'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  CardTitle,
  Row,
  CardFooter,
  Form,
  FormGroup,
  UncontrolledTooltip,
  Label,
  Input,
  Col} from "reactstrap";

import BlobService from "../../../services/BlobService";
import ProductService from "../../../services/ProductService";
import { PRODUCT_TYPES } from "../constants";

class ProductsStoreForm extends React.Component {
    constructor(props){
        super(props);

        this.productService = new ProductService();
        this.blobService = new BlobService();

        this.state = {
            id: '',
            alert: null,
            title:'',
            description: '',
            video: '',
            trademark: '',
            thumbnail: [],
            image:  '',
            price: 0,
            unit: 0,
            type: PRODUCT_TYPES.store,
            fileImage : null,
            previewImage: defaultImage,
            fileThumb1 : {
                file: null,
                imagePreviewUrl: defaultImage
            },
            fileThumb2 : {
                file: null,
                imagePreviewUrl: defaultImage
            },
            formErrors: { 
                title:'',
                description: '',
                video: '',
                trademark: '',
                thumbnail: '',
                image:  '',
                price: '', 
                unit: '', 
            },
            formTouched: { 
                title:false,
                description: false,
                video: false,
                trademark: false,
                thumbnail: false,
                image:  false,
                price: false, 
                unit: false, 
            },
            formValid: false,
            titleValid:false,
            unitValid:false,
            descriptionValid: false,
            trademarkValid: false,
            thumbnailValid: false,
            imageValid:  false,
            priceValid: false, 
            videoValid: true 
        }
    }

    componentDidMount () {
        const {productToEdit} = this.props

        if(productToEdit) {
            const { code, title, trademark, thumbnail, image, description, urlVideo, price, unit, type } = productToEdit

            let thumb1 = {file: null, imagePreviewUrl: defaultImage}
            let thumb2 = {file: null, imagePreviewUrl: defaultImage}
            if(thumbnail.length > 1) {
                thumb1 = {file: null, imagePreviewUrl: thumbnail[0]}
                thumb2 = {file: null, imagePreviewUrl: thumbnail[1]}
            }
            else if (thumbnail.length > 0) {
                thumb1 = {file: null, imagePreviewUrl: thumbnail[0]}
            }

            const toSetState = {
                id: code,
                title: title,
                description: description,
                previewImage: image,
                type: type,
                trademark: trademark,
                unit: unit,
                price: price, 
                video: urlVideo,
                fileThumb1: thumb1,
                fileThumb2: thumb2,
                descriptionValid : true,
                titleValid: true,
                unitValid: true,
                trademarkValid: true,
                priceValid: true,
                imageValid: true
            }

            this.setState(toSetState)
        }

    }

    componentWillUnmount() {
        window.scrollTo(0, 0);
    }

    errorClass(error, touched) {
        if(!touched)
            return '';

        return(error.length === 0 ? 'has-success' : 'has-danger');
    }

    cancel = (e) => {
        e.preventDefault();

        const {removeProductToEdit} = this.props

        removeProductToEdit();
        this.props.history.push('/admin/produtos-loja')
    }

    video = () => {
        const {video, formErrors, formTouched} = this.state;
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.video, formTouched.video)}`}>
                <label className="w-100" htmlFor="video">Url de um vídeo (you tube)</label>
                <Input
                    id="video"
                    name="video"
                    type="text"
                    maxLength={80}
                    value={video}
                    onChange={e => {
                        e.preventDefault()
                        this.setState({video: e.target.value}, this.validateField('video', e.target.value))
                    }}
                    onBlur={e => {
                        e.preventDefault()
                        this.setState({video: e.target.value}, this.validateField('video', e.target.value))
                    }}
                />
                <span className='error'>{formErrors.video}</span>
            </FormGroup>
        )
    }

    description = () => {
        const {description, formErrors, formTouched} = this.state;
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.description, formTouched.description)}`}>
                <label className="w-100" htmlFor="description">Descrição *</label>
                <Input
                    id="description"
                    name="description"
                    type="text"
                    maxLength={80}
                    value={description}
                    onChange={e => {
                        e.preventDefault()
                        this.setState({description: e.target.value}, this.validateField('description', e.target.value))
                    }}
                    onBlur={e => {
                        e.preventDefault()
                        this.setState({description: e.target.value}, this.validateField('description', e.target.value))
                    }}
                />
                <span className='error'>{formErrors.description}</span>
            </FormGroup>
        )
    }
    
    trademark = () => {
        const {trademark, formErrors, formTouched } = this.state;
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.trademark, formTouched.trademark)}`}>
                <label className="w-100" htmlFor="trademark">Marca *</label>
                <Input
                    id="trademark"
                    name="trademark"
                    type="text"
                    maxLength={80}
                    value={trademark}
                    onBlur={e => {
                        e.preventDefault()
                        this.setState({trademark: e.target.value}, this.validateField('trademark', e.target.value));
                    }}
                    onChange={e => {
                        e.preventDefault()
                        this.setState({trademark: e.target.value}, this.validateField('trademark', e.target.value));
                    }}
                />
                <span className='error'>{formErrors.trademark}</span>
            </FormGroup>
        )
    }

    price = () => {
        const {price, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.price, formTouched.price)}`}>
                <label className="w-100" htmlFor="price">Preço *</label>
                <NumberFormat 
                    className="form-control"
                    decimalSeparator="," 
                    maxLength="7"
                    id="price"
                    name="price"
                    type="text"
                    decimalScale={2}
                    value={price}
                    onValueChange={e => {
                        const {floatValue} = e
                        this.setState({price: floatValue}, this.validateField('price', floatValue))
                    }}
                    onBlur={e => {
                        let value = e.target.value 
                        if (!value) {
                            value = '0'
                        }

                        const floatValue = parseFloat(value)
                        this.setState({price: floatValue}, this.validateField('price', floatValue))
                    }}
                />
                
                <span className='error'>{formErrors.price}</span>
            </FormGroup>
        )
    }

    unit = () => {
        const {unit, formErrors, formTouched} = this.state
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.unit, formTouched.unit)}`}>
                <label className="w-100" htmlFor="unit">Número de itens *</label>
                <NumberFormat 
                    className="form-control"
                    decimalSeparator="," 
                    maxLength="7"
                    id="unit"
                    name="unit"
                    type="text"
                    decimalScale={2}
                    value={unit}
                    onValueChange={e => {
                        const {floatValue} = e
                        this.setState({unit: floatValue}, this.validateField('unit', floatValue))
                    }}
                    onBlur={e => {
                        let value = e.target.value 
                        if (!value) {
                            value = '0'
                        }

                        const floatValue = parseFloat(value)
                        this.setState({unit: floatValue}, this.validateField('unit', floatValue))
                    }}
                />
                
                <span className='error'>{formErrors.unit}</span>
            </FormGroup>
        )
    }

    title = () => {
        const {title, formErrors, formTouched} = this.state;
        
        return (
            <FormGroup className={`has-label ${this.errorClass(formErrors.title, formTouched.title)}`}>
                <label className="w-100" htmlFor="title">Título *</label>
                <Input
                    id="title"
                    maxLength={20}
                    name="title"
                    type="text"
                    value={ title}
                    onBlur={e => {
                        e.preventDefault();
                        this.setState({title: e.target.value}, this.validateField('title', e.target.value))
                    }}
                    onChange={e => {
                        e.preventDefault();
                        this.setState({title: e.target.value}, this.validateField('title', e.target.value))
                    }}
                />
                <span className='error'>{formErrors.title}</span>
            </FormGroup>
        )
    }

    handleImageChange = (e) => {
        const { loading, loaded } = this.props

        e.preventDefault();
        if (e.target.files && e.target.files.length > 0){
            let file = e.target.files[0];
            let formData = new FormData();
            for (let i = 0; i < e.target.files.length ; i++) {
                formData.append(e.target.files[i].name, e.target.files[i]);
            }

            loading()
            insertBlobFile(formData)
                .then((result) => {
                    var image = result.response[0]
                    loaded()
                    this.setState({
                        fileImage: file,
                        previewImage: result.response[0]
                    }, this.validateField('image', image));
                })
                .catch((err) => {
                    loaded()
                    this.setState({
                        fileImage: null,
                        previewImage: this.props.avatar ? defaultAvatar : defaultImage
                    }, this.validateField('image', ''));
                });
        }
    }

    image = () => {
        const { previewImage } = this.state
        const { loading, loaded } = this.props

        var btn = 
        <Button className="btn-round" onClick={e => {
            e.preventDefault()
            this.refs.fileInputImage.click()
        }}>
            Escolha a imagem
        </Button>

        if (previewImage !== defaultImage)
            btn = 
            <span>
                <Button className="btn-round" onClick={e => {
                    e.preventDefault()
                    this.refs.fileInputImage.click()
                }}>
                    Alterar
                </Button>
                <Button
                    color="danger"
                    className="btn-round"
                    onClick={e => {
                        e.preventDefault()
                        loading()
                        deleteBlobFile(this.state.previewImage)
                            .then((result) => {
                                this.setState({
                                    fileImage: null,
                                    previewImage: defaultImage
                                }, this.validateField('image', ''));

                                loaded()
                            })
                            .catch((err) => {
                                loaded()
                                this.setState({
                                    fileImage: null,
                                    previewImage: defaultImage
                                }, this.validateField('image', ''));
                            })

                        this.refs.fileInputImage.value = null;
                    }}
                >
                    <i className="fa fa-times" />Remover
                </Button>
            </span>

        return (
            <>
                <label className="w-100" htmlFor="file">Imagem do produto *</label>
                <div className="fileinput text-center">
                    <input id='imageinput' name='imageinput' type="file" onChange={this.handleImageChange} ref="fileInputImage" />
                    <div className={"thumbnail"}>
                        <img src={previewImage} alt="..." />
                    </div>
                    <div>
                        {btn}
                    </div>
                </div>
            </>
        )
    }

    handleThumb1Change = (e) => {
        e.preventDefault();
        const { loading, loaded } = this.props
        if (e.target.files && e.target.files.length > 0){
            let file = e.target.files[0];
            let formData = new FormData();
            for (let i = 0; i < e.target.files.length ; i++) {
                formData.append(e.target.files[i].name, e.target.files[i]);
            }

            loading()
            insertBlobFile(formData)
                .then((result) => {
                    loaded()
                    var thumb1 = result.response[0]
                    var fileThumb1 = {
                        file: file,
                        imagePreviewUrl: result.response[0]
                    }

                    this.setState({fileThumb1}, this.validateField('thumb1', thumb1));
                })
                .catch((err) => {
                    loaded()
                    var fileThumb1 = {
                        file: null,
                        imagePreviewUrl: defaultImage
                    }
                    this.setState({fileThumb1}, this.validateField('thumb1', ''));
                });
        }
    }

    thumb1 = () => {
        const { imagePreviewUrl } = this.state.fileThumb1
        const { loading, loaded } = this.props
        var btn = 
        <Button className="btn-round" onClick={e => {
            e.preventDefault()
            this.refs.fileThumb1Image.click()
        }}>
            Escolha a imagem
        </Button>

        if (imagePreviewUrl !== defaultImage)
            btn = 
            <span>
                <Button className="btn-round" onClick={e =>{
                    e.preventDefault() 
                    this.refs.fileThumb1Image.click();
                }}>
                    Alterar
                </Button>
                <Button
                    color="danger"
                    className="btn-round"
                    onClick={e => {
                        loading()
                        deleteBlobFile(imagePreviewUrl)
                            .then((result) => {
                                loaded()
                                var fileThumb1 = {
                                    file: null,
                                    imagePreviewUrl: defaultImage
                                }
                                this.setState({fileThumb1}, this.validateField('thumb1', ''));
                            })
                            .catch((err) => {
                                loaded()

                                var fileThumb1 = {
                                    file: null,
                                    imagePreviewUrl: defaultImage
                                }
                                this.setState({fileThumb1}, this.validateField('thumb1', ''));
                                
                            })

                        this.refs.fileThumb1Image.value = null;
                    }}
                >
                    <i className="fa fa-times" />Remover
                </Button>
            </span>

        return (
            <>
                <label className="w-100" htmlFor="file">Logo da marca</label>
                <div className="fileinput text-center">
                    <input id='thumb1input' name='thumb1input' type="file" onChange={this.handleThumb1Change} ref="fileThumb1Image" />
                    <div className={"thumbnail"}>
                        <img src={imagePreviewUrl} alt="..." />
                    </div>
                    <div>
                        {btn}
                    </div>
                </div>
            </>
        )
    }

    handleThumb2Change = (e) => {
        e.preventDefault();
        const { loading, loaded } = this.props

        if (e.target.files && e.target.files.length > 0){
            let file = e.target.files[0];
            let formData = new FormData();
            for (let i = 0; i < e.target.files.length ; i++) {
                formData.append(e.target.files[i].name, e.target.files[i]);
            }

            loading()
            insertBlobFile(formData)
                .then((result) => {
                    loaded()
                    var thumb2 = result.response[0]
                    var fileThumb2 = {
                        file: file,
                        imagePreviewUrl: result.response[0]
                    }

                    this.setState({fileThumb2}, this.validateField('thumb2', thumb2));
                })
                .catch((err) => {
                    loaded()
                    var fileThumb2 = {
                        file: null,
                        imagePreviewUrl: defaultImage
                    }
                    this.setState({fileThumb2}, this.validateField('thumb2', ''));
                });
        }
    }

    thumb2 = () => {
        const { imagePreviewUrl } = this.state.fileThumb2
        const { loading, loaded } = this.props

        var btn = 
        <Button className="btn-round" onClick={e => {
            e.preventDefault()
            this.refs.fileThumb2Image.click()
        }}>
            Escolha a imagem
        </Button>

        if (imagePreviewUrl !== defaultImage)
            btn = 
            <span>
                <Button className="btn-round" onClick={e =>{
                    e.preventDefault() 
                    this.refs.fileThumb2Image.click();
                }}>
                    Alterar
                </Button>
                <Button
                    color="danger"
                    className="btn-round"
                    onClick={e => {
                        loading()
                        deleteBlobFile(imagePreviewUrl)
                            .then((result) => {
                                loaded()
                                var fileThumb2 = {
                                    file: null,
                                    imagePreviewUrl: defaultImage
                                }
                                this.setState({fileThumb2}, this.validateField('thumb2', ''));
                            })
                            .catch((err) => {
                                loaded()

                                var fileThumb2 = {
                                    file: null,
                                    imagePreviewUrl: defaultImage
                                }
                                this.setState({fileThumb2}, this.validateField('thumb2', ''));
                                
                            })

                        this.refs.fileThumb2Image.value = null;
                    }}
                >
                    <i className="fa fa-times" />Remover
                </Button>
            </span>

        return (
            <>
                <label className="w-100" htmlFor="file">Miniaturas do produto </label>
                <div className="fileinput text-center">
                    <input id='thumb2input' name='thumb2input' type="file" onChange={this.handleThumb2Change} ref="fileThumb2Image" />
                    <div className={"thumbnail"}>
                        <img src={imagePreviewUrl} alt="..." />
                    </div>
                    <div>
                        {btn}
                    </div>
                </div>
            </>
        )
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let descriptionValid = this.state.descriptionValid;
        let videoValid = this.state.videoValid;
        let priceValid = this.state.priceValid;
        let unitValid = this.state.unitValid;
        let titleValid = this.state.titleValid;
        let trademarkValid = this.state.trademarkValid;
        let thumbnailValid = this.state.thumbnailValid;
        let imageValid = this.state.imageValid;
        let formTouched = this.state.formTouched;
        switch(fieldName) {
            case 'description':
                formTouched.description = true;
                descriptionValid = value.length > 0;
                fieldValidationErrors.description = descriptionValid ? '' : ' Dê uma breve descrição descrição para o produto';
                break;

            case 'title':
                formTouched.title = true;
                titleValid = value.length > 0;
                fieldValidationErrors.title = titleValid ? '' : ' Dê um título para o produto';
                break;
            
            case 'trademark':
                formTouched.trademark = true;
                trademarkValid = value.length > 0;
                fieldValidationErrors.trademark = trademarkValid ? '' : ' Diga qual a marca do produto';
                break;
         
            case 'image':
                formTouched.image = true;
                imageValid = value.length > 0;
                fieldValidationErrors.image = imageValid ? '' : ' Insira uma imagem para o produto';
                break;
         
            case 'thumb1':
                formTouched.thumbnail = true;
                thumbnailValid = value.length > 0;
                fieldValidationErrors.thumbnail = thumbnailValid ? '' : ' Adicione uma imagem de destaque para o produto';
                break;
            
            case 'thumb2':
                formTouched.thumbnail = true;
                thumbnailValid = value.length > 0;
                fieldValidationErrors.thumbnail = thumbnailValid ? '' : ' Adicione uma imagem de destaque para o produto';
                break;
                
            case 'price':
                formTouched.price = true;
                if(value <= 0 ) {
                    priceValid = false
                    fieldValidationErrors.price = ' Dê um preço para o produto';
                }
                else {
                    priceValid = true;
                    fieldValidationErrors.price = '';
                }
                break;
                
            case 'unit':
                formTouched.unit = true;
                if(value <= 0 ) {
                    unitValid = false
                    fieldValidationErrors.unit = ' Diga o número de itens que vem no produto';
                }
                else {
                    unitValid = true;
                    fieldValidationErrors.unit = '';
                }
                break;
        
          default:
            break;
        }

        this.setState({
            formTouched: formTouched,
            formErrors: fieldValidationErrors,
            descriptionValid: descriptionValid,
            titleValid: titleValid,
            trademarkValid: trademarkValid,
            thumbnailValid: thumbnailValid,
            priceValid: priceValid,
            unitValid: unitValid,
            imageValid: imageValid
        }, this.validateForm);
    }

    validateForm(){
        console.log(this.state)
        this.setState({
            formValid: this.state.descriptionValid 
                && this.state.titleValid
                && this.state.unitValid
                && this.state.trademarkValid
                && this.state.fileThumb1.imagePreviewUrl
                && this.state.fileThumb2.imagePreviewUrl
                && this.state.priceValid
                && this.state.imageValid
        });
    }

    async submit() {
        const { id, type, video, unit, formValid, title, description, trademark, price, previewImage, fileThumb1, fileThumb2  } = this.state

        if (!formValid) {
            return;
        }

        let thumbs = []
        if (fileThumb1)
            thumbs.push(fileThumb1.imagePreviewUrl)

        if (fileThumb2)
            thumbs.push(fileThumb2.imagePreviewUrl)
            
        const product = {
            "code": id,
            "title": title,
            "trademark": trademark,
            "thumbnail": thumbs,
            "image": previewImage,
            "description": description,
            "urlVideo": video,
            "price": price,
            "unit": unit,
            "type": type
        }

        const { loading, loaded, history, openSuccessModal, removeProductToEdit } = this.props
        if(id) {
            loading()
            const result = await this.productService.editProduct(product)
            loaded()

            if (result.response) {
                const success = {title: 'Alohaa! . m/', description: `Produto cadastrado já está disponível no app.`}
                openSuccessModal(success)
                removeProductToEdit();
                history.push('/admin/produtos-loja')
            } 

            return
        }

        loading()
        const result = await this.productService.createProduct(product)
        loaded()

        if (result.response) {
            const success = {title: 'Alohaa! . m/', description: `Produto cadastrado já está disponível no app.`}
            openSuccessModal(success)
            history.push('/admin/produtos-loja')
        } 
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        await this.submit();
    };

    handleHideAlert = () => {
        this.setState({
          alert: null
        });
    };

    render() {
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Form id="categories" onSubmit={this.handleSubmit}>
                            <Card className="effect4">
                                <CardHeader>

                                    <CardTitle tag="h4">Produtos de máquina {this.props.categoryToEdit ? ` - ${this.props.categoryToEdit.description }` : ''}</CardTitle>

                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col lg="6" md="6" xs="12">
                                            {this.title()}
                                        </Col>

                                        <Col lg="3" md="3" xs="12">
                                            {this.trademark()}
                                        </Col>

                                        <Col lg="3" md="3" xs="12">
                                            {this.price()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg="3" md="3" xs="12">
                                            {this.unit()}
                                        </Col>
                                        <Col xs="12" lg="9" md="9">
                                            {this.video()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            {this.description()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col lg="4" md="4" xs="12">
                                            {this.image()}
                                        </Col> 
                                        <Col lg="4" md="4" xs="12">
                                            {this.thumb1()}
                                        </Col> 
                                        <Col lg="4" md="4" xs="12">
                                            {this.thumb2()}
                                        </Col> 
                                    </Row>

                                </CardBody>
                                <CardFooter className="text-right">
                                    <Button className=" mt-2 col-xs-2 col-md-2 effect1" onClick={this.cancel} color="secondary">
                                        Cancelar
                                    </Button>

                                    <Button className="ml-md-2 mt-2 col-xs-2 col-md-2 effect1" color="primary" disabled={!this.state.formValid}>
                                        Salvar
                                    </Button>
                                </CardFooter>
                            </Card>
                        </Form>
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { 
        productToEdit: state.products.productToEdit
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions, ...productActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductsStoreForm));
