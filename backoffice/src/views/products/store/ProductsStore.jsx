import React from "react"
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import ReactBSAlert from "react-bootstrap-sweetalert"
import {
  Card,
  CardHeader,
  CardBody,
  Table,
  Button,
  Row,
  UncontrolledTooltip,
  Collapse,
  Pagination,
  PaginationItem,
  PaginationLink,
  Input,
  FormGroup,
  Col} from "reactstrap"
  

import './ProductsStore.scss'
import ProductService from "../../../services/ProductService"

import * as modalActions  from '../../../redux/actions/modals'
import * as loadingActions  from '../../../redux/actions/loading'
import * as productActions  from '../../../redux/actions/products'
import { CURRENCY_OPTIONS } from "../../../variables/currencyOptions";

class ProductsStore extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            products: null,
            alert: null,
            openActives: true,
            defaultFilter: {
                search:'',
                page:1,
                pageSize: 4,
                orderBy: '_id',
                onlyActive: true,
                onlyInactive:false,
                desc: false
            },
            activeFilter: {
                search:'',
                page:1,
                type:2,
                pageSize: 10,
                orderBy: '_id',
                orderByDir: {
                    title: 'sort',
                    price: 'sort',
                },
                onlyActive: true,
                onlyInactive:false,
                desc: false
            }
        }

        this.productService = new ProductService()

        this.getPagingProducts()
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0)
        while (id--) {
            window.clearTimeout(id)
        }
        window.scrollTo(0, 0)
    }

    getPagingProducts = async () => {
        const {loading, loaded} = this.props
        loading()
        const { search, page, pageSize, orderBy, desc } = this.state.activeFilter
        const request = { search, page, pageSize, orderBy, desc}

        const response = await this.productService.getStoreProducts(request)
        this.setState({ products: response})
        loaded()
    }

    orderActive = (field) => {
        let activeFilter = this.state.activeFilter

        if (!field)
            activeFilter = this.state.defaultFilter

        if(activeFilter.orderByDir[field] === 'sort')
            activeFilter.orderByDir[field] = 'sort-up' 
        else if(activeFilter.orderByDir[field] === 'sort-up')
            activeFilter.orderByDir[field] = 'sort-down'
        else
            activeFilter.orderByDir[field] = 'sort'

        if (activeFilter.orderByDir[field] ==='sort-down')
            activeFilter.desc = true
        else if (activeFilter.orderByDir[field] ==='sort-up')
            activeFilter.desc = false
        else {
            activeFilter.desc = this.state.defaultFilter.desc
            activeFilter.orderBy = this.state.defaultFilter.orderBy
            this.setState({activeFilter:activeFilter})
            this.props.getPagingStoreProducts(activeFilter)
            return
        }

        switch (field) {
            case 'title':
                activeFilter.orderBy = 'Description'
                break
            case 'price':
                activeFilter.orderBy = 'Price'
                break

            default:
                activeFilter.orderBy = this.state.defaultFilter.orderBy
        }

        this.setState({activeFilter:activeFilter})
    }

    titles = () => (
        <tr>
            <th className="title" >Nome</th>
            <th className="price" >Preço</th>
            <th className="text-right actions"></th>
        </tr>
    )

    register = (item, index) =>(
        <tr key={index}>
            <td>{`${item.trademark} ${item.title}`}</td>
            <td>{item.price.toLocaleString('pt-BR', CURRENCY_OPTIONS)}</td>
            <td className="text-right">
                <Button value={item.code} onClick={this.handleEdit} id={`edit-${index}`} className="btn-icon" color="success" >
                    <i value={item.code} className="fas fa-edit"></i>
                </Button>{` `}
                <Button className="btn-icon" color="danger"   id={`delete-${index}`} value={item.code} onClick={this.delete}  >
                    <i value={item.code} className="fas fa-trash"></i>
                </Button>
                <UncontrolledTooltip placement="bottom" target={`edit-${index}`} delay={0}>
                    Editar
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="bottom" target={`delete-${index}`} delay={0}>
                    Excluir
                </UncontrolledTooltip>
            </td>
        </tr>
    )
        
    delete = (e) => {
        this.setState({
            toDelete: e.target.attributes["value"].value,
            alert: (
                <ReactBSAlert
                    warning
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Tem certeza?"
                    onConfirm={async () => {
                        this.hideAlert()

                        const {toDelete} = this.state
                        const {openSuccessModal, loading, loaded} = this.props

                        loading()
                        const result = await this.productService.deleteProduct(toDelete)
                        loaded()

                        if(result.response) {
                            const success = {title: 'Alohaa! . m/', description: `Esse produto não está mais disponível no app.`}
                            openSuccessModal(success)
                            this.getPagingProducts()
                        }
                    }}
                    onCancel={() => this.cancelDetele()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    showCancel
                    >
                    Essa ação não poderá ser revertida.
                </ReactBSAlert>
            )
          })
    }
    
    cancelDetele = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Cancelado"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
            >
              O produto não foi excluido :)
            </ReactBSAlert>
          )
        })
      }

    hideAlert = () => {
        this.setState({
          alert: null
        })
    }

    handleEdit = (e) => {
        e.preventDefault()

        const id = e.target.attributes["value"].value
        const {products} = this.state
        
        let item = products.items.find(c => c.code == id)
        
        if(item) {
            this.props.history.push(`/admin/produtos-loja/editar/${item.trademark} ${item.title}`)
            const {setProductToEdit} = this.props
            setProductToEdit(item)
        }
    }   

    handleSearch =  async () => {
        var activeFilter = this.state.activeFilter
        activeFilter.page = 1
        this.setState({search : activeFilter }, await this.getPagingProducts())
    } 

    handleChangePageSize = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.pageSize = e.value

        this.setState({ activeFilter: activeFilter }, await this.getPagingProducts())
    }

    searchBtn = () => {
        return (
            <button className="btn btn-primary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleSearch()
                }} >
                Pesquisar
            </button>
        )
    }
    
    handleExport = async () => {
        const {activeFilter} = this.state
        const {loading, loaded} = this.props

        loading()
        await this.productService.getExcel(activeFilter)
        loaded()
    }

    exportBtn = () => {
        return (
            <button className="btn btn-secondary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleExport()
                }} >
                Exportar
            </button>
        )
    }

    list = () => {
        const {openActives, products} = this.state
        const {search} = this.state.activeFilter

        return (<Row className="products__active">
            <Col md="12">
                <Card className="products__active__card effect4">
                    <CardHeader className="products__active__card__header">
                        <h4 className="mb-3">
                            PRODUTOS LOJA
                        </h4>
                        <hr/>
                    </CardHeader>
                    <CardBody>
                    <Row>
                            <Col xs={12} sm={4} md={4} lg={4} >
                                <FormGroup className="has-label">
                                    <label htmlFor="searchActive">Buscar rápida</label>
                                        <Input 
                                            id="search"
                                            name="search"
                                            type="text"
                                            placeholder="Buscar por..."
                                            value={search}
                                            onChange={e => {
                                                e.preventDefault()
                                                let {activeFilter} = this.state

                                                activeFilter.search = e.target.value
                                                
                                                this.setState({activeFilter: activeFilter})
                                            }}
                                        />
                                </FormGroup>
                            </Col>
                            <Col className="text-right" xs={12} sm={8} md={8} lg={8}>
                                {this.exportBtn()}
                                {this.searchBtn()}
                            </Col>
                        </Row>
                        <hr/>
                        <Row>
                            <Col>
                                <Table id="capture" className="mt-3" responsive striped>
                                    <thead>
                                        {this.titles()}
                                    </thead>
                                    <tbody>
                                        {
                                            products 
                                                && products.items 
                                                && products.items.length > 0
                                                    ? products.items.map((item,index) => this.register(item,index))
                                                    : (<tr></tr>)
                                        }                                        
                                    </tbody>
                                </Table>
                                <Row>
                                    <Col>
                                        <Pagination listClassName="mt-3 justify-content-end">
                                        {products
                                            ? <PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fas fa-list"></i>{"   "} Total: {products.count}
                                                    </PaginationLink>
                                                </PaginationItem>
                                            : null}

                                            {this.state.activeFilter.page > 1
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.handlePrevPage}>
                                                            <i className="fas fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)

                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fas fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                            {products 
                                                && products.hasNextPage
                                                
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.handleNextPage}>
                                                            <i className="fas fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)

                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fas fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                        </Pagination>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </Col>
        </Row>)
    }

    handleNextPage = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.page++

        this.setState({ activeFilter: activeFilter }, await this.getPagingProducts())
    }

    handlePrevPage = async (e) => {
        e.preventDefault()
        let activeFilter = this.state.activeFilter
        activeFilter.page--
        this.setState({ activeFilter: activeFilter }, await this.getPagingProducts())
    }

    add = (e) => {
        e.preventDefault()
        this.props.history.push("/admin/produtos-loja/criar")
    }

    render() {
        return (
            <div className="content products">
                <Row>
                    <Col className="text-right">
                        <Button className="mb-3 col-xs-12 col-md-2 products__create align-self-right effect1" onClick={this.add} color="primary">
                            Adicionar
                        </Button>
                    </Col>
                </Row>
                {this.list()}
                {this.state.alert}
            </div>
        )
    }
}

function mapStateToProps() {
    return { 
    }
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions, ...productActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductsStore))
