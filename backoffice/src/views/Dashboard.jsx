import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as ideaActions from '../redux/actions/ideas'

// react plugin used to create charts
import { Line, Bar, Doughnut } from "react-chartjs-2";

// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

import {} from "variables/charts.jsx";

import './dashboard.scss'

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};


class Dashboard extends React.Component {
  
  constructor(props) {
    super(props)

    props.getDashboard();
  }

  componentWillUnmount() {
    window.scrollTo(0, 0);
  }

  percentStage1 = () => {
    let percentStage1 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Ideias semeada",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#66615b", "#f4f3ef"], //["#4acccd", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage1.options.elements.center.text = '0%' // this.props.dashboard.firstLevelPercent + '%'
    percentStage1.data.datasets[0].data = 100 //[this.props.dashboard.firstLevelPercent, 100 - this.props.dashboard.firstLevelPercent]

    return (
      <Col md="3">
        <Card>
          <CardHeader>
            <CardTitle>IDEIAS SEMEADAS</CardTitle>
            <p className="card-category">Ideias no primeiro estágio</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage1.data}
              options={percentStage1.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i className="fa fa-circle text-secondary" /> 
              Ideias semeadas
            </div>
          </CardFooter>
        </Card>
      </Col>
    );
  }

  percentStage2 = () => {
    let percentStage2 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Ideias brotando",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#4acccd", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage2.options.elements.center.text = this.props.dashboard.secondLevelPercent + '%'
    percentStage2.data.datasets[0].data = [this.props.dashboard.secondLevelPercent, 100 - this.props.dashboard.secondLevelPercent]

    return (
      <Col md="3">
        <Card>
          <CardHeader>
            <CardTitle>IDEIAS BROTANDO</CardTitle>
            <p className="card-category">Ideias no segundo estágio</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage2.data}
              options={percentStage2.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i className="fa fa-circle text-info" /> 
              Ideias brotando
            </div>

          </CardFooter>
        </Card>
      </Col>
    );
  }

  percentStage3 = () => {
    let percentStage3 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Ideias florescendo",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#fcc468", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage3.options.elements.center.text = this.props.dashboard.thirdLevelPercent + '%'
    percentStage3.data.datasets[0].data = [this.props.dashboard.thirdLevelPercent, 100 - this.props.dashboard.thirdLevelPercent]

    return (
      <Col md="3">
        <Card>
          <CardHeader>
            <CardTitle>IDEIAS FLORESCENDO</CardTitle>
            <p className="card-category">Ideias no terceiro estágio</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage3.data}
              options={percentStage3.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i className="fa fa-circle text-warning" /> 
              Ideias florescendo
            </div>

          </CardFooter>
        </Card>
      </Col>
    );
  }

  percentStage4 = () => {
    let percentStage4 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Ideias florescendo",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#f17e5d", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage4.options.elements.center.text = this.props.dashboard.latestLevelPercent + '%'
    percentStage4.data.datasets[0].data = [this.props.dashboard.latestLevelPercent, 100 - this.props.dashboard.latestLevelPercent]

    return (
      <Col md="3">
        <Card>
          <CardHeader>
            <CardTitle>IDEIAS FRUTIFICADAS</CardTitle>
            <p className="card-category">Ideias no último estágio</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage4.data}
              options={percentStage4.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i className="fa fa-circle text-danger" /> 
              Ideias Frutificadas
            </div>

          </CardFooter>
        </Card>
      </Col>
    );
  }

  render() {
    if(!this.props.dashboard)
      return null;

    return (
        <div className="content">
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-seedling text-center text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">HORAS DE CULTIVO</p>
                        <CardTitle tag="p">{this.props.dashboard.totalPoints}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-trophy text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">FRUTOS RECOMPENSADOS</p>
                        <CardTitle tag="p">{this.props.dashboard.totalGratifications}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-lightbulb text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">IDEIAS ATIVAS</p>
                        <CardTitle tag="p">{this.props.dashboard.feedIdeas}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-apple-alt text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">IDEIAS FRUTIFICADAS</p>
                        <CardTitle tag="p">{this.props.dashboard.latestLevelIdeas}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            {this.percentStage1()}         
          </Row>
        </div>
    );
  }
}

function mapStateToProps(state) {
  return { 
      dashboard: state.ideas.dashboard
  };
}

const mapDispatchToProps = dispatch => 
  bindActionCreators(ideaActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dashboard));
