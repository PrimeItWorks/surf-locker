import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row
} from "reactstrap";

import { signInAction } from '../../redux/actions/login'
import { connect } from 'react-redux';
import logo from "assets/img/logo-horizontal.png";



class Login extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      email: "",
      password: "" ,
      loaded: true
    }
  }

  submit() {
    this.props.signInAction(this.state);
    return false;
  }

  changeLogon = (e) => this.setState({email: e.target.value});
  changePassword = (e) => this.setState({password: e.target.value});

  handleSubmit = (e) => {
    e.preventDefault();
    this.submit();
  };

  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }

  render() {
    return (
      <div className="login-page">
        <Container>
          <Row>
            <Col className="" lg="4" md="4">
              <Form action="" className="form" method="" onSubmit={this.handleSubmit}>
                <Card className="card-login effect8">
                  <CardHeader style={{backgroundColor:"#05072D"}}>
                    <CardHeader >
                    <div className="logo-img col-9 ml-auto mr-auto">
                      <img src={logo} alt="Surf Locker" />
                    </div>
                    </CardHeader>
                  </CardHeader>
                  <CardBody>
                    <div className="text-center">
                      <h3>Entrar</h3>
                    </div>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="nc-icon nc-single-02" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input placeholder="E-mail" onChange={this.changeLogon} type="email" />
                    </InputGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="nc-icon nc-key-25" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="Senha"
                        type="password"
                        autoComplete="off"
                        onChange={this.changePassword}
                      />
                    </InputGroup>
                  </CardBody>
                  <CardFooter>
                    <Button
                      block
                      className="btn-round mb-3"
                      color="warning"
                      type="submit"
                    >
                      Entrar
                    </Button>
                  </CardFooter>
                </Card>
              </Form>
            </Col>
          </Row>
        </Container>
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(${require("assets/img/bg/bg-login.jpg")})`
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({ 
  loading: state.loading.status
});

export default connect(mapStateToProps, {signInAction})(Login);
