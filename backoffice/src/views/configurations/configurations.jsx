import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as gameSettings from '../../redux/actions/gameSettings'

import NumberFormat from 'react-number-format';
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  CardTitle,
  Row,
  CardFooter,
  Form,
  FormGroup,
  Popover, 
  PopoverBody, 
  Input,
  Col} from "reactstrap";

import './configurations.scss'

class Configurations extends React.Component {
    constructor(props) {
        super(props)
        props.getSettings()
    }

    state = {
        id: 0,
        ideaLifeTime: '',
        investmentPackages: '',
        authorIdeaAward: '',
        maxInvestmentByUser: '',
        maxCommentByUser: '',
        maxLikeByUser: '',
        investmentCoefficient: '',
        commentCoefficient: '',
        likeCoefficient: '',
        silverByGold: '',
        maxInvestmentValue: '',
        maxInvestmentValueByInventors: '',
        firstStageQualify: '',
        secoundStageQualify: '',
        thirdtageQualify: '',
        fourthStageQualify: 100,
        maxPointsToQualify: '',
        formErrors: { ideaLifeTime:''
            , investmentPackages: '' 
            , authorIdeaAward: '' 
            , maxInvestmentByUser: '' 
            , maxCommentByUser: '' 
            , investmentCoefficient: '' 
            , commentCoefficient: '' 
            , likeCoefficient: '' 
            , silverByGold: '' 
            , maxInvestmentValue: '' 
            , maxInvestmentValueByInventors: '' 
            , firstStageQualify: '' 
            , secoundStageQualify: '' 
            , thirdtageQualify: '' 
            , maxPointsToQualify: '' 
        },
        formTouched: { ideaLifeTime:false
            , investmentPackages:false 
            , authorIdeaAward:false 
            , maxInvestmentByUser:false 
            , maxCommentByUser:false 
            , investmentCoefficient:false 
            , commentCoefficient:false 
            , likeCoefficient:false 
            , silverByGold:false 
            , maxInvestmentValue:false 
            , maxInvestmentValueByInventors:false 
            , firstStageQualify:false 
            , secoundStageQualify:false 
            , thirdtageQualify:false 
            , maxPointsToQualify:false 
        },
        formValid: true,
        ideaLifeTimeValid: true,
        authorIdeaAwardValid: true,
        maxInvestmentByUserValid: true,
        maxCommentByUserValid: true,
        maxLikeByUserValid: true,
        investmentCoefficientValid: true,
        commentCoefficientValid: true,
        likeCoefficientValid: true,
        investmentPackagesValid: true,
        maxInvestmentValueValid: true,
        maxInvestmentValueByInventorsValid: true,
        firstStageQualifyValid: true,
        secoundStageQualifyValid: true,
        thirdtageQualifyValid: true,
        maxPointsToQualifyValid: true,
        silverByGoldValid: true,
        openCalcInvest: false,
        openCalcComment: false,
        openCalcLike: false,
        openPopPackages: false,
        show: false
    }

    errorClass(error, touched) {
        if(!touched)
            return '';

        return(error.length === 0 ? 'has-success' : 'has-danger');
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0);
        while (id--) {
            window.clearTimeout(id);
        }
        window.scrollTo(0, 0);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.defaultSettings) {
            this.setState({
                id: nextProps.defaultSettings.id,
                ideaLifeTime: nextProps.defaultSettings.ideaLifeTime,
                authorIdeaAward: nextProps.defaultSettings.authorIdeaAward,
                maxInvestmentByUser: nextProps.defaultSettings.maxInvestmentByUser,
                maxCommentByUser: nextProps.defaultSettings.maxCommentByUser,
                maxLikeByUser: nextProps.defaultSettings.maxLikeByUser,
                maxLikeByUser: nextProps.defaultSettings.maxLikeByUser,
                investmentCoefficient: nextProps.defaultSettings.investmentCoefficient,
                commentCoefficient: nextProps.defaultSettings.commentCoefficient,
                likeCoefficient: nextProps.defaultSettings.likeCoefficient,
                maxInvestmentValueByInventors: nextProps.defaultSettings.maxInvestmentValueByInventors,
                silverByGold: nextProps.defaultSettings.silverByGold,
                maxInvestmentValue: nextProps.defaultSettings.maxInvestmentValue,
                firstStageQualify: nextProps.defaultSettings.firstStageQualify,
                secoundStageQualify: nextProps.defaultSettings.secoundStageQualify,
                thirdtageQualify: nextProps.defaultSettings.thirdtageQualify,
                maxPointsToQualify: nextProps.defaultSettings.maxPointsToQualify,
                investmentPackages:nextProps.defaultSettings.investmentPackages,
                show:true
            })
        }
    }

    bindIdeaLifeTime = (e) => {
        const {floatValue} = e;
        this.setState({ideaLifeTime: floatValue}, this.validateField('ideaLifeTime', floatValue));
    }
    ideaLifeTime = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.ideaLifeTime, this.state.formTouched.ideaLifeTime)}`}>
            <label htmlFor="ideaLifeTime">Tempo de vida de cada ideia *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="ideaLifeTime"
                name="ideaLifeTime"
                type="text"
                value={this.state.ideaLifeTime}
                onValueChange={this.bindIdeaLifeTime }
            />
            
            <span className='error'>{this.state.formErrors.ideaLifeTime}</span>
        </FormGroup>
    );

    bindFourthStageQualify = (e) => {
        const {floatValue} = e;
        this.setState({maxPointsToQualify: floatValue}, this.validateField('maxPointsToQualify', floatValue));
    }
    maxPointsToQualify = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxPointsToQualify, this.state.formTouched.maxPointsToQualify)}`}>
            <label htmlFor="maxPointsToQualify">Horas de cultivo necessárias para "Frutificou" *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxPointsToQualify"
                name="maxPointsToQualify"
                type="text"
                value={this.state.maxPointsToQualify}
                onValueChange={this.bindFourthStageQualify }
            />
            
            <span className='error'>{this.state.formErrors.maxPointsToQualify}</span>
        </FormGroup>
    );

    bindThirdStagetoQualify = (e) => {
        const {floatValue} = e;
        this.setState({thirdtageQualify: floatValue}, this.validateField('thirdtageQualify', floatValue));
    }
    thirdtageQualify = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.thirdtageQualify, this.state.formTouched.thirdtageQualify)}`}>
            <label htmlFor="thirdtageQualify">Percentual para "Floresceu" *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="thirdtageQualify"
                name="thirdtageQualify"
                type="text"
                value={this.state.thirdtageQualify}
                onValueChange={this.bindThirdStagetoQualify }
            />
            
            <span className='error'>{this.state.formErrors.thirdtageQualify}</span>
        </FormGroup>
    );

    bindSecoundStageQualify = (e) => {
        const {floatValue} = e;
        this.setState({secoundStageQualify: floatValue}, this.validateField('secoundStageQualify', floatValue));
    }
    secoundStageQualify = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.secoundStageQualify, this.state.formTouched.secoundStageQualify)}`}>
            <label htmlFor="secoundStageQualify">Percentual para "Brotou" *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="secoundStageQualify"
                name="secoundStageQualify"
                type="text"
                value={this.state.secoundStageQualify}
                onValueChange={this.bindSecoundStageQualify }
            />
            
            <span className='error'>{this.state.formErrors.secoundStageQualify}</span>
        </FormGroup>
    );
    
    bindFirstStageQualify = (e) => {
        const {floatValue} = e;
        this.setState({firstStageQualify: floatValue}, this.validateField('firstStageQualify', floatValue));
    }
    firstStageQualify = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.firstStageQualify, this.state.formTouched.firstStageQualify)}`}>
            <label htmlFor="firstStageQualify">Percentual para "Semeou" *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="firstStageQualify"
                name="firstStageQualify"
                type="text"
                value={this.state.firstStageQualify}
                onValueChange={this.bindFirstStageQualify }
            />
            
            <span className='error'>{this.state.formErrors.firstStageQualify}</span>
        </FormGroup>
    );
    
    bindMaxInvestmentValueByInventors = (e) => {
        const {floatValue} = e;
        this.setState({maxInvestmentValueByInventors: floatValue}, this.validateField('maxInvestmentValueByInventors', floatValue));
    }
    maxInvestmentValueByInventors = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxInvestmentValueByInventors, this.state.formTouched.maxInvestmentValueByInventors)}`}>
            <label htmlFor="maxInvestmentValueByInventors">Máximo de fertilizante para o autor da ideia *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxInvestmentValueByInventors"
                name="maxInvestmentValueByInventors"
                type="text"
                value={this.state.maxInvestmentValueByInventors}
                onValueChange={this.bindMaxInvestmentValueByInventors }
            />
            
            <span className='error'>{this.state.formErrors.maxInvestmentValueByInventors}</span>
        </FormGroup>
    );

    bindMaxInvestmentValue = (e) => {
        const {floatValue} = e;
        this.setState({maxInvestmentValue: floatValue}, this.validateField('maxInvestmentValue', floatValue));
    }
    maxInvestmentValue = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxInvestmentValue, this.state.formTouched.maxInvestmentValue)}`}>
            <label htmlFor="maxInvestmentValue">Máximo de fertilizantes para uma ideia *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxInvestmentValue"
                name="maxInvestmentValue"
                type="text"
                value={this.state.maxInvestmentValue}
                onValueChange={this.bindMaxInvestmentValue }
            />
            
            <span className='error'>{this.state.formErrors.maxInvestmentValue}</span>
        </FormGroup>
    );

    bindSilverByGold = (e) => {
        const {floatValue} = e;
        this.setState({silverByGold: floatValue}, this.validateField('silverByGold', floatValue));
    }
    silverByGold = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.silverByGold, this.state.formTouched.silverByGold)}`}>
            <label htmlFor="silverByGold">Valor de conversão de prêmios em frutos *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="silverByGold"
                name="silverByGold"
                type="text"
                value={this.state.silverByGold}
                onValueChange={this.bindSilverByGold }
            />
            
            <span className='error'>{this.state.formErrors.silverByGold}</span>
        </FormGroup>
    );
    
    bindIdeaLifeTime = (e) => {
        const {floatValue} = e;
        this.setState({ideaLifeTime: floatValue}, this.validateField('ideaLifeTime', floatValue));
    }
    ideaLifeTime = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.ideaLifeTime, this.state.formTouched.ideaLifeTime)}`}>
            <label htmlFor="ideaLifeTime">Tempo de vida de cada ideia *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="ideaLifeTime"
                name="ideaLifeTime"
                type="text"
                value={this.state.ideaLifeTime}
                onValueChange={this.bindIdeaLifeTime }
            />
            
            <span className='error'>{this.state.formErrors.ideaLifeTime}</span>
        </FormGroup>
    );

    bindInvestmentCoefficient = (e) => {
        const {floatValue} = e;
        this.setState({investmentCoefficient: floatValue}, this.validateField('investmentCoefficient', floatValue));
    }
    focusInvestmentCoefficient =(e) =>this.setState({openCalcInvest: true})
    blurInvestmentCoefficient =(e) =>this.setState({openCalcInvest: false})
    investmentCoefficient = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.investmentCoefficient, this.state.formTouched.investmentCoefficient)}`}>
            <label htmlFor="investmentCoefficient">Quantas horas de cultivo corresponde cada fertilizante? *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="investmentCoefficient"
                name="investmentCoefficient"
                type="text"
                value={this.state.investmentCoefficient}
                onValueChange={this.bindInvestmentCoefficient }
                onFocus={this.focusInvestmentCoefficient}
                onBlur={this.blurInvestmentCoefficient}
            />
            <Popover 
                placement="bottom"
                isOpen={this.state.openCalcInvest && this.state.formErrors.investmentCoefficient.length === 0}
                target="investmentCoefficient"
                className="popover-primary">
                <PopoverBody>{Math.round(this.state.maxInvestmentByUser / this.state.investmentCoefficient) || 0}{" "}fertilizantes disponíveis</PopoverBody>
            </Popover>
            
            <span className='error'>{this.state.formErrors.investmentCoefficient}</span>
        </FormGroup>
    );

    bindCommentCoefficient = (e) => {
        const {floatValue} = e;
        this.setState({commentCoefficient: floatValue}, this.validateField('commentCoefficient', floatValue));
    }
    focusCommentCoefficient =(e) =>this.setState({openCalcComment: true})
    blurCommentCoefficient =(e) =>this.setState({openCalcComment: false})
    commentCoefficient = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.commentCoefficient, this.state.formTouched.commentCoefficient)}`}>
            <label htmlFor="commentCoefficient">Quantas horas de cultivo corresponde cada defensivo? *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="commentCoefficient"
                name="commentCoefficient"
                type="text"
                value={this.state.commentCoefficient}
                onValueChange={this.bindCommentCoefficient }
                onBlur={this.blurCommentCoefficient }
                onFocus={this.focusCommentCoefficient }
            />
            
            <Popover 
                placement="bottom"
                isOpen={this.state.openCalcComment && this.state.formErrors.commentCoefficient.length === 0}
                target="commentCoefficient"
                className="popover-primary">
                <PopoverBody>{Math.round(this.state.maxCommentByUser / this.state.commentCoefficient) || 0}{" "}defensivos disponíveis</PopoverBody>
            </Popover>
            
            <span className='error'>{this.state.formErrors.commentCoefficient}</span>
        </FormGroup>
    );

    focusLikeCoefficient =(e) =>this.setState({openCalcLike: true})
    blurLikeCoefficient =(e) =>this.setState({openCalcLike: false})
    bindLikeCoefficient = (e) => {
        const {floatValue} = e;
        this.setState({likeCoefficient: floatValue}, this.validateField('likeCoefficient', floatValue));
    }
    likeCoefficient = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.likeCoefficient, this.state.formTouched.likeCoefficient)}`}>
            <label htmlFor="likeCoefficient">Quantas horas de cultivo corresponde cada irricação? *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="likeCoefficient"
                name="likeCoefficient"
                type="text"
                value={this.state.likeCoefficient}
                onValueChange={this.bindLikeCoefficient }
                onBlur={this.blurLikeCoefficient }
                onFocus={this.focusLikeCoefficient }
            />
            
            <Popover 
                placement="bottom"
                isOpen={this.state.openCalcLike && this.state.formErrors.likeCoefficient.length === 0}
                target="likeCoefficient"
                className="popover-primary">
                <PopoverBody>{Math.round(this.state.maxLikeByUser / this.state.likeCoefficient) || 0}{" "}irrigações disponíveis</PopoverBody>
            </Popover>
            
            <span className='error'>{this.state.formErrors.likeCoefficient}</span>
        </FormGroup>
    );

    bindmaxCommentByUser = (e) => {
        const {floatValue} = e;
        this.setState({maxCommentByUser: floatValue}, this.validateField('maxCommentByUser', floatValue));
    }
    maxCommentByUser = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxCommentByUser, this.state.formTouched.maxCommentByUser)}`}>
            <label htmlFor="maxCommentByUser">Horas de cultivo disponíveis para defensivos *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxCommentByUser"
                name="maxCommentByUser"
                type="text"
                value={this.state.maxCommentByUser}
                onValueChange={this.bindmaxCommentByUser }
                onFocus={this.focusCommentCoefficient}
                onBlur={this.blurCommentCoefficient}
            />
            
            <span className='error'>{this.state.formErrors.maxCommentByUser}</span>
        </FormGroup>
    );

    bindmaxLikeByUser = (e) => {
        const {floatValue} = e;
        this.setState({maxLikeByUser: floatValue}, this.validateField('maxLikeByUser', floatValue));
    }
    maxLikeByUser = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxLikeByUser, this.state.formTouched.maxLikeByUser)}`}>
            <label htmlFor="maxLikeByUser">Horas de cultivo disponíveis para irrigações *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxLikeByUser"
                name="maxLikeByUser"
                type="text"
                value={this.state.maxLikeByUser}
                onValueChange={this.bindmaxLikeByUser }
                onFocus={this.focusLikeCoefficient}
                onBlur={this.blurLikeCoefficient}
            />
            
            <span className='error'>{this.state.formErrors.maxLikeByUser}</span>
        </FormGroup>
    );

    bindInvestmentPackages = (e) => {
        const value = e.target.value;
        this.setState({investmentPackages: e.target.value}, this.validateField('investmentPackages', value));
    }
    focusInvestmentPackages = (e) => this.setState({openPopPackages: true})
    blurInvestmentPackages = (e) => this.setState({openPopPackages: false})
    investmentPackages = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.investmentPackages, this.state.formTouched.investmentPackages)}`}>
            <label htmlFor="investmentPackages">Opções de fertilizantes (separados por ",") *</label>
            <Input 
                className="form-control"
                maxLength="20"
                id="investmentPackages"
                name="investmentPackages"
                type="text"
                value={this.state.investmentPackages}
                onChange={this.bindInvestmentPackages}
                onFocus={this.focusInvestmentPackages}
                onBlur={this.blurInvestmentPackages}
            />
            <Popover 
                placement="bottom"
                isOpen={this.state.openPopPackages && this.state.formErrors.investmentPackages.length === 0}
                target="investmentPackages"
                className="popover-primary">
                <PopoverBody>
                    São as opções de fertilizantes para o jogador selecionar.
                </PopoverBody>
            </Popover>
            
            <span className='error'>{this.state.formErrors.investmentPackages}</span>
        </FormGroup>
    );

    bindIdeaAuthorAward = (e) => {
        const {floatValue} = e;
        this.setState({authorIdeaAward: floatValue}, this.validateField('authorIdeaAward', floatValue));
    }
    authorIdeaAward = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.authorIdeaAward, this.state.formTouched.authorIdeaAward)}`}>
            <label htmlFor="authorIdeaAward">Frutos para autor de uma ideia aprovada *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="authorIdeaAward"
                name="authorIdeaAward"
                type="text"
                value={this.state.authorIdeaAward}
                onValueChange={this.bindIdeaAuthorAward }
            />
            
            <span className='error'>{this.state.formErrors.authorIdeaAward}</span>
        </FormGroup>
    );

    bindMaxInvestmentByUser = (e) => {
        const {floatValue} = e;
        this.setState({maxInvestmentByUser: floatValue}, this.validateField('maxInvestmentByUser', floatValue));
    }
    maxInvestmentByUser = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.maxInvestmentByUser, this.state.formTouched.maxInvestmentByUser)}`}>
            <label htmlFor="maxInvestmentByUser">Horas de cultivo disponíveis para investimento *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="maxInvestmentByUser"
                name="maxInvestmentByUser"
                type="text"
                value={this.state.maxInvestmentByUser}
                onValueChange={this.bindMaxInvestmentByUser }
                onFocus={this.focusInvestmentCoefficient}
                onBlur={this.blurInvestmentCoefficient}
            />
            
            <span className='error'>{this.state.formErrors.maxInvestmentByUser}</span>
        </FormGroup>
    );

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let ideaLifeTimeValid = this.state.ideaLifeTimeValid;
        let investmentPackagesValid = this.state.investmentPackagesValid;
        let authorIdeaAwardValid = this.state.authorIdeaAwardValid;
        let maxInvestmentByUserValid = this.state.maxInvestmentByUserValid;
        let maxCommentByUserValid = this.state.maxCommentByUserValid;
        let maxLikeByUserValid = this.state.maxLikeByUserValid;
        let investmentCoefficientValid = this.state.investmentCoefficientValid;
        let commentCoefficientValid = this.state.commentCoefficientValid;
        let likeCoefficientValid = this.state.likeCoefficientValid;
        let silverByGoldValid = this.state.silverByGoldValid;
        let maxInvestmentValueValid = this.state.maxInvestmentValueValid;
        let maxInvestmentValueByInventorsValid = this.state.maxInvestmentValueByInventorsValid;
        let firstStageQualifyValid = this.state.firstStageQualifyValid;
        let secoundStageQualifyValid = this.state.secoundStageQualifyValid;
        let thirdtageQualifyValid = this.state.thirdtageQualifyValid;
        let maxPointsToQualifyValid = this.state.maxPointsToQualifyValid;
        let formTouched = this.state.formTouched;
        switch(fieldName) {
            case 'ideaLifeTime':
                formTouched.ideaLifeTime = true;
                if(value === undefined) {
                    ideaLifeTimeValid = false
                    fieldValidationErrors.ideaLifeTime = ' Especifique o tempo de vida de uma ideia.';
                }
                else {
                    ideaLifeTimeValid = true;
                    fieldValidationErrors.ideaLifeTime = '';
                }
                break;
            
            case 'secoundStageQualify':
                formTouched.secoundStageQualify = true;
                if(value === undefined) {
                    secoundStageQualifyValid = false
                    fieldValidationErrors.secoundStageQualify = ' Especifique as horas de cultivo necessárias para "Brotou".';
                }
                else if(this.state.firstStageQualify >= value) {
                    secoundStageQualifyValid = false;
                    fieldValidationErrors.secoundStageQualify = ' Percentual para "Brotou" deve ser maior que "Semeou"';
                }
                else if(value >= 100) {
                    secoundStageQualifyValid = false;
                    fieldValidationErrors.secoundStageQualify = ' Percentual para "Brotou" deve ser menor que 100';
                }
                else {
                    secoundStageQualifyValid = true;
                    fieldValidationErrors.secoundStageQualify = '';
                }
                break;
            
            
            case 'thirdtageQualify':
                formTouched.thirdtageQualify = true;
                    if(value === undefined) {
                    thirdtageQualifyValid = false
                    fieldValidationErrors.thirdtageQualify = ' Especifique as horas de cultivo necessárias para "Floresceu".';
                }
                else if(this.state.secoundStageQualify >= value) {
                    thirdtageQualifyValid = false;
                    fieldValidationErrors.thirdtageQualify = ' Horas de cultivo para "Floresceu" deve ser maior que "Brotou"';
                }
                else if(value >= 100) {
                    thirdtageQualifyValid = false;
                    fieldValidationErrors.thirdtageQualify = ' Percentual para "Floresceu" deve ser menor que 100';
                }
                else {
                    thirdtageQualifyValid = true;
                    fieldValidationErrors.thirdtageQualify = '';
                }
                break;

            case 'maxPointsToQualify':
                formTouched.maxPointsToQualify = true;
                    if(!value) {
                    maxPointsToQualifyValid = false
                    fieldValidationErrors.maxPointsToQualify = ' Especifique as horas de cultivo necessárias para "Frutificou".';
                }
                else {
                    maxPointsToQualifyValid = true;
                    fieldValidationErrors.maxPointsToQualify = '';
                }
                break;
            
            case 'firstStageQualify':
                formTouched.firstStageQualify = true;
                if(value === undefined) {
                    firstStageQualifyValid = false
                    fieldValidationErrors.firstStageQualify = ' Especifique as horas de cultivo necessárias para "Semeou".';
                }
                else if(value >= 100) {
                    firstStageQualifyValid = false;
                    fieldValidationErrors.firstStageQualify = ' Percentual para "Semeou" deve ser menor que 100';
                }
                else {
                    firstStageQualifyValid = true;
                    fieldValidationErrors.firstStageQualify = '';
                }
                break;
            
            case 'maxInvestmentValueByInventors':
                formTouched.maxInvestmentValueByInventors = true;
                if(!value) {
                    maxInvestmentValueByInventorsValid = false
                    fieldValidationErrors.maxInvestmentValueByInventors = ' Especifique o máximo de fertilizante para o autor da ideia.';
                }
                else {
                    maxInvestmentValueByInventorsValid = true;
                    fieldValidationErrors.maxInvestmentValueByInventors = '';
                }
                break;
            
            case 'maxInvestmentValue':
                formTouched.maxInvestmentValue = true;
                if(!value) {
                    maxInvestmentValueValid = false
                    fieldValidationErrors.maxInvestmentValue = ' Especifique o máximo de fertilizantes para uma ideia.';
                }
                else {
                    maxInvestmentValueValid = true;
                    fieldValidationErrors.maxInvestmentValue = '';
                }
                break;
            
            case 'silverByGold':
                formTouched.silverByGold = true;
                if(!value) {
                    silverByGoldValid = false
                    fieldValidationErrors.silverByGold = ' Especifique o tempo de vida de uma ideia.';
                }
                else {
                    silverByGoldValid = true;
                    fieldValidationErrors.silverByGold = '';
                }
                break;
            
            case 'investmentCoefficient':
                formTouched.investmentCoefficient = true;
                if(!value) {
                    investmentCoefficientValid = false
                    fieldValidationErrors.investmentCoefficient = ' Especifique quantas horas de cultivo corresponde cada fertilizante.';
                }
                else {
                    investmentCoefficientValid = true;
                    fieldValidationErrors.investmentCoefficient = '';
                }
                break;

            case 'likeCoefficient':
                formTouched.likeCoefficient = true;
                if(!value) {
                    likeCoefficientValid = false
                    fieldValidationErrors.likeCoefficient = ' Especifique quantas horas de cultivo corresponde cada irrigação.';
                }
                else {
                    likeCoefficientValid = true;
                    fieldValidationErrors.likeCoefficient = '';
                }
                break;

            case 'commentCoefficient':
                formTouched.commentCoefficient = true;
                if(!value) {
                    commentCoefficientValid = false
                    fieldValidationErrors.commentCoefficient = ' Especifique quantas horas de cultivo corresponde cada defensivo.';
                }
                else {
                    commentCoefficientValid = true;
                    fieldValidationErrors.commentCoefficient = '';
                }
                break;
            
            case 'maxInvestmentByUser':
                formTouched.maxInvestmentByUser = true;
                if(!value) {
                    maxInvestmentByUserValid = false
                    fieldValidationErrors.maxInvestmentByUser = ' Especifique o máximo de horas de cultivo para fertilizar uma ideia.';
                }
                else {
                    maxInvestmentByUserValid = true;
                    fieldValidationErrors.maxInvestmentByUser = ''; 
                }
                break;

                
            case 'maxCommentByUser':
                formTouched.maxCommentByUser = true;
                if(!value) {
                    maxCommentByUserValid = false
                    fieldValidationErrors.maxCommentByUser = ' Especifique o máximo de horas de cultivo para defendensivos de uma ideia.';
                }
                else {
                    maxCommentByUserValid = true;
                    fieldValidationErrors.maxCommentByUser = '';
                }
                break;

            case 'maxLikeByUser':
                formTouched.maxLikeByUser = true;
                if(!value) {
                    maxLikeByUserValid = false
                    fieldValidationErrors.maxLikeByUser = ' Especifique o máximo de horas de cultivo para irrigar uma ideia.';
                }
                else {
                    maxLikeByUserValid = true;
                    fieldValidationErrors.maxLikeByUser = '';
                }
                break;
                
            case 'investmentPackages':

                formTouched.investmentPackages = true;
                if(!value) {
                    investmentPackagesValid = false
                    fieldValidationErrors.investmentPackages = ' Especifique as opções de fertilizantes.';
                }
                else {
                    investmentPackagesValid = true;
                    fieldValidationErrors.investmentPackages = '';
                }
                break;
            case 'authorIdeaAward':
                formTouched.authorIdeaAward = true;
                if(value === undefined) {
                    authorIdeaAwardValid = false
                    fieldValidationErrors.authorIdeaAward = ' Especifique a quantidade de frutos destinada ao autor da ideia.';
                }
                else {
                    authorIdeaAwardValid = true;
                    fieldValidationErrors.authorIdeaAward = '';
                }
                break;
          default:
            break;
        }

        this.setState({
            formTouched: formTouched,
            formErrors: fieldValidationErrors,
            ideaLifeTimeValid: ideaLifeTimeValid,
            authorIdeaAwardValid: authorIdeaAwardValid,
            maxInvestmentByUserValid: maxInvestmentByUserValid,
            maxInvestmentValueValid: maxInvestmentValueValid,
            maxCommentByUserValid: maxCommentByUserValid,
            maxLikeByUserValid: maxLikeByUserValid,
            investmentCoefficientValid: investmentCoefficientValid,
            firstStageQualifyValid: firstStageQualifyValid,
            secoundStageQualifyValid: secoundStageQualifyValid,
            thirdtageQualifyValid: thirdtageQualifyValid,
            maxPointsToQualifyValid: maxPointsToQualifyValid,
            commentCoefficientValid: commentCoefficientValid,
            likeCoefficientValid: likeCoefficientValid,
            investmentPackagesValid: investmentPackagesValid,
            silverByGoldValid: silverByGoldValid,
            maxInvestmentValueByInventorsValid: maxInvestmentValueByInventorsValid,
        }, this.validateForm);
    }

    validateForm(){
        this.setState({
            formValid: this.state.ideaLifeTimeValid
                && this.state.investmentPackagesValid
                && this.state.maxInvestmentByUserValid
                && this.state.authorIdeaAwardValid
                && this.state.maxLikeByUserValid
                && this.state.investmentCoefficientValid
                && this.state.commentCoefficientValid
                && this.state.maxCommentByUserValid
                && this.state.likeCoefficientValid
                && this.state.silverByGoldValid
                && this.state.maxInvestmentValueValid
                && this.state.maxInvestmentValueByInventorsValid
                && this.state.firstStageQualifyValid
                && this.state.secoundStageQualifyValid
                && this.state.thirdtageQualifyValid
                && this.state.maxPointsToQualifyValid
        });
    }

    submit() {
        
        this.props.updateSettings(this.state);
        
        setTimeout(() => this.props.history.push("/"),500)
        
        return false;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.submit();
    };

    cancel = (e) => {
        e.preventDefault();

        this.props.history.push("/")
    } 
    
    render = () => {
        if(!this.state.show)
            return null;

        return (
            <div className="content configurations">
                <div className="content configurations__form">
                    <Row className="">
                        <Col md="12">
                            <Form id="configurations" onSubmit={this.handleSubmit}>
                            <Card>
                                <CardHeader>
                                    <CardTitle tag="h4">Configurações do jogo</CardTitle>
                                </CardHeader>

                                <CardBody>
                                    <Row>
                                        <Col lg="6" md="6" xs="12">
                                            {this.ideaLifeTime()}
                                        </Col>

                                        <Col lg="6" md="6" xs="12">
                                            {this.authorIdeaAward()}
                                        </Col>
                                        
                                        <Col lg="6" md="6" xs="12">
                                            {this.silverByGold()}
                                        </Col>
                                        
                                        <Col lg="6" md="6" xs="12">
                                            {this.investmentPackages()}
                                        </Col>

                                        <Col lg="6" md="6" xs="12">
                                            {this.maxInvestmentValue()}
                                        </Col>

                                        <Col lg="6" md="6" xs="12">
                                            {this.maxInvestmentValueByInventors()}
                                        </Col>
                                    </Row>

                                    <h4>Limites de interações do jogador</h4>

                                    <Row>
                                        <Col className="m-0 p-0" lg="4" md="4" xs="12">
                                            <Col xs="12">
                                                {this.maxInvestmentByUser()}
                                            </Col>
                                        
                                            <Col xs="12">
                                                {this.investmentCoefficient()}                                            
                                            </Col>
                                        </Col>
                                        <Col lg="4" md="4" xs="12" className="m-0 p-0">
                                            <Col xs="12">
                                                {this.maxCommentByUser()}
                                            </Col>
                                        
                                            <Col xs="12">
                                                {this.commentCoefficient()}                                            
                                            </Col>
                                        </Col>
                                        <Col lg="4" md="4" xs="12" className="m-0 p-0">
                                            <Col xs="12">
                                                {this.maxLikeByUser()}
                                            </Col>

                                            <Col xs="12">
                                                {this.likeCoefficient()}                                           
                                            </Col>
                                        </Col>
                                    </Row>

                                    <h4>Valores de referência para evolução das ideias</h4>

                                    <Row>
                                        <Col xs="3">
                                            {this.firstStageQualify()}
                                        </Col>
                                    
                                        <Col xs="3">
                                            {this.secoundStageQualify()}
                                        </Col>

                                        <Col xs="3">
                                    	    {this.thirdtageQualify()}
                                        </Col>
                                    
                                        <Col xs="3">
                                            {this.maxPointsToQualify()}
                                        </Col>
                                    </Row>
                                </CardBody>

                                <CardFooter className="text-right">

                                    <Button className=" mt-2 col-xs-2 col-md-2" onClick={this.cancel} color="secondary">
                                        Cancelar
                                    </Button>

                                    <Button className="ml-md-2 mt-2 col-xs-2 col-md-2" color="primary" disabled={!this.state.formValid}>
                                        Salvar
                                    </Button>
                                </CardFooter>
                            </Card>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        
                    </Row>
                </div>
            </div>
        );
    } 
}


function mapStateToProps(state) {
    return { 
        defaultSettings: state.settings.defaultSettings
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators(gameSettings, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Configurations));
  