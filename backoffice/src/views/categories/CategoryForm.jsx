import React from "react";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import NumberFormat from 'react-number-format';
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Button,
  CardTitle,
  Row,
  CardFooter,
  Form,
  FormGroup,
  UncontrolledTooltip,
  Label,
  Input,
  Col} from "reactstrap";
import { setTimeout } from "timers";

class CategoryForm extends React.Component {
    constructor(props){
        super(props);

        if(props.categoryToEdit) {
            this.state = {
                id: props.categoryToEdit.id,
                description: props.categoryToEdit.description,
                coefficient: props.categoryToEdit.coefficient,
                active:  props.categoryToEdit.active,
                formErrors: { description:'',  coefficient: '' },
                formTouched: { description:false,  coefficient: false },
                formValid: true,
                descriptionValid: true,
                coefficientValid: true,
            };
        }
        else {
            this.state = {
                id: 0,
                description: '',
                coefficient: '',
                active: true,
                formErrors: { description:'',  coefficient: '' },
                formTouched: { description:false,  coefficient: false },
                formValid: false,
                descriptionValid: false,
                coefficientValid: false,
            }
        }
    }

    componentWillUnmount() {
        window.scrollTo(0, 0);
    }
    
    errorClass(error, touched) {
        if(!touched)
            return '';

        return(error.length === 0 ? 'has-success' : 'has-danger');
    }


    cancel = (e) => {
        e.preventDefault();
        this.props.removeCategoryToEdit();
        this.props.history.push('/admin/categorias')
    }

    bindDescription = (e) => this.setState({description: e.target.value}, this.validateField('description', e.target.value));
    description = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.description, this.state.formTouched.description)}`}>
            <label htmlFor="description">Título *</label>
            <Input
                id="description"
                name="description"
                type="text"
                value={this.state.description}
                onChange={e => this.bindDescription(e)}
            />
            <span className='error'>{this.state.formErrors.description}</span>
        </FormGroup>
    )

    bindCoefficient = (e) => {
        const {floatValue} = e;
        this.setState({coefficient: floatValue}, this.validateField('coefficient', floatValue));
      }
    coefficient = () => (
        <FormGroup className={`has-label ${this.errorClass(this.state.formErrors.coefficient, this.state.formTouched.coefficient)}`}>
            <label htmlFor="coefficient">Peso *</label>
            <NumberFormat 
                className="form-control"
                decimalSeparator="." 
                maxLength="4"
                id="coefficient"
                name="coefficient"
                type="text"
                value={this.state.coefficient}
                onValueChange={this.bindCoefficient }
            />
            
            <span className='error'>{this.state.formErrors.coefficient}</span>
        </FormGroup>
    )

    bindActive = (e) =>  this.setState({active: !this.state.active});
    active =() => (
        <FormGroup check>  
                <Label  id="active-register" check>
                    <Input onChange={this.bindActive} checked={this.state.active} value={this.state.active} type="checkbox" id="active" />{' '}
                    Esta categoria está ativa?
                    <span className="form-check-sign">
                        <span  className="check"></span>
                    </span>
                </Label>

            <UncontrolledTooltip className="categories__form__tooltip"  placement="bottom" target="active-register" delay={0}>
                <i className="fas fa-exclamation-triangle text-danger" ></i>{"  "}
                                <span className="text-danger">Importante!</span><br/><br/>Após inativar uma categoria não será possível criar uma ideia para a mesma, mas as ideias existentes continuam valendo!<br/><br/> <span className="categories__inactive__card__broh">;-)</span>
            </UncontrolledTooltip>
        </FormGroup>
    )

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let descriptionValid = this.state.descriptionValid;
        let coefficientValid = this.state.coefficientValid;
        let formTouched = this.state.formTouched;
        switch(fieldName) {
            case 'description':
                formTouched.description = true;
                descriptionValid = value.length > 0;
                fieldValidationErrors.description = descriptionValid ? '' : ' Dê um título para a categoria';
                break;

            case 'coefficient':
                formTouched.coefficient = true;
                if(value <= 0 ) {
                    coefficientValid = false
                    fieldValidationErrors.coefficient = ' Dê um peso para a categoria';
                }
                else if(value >= 10) {
                    coefficientValid = false
                    fieldValidationErrors.coefficient = ' O Peso da categoria deve ser menor que 10.';
                }
                else {
                    coefficientValid = true;
                    fieldValidationErrors.coefficient = '';
                }
                
                break;
        
          default:
            break;
        }

        this.setState({
            formTouched: formTouched,
            formErrors: fieldValidationErrors,
            descriptionValid: descriptionValid,
            coefficientValid: coefficientValid
        }, this.validateForm);
    }

    validateForm(){
        this.setState({
            formValid: this.state.descriptionValid 
                && this.state.coefficientValid
        });
    }

    submit() {
        if(this.props.categoryToEdit) {
            this.props.updateCategories(this.state);
            setTimeout(() => this.props.history.push("/admin/categorias"),1000)
            return false;
        }

        this.props.createCategories(this.state);
        setTimeout(() => this.props.history.push("/admin/categorias"),1000)
        return false;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.submit();
    };

    render() {
        return (
            <div className="content">
                <Row>
                    <Col md="12">
                        <Form id="categories" onSubmit={this.handleSubmit}>
                            <Card>
                                <CardHeader>

                                    <CardTitle tag="h4">Cadastro de Categorias {this.props.categoryToEdit ? ` - ${this.props.categoryToEdit.description }` : ''}</CardTitle>

                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col lg="6" md="6" xs="12">
                                            {this.description()}
                                        </Col>
                                        <Col lg="6" md="6" xs="12">
                                            {this.coefficient()}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            {this.active()}
                                        </Col>
                                    </Row>

                                </CardBody>
                                <CardFooter className="text-right">

                                    <Button className=" mt-2 col-xs-2 col-md-2" onClick={this.cancel} color="secondary">
                                        Cancelar
                                    </Button>

                                    <Button className="ml-md-2 mt-2 col-xs-2 col-md-2" color="primary" disabled={!this.state.formValid}>
                                        Salvar
                                    </Button>

                                </CardFooter>
                            </Card>
                        </Form>
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { 
        categoryToEdit: state.categories.categoryToEdit
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators(categoryActions, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CategoryForm));
