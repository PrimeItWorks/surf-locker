import React from "react";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as categoryActions from '../../redux/actions/categories'
import * as messageActions from '../../redux/actions/message'

import ReactBSAlert from "react-bootstrap-sweetalert";
import {
  Card,
  CardHeader,
  CardBody,
  Table,
  Button,
  Row,
  UncontrolledTooltip,
  Modal, 
  Collapse,
  ModalHeader, 
  Pagination,
  PaginationItem,
  PaginationLink,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  FormGroup,
  ModalBody, 
  ModalFooter, 
  Col} from "reactstrap";

import './categories.scss';

class Categories extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            alert: null,
            openActives: true,
            openInactives: true,
            defaultFilter: {
                search:'',
                page:1,
                pageSize: 7,
                orderBy: 'Created',
                onlyActive: true,
                onlyInactive:false,
                desc: false
            },
            activeFilter: {
                search:'',
                page:1,
                pageSize: 7,
                orderBy: 'Created',
                orderByDir: {
                    id: 'sort',
                    description: 'sort',
                    coefficient: 'sort',
                },
                onlyActive: true,
                onlyInactive:false,
                desc: false
            },
            inactiveFilter: {
                search:'',
                page:1,
                pageSize: 7,
                orderBy: 'Created',
                orderByDir: {
                    id: 'sort',
                    description: 'sort',
                    coefficient: 'sort',
                },
                onlyActive: false,
                onlyInactive:true,
                desc: false
            }
        };
        this.getPagingCategories();
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0);
        while (id--) {
            window.clearTimeout(id);
        }
        window.scrollTo(0, 0);
    }

    getPagingCategories = () => {
        this.props.getActiveCategoriesPaging(this.state.activeFilter);
        this.props.getInactiveCategoriesPaging(this.state.inactiveFilter);
    }

    orderActive = (field, direction) => {
        let activeFilter = this.state.activeFilter;

        if (!field)
            activeFilter = this.state.defaultFilter;

        if(activeFilter.orderByDir[field] === 'sort')
            activeFilter.orderByDir[field] = 'sort-up'; 
        else if(activeFilter.orderByDir[field] === 'sort-up')
            activeFilter.orderByDir[field] = 'sort-down';
        else
            activeFilter.orderByDir[field] = 'sort';

        if (activeFilter.orderByDir[field] ==='sort-down')
            activeFilter.desc = true;
        else if (activeFilter.orderByDir[field] ==='sort-up')
            activeFilter.desc = false;
        else {
            activeFilter.desc = this.state.defaultFilter.desc;
            activeFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({activeFilter:activeFilter});
            this.props.getActiveCategoriesPaging(activeFilter);
            return;
        }

        switch (field) {
            case 'description':
                activeFilter.orderBy = 'Description';
                break;

            case 'coefficient':
                activeFilter.orderBy = 'Coefficient';
                break;

            case 'id':
                activeFilter.orderBy = 'Id';
                break;

            default:
                activeFilter.orderBy = this.state.defaultFilter.orderBy;
        }

        this.setState({activeFilter:activeFilter});
        this.props.getActiveCategoriesPaging(activeFilter);
    }

    orderInactive = (field, direction) => {
        let inactiveFilter = this.state.inactiveFilter;

        if (!field)
            inactiveFilter = this.state.defaultFilter;

        if(inactiveFilter.orderByDir[field] === 'sort')
            inactiveFilter.orderByDir[field] = 'sort-up'; 
        else if(inactiveFilter.orderByDir[field] === 'sort-up')
            inactiveFilter.orderByDir[field] = 'sort-down';
        else
            inactiveFilter.orderByDir[field] = 'sort';

        if (inactiveFilter.orderByDir[field] ==='sort-down')
            inactiveFilter.desc = true;
        else if (inactiveFilter.orderByDir[field] ==='sort-up')
            inactiveFilter.desc = false;
        else {
            inactiveFilter.desc = this.state.defaultFilter.desc;
            inactiveFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({inactiveFilter:inactiveFilter});
            this.props.getInactiveCategoriesPaging(inactiveFilter);
            return;
        }

        switch (field) {
            case 'description':
                inactiveFilter.orderBy = 'Description';
                break;

            case 'coefficient':
                inactiveFilter.orderBy = 'Coefficient';
                break;

            case 'id':
                inactiveFilter.orderBy = 'Id';
                break;

            default:
                inactiveFilter.orderBy = this.state.defaultFilter.orderBy;
        }
        
        this.setState({inactiveFilter:inactiveFilter});
        this.props.getInactiveCategoriesPaging(inactiveFilter);
    }

    titlesInactive = () => (
        <tr>
            <th className="clickable id" onClick={(e) => {e.preventDefault(); this.orderInactive('id',this.state.inactiveFilter.orderByDir.id)}} ><i className={`fa fa-${this.state.inactiveFilter.orderByDir.id}`}></i>{'  '}#</th>
            <th className="clickable description" onClick={(e) => {e.preventDefault(); this.orderInactive('description',this.state.inactiveFilter.orderByDir.description)}} ><i className={`fa fa-${this.state.inactiveFilter.orderByDir.description}`}></i>{'  '}Nome</th>
            <th className="clickable coefficient text-center" onClick={(e) => {e.preventDefault(); this.orderInactive('coefficient',this.state.inactiveFilter.orderByDir.coefficient)}} ><i className={`fa fa-${this.state.inactiveFilter.orderByDir.coefficient}`}></i>{'  '}Peso</th>
            <th className="text-right actions">Ações</th>
        </tr>
    );

    titlesActive = () => (
        <tr>
            <th className="clickable id" onClick={(e) => {e.preventDefault(); this.orderActive('id',this.state.activeFilter.orderByDir.id)}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.id}`}></i>{'  '}#</th>
            <th className="clickable description" onClick={(e) => {e.preventDefault(); this.orderActive('description',this.state.activeFilter.orderByDir.description)}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.description}`}></i>{'  '}Nome</th>
            <th className="clickable coefficient text-center" onClick={(e) => {e.preventDefault(); this.orderActive('coefficient',this.state.activeFilter.orderByDir.coefficient)}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.coefficient}`}></i>{'  '}Peso</th>
            <th className="text-right actions">Ações</th>
        </tr>
    );

    deactivate = (e) => {
        e.preventDefault();
        this.props.deactivateCategoryById(e.target.value,this.state.activeFilter,this.state.inactiveFilter);
    }
    registerActive = (item, index) =>(
        <tr key={index}>
            <td>{item.id}</td>
            <td>{item.description}</td>
            <td className="text-center">{item.coefficient}</td>
            <td className="text-right">
                <Button value={item.id} onClick={this.editActive} id={`edit-${index}`} className="btn-icon" color="success" >
                    <i className="fas fa-edit"></i>
                </Button>{` `}
                <Button className="btn-icon" color="danger"  value={item.id} onClick={this.deactivate} id={`inactivate-${index}`}>
                    <i className="fas fa-eye-slash"></i>
                </Button>

                <UncontrolledTooltip placement="bottom" target={`edit-${index}`} delay={0}>
                    Editar a categoria
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="bottom" target={`inactivate-${index}`} delay={0}>
                    Inativar a categoria
                </UncontrolledTooltip>
            </td>
        </tr>
    );
    
    activate = (e) => {
        e.preventDefault();
        this.props.activateCategoryById(e.target.value,this.state.activeFilter,this.state.inactiveFilter);
    }

    delete = (e) => {
        this.setState({
            toDelete: e.target.value,
            alert: (
                <ReactBSAlert
                    warning
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Tem certeza?"
                    onConfirm={() => {
                        this.props.deleteCategoryById(this.state.toDelete, this.state.inactiveFilter)
                        this.hideAlert();
                    }}
                    onCancel={() => this.cancelDetele()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    showCancel
                    >
                    Essa ação não poderá ser revertida.
                </ReactBSAlert>
            )
          });
    }
    

    cancelDetele = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Cancelado"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
            >
              A categoria não foi excluida :)
            </ReactBSAlert>
          )
        });
      };

    hideAlert = () => {
        this.setState({
          alert: null
        });
    };

    registerInactive = (item, index) =>(
        <tr key={index}>
            <td>{item.id}</td>
            <td>{item.description}</td>
            <td className="text-center">{item.coefficient}</td>
            <td className="text-right">
                <Button value={item.id} onClick={this.editInactive} id={`editinactive-${index}`} className="btn-icon" color="success" >
                    <i className="fas fa-edit"></i>
                </Button>{` `}
                <Button className="btn-icon" color="info"   id={`activate-${index}`} value={item.id} onClick={this.activate}  >
                    <i className="fas fa-eye"></i>
                </Button>{` `}
                <Button className="btn-icon" color="danger"   id={`delete-${index}`} value={item.id} onClick={this.delete}  >
                    <i className="fas fa-trash"></i>
                </Button>

                <UncontrolledTooltip placement="bottom" target={`editinactive-${index}`} delay={0}>
                    Editar a categoria
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="bottom" target={`activate-${index}`} delay={0}>
                    Ativar a categoria
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="bottom" target={`delete-${index}`} delay={0}>
                    Deletar categoria permanentemente
                </UncontrolledTooltip>
            </td>
        </tr>
    );

    editActive = (e) => {
        e.preventDefault();
        let id = e.target.value;
        let item = this.props.pagingActiveCategories.items.find(c => c.id == id);

        if(item){
            this.props.setCategoryToEdit(item);
            this.props.history.push(`/admin/categorias/editar/${item.description}`)
        }
    }   
    
    editInactive = (e) => {
        e.preventDefault();
        let id = e.target.value;
        let item = this.props.pagingInactiveCategories.items.find(c => c.id == id);

        if(item){
            this.props.setCategoryToEdit(item);
            this.props.history.push(`/admin/categorias/editar/${item.description}`)
        }
    }   
    bindActiveSearch = (e) => {
        var activeFilter = this.state.activeFilter;
        activeFilter.search = e.target.value;
        activeFilter.page = 1;
        
        this.setState({activeFilter : activeFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getActiveCategoriesPaging(activeFilter);
        }
    } 

    bindInactiveSearch = (e) => {
        var inactiveFilter = this.state.inactiveFilter;
        inactiveFilter.search = e.target.value;
        inactiveFilter.page = 1;
        
        this.setState({inactiveFilter : inactiveFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getInactiveCategoriesPaging(inactiveFilter);
        }
    } 

    activeCategories = () => {
            return (<Row className="categories__active">
            <Col md="12">
                <Card className="categories__active__card">
                    <CardHeader className="categories__active__card__header">
                        <a aria-expanded={this.state.openActives}
                            data-toggle="collapse"
                            onClick={(e) => {e.preventDefault(); this.setState({ openActives: !this.state.openActives})}}>
                                <h4 className="mb-3">
                                    CATEGORIAS ATIVAS
                                    <i className={`float-right nc-icon nc-minimal-${this.state.openActives ? 'up' : 'down'}`} />
                                </h4>
                                <hr/>
                        </a>
                    </CardHeader>
                    <Collapse
                        role="tabpanel"
                        isOpen={this.state.openActives}
                        >
                        <CardBody>
                            <Row>
                                <Col xs={12} md={12} >
                                    <FormGroup className="has-label">
                                        <label htmlFor="searchActive">Nome</label>
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                <i className="fas fa-search" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input 
                                                id="searchActive"
                                                name="searchActive"
                                                type="text"
                                                placeholder="Digite pelo menos três caracteres..."
                                                value={this.state.activeFilter.search}
                                                onChange={this.bindActiveSearch}
                                            />
                                        </InputGroup>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Table className="mt-3" responsive>
                                        <thead>
                                            {this.titlesActive()}
                                        </thead>
                                        <tbody>
                                            {
                                                this.props.pagingActiveCategories 
                                                    && this.props.pagingActiveCategories.items 
                                                    && this.props.pagingActiveCategories.items.length > 0
                                                        ? this.props.pagingActiveCategories.items.map((item,index) => this.registerActive(item,index))
                                                        : (<tr></tr>)
                                            }                                        
                                        </tbody>
                                    </Table>
                                    <Row>
                                        <Col>
                                            <Pagination listClassName="mt-3 justify-content-end">
                                            {this.props.pagingActiveCategories
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-list"></i>{"   "} Total: {this.props.pagingActiveCategories.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}

                                                {this.state.activeFilter.page > 1
                                                    ? (<PaginationItem >
                                                            <PaginationLink href="javascript:void(0)" onClick={this.prevPageActive}>
                                                                <i className="fas fa-arrow-left"></i>
                                                            </PaginationLink>
                                                        </PaginationItem>)

                                                    : (<PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)}
                                                {this.props.pagingActiveCategories 
                                                    && this.props.pagingActiveCategories.hasNextPage
                                                    
                                                    ? (<PaginationItem >
                                                            <PaginationLink href="javascript:void(0)" onClick={this.nextPageActive}>
                                                                <i className="fas fa-arrow-right"></i>
                                                            </PaginationLink>
                                                        </PaginationItem>)

                                                    : (<PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)}
                                            </Pagination>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </CardBody>
                    </Collapse>
                </Card>
            </Col>
        </Row>);
    }

    nextPageActive = (e) => {
        e.preventDefault();
        let activeFilter = this.state.activeFilter;
        activeFilter.page++;
        this.setState({ activeFilter: activeFilter }, this.getPagingCategories());
    }
    nextPageInactive = (e) => {
        e.preventDefault();
        let inactiveFilter = this.state.inactiveFilter;
        inactiveFilter.page++;
        this.setState({ inactiveFilter: inactiveFilter }, this.getPagingCategories());
    }
    prevPageActive = (e) => {
        e.preventDefault();
        let activeFilter = this.state.activeFilter;
        activeFilter.page--;
        this.setState({ activeFilter: activeFilter }, this.getPagingCategories());
    }
    prevPageInactive = (e) => {
        e.preventDefault();
        let inactiveFilter = this.state.inactiveFilter;
        inactiveFilter.page--;
        this.setState({ inactiveFilter: inactiveFilter }, this.getPagingCategories());
    }

    inactiveCategories = () => {
            return (<Row className="categories__inactive">
                <Col lg="9" md="8" sm="12">
                {this.props.pagingInactiveCategories && this.props.pagingInactiveCategories.items && this.props.pagingInactiveCategories.items.length > 0
                    ? (<Card className="categories__inactive__card">
                        <CardHeader className="categories__inactive__card__header">
                            <a aria-expanded={this.state.openInactives} 
                                onClick={(e) => {e.preventDefault(); this.setState({
                                    openInactives: !this.state.openInactives
                                })}}>
                                    <h4 className="mb-3">
                                        CATEGORIAS INATIVAS
                                        <i className={`float-right nc-icon nc-minimal-${this.state.openInactives ? 'up' : 'down'}`} />
                                    </h4>
                                    <hr/>
                            </a>
                        </CardHeader>
                        <Collapse
                            role="tabpanel"
                            isOpen={this.state.openInactives}>
                            <CardBody>
                                <Row>
                                    <Col xs={12} md={12} >
                                        <FormGroup className="has-label">
                                            <label htmlFor="searchInactive">Nome</label>
                                            <InputGroup>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                    <i className="fas fa-search" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input 
                                                    id="searchInactive"
                                                    name="searchInactive"
                                                    type="text"
                                                    placeholder="Digite pelo menos três caracteres..."
                                                    value={this.state.inactiveFilter.search}
                                                    onChange={this.bindInactiveSearch}
                                                />
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Table responsive>
                                            <thead>
                                                {this.titlesInactive()}
                                            </thead>
                                            <tbody>
                                                {this.props.pagingInactiveCategories.items.map((item,index) => this.registerInactive(item,index))}                                      
                                            </tbody>
                                        </Table>
                                        <Pagination listClassName="mt-3 justify-content-end">
                                             {this.props.pagingInactiveCategories
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-list"></i>{"   "} Total: {this.props.pagingInactiveCategories.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}

                                            {this.state.inactiveFilter.page > 1
                                                ?(<PaginationItem >
                                                    <PaginationLink href="javascript:void(0)" onClick={this.prevPageInactive}>
                                                        <i className="fas fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)

                                                :(<PaginationItem disabled>
                                                <PaginationLink href="#">
                                                    <i className="fas fa-arrow-left"></i>
                                                </PaginationLink>
                                            </PaginationItem>)}

                                            {this.props.pagingInactiveCategories.hasNextPage
                                                ?(<PaginationItem >
                                                    <PaginationLink href="javascript:void(0)" onClick={this.nextPageInactive}>
                                                        <i className="fas fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)

                                                :(<PaginationItem disabled>
                                                <PaginationLink href="#">
                                                    <i className="fas fa-arrow-right"></i>
                                                </PaginationLink>
                                            </PaginationItem>)}                                    
                                        </Pagination>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Collapse>
                    </Card>) : null}
                </Col>
                {this.props.pagingInactiveCategories && this.props.pagingInactiveCategories.items && this.props.pagingInactiveCategories.items.length > 0
                    || this.props.pagingActiveCategories && this.props.pagingActiveCategories.items && this.props.pagingActiveCategories.items.length > 0
                    ? (<Col lg="3" md="4"  >
                    <Card>
                        <CardHeader className="categories__inactive__card__header">
                            <h4 className="mb-3 text-center">
                                <i className="fas fa-exclamation-triangle" ></i>{"  "}
                                Importante!
                            </h4>
                        </CardHeader>
                        <CardBody>
                            <p className="text-center">Após inativar uma categoria não será possível criar uma ideia para a mesma, mas as ideias existentes continuam valendo!<br/><br/> <span className="categories__inactive__card__broh">;-)</span></p>
                        </CardBody>
                    </Card>
                </Col>)
                    : null
                }
            </Row>)
    }

    add = (e) => {
        e.preventDefault();
        this.props.history.push("/admin/categorias/criar");
    }

    toggleConfirm = () => this.setState({openModal: !this.state.openModal});
    modal = () => ( <Modal isOpen={this.state.openModal} toggle={this.toggleConfirm}>
        <ModalHeader>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.toggleConfirm}>
                <span aria-hidden="true">×</span>
            </button>
            <h5 className="modal-title">DELETAR CATEGORIA</h5>
        </ModalHeader>
        <ModalBody>
            <p>Tem certeza que deseja excluir o registro?</p>
            <small className="danger">Essa ação não poderá ser revertida.</small>
        </ModalBody>
        <ModalFooter>
            <Button color="secondary" onClick={this.toggleConfirm}>
                Cancelar
            </Button>
            <Button onClick={this.state.confirmCallBack} color="danger">
                Deletar
            </Button>
        </ModalFooter>
    </Modal>)

    render() {
        return (
            <div className="content categories">
                <Row>
                    <Col className="text-right ">
                        <Button className="mb-3 col-xs-12 col-md-12 categories__create" onClick={this.add} color="primary">
                            Criar Nova Categoria
                        </Button>
                    </Col>
                </Row>
                {this.activeCategories()}
                {this.inactiveCategories()}    
                {this.state.alert}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { 
        categories: state.categories.list,
        pagingActiveCategories : state.categories.pagingActiveCategories,
        pagingInactiveCategories : state.categories.pagingInactiveCategories
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({ ...categoryActions, ...messageActions }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Categories));
  