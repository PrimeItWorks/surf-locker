import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as ideaActions from '../../redux/actions/ideas'
import './ideas.scss'
import ToApprove from "./_toApprove";
import Approved from "./_approved";
import Active from "./_active";
import Disapproved from "./_disapproved";
import { Row, Col } from "reactstrap";

class Ideas extends React.Component {
    componentWillUnmount() {
        window.scrollTo(0, 0);
    }
    
    render = () => {
        return (
            <div className="content ideas">
                <Row className="ideas__toApprove">
                    <Col md="12">
                        <ToApprove {...this.props} />
                    </Col>
                </Row>
                <Row className="ideas__feed">
                    <Col md="12">
                        <Active {...this.props} />
                    </Col>
                </Row>
                <Row>
                    <Col md="6" className="ideas__approved">
                        <Approved {...this.props} />
                    </Col>
                    <Col md="6" className="ideas__disapproved">
                        <Disapproved {...this.props} />
                    </Col>
                </Row>
            </div>
        );
    } 
}


function mapStateToProps(state) {
    return { 
        pagingActiveIdeas: state.ideas.pagingActiveIdeas,
        pagingToApproveIdeas: state.ideas.pagingToApproveIdeas,
        pagingApprovedIdeas: state.ideas.pagingApprovedIdeas,
        pagingDisapprovedIdeas: state.ideas.pagingDisapprovedIdeas,
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators(ideaActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Ideas));
  