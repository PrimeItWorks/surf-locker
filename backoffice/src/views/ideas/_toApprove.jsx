import React from "react";
import { withRouter} from 'react-router-dom';
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
    Col,
    Card,
    CardHeader,
    CardBody,
    Table,
    Button,
    Row,
    UncontrolledTooltip,
    Collapse,
    Pagination,
    PaginationItem,
    PaginationLink,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    FormGroup
} from "reactstrap";
import { getStage } from "./functions";

class ToApprove extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: true,
            alert:null,
            defaultFilter: {
                page:1,
                pageSize: 4,
                orderBy: 'Created',
                onlyActive: true,
                desc: false
            },
            toApproveFilter: {
                page:1,
                search: '',
                pageSize: 4,
                orderBy: 'Created',
                orderByDir: {
                    title: 'sort',
                    points: 'sort',
                    investment: 'sort',
                    comments: 'sort',
                    likes: 'sort'
                },
                onlyActive: true,
                desc: false
            },
            approvation : {
                justify: '',
                ideaId: 0
            }
        };

        this.props.getToApprovePaging(this.state.activeFilter);
    }

    orderToApprove = (field) => {
        let toApproveFilter = this.state.toApproveFilter;

        if (!field)
            toApproveFilter = this.state.defaultFilter;

        if(toApproveFilter.orderByDir[field] === 'sort')
            toApproveFilter.orderByDir[field] = 'sort-up'; 
        else if(toApproveFilter.orderByDir[field] === 'sort-up')
            toApproveFilter.orderByDir[field] = 'sort-down';
        else
            toApproveFilter.orderByDir[field] = 'sort';

        if (toApproveFilter.orderByDir[field] ==='sort-down')
            toApproveFilter.desc = true;
        else if (toApproveFilter.orderByDir[field] ==='sort-up')
            toApproveFilter.desc = false;
        else {
            toApproveFilter.desc = this.state.defaultFilter.desc;
            toApproveFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({toApproveFilter:toApproveFilter});
            this.props.getToApprovePaging(toApproveFilter);
            return;
        }

        switch (field) {
            case 'title':
                toApproveFilter.orderBy = 'Title';
                break;

            case 'points':
                toApproveFilter.orderBy = 'Points';
                break;

            case 'investment':
                toApproveFilter.orderBy = 'InvestmentCount';
                break;

            case 'comments':
                toApproveFilter.orderBy = 'CommentCount';
                break;

            case 'likes':
                toApproveFilter.orderBy = 'Likes';
                break;

            default:
                toApproveFilter.orderBy = this.state.defaultFilter.orderBy;
        }
        
        this.setState({toApproveFilter:toApproveFilter});
        this.props.getToApprovePaging(toApproveFilter);
    }

    toApproveTitles = () => {
       return ( <tr>
            <th className="text-center" />
            <th className="clickable idea-box" onClick={(e) => {e.preventDefault(); this.orderToApprove('title')}} ><i className={`fa fa-${this.state.toApproveFilter.orderByDir.title}`}></i>{'  '}Ideia</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderToApprove('points')}} ><i className={`fa fa-${this.state.toApproveFilter.orderByDir.points}`}></i>{'  '}Horas de cultivo</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderToApprove('investment')}} ><i className={`fa fa-${this.state.toApproveFilter.orderByDir.investment}`}></i>{'  '}Fertilizantes</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderToApprove('comments')}} ><i className={`fa fa-${this.state.toApproveFilter.orderByDir.comments}`}></i>{'  '}Defensivos</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderToApprove('likes')}} ><i className={`fa fa-${this.state.toApproveFilter.orderByDir.likes}`}></i>{'  '}Irrigações</th>
            <th className="text-right" >Ações</th>
        </tr>)
    }

    toApproveRegister = (idea,index) => {
        return ( <tr key={index}>
            <td>
                <div className="img-container">
                    <img alt="..." src={idea.image} />
                </div>
            </td>
            <td className="td-name">
                <small>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <a href="javascript:void(0)" value={idea.id} onClick={this.goToDetail}>
                            {idea.title}
                        </a>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                            <small>
                                <span id={`user-toApprove-${index}`} className="tag">{`${idea.author.firstName} ${idea.author.lastName}`}</span>{" "}
                                <UncontrolledTooltip placement="right" target={`user-toApprove-${index}`}  >
                                    <div className="img-container"  >
                                        <img alt={`${idea.author.firstName} ${idea.author.lastName}`} src={idea.author.image} />
                                    </div>
                                </UncontrolledTooltip>
                                <span  className="tag">{idea.categoryDescription}</span>{" "}
                                {getStage(idea.stage)}
                            </small>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <small>{idea.description.length > 70 ? `${idea.description.substring(0,67)}...` : idea.description}</small>
                    </Col>
                </small>
            </td>
            <td className="td-number">
                {idea.points}
            </td>
            <td className="td-number">
                {idea.investmentCount}
            </td>
            <td className="td-number">
                {idea.commentCount}
            </td>
            <td className="td-number">
                {idea.likes}
            </td>
            <td className="text-right">
                <Button value={idea.id} onClick={this.approve} id={`approve-${index}`} className="btn-icon mb-1" color="success" >
                    <i  value={idea.id}  className="fa fa-thumbs-o-up"></i>
                </Button>
                <br />
                <Button className="btn-icon" color="danger" id={`disapprove-${index}`} value={idea.id} onClick={this.disapprove}  >
                    <i  value={idea.id}  className="fa fa-thumbs-o-down"></i>
                </Button>

                <UncontrolledTooltip placement="bottom" target={`approve-${index}`} delay={0}>
                    Aprovar a ideia
                </UncontrolledTooltip>
                <UncontrolledTooltip placement="bottom" target={`disapprove-${index}`} delay={0}>
                    Reprovar a ideia
                </UncontrolledTooltip>
            </td>
         </tr>)
    }

    goToDetail = (e) => {
        e.preventDefault();
        let id = e.target.attributes['value'].value;
        let item = this.props.pagingToApproveIdeas.items.find(c => c.id == id);
        if(item){
            this.props.setIdeaToDetail(item);
            this.props.history.push(`/admin/ideias/detalhe/${item.title}`)
        }
    }   

    approve = (e) => {
        e.preventDefault();

        let approvation = this.state.approvation;
        approvation.ideaId = e.target.attributes['value'].value;
        approvation.justify = '';
        this.setState({
            approvation: approvation,
            alert: (
                <ReactBSAlert
                    input
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Aprovar ideia"
                    onConfirm={e => this.approveConfirm(e) }
                    onCancel={() => this.cancelApprove()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    validationMsg="Você deve justificar a aprovação desta ideia!"
                    showCancel
                    >
                    <div className="ideas__approveModal">
                        <span className="ideas__approveModal__alert">
                            Essa ação não poderá ser revertida.
                        </span>
                    </div>
                </ReactBSAlert>
            )
          });
    }

    approveConfirm = e => {
        let approvation = this.state.approvation;
        approvation.justify = e;

        this.props.setApprovedIdea(approvation, this.state.defaultFilter);
        this.setState({
            approvation : {
                justify: '',
                ideaId: 0
            },
            alert: null
        });
    };

    cancelApprove = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Aprovar ideia"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
              validationMsg="Você deve justificar a aprovação desta ideia!"
            >
              A ideia ainda não foi aprovada :(
            </ReactBSAlert>
          )
        });
    };

      
    disapproveConfirm = e => {
        let approvation = this.state.approvation;
        approvation.justify = e;

        this.props.setDisapprovedIdea(approvation, this.state.defaultFilter);
        this.setState({
            approvation : {
                justify: '',
                ideaId: 0
            },
            alert: null
        });
    };

    cancelDispprove = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Reprovar ideia"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
              validationMsg="Você deve justificar a reprovação desta ideia!"
            >
              A ideia ainda não foi reprovada :)
            </ReactBSAlert>
          )
        });
      };

    hideAlert = () => {
        this.setState({
          alert: null
        });
    };

    disapprove = (e) => {
        e.preventDefault();

        let approvation = this.state.approvation;
        approvation.ideaId = e.target.attributes['value'].value;
        approvation.justify = '';
        this.setState({
            approvation: approvation,
            alert: (
                <ReactBSAlert
                    input
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Reprovar ideia"
                    onConfirm={e => this.disapproveConfirm(e) }
                    onCancel={() => this.cancelDispprove()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    validationMsg="Você deve justificar a reprovação desta ideia!"
                    showCancel
                    >
                    <div className="ideas__approveModal">
                        <span className="ideas__approveModal__alert">
                            Essa ação não poderá ser revertida.
                        </span>
                    </div>
                </ReactBSAlert>
            )
          });
    }

    nextPageToApprove = (e) => {
        e.preventDefault();
        let toApproveFilter = this.state.toApproveFilter;
        toApproveFilter.page++;
        this.setState({ toApproveFilter: toApproveFilter });
        this.props.getToApprovePaging(toApproveFilter);
    }

    prevPageToApprove = (e) => {
        e.preventDefault();
        let toApproveFilter = this.state.toApproveFilter;
        toApproveFilter.page--;
        this.setState({ toApproveFilter: toApproveFilter });
        this.props.getToApprovePaging(toApproveFilter);
    }


    bindToApproveSearch = (e) => {
        e.preventDefault();

        var toApproveFilter = this.state.toApproveFilter;
        toApproveFilter.search = e.target.value;
        toApproveFilter.page = 1;
        
        this.setState({toApproveFilter : toApproveFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getToApprovePaging(toApproveFilter);
        }
    }

    render = () => {
        if (this.props.pagingToApproveIdeas && this.props.pagingToApproveIdeas.items && this.props.pagingToApproveIdeas.items.length > 0) {
            return (<>
                
                    <Card className="ideas__toApprove__card">
                        <CardHeader className="ideas__toApprove__card__header">
                            <a aria-expanded={this.state.open}
                                data-toggle="collapse"
                                onClick={(e) => {
                                        e.preventDefault(); 
                                        this.setState({ open: !this.state.open})}}
                                >
                                <h4 className="mb-3">
                                    IDEIAS AGUARDANDO APROVAÇÃO
                                    <i className={`float-right nc-icon nc-minimal-${this.state.open ? 'up' : 'down'}`} />
                                </h4>
                                <hr/>
                            </a>
                        </CardHeader>
                        <Collapse
                            role="tabpanel"
                            isOpen={this.state.open}
                            >
                            <CardBody>
                                <Row>
                                    <Col xs={12} md={12} >
                                        <FormGroup className="has-label">
                                            <label htmlFor="searchInvestment">Busca</label>
                                            <InputGroup>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="fa fa-search" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input 
                                                    id="searchInvestment"
                                                    name="searchInvestment"
                                                    type="text"
                                                    placeholder="Digite pelo menos três caracteres..."
                                                    value={this.state.toApproveFilter.search}
                                                    onChange={this.bindToApproveSearch}
                                                />
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Table className="table-shopping" responsive>
                                    <thead>
                                        {this.toApproveTitles()}
                                    </thead>
                                    <tbody>
                                        {this.props.pagingToApproveIdeas.items.map((idea,index) => this.toApproveRegister(idea,index))}
                                    </tbody>
                                </Table>
                                <Row>
                                    <Col>
                                        <Pagination listClassName="mt-3 justify-content-end">
                                            {this.props.pagingToApproveIdeas
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fa fa-list"></i>{"   "} Total: {this.props.pagingToApproveIdeas.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}
                
                                            {this.state.toApproveFilter.page > 1
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.prevPageToApprove}>
                                                            <i className="fa fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                            {this.props.pagingToApproveIdeas 
                                                && this.props.pagingToApproveIdeas.hasNextPage
                                                
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.nextPageToApprove}>
                                                            <i className="fa fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                        </Pagination>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Collapse>
                    </Card>
                {this.state.alert}
            </>);
        }

        return null;
    }
}

export default withRouter(ToApprove)