import React from "react";
import { withRouter} from 'react-router-dom';

import ReactBSAlert from "react-bootstrap-sweetalert";

import {
    Col,
    Card,
    CardHeader,
    CardBody,
    Table,
    Button,
    Row,
    UncontrolledTooltip,
    Collapse,
    Pagination,
    PaginationItem,
    PaginationLink,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    FormGroup
} from "reactstrap";

import { getStage } from "./functions";

class Active extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: true,
            alert:null,
            defaultFilter: {
                page:1,
                pageSize: 4,
                orderBy: 'Created',
                onlyActive: true,
                desc: false
            },
            activeFilter: {
                page:1,
                search: '',
                pageSize: 4,
                orderBy: 'Created',
                orderByDir: {
                    title: 'sort',
                    points: 'sort',
                    investment: 'sort',
                    comments: 'sort',
                    likes: 'sort'
                },
                onlyActive: true,
                desc: false
            },
            approvation : {
                justify: '',
                ideaId: 0
            }
        };

        this.props.getActivePaging(this.state.activeFilter);
    }

    orderActive = (field) => {
        let activeFilter = this.state.activeFilter;

        if (!field)
            activeFilter = this.state.defaultFilter;

        if(activeFilter.orderByDir[field] === 'sort')
            activeFilter.orderByDir[field] = 'sort-up'; 
        else if(activeFilter.orderByDir[field] === 'sort-up')
            activeFilter.orderByDir[field] = 'sort-down';
        else
            activeFilter.orderByDir[field] = 'sort';

        if (activeFilter.orderByDir[field] ==='sort-down')
            activeFilter.desc = true;
        else if (activeFilter.orderByDir[field] ==='sort-up')
            activeFilter.desc = false;
        else {
            activeFilter.desc = this.state.defaultFilter.desc;
            activeFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({activeFilter:activeFilter});
            this.props.getActivePaging(activeFilter);
            return;
        }

        switch (field) {
            case 'title':
                activeFilter.orderBy = 'Title';
                break;

            case 'points':
                activeFilter.orderBy = 'Points';
                break;

            case 'investment':
                activeFilter.orderBy = 'InvestmentCount';
                break;

            case 'comments':
                activeFilter.orderBy = 'CommentCount';
                break;

            case 'likes':
                activeFilter.orderBy = 'Likes';
                break;

            default:
                activeFilter.orderBy = this.state.defaultFilter.orderBy;
        }
        
        this.setState({activeFilter:activeFilter});
        this.props.getActivePaging(activeFilter);
    }
    activeTitles = () => {
       return ( <tr>
            <th className="text-center" />
            <th className="clickable idea-box" onClick={(e) => {e.preventDefault(); this.orderActive('title')}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.title}`}></i>{'  '}Ideia</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderActive('points')}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.points}`}></i>{'  '}Horas de cultivo</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderActive('investment')}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.investment}`}></i>{'  '}Fertilizantes</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderActive('comments')}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.comments}`}></i>{'  '}Defensivos</th>
            <th className="clickable text-right" onClick={(e) => {e.preventDefault(); this.orderActive('likes')}} ><i className={`fa fa-${this.state.activeFilter.orderByDir.likes}`}></i>{'  '}Irrigações</th>
            <th className="text-right" >Ações</th>
        </tr>)
    }

    activeRegister = (idea,index) => {
        return ( <tr key={index}>
            <td>
                <div className="img-container">
                    <img alt="..." src={idea.image} />
                </div>
            </td>
            <td className="td-name">
                <small>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <a href="javascript:void(0)" value={idea.id} onClick={this.goToDetail}>
                            {idea.title}
                        </a>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                            <small>
                                <span id={`user-active-${index}`} className="tag">{`${idea.author.firstName} ${idea.author.lastName}`}</span>{" "}
                                <UncontrolledTooltip placement="right" target={`user-active-${index}`}  >
                                    <div className="img-container"  >
                                        <img alt={`${idea.author.firstName} ${idea.author.lastName}`} src={idea.author.image} />
                                    </div>
                                </UncontrolledTooltip>
                                <span  className="tag">{idea.categoryDescription}</span>{" "}
                                {getStage(idea.stage)}
                            </small>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <small>{idea.description.length > 70 ? `${idea.description.substring(0,67)}...` : idea.description}</small>
                    </Col>
                </small>
            </td>
            <td className="td-number">
                {idea.points}
            </td>
            <td className="td-number">
                {idea.investmentCount}
            </td>
            <td className="td-number">
                {idea.commentCount}
            </td>
            <td className="td-number">
                {idea.likes}
            </td>
            <td className="text-right">
                <Button className="btn-icon" color="danger" id={`disapproveActive-${index}`} value={idea.id} onClick={this.disapprove}  >
                    <i value={idea.id}  className="fa fa-thumbs-o-down"></i>
                </Button>
                <UncontrolledTooltip placement="bottom" target={`disapproveActive-${index}`} delay={0}>
                    Reprovar a ideia
                </UncontrolledTooltip>
            </td>
         </tr>)
    }

    goToDetail = (e) => {
        e.preventDefault();
        let id = e.target.attributes['value'].value;
        let item = this.props.pagingActiveIdeas.items.find(c => c.id == id);
        if(item){
            this.props.setIdeaToDetail(item);
            this.props.history.push(`/admin/ideias/detalhe/${item.title}`)
        }
    }   

    disapproveConfirm = e => {
        let approvation = this.state.approvation;
        approvation.justify = e;

        this.props.setDisapprovedIdea(approvation, this.state.defaultFilter);
        this.setState({
            approvation : {
                justify: '',
                ideaId: 0
            },
            alert: null
        });
    };

    cancelDispprove = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Reprovar ideia"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
              validationMsg="Você deve justificar a reprovação desta ideia!"
            >
              A ideia ainda não foi reprovada :)
            </ReactBSAlert>
          )
        });
      };

    hideAlert = () => {
        this.setState({
          alert: null
        });
    };

    disapprove = (e) => {
        e.preventDefault();
        
        let approvation = this.state.approvation;
        approvation.ideaId = e.target.attributes['value'].value;
        approvation.justify = '';
        this.setState({
            approvation: approvation,
            alert: (
                <ReactBSAlert
                    input
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Reprovar ideia"
                    onConfirm={e => this.disapproveConfirm(e) }
                    onCancel={() => this.cancelDispprove()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    validationMsg="Você deve justificar a reprovação desta ideia!"
                    showCancel
                    >
                    <div className="ideas__approveModal">
                        <span className="ideas__approveModal__alert">
                            Essa ação não poderá ser revertida.
                        </span>
                    </div>
                </ReactBSAlert>
            )
          });
    }

    nextPageActive = (e) => {
        e.preventDefault();
        let activeFilter = this.state.activeFilter;
        activeFilter.page++;
        this.setState({ activeFilter: activeFilter });
        this.props.getActivePaging(activeFilter);
    }

    prevPageActive = (e) => {
        e.preventDefault();
        let activeFilter = this.state.activeFilter;
        activeFilter.page--;
        this.setState({ activeFilter: activeFilter });
        this.props.getActivePaging(activeFilter);
    }

    bindActiveSearch = (e) => {
        e.preventDefault();

        var activeFilter = this.state.activeFilter;
        activeFilter.search = e.target.value;
        activeFilter.page = 1;
        
        this.setState({activeFilter : activeFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getActivePaging(activeFilter);
        }
    }

    render = () => {
        if (this.props.pagingActiveIdeas && this.props.pagingActiveIdeas.items && this.props.pagingActiveIdeas.items.length > 0)
            return (
                <>
                    <Card className="ideas__feed__card">
                        <CardHeader className="ideas__feed__card__header">
                            <a aria-expanded={this.state.open}
                                data-toggle="collapse"
                                onClick={(e) => {
                                        e.preventDefault(); 
                                        this.setState({ open: !this.state.open})}}
                                >
                                <h4 className="mb-3">
                                    IDEIAS ATIVAS NO FEED
                                    <i className={`float-right nc-icon nc-minimal-${this.state.open ? 'up' : 'down'}`} />
                                </h4>
                                <hr/>
                            </a>
                        </CardHeader>
                        <Collapse
                            role="tabpanel"
                            isOpen={this.state.open}
                            >
                            <CardBody>
                                <Row>
                                    <Col xs={12} md={12} >
                                        <FormGroup className="has-label">
                                            <label htmlFor="searchInvestment">Busca</label>
                                            <InputGroup>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="fa fa-search" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input 
                                                    id="searchInvestment"
                                                    name="searchInvestment"
                                                    type="text"
                                                    placeholder="Digite pelo menos três caracteres..."
                                                    value={this.state.activeFilter.search}
                                                    onChange={this.bindActiveSearch}
                                                />
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Table className="table-shopping" responsive>
                                    <thead>
                                        {this.activeTitles()}
                                    </thead>
                                    <tbody>
                                        {this.props.pagingActiveIdeas.items.map((idea,index) => this.activeRegister(idea,index))}
                                    </tbody>
                                </Table>
                                <Row>
                                    <Col>
                                        <Pagination listClassName="mt-3 justify-content-end">
                                            {this.props.pagingActiveIdeas
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fa fa-list"></i>{"   "} Total: {this.props.pagingActiveIdeas.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}
                
                                            {this.state.activeFilter.page > 1
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.prevPageActive}>
                                                            <i className="fa fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                            {this.props.pagingActiveIdeas 
                                                && this.props.pagingActiveIdeas.hasNextPage
                                                
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.nextPageActive}>
                                                            <i className="fa fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                        </Pagination>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Collapse>
                    </Card>
                    {this.state.alert}
                </>);

        return null;
    }    
}

export default withRouter(Active)