import React from "react";

export function getStage(stage) {
    switch (stage) {
        case 1:
            return (<span className="tag">Semeou</span>)
        case 2:
            return (<span className="tag">Brotou</span>)
        case 3:
            return (<span className="tag">Floresceu</span>)
        case 4:
            return (<span className="tag">Frutificou</span>)
        default:
            return (<span className="tag">Semeou</span>)
    }
}

export function datediff(first, second) {
    // Take the difference between the dates and divide by milliseconds per day.
    // Round to nearest whole number to deal with DST.
    return Math.round((second-first)/(1000*60*60*24));
}