import React from "react";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as ideaActions from '../../redux/actions/ideas'
import './ideas.scss'
import ReactBSAlert from "react-bootstrap-sweetalert";
import {
    Col,
    Card,
    CardHeader,
    CardBody,
    Button,
    Row
} from "reactstrap";

import stage1 from '../../assets/img/img-painel-status-desk-1.png'
import stage2 from '../../assets/img/img-painel-status-desk-2.png'
import stage3 from '../../assets/img/img-painel-status-desk-3.png'
import stage4 from '../../assets/img/img-painel-status-desk-4.png'
import stageNone from '../../assets/img/img-painel-status-desk-5.png'
import no_avatar from '../../assets/img/no_avatar.png'

class IdeasDetail extends React.Component {
    state = {
        limit: 3,
        loadMorequantity: 3,
        approvation: {
            ideaId: 0,
            justify: '',
            approvation:false
        },
        alert : null
    }

    constructor(props){
        super(props)
    }

    componentWillUnmount() {
        window.scrollTo(0, 0);
    }

    approve = (e) => {
        e.preventDefault();

        let approvation = this.state.approvation;
        approvation.ideaId = e.target.value;
        approvation.justify = '';
        this.setState({
            approvation: approvation,
            alert: (
                <ReactBSAlert
                    input
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Aprovar ideia"
                    onConfirm={e => this.approveConfirm(e) }
                    onCancel={() => this.cancelApprove()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    validationMsg="Você deve justificar a aprovação desta ideia!"
                    showCancel
                    >
                    <div className="ideas__approveModal">
                        <span className="ideas__approveModal__alert">
                            Essa ação não poderá ser revertida.
                        </span>
                    </div>
                </ReactBSAlert>
            )
          });
    }

    approveConfirm = e => {
        let approvation = this.state.approvation;
        approvation.justify = e;

        this.props.setApprovedIdea(approvation, this.state.defaultFilter);
        this.setState({
            approvation : {
                justify: '',
                ideaId: 0
            },
            alert: null
        });
        this.props.history.push(`/admin/ideias`);
    };

    cancelApprove = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Aprovar ideia"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
              validationMsg="Você deve justificar a aprovação desta ideia!"
            >
              A ideia ainda não foi aprovada :(
            </ReactBSAlert>
          )
        });
    };

      
    disapproveConfirm = e => {
        let approvation = this.state.approvation;
        approvation.justify = e;

        this.props.setDisapprovedIdea(approvation, this.state.defaultFilter);
        this.setState({
            approvation : {
                justify: '',
                ideaId: 0
            },
            alert: null
        });
        
        this.props.history.push(`/admin/ideias`);
    };

    cancel = (e) => {
        e.preventDefault();
        this.props.history.push(`/admin/ideias`);
    }

    cancelDispprove = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Reprovar ideia"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
              validationMsg="Você deve justificar a reprovação desta ideia!"
            >
              A ideia ainda não foi reprovada :)
            </ReactBSAlert>
          )
        });
      };

      hideAlert = () => {
        this.setState({
          alert: null
        });
    };

    disapprove = (e) => {
        e.preventDefault();

        let approvation = this.state.approvation;
        approvation.ideaId = e.target.value;
        approvation.justify = '';
        this.setState({
            approvation: approvation,
            alert: (
                <ReactBSAlert
                    input
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Reprovar ideia"
                    onConfirm={e => this.disapproveConfirm(e) }
                    onCancel={() => this.cancelDispprove()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    validationMsg="Você deve justificar a reprovação desta ideia!"
                    showCancel
                    >
                    <div className="ideas__approveModal">
                        <span className="ideas__approveModal__alert">
                            Essa ação não poderá ser revertida.
                        </span>
                    </div>
                </ReactBSAlert>
            )
          });
    }

    stageImg() {
        switch (this.props.idea.stage) {
            case 1:
                return (<img className="ideas__detail__figure__caption__stage__img" 
                            src={stage1} 
                            alt="Semeou" />)
            case 2:
                return (<img className="ideas__detail__figure__caption__stage__img" 
                            src={stage2} 
                            alt="Brotou" />)
            case 3:
                return (<img className="ideas__detail__figure__caption__stage__img" 
                            src={stage3} 
                            alt="Floreceu" />)
            case 4:
                return (<img className="ideas__detail__figure__caption__stage__img" 
                            src={stage4} 
                            alt="Frutificou" />)
            default:
                return (<img className="ideas__detail__figure__caption__stage__img" 
                                src={stageNone} 
                                alt="Frutificou" />)
        }
    }

    comment = (comment) => (
        <div key={comment.id}>
            <div className="message parent">
                <Row>
                    <Col xs={3} sm={3} md={3}>
                        <img src={comment.authorImage ? comment.authorImage : no_avatar} className="img-responsive img-circle" />
                    </Col>
                    <Col xs={9} sm={9} md={9}>
                        <p style={
                            comment.authorId == this.props.idea.authorId 
                            ? { backgroundColor: '#0088FF', color: "#FFF" }
                            : {}
                        }>
                            <strong style={
                                comment.authorId == this.props.idea.authorId 
                                    ? { color: "#FFF" }
                                    : {}}>
                                {comment.authorName}
                            </strong>{' '}
                            {comment.description}
                        </p>
                        <span className="reply">
                            {comment.createdFormat} 
                        </span>
                    </Col>
                </Row>
                <div className="clearfix"></div>
            </div>
            {comment.comments.map((reply) => 
                this.commentReply(reply)
            )}
        </div>
    );

    commentReply = (reply) => (
        <div key={reply.id} className="message">
            <Col xs={12} sm={12} md={12}>
                <Row>
                    <Col xs={3} sm={3} md={3}>
                        <img src={reply.authorImage ? reply.authorImage : no_avatar} className="img-responsive img-circle" />
                    </Col>
                    <Col xs={9} sm={9} md={9}>
                        <p style={
                            reply.authorId == this.props.idea.authorId 
                                ? { backgroundColor: '#0088FF', color: "#FFF" }
                                : {}
                        }>
                            <strong style={
                                reply.authorId == this.props.idea.authorId 
                                    ? { color: "#FFF" }
                                    : {}}>
                                {reply.authorName}
                            </strong>{'  '}
                            {reply.description}
                        </p>
                        <span className="reply">
                            {reply.createdFormat} 
                        </span>
                    </Col>
                </Row>
                <div className="clearfix"></div>
            </Col>
            <div className="clearfix"></div>
        </div>
    );

    loadMoreComments = (e) => this.setState({limit: this.state.limit + this.state.loadMorequantity});

    render = () => {
        if(this.props.idea)
            return (
                <div className="content ideas__detail">
                    <Row className="ideas__detail">
                        <Col md={this.props.idea.comments.length > 0 ? 8 : 12}>
                            <Card className="ideas__detail__card demo-icons" >
                                <CardHeader className="ideas__detail__card__header" >
                                    <a>
                                        <h4 className="mb-3">
                                            <i className="nc-icon nc-bulb-63 ml-2 mr-3" /> {"   "}
                                            {this.props.idea.title}
                                        </h4>
                                        <hr/>
                                    </a>
                                </CardHeader>
                                <CardBody>
                                    <div id="icons-wrapper">
                                        <section>
                                            <ul>
                                                <li>
                                                    <i className="nc-icon nc-money-coins" />
                                                    <p>{this.props.idea.investmentCount}</p>
                                                </li>
                                                <li>
                                                    <i className="nc-icon nc-chat-33" />
                                                    <p>{this.props.idea.commentCount}</p>
                                                </li>
                                                <li>
                                                    <i className="nc-icon nc-favourite-28" />
                                                    <p>{this.props.idea.likes}</p>
                                                </li>
                                            </ul>
                                        </section>
                                    </div>
                                    <figure className="ideas__detail__figure">

                                        <img src={this.props.idea.image} 
                                            className="ideas__detail__figure__img img-responsive" 
                                            alt={this.props.idea.title} />

                                        <figcaption className="ideas__detail__figure__caption">
                                            <p className="ideas__detail__figure__caption__text">{this.props.idea.createdFormat.toUpperCase()}</p>
                                            <div className="ideas__detail__figure__caption__stage">
                                                {this.stageImg()}
                                                <span className="ideas__detail__figure__caption__stage__points">
                                                    {this.props.idea.categoryCoefficient.toFixed(1)}
                                                </span>
                                            </div>
                                        </figcaption>

                                    </figure>
                                    <div className="ideas__detail__content caption ">

                                        <img src={this.props.idea.author.image  ? this.props.idea.author.image : no_avatar} 
                                            className="ideas__detail__content__img img-responsive" 
                                            alt={`${this.props.idea.author.firstName} ${this.props.idea.author.lastName}`} />

                                        <h3 className="ideas__detail__content__title">{`${this.props.idea.author.firstName} ${this.props.idea.author.lastName}`}</h3>
                                        <h5 className="ideas__detail__content__email">{this.props.idea.author.login}</h5>

                                        <p className="ideas__detail__content__text">{this.props.idea.description}</p>
                                        <div className="clearfix"></div>
                                    </div>
                                </CardBody>
                            </Card>
                            <Col md={12}>
                                <div className="text-right">
                                    <Button className="mt-2 col-xs-2 col-md-2" onClick={this.cancel} color="secondary">
                                        <i className="fa fa-hand-peace-o"></i> {"     "}
                                        Cancelar
                                    </Button>

                                    <Button className="ml-md-2 mt-2 col-xs-2 col-md-2"  value={this.props.idea.id} onClick={this.disapprove} color="danger">
                                        <i className="fa fa-thumbs-o-down"></i> {"     "}
                                        Reprovar
                                    </Button>

                                    <Button className="ml-md-2  mt-2 col-xs-2 col-md-2" color="primary"  value={this.props.idea.id} onClick={this.approve}>
                                        <i className="fa fa-thumbs-o-up"></i> {"     "}
                                        Aprovar
                                    </Button>
                                </div>
                            </Col>
                        </Col>
                        {this.props.idea.comments.length > 0 
                            ? <Col md={4}>
                                    <Card>
                                        <CardBody>
                                            <Row>
                                                <Col xs={4}>
                                                    <img src={this.props.idea.author.image  ? this.props.idea.author.image : no_avatar} 
                                                        className="img-responsive" 
                                                        alt={`${this.props.idea.author.firstName} ${this.props.idea.author.lastName}`} />
                                                </Col>
                                                <Col xs={8}>
                                                    <Row>
                                                        <h3 className="author-title">{`${this.props.idea.author.firstName} ${this.props.idea.author.lastName}`}</h3>
                                                    </Row>
                                                    <Row>
                                                        <h5 className="author-email">{this.props.idea.author.login}</h5>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                    <Card  className="ideas__detail__card__header" >
                                        <CardHeader>
                                            <a>
                                                <h4 className="mb-3">
                                                    <i className="nc-icon nc-chat-33 ml-3 mr-3" /> {"   "}
                                                    COMENTÁRIOS
                                                </h4>
                                                <hr/>
                                            </a>
                                        </CardHeader>
                                        <CardBody>
                                            <div className="ideas__detail__card__comment__content">
                                                <div className="ideas__detail__card__comment__content__conversation">
                                                {this.props.idea.comments && this.props.idea.comments.length > 0 
                                                    ? this.props.idea.comments.map((c,i) =>  {
                                                        if (i >= this.state.limit)
                                                            return;

                                                        return this.comment(c)
                                                    })
                                                    : null}
                                                </div>

                                                {this.props.idea.comments && this.props.idea.comments.length > this.state.limit
                                                    ? <div className="message">
                                                        <div className="message__loadmore">
                                                            <a onClick={this.loadMoreComments}>MAIS COMENTÁRIOS</a>
                                                        </div>
                                                    </div>
                                                    : null}
                                            </div>
                                        </CardBody>
                                    </Card>
                                </Col>
                            : null}
                    </Row>
                    {this.state.alert}
                </div>
            );
        
        return null;
    } 
}

function mapStateToProps(state) {
    return { 
        idea : state.ideas.ideaToDetail
    };
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators(ideaActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(IdeasDetail));
  