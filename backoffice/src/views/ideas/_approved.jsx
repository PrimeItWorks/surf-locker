import React from "react";
import {
    Col,
    Card,
    CardHeader,
    CardBody,
    Table,
    Button,
    Row,
    UncontrolledTooltip,
    Modal, 
    Collapse,
    ModalHeader, 
    Pagination,
    PaginationItem,
    PaginationLink,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    FormGroup, 
} from "reactstrap";

import { getStage } from "./functions";

class Approved extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: true,
            defaultFilter: {
                page:1,
                pageSize: 4,
                orderBy: 'Created',
                onlyActive: true,
                desc: false
            },
            approvedFilter: {
                page:1,
                search: '',
                pageSize: 4,
                orderBy: 'Created',
                orderByDir: {
                    title: 'sort',
                    points: 'sort',
                    investment: 'sort',
                    comments: 'sort',
                    likes: 'sort'
                },
                onlyActive: true,
                desc: false
            }
        };

        this.props.getApprovedPaging(this.state.activeFilter);
    }

    orderApproved = (field) => {
        let approvedFilter = this.state.approvedFilter;

        if (!field)
            approvedFilter = this.state.defaultFilter;

        if(approvedFilter.orderByDir[field] === 'sort')
            approvedFilter.orderByDir[field] = 'sort-up'; 
        else if(approvedFilter.orderByDir[field] === 'sort-up')
            approvedFilter.orderByDir[field] = 'sort-down';
        else
            approvedFilter.orderByDir[field] = 'sort';

        if (approvedFilter.orderByDir[field] ==='sort-down')
            approvedFilter.desc = true;
        else if (approvedFilter.orderByDir[field] ==='sort-up')
            approvedFilter.desc = false;
        else {
            approvedFilter.desc = this.state.defaultFilter.desc;
            approvedFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({approvedFilter:approvedFilter});
            this.props.getApprovedPaging(approvedFilter);
            return;
        }

        switch (field) {
            case 'title':
                approvedFilter.orderBy = 'Title';
                break;

            case 'points':
                approvedFilter.orderBy = 'Points';
                break;

            case 'investment':
                approvedFilter.orderBy = 'InvestmentCount';
                break;

            case 'comments':
                approvedFilter.orderBy = 'CommentCount';
                break;

            case 'likes':
                approvedFilter.orderBy = 'Likes';
                break;

            default:
                approvedFilter.orderBy = this.state.defaultFilter.orderBy;
        }
        
        this.setState({approvedFilter:approvedFilter});
        this.props.getApprovedPaging(approvedFilter);
    }

    approvedTitles = () => {
       return ( <tr>
            <th className="text-center" />
            <th className="clickable" onClick={(e) => {e.preventDefault(); this.orderApproved('title')}} ><i className={`fa fa-${this.state.approvedFilter.orderByDir.title}`}></i>{'  '}Ideia</th>
        </tr>)
    }

    approvedRegister = (idea,index) => {
        return ( <tr key={index}>
            <td>
                <div className="img-container">
                    <img alt="..." src={idea.image} />
                </div>
            </td>
            <td className="td-name">
                <small>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <a href="javascript:void(0)" onClick={e => e.preventDefault()}>
                            {idea.title}
                        </a>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                            <small>
                                <span id={`user-approved-${index}`} className="tag">{`${idea.author.firstName} ${idea.author.lastName}`}</span>{" "}
                                <UncontrolledTooltip placement="right" target={`user-approved-${index}`}  >
                                    <div className="img-container"  >
                                        <img alt={`${idea.author.firstName} ${idea.author.lastName}`} src={idea.author.image} />
                                    </div>
                                </UncontrolledTooltip>
                                <span  className="tag">{idea.categoryDescription}</span>{" "}
                                {getStage(idea.stage)}
                            </small>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <small>{idea.description.length > 70 ? `${idea.description.substring(0,67)}...` : idea.description}</small>
                    </Col>
                </small>
            </td>
         </tr>)
    }

    nextPageApproved = (e) => {
        e.preventDefault();
        let approvedFilter = this.state.approvedFilter;
        approvedFilter.page++;
        this.setState({ approvedFilter: approvedFilter });
        this.props.getApprovedPaging(approvedFilter);
    }

    prevPageApproved = (e) => {
        e.preventDefault();
        let approvedFilter = this.state.approvedFilter;
        approvedFilter.page--;
        this.setState({ approvedFilter: approvedFilter });
        this.props.getApprovedPaging(approvedFilter);
    }

    bindApprovedSearch = (e) => {
        e.preventDefault();

        var approvedFilter = this.state.approvedFilter;
        approvedFilter.search = e.target.value;
        approvedFilter.page = 1;
        
        this.setState({approvedFilter : approvedFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getApprovedPaging(approvedFilter);
        }
    }

    render = () => {
        if (this.props.pagingApprovedIdeas && this.props.pagingApprovedIdeas.items && this.props.pagingApprovedIdeas.items.length > 0)
            return (
                <>
                    <Card className="ideas__approved__card">
                        <CardHeader className="ideas__approved__card__header">
                            <a aria-expanded={this.state.open}
                                data-toggle="collapse"
                                onClick={(e) => {
                                        e.preventDefault(); 
                                        this.setState({ open: !this.state.open})}}
                                >
                                <h4 className="mb-3">
                                    IDEIAS APROVADAS
                                    <i className={`float-right nc-icon nc-minimal-${this.state.open ? 'up' : 'down'}`} />
                                </h4>
                                <hr/>
                            </a>
                        </CardHeader>
                        <Collapse
                            role="tabpanel"
                            isOpen={this.state.open}
                            >
                            <CardBody>
                                <Row>
                                    <Col xs={12} md={12} >
                                        <FormGroup className="has-label">
                                            <label htmlFor="searchInvestment">Busca</label>
                                            <InputGroup>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>
                                                        <i className="fa fa-search" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                                <Input 
                                                    id="searchInvestment"
                                                    name="searchInvestment"
                                                    type="text"
                                                    placeholder="Digite pelo menos três caracteres..."
                                                    value={this.state.approvedFilter.search}
                                                    onChange={this.bindApprovedSearch}
                                                />
                                            </InputGroup>
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Table className="table-shopping" responsive>
                                    <thead>
                                        {this.approvedTitles()}
                                    </thead>
                                    <tbody>
                                        {this.props.pagingApprovedIdeas.items.map((idea,index) => this.approvedRegister(idea,index))}
                                    </tbody>
                                </Table>
                                <Row>
                                    <Col>
                                        <Pagination listClassName="mt-3 justify-content-end">
                                            {this.props.pagingApprovedIdeas
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fa fa-list"></i>{"   "} Total: {this.props.pagingApprovedIdeas.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}
                
                                            {this.state.approvedFilter.page > 1
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.prevPageApproved}>
                                                            <i className="fa fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                            {this.props.pagingApprovedIdeas 
                                                && this.props.pagingApprovedIdeas.hasNextPage
                                                
                                                ? (<PaginationItem >
                                                        <PaginationLink href="javascript:void(0)" onClick={this.nextPageApproved}>
                                                            <i className="fa fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)
                
                                                : (<PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)}
                                        </Pagination>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Collapse>
                    </Card>
                </>);

        return null;
    }
}

export default Approved