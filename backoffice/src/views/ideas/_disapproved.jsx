import React from "react";

import {
    Col,
    Card,
    CardHeader,
    CardBody,
    Table,
    Row,
    UncontrolledTooltip,
    Collapse,
    Pagination,
    PaginationItem,
    PaginationLink,
    Input,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    FormGroup, 
} from "reactstrap";

import { getStage } from "./functions";

class Disapproved extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            open: true,
            defaultFilter: {
                page:1,
                pageSize: 4,
                orderBy: 'Created',
                onlyActive: true,
                desc: false
            },
            disapprovedFilter: {
                page:1,
                search: '',
                pageSize: 4,
                orderBy: 'Created',
                orderByDir: {
                    title: 'sort',
                    points: 'sort',
                    investment: 'sort',
                    comments: 'sort',
                    likes: 'sort'
                },
                onlyActive: true,
                desc: false
            }
        };

        this.props.getDisapprovedPaging(this.state.activeFilter);
    }

    orderDisapproved = (field) => {
        let disapprovedFilter = this.state.disapprovedFilter;

        if (!field)
            disapprovedFilter = this.state.defaultFilter;

        if(disapprovedFilter.orderByDir[field] === 'sort')
            disapprovedFilter.orderByDir[field] = 'sort-up'; 
        else if(disapprovedFilter.orderByDir[field] === 'sort-up')
            disapprovedFilter.orderByDir[field] = 'sort-down';
        else
            disapprovedFilter.orderByDir[field] = 'sort';

        if (disapprovedFilter.orderByDir[field] ==='sort-down')
            disapprovedFilter.desc = true;
        else if (disapprovedFilter.orderByDir[field] ==='sort-up')
            disapprovedFilter.desc = false;
        else {
            disapprovedFilter.desc = this.state.defaultFilter.desc;
            disapprovedFilter.orderBy = this.state.defaultFilter.orderBy;
            this.setState({disapprovedFilter:disapprovedFilter});
            this.props.getApprovedPaging(disapprovedFilter);
            return;
        }

        switch (field) {
            case 'title':
                disapprovedFilter.orderBy = 'Title';
                break;

            case 'points':
                disapprovedFilter.orderBy = 'Points';
                break;

            case 'investment':
                disapprovedFilter.orderBy = 'InvestmentCount';
                break;

            case 'comments':
                disapprovedFilter.orderBy = 'CommentCount';
                break;

            case 'likes':
                disapprovedFilter.orderBy = 'Likes';
                break;

            default:
                disapprovedFilter.orderBy = this.state.defaultFilter.orderBy;
        }
        
        this.setState({disapprovedFilter:disapprovedFilter});
        this.props.getDisapprovedPaging(disapprovedFilter);
    }

    disapprovedTitles = () => {
       return ( <tr>
            <th className="text-center" />
            <th className="clickable" onClick={(e) => {e.preventDefault(); this.orderDisapproved('title')}} ><i className={`fa fa-${this.state.disapprovedFilter.orderByDir.title}`}></i>{'  '}Ideia</th>
        </tr>)
    }

    disapprovedRegister = (idea,index) => {
        return ( <tr key={index}>
            <td>
                <div className="img-container">
                    <img alt="..." src={idea.image} />
                </div>
            </td>
            <td className="td-name">
                <small>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <a href="javascript:void(0)" onClick={e => e.preventDefault()}>
                            {idea.title}
                        </a>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                            <small>
                                <span id={`user-disapprove-${index}`} className="tag">{`${idea.author.firstName} ${idea.author.lastName}`}</span>{" "}
                                <UncontrolledTooltip placement="right" target={`user-disapprove-${index}`}  >
                                    <div className="img-container"  >
                                        <img alt={`${idea.author.firstName} ${idea.author.lastName}`} src={idea.author.image} />
                                    </div>
                                </UncontrolledTooltip>
                                <span  className="tag">{idea.categoryDescription}</span>{" "}
                                {getStage(idea.stage)}
                            </small>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <small>{idea.description.length > 70 ? `${idea.description.substring(0,67)}...` : idea.description}</small>
                    </Col>
                </small>
            </td>
         </tr>)
     }

     nextPageDisapproved = (e) => {
        e.preventDefault();
        let disapprovedFilter = this.state.disapprovedFilter;
        disapprovedFilter.page++;
        this.setState({ disapprovedFilter: disapprovedFilter });
        this.props.getDisapprovedPaging(disapprovedFilter);
    }

    prevPageDisapproved = (e) => {
        e.preventDefault();
        let disapprovedFilter = this.state.disapprovedFilter;
        disapprovedFilter.page--;
        this.setState({ disapprovedFilter: disapprovedFilter });
        this.props.getDisapprovedPaging(disapprovedFilter);
    }

    bindDisapprovedSearch = (e) => {
        e.preventDefault();

        var disapprovedFilter = this.state.disapprovedFilter;
        disapprovedFilter.search = e.target.value;
        disapprovedFilter.page = 1;
        
        this.setState({disapprovedFilter : disapprovedFilter });
        if (e.target.value.length > 3 || e.target.value.length == 0) {
            this.props.getDisapprovedPaging(disapprovedFilter);
        }
    }

    render = () => {
        if (this.props.pagingDisapprovedIdeas && this.props.pagingDisapprovedIdeas.items && this.props.pagingDisapprovedIdeas.items.length > 0)
            return (
                <Card className="ideas__disapproved__card">
                    <CardHeader className="ideas__disapproved__card__header">
                        <a aria-expanded={this.state.open}
                            data-toggle="collapse"
                            onClick={(e) => {
                                    e.preventDefault(); 
                                    this.setState({ open: !this.state.open})}}
                            >
                            <h4 className="mb-3">
                                IDEIAS REPROVADAS
                                <i className={`float-right nc-icon nc-minimal-${this.state.open ? 'up' : 'down'}`} />
                            </h4>
                            <hr/>
                        </a>
                    </CardHeader>
                    <Collapse
                        role="tabpanel"
                        isOpen={this.state.open}
                        >
                        <CardBody>
                            <Row>
                                <Col xs={12} md={12} >
                                    <FormGroup className="has-label">
                                        <label htmlFor="searchInvestment">Busca</label>
                                        <InputGroup>
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="fa fa-search" />
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input 
                                                id="searchInvestment"
                                                name="searchInvestment"
                                                type="text"
                                                placeholder="Digite pelo menos três caracteres..."
                                                value={this.state.disapprovedFilter.search}
                                                onChange={this.bindDisapprovedSearch}
                                            />
                                        </InputGroup>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Table className="table-shopping" responsive>
                                <thead>
                                    {this.disapprovedTitles()}
                                </thead>
                                <tbody>
                                    {this.props.pagingDisapprovedIdeas.items.map((idea,index) => this.disapprovedRegister(idea,index))}
                                </tbody>
                            </Table>
                            <Row>
                                <Col>
                                    <Pagination listClassName="mt-3 justify-content-end">
                                        {this.props.pagingDisapprovedIdeas
                                            ? <PaginationItem disabled>
                                                    <PaginationLink href="#">
                                                        <i className="fa fa-list"></i>{"   "} Total: {this.props.pagingDisapprovedIdeas.count}
                                                    </PaginationLink>
                                                </PaginationItem>
                                            : null}
            
                                        {this.state.disapprovedFilter.page > 1
                                            ? (<PaginationItem >
                                                    <PaginationLink href="javascript:void(0)" onClick={this.prevPageDisapproved}>
                                                        <i className="fa fa-arrow-left"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)
            
                                            : (<PaginationItem disabled>
                                                <PaginationLink href="#">
                                                    <i className="fa fa-arrow-left"></i>
                                                </PaginationLink>
                                            </PaginationItem>)}
                                        {this.props.pagingDisapprovedIdeas 
                                            && this.props.pagingDisapprovedIdeas.hasNextPage
                                            
                                            ? (<PaginationItem >
                                                    <PaginationLink href="javascript:void(0)" onClick={this.nextPageDisapproved}>
                                                        <i className="fa fa-arrow-right"></i>
                                                    </PaginationLink>
                                                </PaginationItem>)
            
                                            : (<PaginationItem disabled>
                                                <PaginationLink href="#">
                                                    <i className="fa fa-arrow-right"></i>
                                                </PaginationLink>
                                            </PaginationItem>)}
                                    </Pagination>
                                </Col>
                            </Row>
                        </CardBody>
                    </Collapse>
                </Card>);

        return null;
    }
}

export default Disapproved