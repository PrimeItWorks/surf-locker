import React from "react"
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter} from 'react-router-dom'
import ReactBSAlert from "react-bootstrap-sweetalert"
import {
  Card,
  CardHeader,
  CardBody,
  Table,
  Button,
  Row,
  UncontrolledTooltip,
  Collapse,
  UncontrolledCollapse,
  Pagination,
  PaginationItem,
  PaginationLink,
  Input,
  FormGroup,
  Col} from "reactstrap"
  

import './Users.scss'
import UserService from "../../services/UserService"

import * as modalActions  from '../../redux/actions/modals'
import * as loadingActions  from '../../redux/actions/loading'
import * as userActions  from '../../redux/actions/users'
import { CURRENCY_OPTIONS } from "../../variables/currencyOptions";

class Users extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            users: null,
            alert: null,
            openActives: true,
            defaultFilter: {
                search:'',
                page:1,
                pageSize: 4,
                orderBy: '_id',
                onlyActive: true,
                onlyInactive:false,
                desc: false
            },
            activeFilter: {
                search:'',
                page:1,
                pageSize: 10,
                orderBy: '_id',
                orderByDir: {
                    title: 'sort',
                    price: 'sort',
                },
                onlyActive: true,
                onlyInactive:false,
                desc: false
            }
        }

        this.userService = new UserService()

        this.getPagingUsers()
    }

    // to stop the warning of calling setState of unmounted component
    componentWillUnmount() {
        var id = window.setTimeout(null, 0)
        while (id--) {
            window.clearTimeout(id)
        }
        window.scrollTo(0, 0)
    }

    getPagingUsers = async () => {
        const {loading, loaded} = this.props
        loading()
        const { search, page, pageSize, orderBy, desc } = this.state.activeFilter
        const request = { search, page, pageSize, orderBy, desc}

        const response = await this.userService.getUsers(request)
        this.setState({ users: response})
        loaded()
        console.log("response >>>>>>>>>>>", response)
    }

    // exportUsers = async () => {
    //     const {loading, loaded} = this.props
    //     loading()
    //     const { search, page, pageSize, orderBy, desc } = this.state.activeFilter
    //     const request = { search, page, pageSize, orderBy, desc}

    //     const response = await this.userService.getUsers(request)
    //     this.setState({ users: response})
    //     loaded()
    //     console.log("response >>>>>>>>>>>", response)
    // }

    orderActive = (field) => {
        let activeFilter = this.state.activeFilter

        if (!field)
            activeFilter = this.state.defaultFilter

        if(activeFilter.orderByDir[field] === 'sort')
            activeFilter.orderByDir[field] = 'sort-up' 
        else if(activeFilter.orderByDir[field] === 'sort-up')
            activeFilter.orderByDir[field] = 'sort-down'
        else
            activeFilter.orderByDir[field] = 'sort'

        if (activeFilter.orderByDir[field] ==='sort-down')
            activeFilter.desc = true
        else if (activeFilter.orderByDir[field] ==='sort-up')
            activeFilter.desc = false
        else {
            activeFilter.desc = this.state.defaultFilter.desc
            activeFilter.orderBy = this.state.defaultFilter.orderBy
            this.setState({activeFilter:activeFilter})
            this.props.getPagingUserUsers(activeFilter)
            return
        }

        switch (field) {
            case 'title':
                activeFilter.orderBy = 'Description'
                break
            case 'price':
                activeFilter.orderBy = 'Price'
                break

            default:
                activeFilter.orderBy = this.state.defaultFilter.orderBy
        }

        this.setState({activeFilter:activeFilter})
    }

    titles = () => (
        <tr>
            <th className="title" >Nome</th>
            <th className="email" >E-mail</th>
            <th className="phone" >Telefone</th>
        </tr>
    )

    register = (item, index) => (
            <tr key={index}>
                <td>
                    {item.name}
                </td>
                <td>
                    {item.email}
                </td>
                <td>
                    {item.phone}
                </td>
            </tr>
    )
        
    delete = (e) => {
        this.setState({
            toDelete: e.target.attributes["value"].value,
            alert: (
                <ReactBSAlert
                    warning
                    style={{ display: "block", marginTop: "-100px" }}
                    title="Tem certeza?"
                    onConfirm={async () => {
                        this.hideAlert()

                        const {toDelete} = this.state
                        const {openSuccessModal, loading, loaded} = this.props

                        loading()
                        const result = await this.userService.deleteUser(toDelete)
                        loaded()

                        if(result.response) {
                            const success = {title: 'Alohaa! . m/', description: `Esse produto não está mais disponível no app.`}
                            openSuccessModal(success)
                            this.getPagingUsers()
                        }
                    }}
                    onCancel={() => this.cancelDetele()}
                    confirmBtnBsStyle="info"
                    cancelBtnBsStyle="danger"
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    showCancel
                    >
                    Essa ação não poderá ser revertida.
                </ReactBSAlert>
            )
          })
    }
    
    cancelDetele = () => {
        this.setState({
          alert: (
            <ReactBSAlert
              danger
              style={{ display: "block", marginTop: "-100px" }}
              title="Cancelado"
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              confirmBtnBsStyle="info"
            >
              O produto não foi excluido :)
            </ReactBSAlert>
          )
        })
      }

    hideAlert = () => {
        this.setState({
          alert: null
        })
    }

    handleEdit = (e) => {
        e.preventDefault()

        const id = e.target.attributes["value"].value
        const {users} = this.state
        
        let item = users.items.find(c => c.code == id)
        
        if(item) {
            this.props.history.push(`/admin/maquinas/editar/${item.trademark} ${item.title}`)
            const {setUserToEdit} = this.props
            setUserToEdit(item)
        }
    }   

    handleSearch =  async () => {
        var activeFilter = this.state.activeFilter
        activeFilter.page = 1
        this.setState({search : activeFilter }, await this.getPagingUsers())
    } 

    handleChangePageSize = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.pageSize = e.value

        this.setState({ activeFilter: activeFilter }, await this.getPagingUsers())
    }

    searchBtn = () => {
        return (
            <button className="btn btn-primary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleSearch()
                }} >
                Pesquisar
            </button>
        )
    }

    handleExport = async () => {
        const {activeFilter} = this.state
        const {loading, loaded} = this.props

        loading()
        await this.userService.getExcel(activeFilter)
        loaded()
    }

    exportBtn = () => {
        return (
            <button className="btn btn-secondary effect1"
                onClick={(e) => {
                    e.preventDefault()
                    this.handleExport()
                }} >
                Exportar
            </button>
        )
    }

    list = () => {
        const {openActives, users} = this.state
        const {search} = this.state.activeFilter

        return (<Row className="users__active">
            <Col md="12">
                <Card className="users__active__card effect4">
                    <CardHeader className="users__active__card__header">
                        <h4 className="mb-3">
                            CLIENTES
                        </h4>
                        <hr/>
                    </CardHeader>
                    <Collapse
                        role="tabpanel"
                        isOpen={openActives}
                        >
                        <CardBody>
                            <Row>
                                <Col xs={12} sm={4} md={4} lg={4} >
                                    <FormGroup className="has-label">
                                        <label htmlFor="searchActive">Buscar rápida</label>
                                            <Input 
                                                id="search"
                                                name="search"
                                                type="text"
                                                placeholder="Buscar por..."
                                                value={search}
                                                onChange={e => {
                                                    e.preventDefault()
                                                    let {activeFilter} = this.state

                                                    activeFilter.search = e.target.value
                                                    
                                                    this.setState({activeFilter: activeFilter})
                                                }}
                                            />
                                    </FormGroup>
                                </Col>
                                <Col className="text-right" xs={12} sm={8} md={8} lg={8}>
                                    {this.exportBtn()}
                                    {this.searchBtn()}
                                </Col>
                            </Row>
                            <hr/>

                            <Row>
                                <Col>
                                    <Table id="capture" className="mt-3" responsive striped>
                                        <thead>
                                            {this.titles()}
                                        </thead>
                                        <tbody>
                                            {
                                                users 
                                                    && users.items 
                                                    && users.items.length > 0
                                                        ? users.items.map((item,index) => this.register(item,index))
                                                        : (<tr></tr>)
                                            }                                        
                                        </tbody>
                                    </Table>
                                    <Row>
                                        <Col>
                                            <Pagination listClassName="mt-3 justify-content-end">
                                            {users
                                                ? <PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-list"></i>{"   "} Total: {users.count}
                                                        </PaginationLink>
                                                    </PaginationItem>
                                                : null}

                                                {this.state.activeFilter.page > 1
                                                    ? (<PaginationItem >
                                                            <PaginationLink href="javascript:void(0)" onClick={this.handlePrevPage}>
                                                                <i className="fas fa-arrow-left"></i>
                                                            </PaginationLink>
                                                        </PaginationItem>)

                                                    : (<PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-arrow-left"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)}
                                                {users 
                                                    && users.hasNextPage
                                                    
                                                    ? (<PaginationItem >
                                                            <PaginationLink href="javascript:void(0)" onClick={this.handleNextPage}>
                                                                <i className="fas fa-arrow-right"></i>
                                                            </PaginationLink>
                                                        </PaginationItem>)

                                                    : (<PaginationItem disabled>
                                                        <PaginationLink href="#">
                                                            <i className="fas fa-arrow-right"></i>
                                                        </PaginationLink>
                                                    </PaginationItem>)}
                                            </Pagination>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </CardBody>
                    </Collapse>
                </Card>
            </Col>
        </Row>)
    }

    handleNextPage = async (e) => {
        e.preventDefault()
        
        let {activeFilter} = this.state
        activeFilter.page++

        this.setState({ activeFilter: activeFilter }, await this.getPagingUsers())
    }

    handlePrevPage = async (e) => {
        e.preventDefault()
        let activeFilter = this.state.activeFilter
        activeFilter.page--
        this.setState({ activeFilter: activeFilter }, await this.getPagingUsers())
    }

    add = (e) => {
        e.preventDefault()
        this.props.history.push("/admin/maquinas/criar")
    }

    render() {
        return (
            <div className="content users">
                <Row>
                    <Col className="text-right">
                        <Button className="mb-3 col-xs-12 col-md-2 users__create align-self-right effect1" onClick={this.add} color="primary">
                            Adicionar
                        </Button>
                    </Col>
                </Row>
                {this.list()}
                {this.state.alert}
            </div>
        )
    }
}

function mapStateToProps() {
    return { 
    }
}
  
const mapDispatchToProps = dispatch => 
    bindActionCreators({...modalActions, ...loadingActions, ...userActions}, dispatch)
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Users))
