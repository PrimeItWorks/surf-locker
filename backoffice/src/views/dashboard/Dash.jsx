import React from "react";
// react plugin used to create charts
import { Doughnut } from "react-chartjs-2";

// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";

// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

import {} from "variables/charts.jsx";

import './Dash.scss'
import SaleService from "../../services/SaleService";
import { CURRENCY_OPTIONS_WHITOUT_SYMBOL } from "../../variables/currencyOptions";

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};


class Dash extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      totals: {
        sales: 0,
        salesApp: 0,
        salesMachine: 0,
        billed: 0,
        salesAppPercent: 0,
        salesMachinePercent: 0
      }
    }

    this.salesService = new SaleService()
  }

  async componentWillMount() {
    const totals = await this.salesService.getTotals()
    this.setState({totals})
  }

  componentWillUnmount() {
    window.scrollTo(0, 0)
  }

  percentStage1 = (totals) => {
    let percentStage1 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Vendas em dinheiro",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#f6921e", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage1.options.elements.center.text = totals.salesMachinePercent + '%'
    percentStage1.data.datasets[0].data = [totals.salesMachinePercent, 100 - totals.salesMachinePercent]

    return (
      <Col md="6">
        <Card>
          <CardHeader>
            <CardTitle>Dinheiro</CardTitle>
            <p className="card-category">Vendas nas máquinas usando noteiro</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage1.data}
              options={percentStage1.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i style={{color: "#f6921e"}} className="fa fa-circle" /> 
              Vendas no dinheiro
            </div>
          </CardFooter>
        </Card>
      </Col>
    );
  }

  percentStage2 = (totals) => {
    let percentStage2 = {
      data: {
        labels: [1, 2],
        datasets: [
          {
            label: "Vendas pelo app",
            pointRadius: 0,
            pointHoverRadius: 0,
            backgroundColor: ["#3babb3", "#f4f3ef"],
            borderWidth: 0,
            data: [60, 40]
          }
        ]
      },
      options: {
        elements: {
          center: {
            text: "60%",
            color: "#66615c", // Default is #000000
            fontStyle: "Arial", // Default is Arial
            sidePadding: 60 // Defualt is 20 (as a percentage)
          }
        },
        cutoutPercentage: 90,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                drawBorder: false,
                zeroLineColor: "transparent",
                color: "rgba(255,255,255,0.05)"
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                color: "rgba(255,255,255,0.1)",
                zeroLineColor: "transparent"
              },
              ticks: {
                display: false
              }
            }
          ]
        }
      }
    }

    percentStage2.options.elements.center.text = totals.salesAppPercent + '%'
    percentStage2.data.datasets[0].data = [totals.salesAppPercent, 100 - totals.salesAppPercent]

    return (
      <Col md="6">
        <Card>
          <CardHeader>
            <CardTitle>App</CardTitle>
            <p className="card-category">Vendas nas máquinas usando código</p>
          </CardHeader>
          <CardBody>
            <Doughnut
              data={percentStage2.data}
              options={percentStage2.options}
              className="ct-chart ct-perfect-fourth"
              height={300}
              width={456}
            />
          </CardBody>
          <CardFooter>
            <div className="legend">
              <i style={{color: "#3babb3"}} className="fa fa-circle" /> 
              Vendas pelo app
            </div>
          </CardFooter>
        </Card>
      </Col>
    );
  }

  render() {
    const {totals} = this.state
    return <div className="content">
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                        <div className="icon-big text-center icon-warning">
                            <i className="nc-icon nc-atom text-center text-secondary" />
                        </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">VENDAS APP</p>
                            <CardTitle tag="p">{totals.salesApp}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="nc-icon nc-box-2 text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">VENDAS MÁQUINA</p>
                        <CardTitle tag="p">{totals.salesMachine}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="far fa-arrow-alt-circle-up text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">TOTAL VENDAS</p>
                        <CardTitle tag="p">{totals.sales}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-dollar-sign text-secondary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <p className="card-category">TOTAL FATURADO</p>
                        <CardTitle tag="p">{totals.billed.toLocaleString('pt-BR', CURRENCY_OPTIONS_WHITOUT_SYMBOL)}</CardTitle>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                </CardFooter>
              </Card>
            </Col>
          </Row>

          <Row>
            {this.percentStage1(totals)}
            {this.percentStage2(totals)}
          </Row>
          
        </div>
  }
}

export default Dash