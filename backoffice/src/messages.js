export const messages = {
    ERROR_500: "Erro ao acessar o servidor, entre em contato com o setor responsável.",

    Category_03: "Existem ideias ativas para esta categoria.",

    

    APPROVATION_01: "Justifique a aprovação desta ideia.",
    APPROVATION_02: "Qual ideia deseja aprovar?",
    APPROVATION_03: "Não foi possível identificar a data da aprovação.",

    SignIn_01 : "Nome de usuário e/ou senha inválidos."
};