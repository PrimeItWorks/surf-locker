import {createStore, applyMiddleware } from 'redux';  
import appReducer from '../redux/reducers/reducers';  
import reduxThunk from 'redux-thunk';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);

export default function configureStore() {  
  return createStoreWithMiddleware(appReducer);
}