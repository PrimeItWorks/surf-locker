export const AUTH_DATA_KEY = "AUTH_DATA";

class AuthStorage {
  store(authData) {
    localStorage.setItem(AUTH_DATA_KEY, JSON.stringify(authData));
  }

  clear() {
    localStorage.removeItem(AUTH_DATA_KEY);
  }

  getData() {
    return JSON.parse(localStorage.getItem(AUTH_DATA_KEY));
  }

  getUseId() {
    let tokenObj = JSON.parse(localStorage.getItem(AUTH_DATA_KEY))    
    return tokenObj.user;
  }

  hasData() {
    return (localStorage.getItem(AUTH_DATA_KEY)) !== null;
  }
}

export default AuthStorage;
