export const PAGE_SIZES = [
    {value: 10, label: 10},
    {value: 30, label: 30},
    {value: 50, label: 50},
    {value: 100, label: 100}
]