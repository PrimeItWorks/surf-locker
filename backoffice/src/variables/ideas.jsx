export const ideaStatus = {
    Investment: 1,
    Evaluation: 2,
    Approved: 3,
    Disapproved: 4,
    Running: 5,
    Fail: 6,
    Executed: 7
}
