import fetchIntercept from 'fetch-intercept'
import {signOutAction} from './redux/actions/login'
import {sendMessage} from './redux/actions/message'
import {messages} from './messages'
import AuthStorage from './_helpers/AuthStorage'


export const unregisterFetchInterceptor = fetchIntercept.register({
    request: function (url, config) {
        const authStorage = new AuthStorage()
        const auth = authStorage.getData()

        if(auth) {
            let token = auth.token
            if(token) {
                if (config.headers)
                    config.headers['Authorization'] = `Bearer ${token.value}`
            }
        }

        return [url, config]
    },
    response: (response) => {
        switch (response.status) {
            case 401:
                signOutAction()
                window.location.href = '/auth/login'
                return {}  
            default:
                break
        }

        return response
    },
})