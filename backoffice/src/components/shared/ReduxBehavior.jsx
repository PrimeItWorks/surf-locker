import React from "react";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';

import * as modalsActions from '../../redux/actions/modals';
import * as messageActions from '../../redux/actions/message';
import * as userActions from '../../redux/actions/users';

import ReactBSAlert from "react-bootstrap-sweetalert";
import NotificationAlert from "react-notification-alert";
import loading from '../../assets/img/loading_6.gif';
import AuthStorage from "../../_helpers/AuthStorage";

class ReduxBehavior extends React.Component {
  constructor(props) {
    super(props);
    
    this.authStorage = new AuthStorage()
    
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.message) {
      this.openNotification(nextProps.message);
      this.props.sendMessage('');
    }

    if(nextProps.user) {
      const authData = this.authStorage.getData()

      console.log('authData >>>>>>', authData)

      if (authData.user) {
        let user = JSON.parse(authData.user);
        this.props.getUser(user.code);
      }
    }
  }

  hideAlert = () => {
    this.props.closeSuccessModal();
  }

  openNotification = message => {
    var options = {};

    options = {
      place: "bc",
      message: (
        <div>
          <div>
            {message}
          </div>
        </div>
      ),
      type: "danger",
      autoDismiss: 7
    };

    this.refs.notificationAlert.notificationAlert(options);
  }

  render() {
    return (
      <>
        {this.props.openSuccess 
          ? (
              <ReactBSAlert
                success
                style={{ display: "block", marginTop: "-100px" }}
                title={this.props.openSuccess.success.title}
                onCancel={() => this.hideAlert()}
                onConfirm={() => this.hideAlert()}
                confirmBtnBsStyle="info"
                confirmBtnText="Continuar"
              >
                {this.props.openSuccess.success.description}
              </ReactBSAlert>
          )
          : null}

        <NotificationAlert ref="notificationAlert" />
        {this.props.loading
            ? 
            <div className="shadow-overlay">
                <img className="loader" src={loading} />
            </div>
        : null}
      </>
    );
  }
}

function mapStateToProps(state) {
    return { 
        openSuccess : state.modals.openSuccess,
        message: state.message.message,
        loading: state.loading.status
    };
  }
  
  const mapDispatchToProps = dispatch => 
    bindActionCreators({ ...modalsActions, ...messageActions, ...userActions }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ReduxBehavior);
