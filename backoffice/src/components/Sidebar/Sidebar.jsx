import React from "react";
import { Link, NavLink } from "react-router-dom";
import { Nav, Collapse } from "reactstrap";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../redux/actions/users'

import avatar from "assets/img/no_avatar.png";
import logo from "assets/img/logo-horizontal.png";

import './Sidebar.scss'
import AuthStorage from "../../_helpers/AuthStorage";

var ps;

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.authStorage = new AuthStorage()

    this.auth = this.authStorage.getData()
    if(this.auth) {
      if (this.auth.user && !this.props.user) {
        this.props.getUser(this.auth.user);
      }
    }

    this.state = this.getCollapseStates(props.routes);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.authenticated  && !this.props.user) {
      if(this.auth) {
        if (this.auth.user) {  
          this.props.getUser(this.auth.user);
        }
      }
    }
  }

  // this creates the intial state of this component based on the collapse routes
  // that it gets through this.props.routes
  getCollapseStates = routes => {
    let initialState = {};
    routes.map((prop, key) => {
      if (prop.collapse) {
        initialState = {
          [prop.state]: this.getCollapseInitialState(prop.views),
          ...this.getCollapseStates(prop.views),
          ...initialState
        };
      }
      return null;
    });
    return initialState;
  };

  // this verifies if any of the collapses should be default opened on a rerender of this component
  // for example, on the refresh of the page,
  // while on the src/views/forms/RegularForms.jsx - route /admin/regular-forms
  getCollapseInitialState(routes) {
    for (let i = 0; i < routes.length; i++) {
      if (routes[i].collapse && this.getCollapseInitialState(routes[i].views)) {
        return true;
      } else if (window.location.pathname.indexOf(routes[i].path) !== -1) {
        return true;
      }
    }
    return false;
  }

  // this function creates the links and collapses that appear in the sidebar (left menu)
  createLinks = routes => {
    return routes.map((prop, key) => {
      if (prop.redirect) {
        return null;
      }
      if (prop.collapse) {
        var st = {};
        st[prop["state"]] = !this.state[prop.state];
        return (
          <li
            className={this.getCollapseInitialState(prop.views) ? "active" : ""}
            key={key}
          >
            <a href="javascript:void(0)"
              data-toggle="collapse"
              aria-expanded={this.state[prop.state]}
              onClick={e => {
                e.preventDefault();
                this.setState(st);
              }}
            >
              {prop.icon !== undefined ? (
                <div>
                  <i className={prop.icon} />
                  <p>
                    {prop.name}
                    <b className="caret" />
                  </p>
                </div>
              ) : (
                <div>
                  {prop.mini}
                  <span className="sidebar-normal">
                    {prop.name}
                    <b className="caret" />
                  </span>
                </div>
              )}
            </a>
            <Collapse isOpen={this.state[prop.state]}>
              <ul className="nav">{this.createLinks(prop.views)}</ul>
            </Collapse>
          </li>
        );
      }
      return (
        <li className={this.activeRoute(prop.layout + prop.path)} key={key}>
          <NavLink to={prop.layout + prop.path} activeClassName="">
            {prop.icon !== undefined ? (
              <div>
                <i className={prop.icon} />
                <p>{prop.name}</p>
              </div>
            ) : (
              <div>
                <span className="sidebar-mini-icon">{prop.mini}</span>
                <span className="sidebar-normal">{prop.name}</span>
              </div>
            )}
          </NavLink>
        </li>
      );
    });
  };
  // verifies if routeName is the one active (in browser input)
  activeRoute = routeName => {
    return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
  };
  componentDidMount() {
    // if you are using a Windows Machine, the scrollbars will have a Mac look
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.sidebar, {
        suppressScrollX: true,
        suppressScrollY: false
      });
    }
  }
  componentWillUnmount() {
    // we need to destroy the false scrollbar when we navigate
    // to a page that doesn't have this component rendered
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }
  render() {
    return (
      <div className="sidebar"
        data-color={this.props.bgColor}
        data-active-color={this.props.activeColor}
      >

        <div className="logo sidebar__logo">
        <Link to="/admin/dashboard">
            <div className="logo-img">
              <img src={logo} alt="Surf Locker" />
            </div>
        </Link>

        </div>

        <div className="sidebar-wrapper" ref="sidebar">
          <div className="user">

            <div className="photo">
              <img src={this.props.user && this.props.user.image ? this.props.user.image : avatar} alt='Administrador' />
            </div>

            <div className="info">
              <a href="javascript:void(0)">
                <span>
                  Administrador
                </span>
              </a>
            </div>
          </div>
          <Nav>{this.createLinks(this.props.routes)}</Nav>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { 
      user: state.user.user,
      authenticated: state.auth.authenticated
  };
}

const mapDispatchToProps = dispatch => 
  bindActionCreators(userActions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
