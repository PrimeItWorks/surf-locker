import api from './api'

const queryString = require('query-string');

export const getAllCategories = async () => {
    let categories = await fetch(api.categories.getAll.endpont, {
        method: api.categories.getAll.method,
        headers: api.categories.getAll.headers
    });
    
    let data = await  categories.json();
    return data;
}

export const getPagingCategories = async (request) => {
    let categories = await fetch(`${api.categories.getPaging.endpont}?${queryString.stringify(request)}`, {
        method: api.categories.getPaging.method,
        headers: api.categories.getPaging.headers
    });
    
    let data = await  categories.json();
    return data;
}

export const createCategory = async (request) => {
    let category = await fetch(api.categories.create.endpont, {
        method: api.categories.create.method,
        headers: api.categories.create.headers,
        body: JSON.stringify(request)
    });
    
    let data = await  category.json();
    return data;
}

export const updateCategory = async (request) => {
    let category = await fetch(api.categories.update.endpont, {
        method: api.categories.update.method,
        headers: api.categories.update.headers,
        body: JSON.stringify(request)
    });
    
    let data = await  category.json();
    return data;
}

export const activateCategory = async (request) => {
    let category = await fetch(api.categories.activate.endpont, {
        method: api.categories.activate.method,
        headers: api.categories.activate.headers,
        body: request
    });
    
    let data = await  category.json();
    return data;
}

export const deactivateCategory = async (request) => {
    let category = await fetch(api.categories.deactivate.endpont, {
        method: api.categories.deactivate.method,
        headers: api.categories.deactivate.headers,
        body: request
    });
    
    let data = await  category.json();
    return data;
}

export const deleteCategory = async (request) => {
    let category = await fetch(`${api.categories.delete.endpont}?id=${request}`, {
        method: api.categories.delete.method,
        headers: api.categories.delete.headers
    });
    
    let data = await  category.json();
    return data;
}
