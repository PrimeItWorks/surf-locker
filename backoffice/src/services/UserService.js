import api from './api-surf-locker';
import ApiClient from './apiClient'

const queryString = require('query-string')

export const getUserById = async (request) => {
    let response = await fetch(`${api.user.byId.endpoint}/${request.code}`, {
        method: api.user.byId.method,
        headers: api.user.byId.headers
    });
    
    let data = await response.json();

    return data;
}


export default class UserService {
    constructor(){
        this.apiClient = new ApiClient()
    }

    async getUsers(request) {
        const resp  = await this.apiClient.getApi(
            `${api.user.getUsersPaging.endpoint}?${queryString.stringify(request)}`
        )

        return resp.response
    }

    async getExcel(request) {
        await this.apiClient.getExcel(`${api.user.excel.endpoint}?${queryString.stringify(request)}`, 'SurfLocker_Clientes')
    }
} 
