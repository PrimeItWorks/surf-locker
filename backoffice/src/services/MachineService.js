'use strict'

import api from './api-surf-locker'
import ApiClient from './apiClient'

const queryString = require('query-string')

class MachineService {
    constructor(){
        this.apiClient = new ApiClient()
    }

    async getMachines(request) {
        const resp  = await this.apiClient.getApi(
            `${api.machines.getMachinesPaging.endpont}?${queryString.stringify(request)}`
        )

        return resp.response
    }

    async createMachine(request) {
        const resp = await this.apiClient.postApi(
            api.machines.create.endpont,
            request
        )
        return resp;
    }

    async editMachine(request) {
        const resp = await this.apiClient.putApi(
            api.machines.update.endpont,
            request
        )
        return resp;
    }

    async deleteMachine(request) {
        const resp = await this.apiClient.deleteApi(
            `${api.machines.delete.endpont}?id=${request}`
        )
            
        return resp;
    }

    async getExcel(request) {
        await this.apiClient.getExcel(`${api.machines.excel.endpoint}?${queryString.stringify(request)}`, 'SurfLocker_Maquinas')
    }
}

export default MachineService
