'use strict';

import AuthStorage from "../_helpers/AuthStorage";
import {messages} from '../messages';
import {sendMessage} from '../redux/actions/message'

class ApiClient {
  constructor() {
    this.authToken = null;
    this.storage = new AuthStorage();
  }

  postApi(endpoint, data, Authorization = false) {
    console.log('insert post api', JSON.stringify(data))
    if(Authorization) {
      var dadosJSON = this.storage.getData();   
      this.authToken = dadosJSON.accessToken;
    }

    return fetch(endpoint,
    {
        method: "POST",
        headers: {
          'Authorization': 'Bearer ' + this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(response => {
      if(response.status !== 400 && response.status !== 200){
        throw response;
      }
      return response.json()
        .then(resp => {
          let notifications = resp.notifications;
          if(notifications && notifications.lenght > 0)
          {
              notifications.forEach(not => {
                  let message = messages[not.code];
                  if(message) {
                    sendMessage(message)
                  } 
              });
          }
        })
     })
     .then(responseJson => responseJson)
     .catch(err => {
        console.log('erro post', err)

       return err;
      });
  }

  postFile(endpoint, data, Authorization = false) {
    console.log('insert post api')
    if(Authorization) {
      var dadosJSON = this.storage.getData();   
      this.authToken = dadosJSON.accessToken;
    }

    return fetch(endpoint,
    {
        method: "POST",
        headers: {
          'Authorization': 'Bearer ' + this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        processData: false,
        contentType: false,
        body: data
      })
      .then(response => {
         if(response.status !== 200){
           throw response;
         }

         return response.json()
         .then(resp => {
           let notifications = resp.notifications;
           if(notifications && notifications.lenght > 0)
           {
               notifications.forEach(not => {
                   let message = messages[not.code];
                   if(message) {
                     sendMessage(message)
                   } 
               });
           }

           return resp
         })
      })
      .then(response => {
         if(response.status !== 400 && response.status !== 200){
           throw response;
         }

         return response.json()
    })
     .then(responseJson => responseJson)
     .catch(err => {
        console.log('erro post', err)

       return err;
      });
  }

  getApi(endpoint, Authorization = false) {
    if(Authorization){
        this.authToken = this.storage.getData().accessToken;        
    }

    return fetch(endpoint,
      {
        method: "GET",
        headers: {
          'Authorization': this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then(response => {
            if(response.status !== 200){
              throw response;
            }

            return response.json()
     })
     .then(responseJson => responseJson)
     .catch(err => err);
  }

  getExcel(endpoint, title, Authorization = false) {
    if(Authorization){
      this.authToken = this.storage.getData().accessToken;        
    }

    return fetch(endpoint, {
        method: "GET",
        headers: {
          'Authorization': this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      .then(r => r.blob())
      .then(r => {
          const url = window.URL.createObjectURL(new Blob([r]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', `${title}_${new Date().toISOString()}.xlsx`);
          document.body.appendChild(link);
          link.click();
          link.parentNode.removeChild(link);
      });
  }

  deleteApi(endpoint, Authorization = false) {
    if(Authorization){
        this.authToken = this.storage.getData().accessToken;        
    }

    return fetch(endpoint,
       {
          method: "DELETE",
          headers: {
            'Authorization': this.authToken,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          }
       })
       .then(response => {
          if(response.status !== 200){
            throw response;
          }

          return response.json()
          .then(resp => {
            let notifications = resp.notifications;
            if(notifications && notifications.lenght > 0)
            {
                notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                      sendMessage(message)
                    } 
                });
            }

            return resp
          })
       })
       .then(response => {
          if(response.status !== 400 && response.status !== 200){
            throw response;
          }

          return response.json()
     })
     .then(responseJson => responseJson)
     .catch(err => err);
  }

  putApi(endpoint, data, Authorization = false) {
    debugger
    if(Authorization){
        this.authToken = this.storage.getData().accessToken;        
    }

    return fetch(endpoint,
      {
        method: "PUT",
        headers: {
          'Authorization': this.authToken,
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
      .then(response => {
         if(response.status !== 200){
           throw response;
         }

         return response.json()
         .then(resp => {
           let notifications = resp.notifications;
           if(notifications && notifications.lenght > 0)
           {
               notifications.forEach(not => {
                   let message = messages[not.code];
                   if(message) {
                     sendMessage(message)
                   } 
               });
           }

           return resp
         })
      })
      .then(response => {
         if(response.status !== 400 && response.status !== 200){
           throw response;
         }

         return response.json()
    })
      .then(responseJson => responseJson)
      .catch(err => err);
  }

}

export default ApiClient;