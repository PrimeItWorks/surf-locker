import api from './api-surf-locker';

export const insertBlobFile = async (form) => {
    let response = await fetch(api.blobStorage.insert.endpoint, {
        method: api.blobStorage.insert.method,
        processData: false,
        contentType: false,
        body: form,
    });

    let data = await response.json();   
    return data;
}

export const deleteBlobFile = async (toDelete) => {
    let response = await fetch(`${api.blobStorage.delete.endpoint}?name=${toDelete}`, {
        method: api.blobStorage.delete.method,
        headers: api.blobStorage.delete.headers
    });

    if(response.status != 200)
        return true;

    return true;
}