import api from './api';
const queryString = require('query-string');


export const updateIdea = async (idea) => {
    let response = await fetch(api.ideas.edit.endpont, {
        method: api.ideas.edit.method,
        headers: api.ideas.edit.headers,
        body: JSON.stringify(idea)
    });

    if(!response.json) {
        return {}
    }

    let data = await response.json();
    
    return data;
}

export const getIdeas = async (request) => {
    let response = await fetch(`${api.ideas.getAll.endpont}?${queryString.stringify(request)}`, {
        method: api.ideas.getAll.method,
        headers: api.ideas.getAll.headers
    });
    
    if(!response.json) {
        return {}
    }

    let data = await response.json();
    return data;
}

export const getTotalsDash = async () => {
    let response = await fetch(`${api.ideas.getDashboard.endpont}`, {
        method: api.ideas.getDashboard.method,
        headers: api.ideas.getDashboard.headers
    });
    
    if(!response.json) {
        return {}
    }

    let data = await response.json();
    return data;
}

export const getIdeasToApprove = async (request) => {
    let response = await fetch(`${api.ideas.getAllToApprove.endpont}?${queryString.stringify(request)}`, {
        method: api.ideas.getAll.method,
        headers: api.ideas.getAll.headers
    });
    
    if(!response.json) {
        return {}
    }

    let data = await response.json();
    return data;
}

export const getIdeasApproved = async (request) => {
    let response = await fetch(`${api.ideas.getAllApproved.endpont}?${queryString.stringify(request)}`, {
        method: api.ideas.getAll.method,
        headers: api.ideas.getAll.headers
    });
    
    if(!response.json) {
        return {}
    }

    let data = await response.json();
    return data;
}

export const getIdeasDisapproved = async (request) => {
    let response = await fetch(`${api.ideas.getAllDisapproved.endpont}?${queryString.stringify(request)}`, {
        method: api.ideas.getAll.method,
        headers: api.ideas.getAll.headers
    });
    
    if(!response.json) {
        return {}
    }

    let data = await response.json();
    return data;
}

export const approveIdea = async (approvation) => {
    let response = await fetch(api.ideas.approve.endpont, {
        method: api.ideas.approve.method,
        headers: api.ideas.approve.headers,
        body: JSON.stringify(approvation)
    });

    if(!response.json) {
        return {}
    }

    let data = await response.json();
    
    return data;
}

export const disapproveIdea = async (approvation) => {
    let response = await fetch(api.ideas.disapprove.endpont, {
        method: api.ideas.disapprove.method,
        headers: api.ideas.disapprove.headers,
        body: JSON.stringify(approvation)
    });

    if(!response.json) {
        return {}
    }

    let data = await response.json();
    
    return data;
}
