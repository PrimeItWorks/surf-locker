import api from './api'

export const getGameSettings = async () => {
    let gameSettings = await fetch(api.gammeSettings.get.endpont, {
        method: api.gammeSettings.get.method,
        headers: api.gammeSettings.get.headers
    });
    
    let data = await  gameSettings.json();
    return data;
}

export const updateGameSettings = async (settings) => {
    let gameSettings = await fetch(api.gammeSettings.update.endpont, {
        method: api.gammeSettings.update.method,
        headers: api.gammeSettings.update.headers,
        body:JSON.stringify(settings)
    });
    
    let data = await  gameSettings.json();
    return data;
}