const base = 'https://surflocker-app.azurewebsites.net/api'
// const base = 'https://localhost:5001/api'
// const base = 'https://79473394.ngrok.io/api'

const api = {
    products: {
        getMachinePaging: {
            endpont: `${base}/products/machine`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        getProducts: {
            endpont: `${base}/products`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        excel: {
            endpoint : `${base}/products/export`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        getStorePaging: {
            endpont: `${base}/products/store`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        create : {
            endpont: `${base}/products`,
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        update : {
            endpont: `${base}/products`,
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        delete: {
            endpont: `${base}/products`,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
    },
    machines: {
        getMachinesPaging: {
            endpont: `${base}/machines`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        excel: {
            endpoint : `${base}/machines/export`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        create : {
            endpont: `${base}/machines`,
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        update : {
            endpont: `${base}/machines`,
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        delete: {
            endpont: `${base}/machines`,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
    },
    blobStorage : {
        insert: {
            endpoint: `${base}/files`,
            method: "POST"
        },
        delete: {
            endpoint: `${base}/files`,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
    },
    user: {
        login : {
            endpont: `${base}/signin/admin`,
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        getUsersPaging : {
            endpoint: `${base}/accounts`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        export: {
            endpont: `${base}/accounts/export`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        checkAuth : {
            endpoint : `${base}/signin/checkauthadmin`,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        byId: {
            endpoint : `${base}/accounts`,
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        },
        excel: {
            endpoint : `${base}/accounts/export`,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }
    },
    sale: {
        getSalesPaging: {
            endpoint:  `${base}/sales`
        },
        getTotals: {
            endpoint:  `${base}/sales/totals`
        },
        excel: {
            endpoint:  `${base}/sales/export`
        }
    }
}

export default api;