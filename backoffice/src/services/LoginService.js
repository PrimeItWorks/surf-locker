import api from './api-surf-locker';

export const login = async (body) => {
    let response = await fetch(api.user.login.endpont, {
        method: api.user.login.method,
        headers: api.user.login.headers,
        body: JSON.stringify(body)
    });

    let data = await response.json();   

    return data;
}

export const checkAuthUser = () => {
    fetch(api.user.checkAuth.endpoint, {
        method: api.user.checkAuth.method,
        headers: api.user.checkAuth.headers
    });
}
