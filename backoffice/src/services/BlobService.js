'use strict';

import api from './api-surf-locker'
import ApiClient from './apiClient';

class BlobService {
    constructor(){
        this.apiClient = new ApiClient();
    }

    async insertBlobFile(request) {
        console.log('insert service')
        const resp  = await this.apiClient.postFile(
            api.blobStorage.insert.endpoint,
            request
        )

        return resp.response[0];
    }
    
    async deleteBlobFile(request) {
        const resp  = await this.apiClient.deleteApi(
            `${api.blobStorage.delete.endpoint}?name=${request}`
        )
        
        return resp.response;
    }
}

export default BlobService;
