'use strict'

import api from './api-surf-locker'
import ApiClient from './apiClient'

const queryString = require('query-string')

class ProductService {
    constructor(){
        this.apiClient = new ApiClient()
    }

    async getMachineProducts(request) {
        const resp  = await this.apiClient.getApi(
            `${api.products.getMachinePaging.endpont}?${queryString.stringify(request)}`
        )

        return resp.response
    
    }
    
    async getProducts(request) {
        const resp  = await this.apiClient.getApi(
            `${api.products.getProducts.endpont}?${queryString.stringify(request)}`
        )

        return resp.response
    }
    
    async getStoreProducts(request) {
        const resp  = await this.apiClient.getApi(
            `${api.products.getStorePaging.endpont}?${queryString.stringify(request)}`
        )

        return resp.response
    }

    async createProduct(request) {
        const resp = await this.apiClient.postApi(
            api.products.create.endpont,
            request
        )
        return resp;
    }

    async editProduct(request) {
        const resp = await this.apiClient.putApi(
            api.products.update.endpont,
            request
        )
        return resp;
    }

    async deleteProduct(request) {
        const resp = await this.apiClient.deleteApi(
            `${api.products.delete.endpont}?id=${request}`
        )
            
        return resp;
    }

    async getExcel(request) {
        await this.apiClient.getExcel(`${api.products.excel.endpoint}?${queryString.stringify(request)}`, 'SurfLocker_Produtos_Maquina')
    }
}

export default ProductService
