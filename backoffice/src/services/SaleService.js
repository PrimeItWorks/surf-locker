'use strict'

import api from './api-surf-locker'
import ApiClient from './apiClient'

const queryString = require('query-string')

class SaleService {
    constructor(){
        this.apiClient = new ApiClient()
    }

    async getSales(request) {
        const resp  = await this.apiClient.getApi(
            `${api.sale.getSalesPaging.endpoint}?${queryString.stringify(request)}`
        )

        return resp.response
    }
    
    async getTotals() {
        const resp  = await this.apiClient.getApi(
            `${api.sale.getTotals.endpoint}`
        )
    
        return resp.response
    }


    async getExcel(request) {
        await this.apiClient.getExcel(`${api.sale.excel.endpoint}?${queryString.stringify(request)}`, 'SurfLocker_Vendas')
    }
}

export default SaleService
