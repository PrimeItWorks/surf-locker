import {
    getAllCategories, 
    createCategory,
    updateCategory, 
    activateCategory, 
    deactivateCategory,
    deleteCategory,
    getPagingCategories
} from '../../services/CategoryService';

import {OPEN_SUCCESS_MODAL} from './modals'
import {MESSAGE} from './message';
import {LOADING, LOADED} from './loading';
import {messages} from '../../messages';

export const GET_CATEGORIES = 'GET_CATEGORIES';
export function getCategories() {
    return async (dispatch) => {
        const categories = await getAllCategories();

        if(!categories.notifications) {
            dispatch({ type: GET_CATEGORIES, categories });
            return;
        }

        let notifications = categories.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const GET_PAGING_ACTIVE_CATEGORIES = 'GET_PAGING_ACTIVE_CATEGORIES';
export function getActiveCategoriesPaging(request) {
    return async (dispatch) => {
        request.onlyActive = true;
        request.onlyInactive = false;
        const categories = await getPagingCategories(request);
        if(!categories.notifications) {
            dispatch({ type: GET_PAGING_ACTIVE_CATEGORIES, pagingActiveCategories : categories });
            return;
        }

        let notifications = categories.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const GET_PAGING_INACTIVE_CATEGORIES = 'GET_PAGING_INACTIVE_CATEGORIES';
export function getInactiveCategoriesPaging(request) {
    return async (dispatch) => {
        request.onlyActive = false;
        request.onlyInactive = true;
        const categories = await getPagingCategories(request);
        if(!categories.notifications) {
            dispatch({ type: GET_PAGING_INACTIVE_CATEGORIES, pagingInactiveCategories : categories });
            return;
        }

        let notifications = categories.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const CREATE_CATEGORY = 'CREATE_CATEGORY';
export function createCategories(request) {
    return async (dispatch) => {
        const category = await createCategory(request);
        if(!category.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria ${request.description} foi criada.`} });
            return;
        }

        let notifications = category.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const UPDATE_CATEGORY = 'UPDATE_CATEGORY';
export function updateCategories(request) {
    return async (dispatch) => {
        const category = await updateCategory(request);

        if(!category.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria ${request.description} foi editada.`} });
            return;
        }

        let notifications = category.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function activateCategoryById(request, activerequest, inactiveRequest) {
    return async (dispatch) => {
        dispatch({ type: LOADING });
        const category = await activateCategory(request);
        dispatch({ type: LOADED });

        if(!category.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria foi ativada.`} });
            if(activerequest){
                const actives = await getPagingCategories(activerequest);
                dispatch({ type: GET_PAGING_ACTIVE_CATEGORIES, pagingActiveCategories : actives });
            }
            if(inactiveRequest){
                const inactives = await getPagingCategories(inactiveRequest);
                dispatch({ type: GET_PAGING_INACTIVE_CATEGORIES, pagingInactiveCategories : inactives });
            }

            return;
        }

        let notifications = category.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function deactivateCategoryById(request, activerequest, inactiveRequest) {
    return async (dispatch) => {
        const category = await deactivateCategory(request);

        if(!category.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria foi inativada.`} });
            if(activerequest){
                const actives = await getPagingCategories(activerequest);
                dispatch({ type: GET_PAGING_ACTIVE_CATEGORIES, pagingActiveCategories : actives });
            }
            if(inactiveRequest){
                const inactives = await getPagingCategories(inactiveRequest);
                dispatch({ type: GET_PAGING_INACTIVE_CATEGORIES, pagingInactiveCategories : inactives });
            }

            return;
        }

        let notifications = category.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const DELETE_CATEGORY = 'DELETE_CATEGORY';
export function deleteCategoryById(request, inactiveRequest) {
    return async (dispatch) => {
        dispatch({ type: LOADING });
        const category = await deleteCategory(request);
        dispatch({ type: LOADED });

        if(!category.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria foi excluida.`} });
            if(inactiveRequest){
                const inactives = await getPagingCategories(inactiveRequest);
                dispatch({ type: GET_PAGING_INACTIVE_CATEGORIES, pagingInactiveCategories : inactives });
            }

            return;
        }

        let notifications = category.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export const SET_CATEGORY_TO_EDIT = 'SET_CATEGORY_TO_EDIT';
export function setCategoryToEdit(category) {
    return async (dispatch) => {
        dispatch({ type: SET_CATEGORY_TO_EDIT, category });
    }
}
export function removeCategoryToEdit() {
    return async (dispatch) => {
        dispatch({ type: SET_CATEGORY_TO_EDIT, category : null });
    }
}
