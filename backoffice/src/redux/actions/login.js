import {login} from '../../services/LoginService';
import { MESSAGE } from './message';
import {OPEN_SUCCESS_MODAL} from './modals';
import { LOADING, LOADED } from './loading';
import AuthStorage from '../../_helpers/AuthStorage'

export const AUTHENTICATED = 'authenticated_user';
export const AUTHENTICATION_ERROR = 'authentication_error';

export function signInAction({ email, password }) {
    return async (dispatch) => {
        try {
            dispatch({ type: LOADING });
            const res = await login({ email, password });
            dispatch({ type: LOADED });

            if (res.notifications)
            {
                let notifications = res.notifications;
                if(notifications.length > 0)
                {
                    dispatch({
                        type: AUTHENTICATION_ERROR,
                        payload: 'E-mail e/ou senha inválidos.'
                    });
        
                    dispatch({
                        type: MESSAGE,
                        message: 'E-mail e/ou senha inválidos.'
                    });
                }
            }
            else {
                dispatch({ type: AUTHENTICATED });
                var authStorage  = new AuthStorage()
                authStorage.store(res.response);
                dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Surf Locker', description: 'Seja bem vindo ao Back Surf Locker!'} });

            }
        } catch(error) {
            dispatch({ type: LOADED });

            dispatch({
                type: AUTHENTICATION_ERROR,
                payload: 'E-mail e/ou senha inválidos.'
            });

            dispatch({
                type: MESSAGE,
                message: 'E-mail e/ou senha inválidos.'
            });
        }
    }
}

export const UNAUTHENTICATED = 'unauthenticated_user';
export function signOutAction() {
    localStorage.clear();
    return {
      type: UNAUTHENTICATED
    };
}
