export const MESSAGE = 'MESSAGE';
export function sendMessage(message) {
    return async (dispatch) => {
        dispatch({ type: MESSAGE, message });
    }
}
