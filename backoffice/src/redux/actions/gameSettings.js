import {getGameSettings, updateGameSettings} from '../../services/SettingsService';
import { MESSAGE } from './message';
import { messages } from '../../messages';
import { OPEN_SUCCESS_MODAL } from './modals';

export const GET_SETTINGS = 'GET_SETTINGS';
export function getSettings() {
    return async (dispatch) => {
        const settings = await getGameSettings();
        dispatch({ type: GET_SETTINGS, settings });
    }
}

export const UPDATE_SETTINGS = 'UPDATE_SETTINGS';
export function updateSettings(request){
    return async (dispatch) => {
        const settings = await updateGameSettings(request);

        if(!settings.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Parabéns!', description: `Categoria ${request.description} foi criada.`} });
            dispatch({ type: UPDATE_SETTINGS, settings });
            return;
        }

        let notifications = settings.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}
