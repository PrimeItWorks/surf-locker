export const SET_PRODUCT_TO_EDIT = 'SET_PRODUCT_TO_EDIT';
export function setProductToEdit(product) {
    return async (dispatch) => {
        dispatch({ type: SET_PRODUCT_TO_EDIT, product });
    }
}

export function removeProductToEdit() {
    return async (dispatch) => {
        dispatch({ type: SET_PRODUCT_TO_EDIT, product : null });
    }
}
