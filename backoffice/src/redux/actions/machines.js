export const SET_MACHINE_TO_EDIT = 'SET_MACHINE_TO_EDIT';
export function setMachineToEdit(machine) {
    return async (dispatch) => {
        dispatch({ type: SET_MACHINE_TO_EDIT, machine });
    }
}

export function removeMachineToEdit() {
    return async (dispatch) => {
        dispatch({ type: SET_MACHINE_TO_EDIT, machine : null });
    }
}
