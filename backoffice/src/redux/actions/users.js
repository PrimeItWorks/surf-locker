import {getUserById} from '../../services/UserService';

export const GET_USER = 'GET_USER';
export function getUser(id) {
    return async (dispatch) => {
        let user = await getUserById(id);
        dispatch({ type: GET_USER, user });
    }
}
