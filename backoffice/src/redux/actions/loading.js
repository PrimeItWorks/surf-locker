export const LOADING = 'LOADING';
export function loading() {
    return async (dispatch) => {
        dispatch({ type: LOADING })
    }
}

export const LOADED = 'LOADED';
export function loaded() {
    return async (dispatch) => {
        dispatch({ type: LOADED })
    }
}