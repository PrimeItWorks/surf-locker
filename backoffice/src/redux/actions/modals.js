export const OPEN_SUCCESS_MODAL = 'OPEN_SUCCESS_MODAL'
export function openSuccessModal(success) {
    return async (dispatch) => {
        dispatch({ type: OPEN_SUCCESS_MODAL, openSuccess: {open:true, success: success} })
    }
}

export function closeSuccessModal() {
    return async (dispatch) => {
        dispatch({ type: OPEN_SUCCESS_MODAL, openSuccess: false })
    }
}