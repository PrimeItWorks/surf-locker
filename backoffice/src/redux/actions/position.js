export const UPDATE_LAST_POSITION = 'UPDATE_LAST_POSITION';
export function updateLastPosition(position) {
    return async (dispatch) => {
        dispatch({ type: UPDATE_LAST_POSITION, position });
    }
}