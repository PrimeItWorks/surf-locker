import {
    getIdeas,
    getIdeasToApprove,
    getIdeasApproved,
    getIdeasDisapproved,
    approveIdea,
    disapproveIdea,
    getTotalsDash
} from '../../services/IdeaService';

import {OPEN_SUCCESS_MODAL} from './modals'
import {MESSAGE} from './message';
import {LOADING, LOADED} from './loading';
import {messages} from '../../messages';

export const GET_PAGING_IDEAS = 'GET_PAGING_IDEAS';
export const GET_TO_APPROVE_IDEAS = 'GET_TO_APPROVE_IDEAS';
export const GET_APPROVED_IDEAS = 'GET_APPROVED_IDEAS';
export const GET_DISAPPROVED_IDEAS = 'GET_DISAPPROVED_IDEAS';
export const GET_DASHBOARD = 'GET_DASHBOARD';
export const SET_IDEA_TO_DETAIL = 'SET_IDEA_TO_DETAIL';

export function setIdeaToDetail(idea) {
    return async (dispatch) => {
        dispatch({ type: SET_IDEA_TO_DETAIL, idea });
    }
}

export function getDashboard() {
    return async (dispatch) => {
        const totals = await getTotalsDash();
        dispatch({ type: GET_DASHBOARD, dashboard : totals });
    }
}

export function removeIdeaToDetail() {
    return async (dispatch) => {
        dispatch({ type: SET_IDEA_TO_DETAIL, idea : null });
    }
}

export function getActivePaging(request) {
    return async (dispatch) => {
        const actives = await getIdeas(request);

        if(!actives.notifications) {
            dispatch({ type: GET_PAGING_IDEAS, pagingActiveIdeas : actives });
            return;
        }

        let notifications = actives.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function getToApprovePaging(request) {
    return async (dispatch) => {
        const toApprove = await getIdeasToApprove(request);

        if(!toApprove.notifications) {
            dispatch({ type: GET_TO_APPROVE_IDEAS, pagingToApproveIdeas : toApprove });
            return;
        }

        let notifications = toApprove.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function getApprovedPaging(request) {
    return async (dispatch) => {
        const approved = await getIdeasApproved(request);

        if(!approved.notifications) {
            dispatch({ type: GET_APPROVED_IDEAS, pagingApprovedIdeas : approved });
            return;
        }

        let notifications = approved.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function getDisapprovedPaging(request) {
    return async (dispatch) => {
        const disapproved = await getIdeasDisapproved(request);

        if(!disapproved.notifications) {
            dispatch({ type: GET_DISAPPROVED_IDEAS, pagingDisapprovedIdeas : disapproved });
            return;
        }

        let notifications = disapproved.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function setApprovedIdea(request, defaultFilter) {
    return async (dispatch) => {
        request.approve = true;
        
        dispatch({ type: LOADING });
        const approved = await approveIdea(request);
        dispatch({ type: LOADED });

        if(!approved.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Pronto!', description: `Essa ideia foi aprovada.`} });

            const active = await getIdeas(defaultFilter);
            if(!active.notifications) {
                dispatch({ type: GET_PAGING_IDEAS, pagingActiveIdeas : active });
                return;
            }
            if(active.notifications.length > 0)
            {
                active.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const toApprove = await getIdeasToApprove(defaultFilter);
            if(!toApprove.notifications) {
                dispatch({ type: GET_TO_APPROVE_IDEAS, pagingToApproveIdeas : toApprove });
                return;
            }
            if(toApprove.notifications.length > 0)
            {
                toApprove.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const approved = await getIdeasApproved(defaultFilter);
            if(!approved.notifications) {
                dispatch({ type: GET_APPROVED_IDEAS, pagingApprovedIdeas : approved });
                return;
            }
            if(approved.notifications.length > 0)
            {
                approved.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const disapproved = await getIdeasDisapproved(defaultFilter);
            if(!disapproved.notifications) {
                dispatch({ type: GET_DISAPPROVED_IDEAS, pagingDisapprovedIdeas : disapproved });
                return;
            }
            if(disapproved.notifications.length > 0)
            {
                disapproved.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            return;
        }

        let notifications = approved.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}

export function setDisapprovedIdea(request, defaultFilter) {
    return async (dispatch) => {
        request.approve = false;
        
        dispatch({ type: LOADING });
        const approved = await disapproveIdea(request);
        dispatch({ type: LOADED });

        if(!approved.notifications) {
            dispatch({ type: OPEN_SUCCESS_MODAL, success: {title: 'Pronto!', description: `Essa ideia foi reprovada.`} });

            const active = await getIdeas(defaultFilter);
            if(!active.notifications) {
                dispatch({ type: GET_PAGING_IDEAS, pagingActiveIdeas : active });
                return;
            }
            if(active.notifications.length > 0)
            {
                active.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const toApprove = await getIdeasToApprove(defaultFilter);
            if(!toApprove.notifications) {
                dispatch({ type: GET_TO_APPROVE_IDEAS, pagingToApproveIdeas : toApprove });
                return;
            }
            if(toApprove.notifications.length > 0)
            {
                toApprove.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const approved = await getIdeasApproved(defaultFilter);
            if(!approved.notifications) {
                dispatch({ type: GET_APPROVED_IDEAS, pagingApprovedIdeas : approved });
                return;
            }
            if(approved.notifications.length > 0)
            {
                approved.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            const disapproved = await getIdeasDisapproved(defaultFilter);
            if(!disapproved.notifications) {
                dispatch({ type: GET_DISAPPROVED_IDEAS, pagingDisapprovedIdeas : disapproved });
                return;
            }
            if(disapproved.notifications.length > 0)
            {
                disapproved.notifications.forEach(not => {
                    let message = messages[not.code];
                    if(message) {
                        dispatch({
                            type: MESSAGE,
                            message: message
                        });
                    } 
                });
            }

            return;
        }

        let notifications = approved.notifications;
        if(notifications.length > 0)
        {
            notifications.forEach(not => {
                let message = messages[not.code];
                if(message) {
                    dispatch({
                        type: MESSAGE,
                        message: message
                    });
                } 
            });
        }
    }
}
