import { combineReducers } from 'redux';
import {ideas} from './ideas';
import {categories} from './categories';
import {auth} from './auth';
import {user} from './users';
import {loading} from './loading';
import {message} from './message';
import {settings} from './gameSettings';
import {position} from './position';
import {modals} from './modals';
import {products} from './products';
import {machines} from './machines';

const appReducer = combineReducers({
    ideas, 
    categories,
    user,
    auth,
    loading,
    message,
    settings,
    position,
    modals,
    products,
    machines
});

export default appReducer;