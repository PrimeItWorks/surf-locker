import { 
    SET_MACHINE_TO_EDIT
} from '../actions/machines';

export function machines(state = [], action) {
    switch (action.type) {
        case SET_MACHINE_TO_EDIT:
            console.log(action.machine)
            return  { ...state, machineToEdit: action.machine };

        default:
            return state;
    }
}