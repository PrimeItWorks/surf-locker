import {LOADING, LOADED} from '../actions/loading'
export function loading(state = {}, action) {
    switch (action.type) {
        case LOADING:
            return {...state, status: true };
        case LOADED:
            return {...state, status: false };
        default:
            return state;
    }
}