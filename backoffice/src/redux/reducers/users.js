import { GET_USER } from '../actions/users';

export function user(state = {}, action) {
    switch (action.type) {
        case GET_USER:
            return {...state,  user: action.user.response};
        default:
            return state;
    }
}