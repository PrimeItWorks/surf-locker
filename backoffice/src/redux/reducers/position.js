import {UPDATE_LAST_POSITION} from '../actions/position'
export function position(state = {}, action) {
    switch (action.type) {
        case UPDATE_LAST_POSITION:
            return {...state, position: action.position };
        default:
            return state;
    }
}