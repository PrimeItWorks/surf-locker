import {OPEN_SUCCESS_MODAL} from '../actions/modals'
export function modals(state = {}, action) {
    switch (action.type) {
        case OPEN_SUCCESS_MODAL:

            return {...state, openSuccess: action.openSuccess };
        default:
            return state;
    }
}