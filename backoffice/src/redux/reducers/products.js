import { 
    SET_PRODUCT_TO_EDIT
} from '../actions/products';

export function products(state = [], action) {
    switch (action.type) {
        case SET_PRODUCT_TO_EDIT:
            return  { ...state, productToEdit: action.product };

        default:
            return state;
    }
}