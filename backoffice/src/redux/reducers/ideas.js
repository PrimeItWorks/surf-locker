import {  
    GET_PAGING_IDEAS,
    GET_TO_APPROVE_IDEAS,
    GET_APPROVED_IDEAS,
    GET_DISAPPROVED_IDEAS,
    SET_IDEA_TO_DETAIL,
    GET_DASHBOARD
} from '../actions/ideas';

export function ideas(state = {}, action) {
    switch (action.type) {
        case SET_IDEA_TO_DETAIL:
            return  { ...state, ideaToDetail: action.idea };
        case GET_PAGING_IDEAS:
            return {...state, pagingActiveIdeas: action.pagingActiveIdeas.response}
        case GET_TO_APPROVE_IDEAS:
            return {...state, pagingToApproveIdeas: action.pagingToApproveIdeas.response}
        case GET_APPROVED_IDEAS:
            return {...state, pagingApprovedIdeas: action.pagingApprovedIdeas.response}
        case GET_DISAPPROVED_IDEAS:
            return {...state, pagingDisapprovedIdeas: action.pagingDisapprovedIdeas.response}
        case GET_DASHBOARD:
            return {...state, dashboard: action.dashboard.response}
        default:
            return state;
    }
}
