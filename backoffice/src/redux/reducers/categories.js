import { 
    GET_CATEGORIES, 
    SET_CATEGORY_TO_EDIT, 
    CREATE_CATEGORY, 
    UPDATE_CATEGORY,
    DELETE_CATEGORY,
    GET_PAGING_ACTIVE_CATEGORIES,
    GET_PAGING_INACTIVE_CATEGORIES
} from '../actions/categories';

export function categories(state = [], action) {
    let lists = {
        active:[],
        inactive:[]
    };
    switch (action.type) {
        case GET_CATEGORIES:
            lists.active = action.categories.response.filter(c => c.active)
            lists.inactive = action.categories.response.filter(c => !c.active)

            return  { ...state, list: lists };
        
        case GET_PAGING_ACTIVE_CATEGORIES:
            return {...state, pagingActiveCategories: action.pagingActiveCategories.response}

        case GET_PAGING_INACTIVE_CATEGORIES:
            return {...state, pagingInactiveCategories: action.pagingInactiveCategories.response}

        case SET_CATEGORY_TO_EDIT:
            return  { ...state, categoryToEdit: action.category };

        case CREATE_CATEGORY:
            let newItem = [action.category];
            if(newItem.active) {
                lists.active = newItem.concat(state.list.active);
                lists.inactive = state.lists.inactive;
            } else {
                lists.inactive = newItem.concat(state.lists.inactive);
                lists.active = state.lists.active;
            }

            return { ...state, list: lists};

        case UPDATE_CATEGORY:
            if(state.lists){
                lists.active = state.list.active.map(c => {
                    if(c.id == action.category.id && action.category.active) {
                        c = action.category;
                    }
                    return c;
                })
    
                lists.inactive = state.list.inactive.map(c => {
                        if(c.id == action.category.id && !action.category.active) {
                            c = action.category;
                        }
                        return c;
                })
            }

            return  { ...state, list: lists };

        case DELETE_CATEGORY:
            if (state.list) {
                let indexActive = state.list.active.findIndex(c => c.id == action.deletedCategory);
                if(indexActive > -1)
                    state.list.active.splice(indexActive,1);
                
                let indexInactive = state.list.active.findIndex(c => c.id == action.deletedCategory);
                if (indexInactive > -1)
                    state.list.inactive.splice(indexInactive,1);

                lists.active = state.list.active;
                lists.inactive = state.list.inactive;
            }    

            return  { ...state, list: lists };
            
        default:
            return state;
    }
}