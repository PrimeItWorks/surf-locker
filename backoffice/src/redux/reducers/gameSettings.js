import { GET_SETTINGS, UPDATE_SETTINGS } from '../actions/gameSettings';

export function settings(state = {}, action) {
    switch(action.type) {
      case GET_SETTINGS:
        return { ...state, defaultSettings: action.settings.response };
      case UPDATE_SETTINGS:
        return { ...state, defaultSettings: action.settings.response };
      default:
        return state;
    }
  }  
  