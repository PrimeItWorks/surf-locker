import { MESSAGE } from '../actions/message';

export function message(state = {}, action) {
    switch(action.type) {
      case MESSAGE:
        return { ...state, message: action.message };
      default:
        return state;
    }
  }  



