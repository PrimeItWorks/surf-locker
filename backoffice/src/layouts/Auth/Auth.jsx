import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import { Route, Switch } from "react-router-dom";

import routes from "routes";
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter} from 'react-router-dom';
import * as modalsActions from '../../redux/actions/modals';
import * as messageActions from '../../redux/actions/message';
import notrequireAuth from 'components/hoc/NotAuthentication';

import ReduxBehavior from "../../components/shared/ReduxBehavior";

var ps;

class Pages extends React.Component {
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps = new PerfectScrollbar(this.refs.fullPages);
    }
  }
  componentWillUnmount() {
    if (navigator.platform.indexOf("Win") > -1) {
      ps.destroy();
    }
  }

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.layout === "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={notrequireAuth(prop.component)}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    return (
        <div className="wrapper wrapper-full-page" ref="fullPages">
          <div className="full-page section-image">
            <Switch>{this.getRoutes(routes)}</Switch>
          </div>
          <ReduxBehavior />
        </div>
    );
  }
}

function mapStateToProps(state) {
  return { 
      message: state.message.message,
      loading: state.loading.status
  };
}

const mapDispatchToProps = dispatch => 
  bindActionCreators({ ...messageActions, ...modalsActions}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Pages));